<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function carga_inputs(&$data,$id_propiedad = FALSE)
{
	### Instanciar
	$CI =& get_instance();
	
	if($id_propiedad)
	{
		### Se hace la consulta para obtener la información de la propiedad
		$join[] = array('table'=>'tipoproptipo','cond'=>'tipoproptipo.tpt_id_prop = propiedades.prp_id_propiedad','type'=>'LEFT');
		$join[] = array('table'=>'ubicaciones','cond'=>'ubicaciones.ubi_id_ubicacion = propiedades.prp_id_ubicacion','type'=>'LEFT');
		$join[] = array('table'=>'costos','cond'=>'costos.cos_id_propiedad = propiedades.prp_id_propiedad','type'=>'LEFT');
		$propiedad = $CI->propiedades_model->getOnePropiedades(array('prp_id_propiedad'=>$id_propiedad),$join);
		
		// echo "<pre>";
		// print_r($propiedad);
		// echo "</pre>";
		
		if($propiedad)
		{
			### {values} es la variable que contiene la información necesaria para llenar el formulario
			$values = array();
			
			$values['tipo_inmueble'] = $propiedad['tpt_id_tipo'];
			$values['proposito'] = getProposito($propiedad['prp_proposito']);
			
			### Se obtienen los usos de la propiedad
			$usosx = $CI->tipousopropiedad_model->getAllPropertyUseTypes(array('rlu_id_propiedad'=>$id_propiedad));
			foreach($usosx as $key => $u)
			{
				$usos[] = $u['rlu_id_uso'];
			}
			$values['usos'] = $usos;
			
			### Se obtienen los atributos de la propiedad
			$atributosx = $CI->propatributosn_model->getAllPropatributosn(array('pan_id_propiedad'=>$id_propiedad), NULL, NULL, NULL);
			foreach($atributosx as $key => $a)
			{
				$atributos[$a['pan_id_atributo']] = $a['pan_valor'];
			}
			$values['atributos'] = $atributos;
			
			### Se obtienen los la descricion, clave interna y mas indicaciones de la propiedad
			$descripx = $CI->propatributosv_model->getAllPropatributosv(array('pav_id_propiedad'=>$id_propiedad), NULL, NULL, NULL);
			foreach($descripx as $key => $a)
			{
				switch($a['pav_id_atributo'])
				{
					case 18 : $values['descripcion'] 		= $a['pav_valor']; break;
					case 29 : $values['clave_interna'] 		= $a['pav_valor']; break;
					case 23 : $values['mas_indicaciones']	= $a['pav_valor']; break;
				}
			}
			
			$values['moneda']		= $propiedad['cos_moneda'];
			$values['costo_venta']	= $propiedad['cos_costov'];
			$values['costo_renta']	= $propiedad['cos_costor'];
			$values['estado']		= $propiedad['ubi_estado'];
			$values['municipio']	= $propiedad['ubi_municipio'];
			$values['colonia']		= $propiedad['ubi_colonia'];
			$values['calle']		= $propiedad['ubi_calle'];
			$values['no_ext']		= $propiedad['ubi_numero_ext'];
			$values['no_int']		= $propiedad['ubi_interior'];
			$values['lat']			= $propiedad['ubi_latitud'];
			$values['lon']			= $propiedad['ubi_longitud'];
			
			// echo "<pre>";
			// print_r($values);
			// echo "</pre>";
			
			$municipios				= hmunicipios($values['estado']);
			$colonias				= hcolonias($values['estado'], $values['municipio']);
		}
		else
		{
			### Redirigir a altas de propiedades
			redirect(base_url().'zp/alta_propiedad/paso_uno');
		}
	}
	else
	{
		$values = FALSE;
		$municipios = array('0'=>'-Selecciona-');
		$colonias = array('0'=>'-Selecciona-');
	}
	
	### Tipos de propiedades
	$tipos_prop 				= $CI->tipospropiedades_model->getAllTiprop(array('tpr_activo'=>1),NULL,NULL,array('tpr_descripcion'=>'ASC'),NULL);
	$tp[0] 						= 'Seleccione un tipo de propiedad';
	foreach($tipos_prop as $key => $tipos)
	{
		$tp[$tipos['tpr_id_tipoprop']] = $tipos['tpr_descripcion'];
	}
	
	### options 1 - 20
	$op20['']='-';
	for($z=1;$z<=20;$z++)
	{
		$op20[$z]=$z;
	}
	
	### options 1 - 50
	$op50['']='-';
	for($z=1;$z<=50;$z++)
	{
		$op50[$z]=$z;
	}
	
	### options 1 - 300
	$op300['']='-';
	for($z=1;$z<=300;$z++)
	{
		$op300[$z]=$z;
	}
	
	### options baños
	$banop['']='-';
	for($x=.5;$x<=800;$x+=.5)
	{
		$banop["$x"]=$x;
	}
	
	### options 1 - 300  (estacionamientos)
	$est['']='-';
	for($x=1;$x<=300;$x++)
	{
		$est[$x]=$x;
	}
	
	### options edad
	$edad_op['']='-';
	for($z=1;$z<=49;$z++)
	{
		$edad_op[$z]=$z.' años';
	}
	
	### options 1 - 5
	$op05['']='-';
	for($z=1;$z<=5;$z++)
	{
		$op05[$z]=$z;
	}
	
	### options moneda
	$monx = $CI->monedas_model->getAllMonedas(NULL, NULL, array('mon_id_moneda'=>'ASC'), NULL);
	foreach($monx as $key => $m)
	{
		$moneda[$m['mon_id_moneda']] = $m['mon_descripcion'];
	}
	
	### options cuarto de servicio
	$cs['']='-';
	$cs[1]='Azotea';
	$cs[2]='Integrado';
	$cs[3]='Separado';
	
	### Relacion Input lista
	$rel_in_list[1]		= $banop;
	$rel_in_list[3]		= $est;
	$rel_in_list[4]		= $op20;
	$rel_in_list[5]		= $edad_op;
	$rel_in_list[6]		= $op300;
	$rel_in_list[8]		= $op05;
	$rel_in_list[9]		= $op50;
	$rel_in_list[10]	= $cs;
	$rel_in_list[11]	= $op05;
	
	### Cargando los estados
	$states						= file_get_contents(base_url() . "webservices/getStates");
	$std						= json_decode($states, TRUE);
	$estados[0]					= '-Selecciona-';
	foreach($std as $key => $st)
	{
		$estados[$st['est_num_estado']] = $st['est_nombre'];
	}
	
	### Cargando la información de usos de suelo
	$tipos_uso = $CI->tiposuso_model->getAllUseTypes(array('uso_activo'=>1), NULL, NULL,array('uso_id_uso'=>'ASC'), NULL);
	foreach($tipos_uso as $key => $tipo)
	{
		$ts[] =	array	(
							'label'	=> $tipo['uso_nombre'],
							'input'	=> form_checkbox(array('name'=>'uso_inmueble[]','id'=>'uso_'.$tipo['uso_id_uso'],'value'=>$tipo['uso_id_uso'],'checked'=>isset($values['usos'])?in_array($tipo['uso_id_uso'],$values['usos'])?TRUE:FALSE:FALSE,'class'=>'checkbox','style'=>'margin:0 10px 0 20px;'))
						);
	}
	
	### Cargando los atributos variables (Habitaciones, Baños, Estacionamientos, Edad, etc...)
	$atributos_variables = $CI->atributos_model->getAllAttributes(array('atri_activo'=>1, 'atri_tipo !='=>'D'), NULL, array('atri_ubicacion'=>'ASC'), NULL, NULL);
	foreach($atributos_variables as $key => $av)
	{
		if(isset($values['atributos']))
		{
			if(isset($values['atributos'][$av['atri_id_atributo']]))
			{
				$sel = $values['atributos'][$av['atri_id_atributo']];
			}
			else
			{
				$sel = '';
			}
		}
		else
		{
			$sel = '';
		}
		
		$oblig			= 1==$av['atri_forzoso']?'<span class="color-3">*</span>':'';
		$atrib_var[]	= '	<div style="float:left; margin-right: 18px; display:none;" id="option_'.$av['atri_id_atributo'].'">
								<p style="margin-bottom: 0;"><label>'.$av['atri_descripcion'].'</label>'.$oblig.'</p>
								'.form_dropdown('atri_'.$av['atri_id_atributo'], $rel_in_list[$av['atri_id_atributo']], $sel, 'id="atri_'.$av['atri_id_atributo'].'" style="width:98px;"').'
							</div>';
	}
	
	### Cargando los atributos de dimensiones
	$atributos_medidas = $CI->atributos_model->getAllAttributes(array('atri_activo'=>1, 'atri_tipo'=>'D'), NULL, array('atri_ubicacion'=>'ASC'), NULL, NULL);
	foreach($atributos_medidas as $key => $am)
	{
		if(isset($values['atributos']))
		{
			if(isset($values['atributos'][$am['atri_id_atributo']]))
			{
				$val = $values['atributos'][$am['atri_id_atributo']];
			}
			else
			{
				$val = '';
			}
		}
		else
		{
			$val = '';
		}
		
		$oblig 			= 1==$am['atri_forzoso']?'<span class="color-3">*</span>':'';
		$atrib_med[]	= '	<div class="column" style="float:left; margin-right: 30px; display:none;" id="option_'.$am['atri_id_atributo'].'">
								<p>'.$am['atri_descripcion'].''.$oblig.'<br />
								'.form_input(array('name'=>'atri_'.$am['atri_id_atributo'],'id'=>'atri_'.$am['atri_id_atributo'], 'value'=>$val)).'
								m<sup>2</sup></p>
							</div>';
	}
	
	$latitud 				= isset($values['lat'])?$values['lat']:'';
	$longitud 				= isset($values['lon'])?$values['lon']:'';
	
	// INICIAN INPUTS
	$data['TIPO_INMUE']		= form_dropdown('tipo_inmueble',$tp,$values['tipo_inmueble'],'id="tipo_inmueble"');
	$data['TIPO_VENTA']		= form_checkbox(array('name'=>'tipo[]','id'=>'fin_venta','value'=>'Venta','checked'=>isset($values['proposito'])?in_array('Venta',$values['proposito'])?TRUE:FALSE:FALSE,'class'=>'checkbox','style'=>'margin:0 10px 0 20px;argin-left:20px;'));
	$data['TIPO_RENTA']		= form_checkbox(array('name'=>'tipo[]','id'=>'fin_renta','value'=>'Renta','checked'=>isset($values['proposito'])?in_array('Renta',$values['proposito'])?TRUE:FALSE:FALSE,'class'=>'checkbox','style'=>'margin:0 10px 0 20px;'));
	$data['USOS']			= $ts;
	$data['CLAVE_INTE']		= form_input(array('name'=>'clave_interna','id'=>'clave_interna','value'=>isset($values['clave_interna'])?$values['clave_interna']:''));
	$data['PREC_VENTA']		= form_input(array('name'=>'precio_venta','id'=>'precio_venta','value'=>isset($values['costo_venta'])?$values['costo_venta']:'','style'=>'width:7em;'));
	$data['PREC_VTAM2']		= form_input(array('name'=>'precio_m2','id'=>'precio_m2','style'=>'width:7em;'));
	$data['PREC_RENTA']		= form_input(array('name'=>'precio_renta','id'=>'precio_renta','value'=>isset($values['costo_renta'])?$values['costo_renta']:'','style'=>'width:7em;'));
	$data['TIP_MON']		= form_dropdown('moneda', $moneda, $values['moneda'], 'id="moneda"');
	$data['ATRIB_VAR']		= $atrib_var;
	$data['ATRIB_MED']		= $atrib_med;
	
	$data['DESCRIP']		= form_textarea(array('name'=>'descripcion','id'=>'descripcion','rows'=>'10','value'=>isset($values['descripcion'])?$values['descripcion']:'','style'=>'width:500px; resize:none;'));
	$data['ESTADOS']		= form_dropdown('u_estado', $estados, $values['estado'], 'id="u_estado" style="width:200px; margin:0 20px 0 0px;" class="dlocalidad"');
	$data['MUNICIPIOS']		= form_dropdown('u_municipio', $municipios, $values['municipio'], 'id="u_municipio" style="width:200px; margin:0 20px 0 0px;" class="dlocalidad"');
	$data['COLONIAS']		= form_dropdown('u_colonia', $colonias, $values['colonia'], 'id="u_colonia" style="width:200px; margin:0 20px 0 0px;" class="dlocalidad"');
	$data['U_CALLE']		= form_input(array('name'=>'u_calle','id'=>'u_calle','value'=> isset($values['calle'])?$values['calle']:''));
	$data['U_NUMER']		= form_input(array('name'=>'u_numero','id'=>'u_numero','value'=> isset($values['no_ext'])?$values['no_ext']:'','style'=>'width:50px;'));
	$data['U_NUMIN']		= form_input(array('name'=>'u_numero_int','id'=>'u_numero_int','value'=> isset($values['no_int'])?$values['no_int']:'','style'=>'width:50px;'));
	// $data['U_CP']			= form_input(array('name'=>'u_cp','id'=>'u_cp','value'=> '','style'=>'width:50px;'));
	$data['DESU_ESPAN']		= form_textarea(array('name'=>'u_espanol','id'=>'u_espanol','value'=> isset($values['mas_indicaciones'])?$values['mas_indicaciones']:'','rows'=>'4','style'=>'width:300px; resize:none;'));
	$data['LAT']			= '<input type="hidden" id="latitud" name="latitud" value="'.$latitud.'" />';
	$data['LON']			= '<input type="hidden" id="longitud" name="longitud" value="'.$longitud.'" />';
}

function hmunicipios($estado)
{
	### Instanciar
	$CI =& get_instance();
	
	$metropolitana[14] = array('Guadalajara','Tlaquepaque','Tonalá','Tlajomulco de Zúñiga','Zapopan');
	$arr_zmp = array();
	
	$muns_x = $CI->municipios_model->getAllMuni(array('mun_id_estado'=>$estado), NULL, array('mun_nombre'=>'ASC'), NULL);
	
	foreach($muns_x as $x => $m)
	{
		$muns[$m['mun_mun_inegi']] = $m['mun_nombre'];
	}
	
	if(array_key_exists($estado,$metropolitana)==1)
	{
		foreach($metropolitana[$estado] as $zmp)
		{
			$key = FALSE;
			$key=array_search($zmp,$muns);
			if($key)
			{
				$arr_zmp[$key] = $muns[$key];
				unset($muns[$key]);
			}
		}
		
		$option[0]	= '-Selecciona-';
		if(count($arr_zmp)>0)
		{
			foreach($arr_zmp as $kzmp => $zmp)
			{
				$mp[$kzmp] = $zmp;
			}
			
			foreach($muns as $x => $m)
			{
				$n[$x] = $m;
			}
			
			$option['Zona Metropolitana'] = $mp;
			$option['Otros Municipios'] = $n;
		}
		else
		{
			foreach($muns as $x => $m)
			{
				$option[$x] = $m;
			}
		}
	}
	else
	{
		$option[0]	= '-Selecciona-';
		foreach($muns as $x => $m)
		{
			$option[$x] = $m;
		}
	}
	
	return $option;
}

function hcolonias($estado,$municipio)
{
	### Instanciar
	$CI =& get_instance();
	
	$where['col_id_estado'] = $estado;
	$where['col_id_municipio'] = $municipio;
	
	$colonias = $CI->colonias_model->getAllSuburbs($where,NULL,array('col_nombre'=>'ASC'), NULL);
	
	$option[0]	= '-Selecciona-';
	foreach($colonias as $x => $c)
	{
		$option[$c['col_id_colonia']] = $c['col_nombre'];
	}
	
	return $option;
}
