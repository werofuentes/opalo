<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CodeIgniter Form Helpers
 *
 * @package	Helpers
 * @author	Casas y Terrenos
 * @version     1.0
 */

// ------------------------------------------------------------------------

/** Crea un archivo pdf a partir de una cadena html que recibe
*  @author Casas y Terrenos
*  @access public
*  @param  string $html Cuerpo del archivo pdf
*  @param  string $filename Nombre del fichero
*  @param  bool $stream Indica si el fichero se generará en la carpeta download, en caso contrario retorna el pdf como un string
*  @return string Pdf
*/
function pdf_create($html, $filename='', $stream = TRUE, $orientation = 'V')
{
    //ini_set('memory_limit', '300M');
    $dompdf = new DOMPDF();
    //$dompdf->set_paper("legal");
    if( $orientation == 'H')
    {
        $dompdf->set_paper("letter","landscape");
    }
    $dompdf->load_html($html);
    $dompdf->render();
    
    if ($stream)
    {
        $dompdf->stream('uploads/cuestionario_'.$filename.".pdf", array("Attachment" => 1));
    } 
    else
    {
        return $dompdf->output();
    }
}
?>