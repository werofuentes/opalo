<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function define_proposito($proposito = FALSE)
{
	if($proposito)
	{
		if(0<count($proposito))
		{
			$tipo = $proposito;
			if( (in_array('Venta',$tipo)) && (!in_array('Renta',$tipo)) && (!in_array('Traspaso',$tipo)) )
			{
				$prop = 1;
			}
			else if( (!in_array('Venta',$tipo)) && (in_array('Renta',$tipo)) && (!in_array('Traspaso',$tipo)) )
			{
				$prop = 2;
			}
			else if( (!in_array('Venta',$tipo)) && (!in_array('Renta',$tipo)) && (in_array('Traspaso',$tipo)) )
			{
				$prop = 3;
			}
			else if( (in_array('Venta',$tipo)) && (in_array('Renta',$tipo)) && (!in_array('Traspaso',$tipo)) )
			{
				$prop = 4;
			}
			else if( (in_array('Venta',$tipo)) && (!in_array('Renta',$tipo)) && (in_array('Traspaso',$tipo)) )
			{
				$prop = 5;
			}
			else if( (!in_array('Venta',$tipo)) && (in_array('Renta',$tipo)) && (in_array('Traspaso',$tipo)) )
			{
				$prop = 6;
			}
			else if( (in_array('Venta',$tipo)) && (in_array('Renta',$tipo)) && (in_array('Traspaso',$tipo)) )
			{
				$prop = 7;
			}
			else
			{
				$prop = FALSE;
			}
			
			return $prop;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
}

/**
 * getProposito
 * Returns a link tag with the selected css file
 */
if ( ! function_exists('getProposito'))
{
	function getProposito($num = FALSE)
	{
		if($num)
		{
			$proposito = "";
			
			switch ($num) 
			{
				case '1':
					$proposito = 'Venta';
					break;
				case '2':
					$proposito = 'Renta';
					break;
				case '3':
					$proposito = 'Traspaso';
					break;
				case '4':
					$proposito = array('Venta','Renta');
					break;
				case '5':
					$proposito = array('Venta','Traspaso');
					break;
				case '6':
					$proposito = array('Renta','Traspaso');
					break;
				case '7':
					$proposito = array('Venta','Renta','Traspaso');
					break;	
			}
			return $proposito;
		}
		return FALSE;
	}
}
