<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Casas y terrenos
 *
 * helper social
 *
 * @Src:       /application/helper
 * @File:      social_helper.php
 * @Link:      http://www.casasyterrneos.com
 * @Copyright: Copyright (c) 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Eduardo Hernandez Arenas (eduardo.hernandez@casasyterrenos.com)
 * @Create:    06-Julio-2012
 */
 
 
function checkFace($text)
{
	$search = json_decode(file_get_contents("https://graph.facebook.com/{$text}"),true);
	
	if($search["id"])
	{
		echo 'true';			
	}
	else
	{
		echo 'false';			
	}		
}
	
function checkYoutube()
{
	
	
//  		$this->load->library('MY_ClassXmlArray');
	
// 		$Xml = new ClassXmlArray;
	
	$account = $this->input->get('liga_youtube');
	
	$search = file_get_contents("http://gdata.youtube.com/feeds/api/videos/{$account}");
	
	if(0 != strlen(trim($search)))
	{
		echo 'true';
	}
	else
	{
		echo 'false';
	}
}

function checkTwit()
{		
	$account = $this->input->get('liga_twitter');
	ini_set('display_errors','Off');
	
	$search = json_decode(file_get_contents("https://api.twitter.com/1/users/lookup.json?screen_name=$account"),true);
			
	if($search[0]['id'])
	{
		echo 'true';		
	}
	else
	{
		echo 'false';	
	}	
}

function likeFace($uri)
{
    echo '<iframe src="//www.facebook.com/plugins/like.php?href='.urlencode($uri).'&amp;send=false&amp;layout=standard&amp;width=80&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=30" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:80px; height:30px;" allowTransparency="true"></iframe>';
}

function likeTwitter($uri)
{
    $button  = '';
    $button  = '<a href="https://twitter.com/share" class="twitter-share-button" data-url="'.urlencode($uri).'" data-lang="es" data-count="none">Twittear</a>'.
    $button .= '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
    
    echo $button;
}
 
 // END social helper

/* End of file social_helper.php */
/* Location: ./application/helper/social_helper.php */