<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * getCss
 * Returns a link tag with the selected css file
 */
if ( ! function_exists('getCss'))
{
	function getCss($css, $media = 'all')
	{
		if(!empty($css))
		{
			$css = (is_array($css)) ? $css : explode(",", $css);
			$css = str_replace(".css", "", $css);
			$link_tag = '';
			$media = (!empty($media)) ? $media : 'all';
			foreach($css as $file)
			{
				$link_tag .= '<link href="' . base_url() . 'inc/css/'.trim($file).'.css" rel="stylesheet" type="text/css" media="'.$media.'" />'."\n";
			}			
			return $link_tag;			
		}
		else 
		{
			return '';
		}

	}
}

// ------------------------------------------------------------------------

/**
 * getJs
 * Returns script tag with the defined file
 */
if ( ! function_exists('getJs'))
{
    function getJs($js)
	{
		if(!empty($js))
		{
			$js = (is_array($js)) ? $js : explode(",", $js);
			$js = str_replace(".js", "", $js);
			$script_tag = '';
			foreach($js as $file)
			{
		        $script_tag .= '<script src="' . base_url() . 'inc/js/'.trim($file).'.js"  type="text/javascript" language="javascript"></script>'."\n";
			}
	        return $script_tag;			
		}
		else 
		{
			return '';
		}

    }
}

/**
 * wordwrap_text
 * Returns text by number words
 */
if ( ! function_exists('wordwrap_text'))
{
	function wordwrap_text($string, $limit, $break = ".", $pad = "...")
	{
		// return with no change if string is shorter than $limit
		if(strlen($string) <= $limit)
			return $string;
		
		// is $break present between $limit and the end of the string?
		if(false !== ($breakpoint = strpos($string, $break, $limit))) {
			
			if($breakpoint < (strlen($string) - 1)) {
				
				$string = substr($string, 0, $breakpoint) . $pad;
			}
		}
		return $string;
	}
}

/* End of file assets_helper.php */
/* Location: ./system/helpers/assets_helper.php */