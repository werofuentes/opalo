<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Casas y terrenos
 * 
 * helper mailer
 *
 * @Src:       /application/helper
 * @File:      mailer_helper.php
 * @Link:      http://www.casasyterrneos.com
 * @Copyright: Copyright (c) 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Francisco González (francisco.gonzalez@casasyterrenos.com)
 * @Create:    11-Septiembre-2012
 */
 

 
/**
*  uriDetail 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $name
*  @param  string $to
*  @param  string $to
*  @param  string (html) $html
*  @return bolean 
*/
if ( ! function_exists('mailSend'))
{
    function mailSend($name, $to, $subjet, $html)
    {
       
        $CI =& get_instance();
        $CI->load->library('MY_PHPMailer');
        
        $mail             = new PHPMailer();
        $mail->IsSMTP();
        //$mail->SMTPDebug  = 1;                           // send via SMTP
        
        $mail->Host       = 'ssl://smtp.gmail.com';
        $mail->Port       = 465;
        $mail->SMTPAuth   = true;
        $mail->CharSet    = "UTF-8";
        $mail->Username   = 'comentariosweb@quid.com.mx';
        $mail->Password   = 'Comentariosweb';
        $mail->From       = 'comentariosweb@quid.com.mx';
        $mail->FromName   = $name;
        $mail->AddAddress($to);
        $mail->WordWrap   = 50;                              // set word wrap
        $mail->IsHTML(true);                               // send as HTML
        $mail->Subject    = $subjet;
        $mail->Body       = $html;
        
        if(!$mail->Send())
        {
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            
            return FALSE;
        }
        else
        {
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            
            return TRUE;
        }
    }
}


 // END mailer helper

/* End of file mailer_helper.php */
/* Location: ./application/helper/mailer_helper.php */