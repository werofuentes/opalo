<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Casas y terrenos
 * 
 * helper uri
 *
 * @Src:       /application/helper
 * @File:      uri_helper.php
 * @Link:      http://www.casasyterrneos.com
 * @Copyright: Copyright (c) 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Eduardo Hernandez Arenas (francisco.gonzalez@casasyterrenos.com)
 * @Create:    09-Julio-2012
 */
 

 if ( ! function_exists('sanear_string'))
{
    function sanear_string($string)
    {
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
 
    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(array("º", "-", "~", "#", "@", "|", "!","·", "$", "%", "&", "/", "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":", " "),"",
        $string
    );
 
 
    return $string;
    }
}

 
/**
*  uriDetail 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $type
*  @param  string $transaction
*  @param  string $suburb
*  @param  string $municipality
*  @param  int $id
*  @return string 
*/
if ( ! function_exists('uriDetail'))
{
    function uriDetail($type, $transaction, $suburb, $municipality, $id)
    {
        $concatinate = strToUri($type) ." " . strToUri($transaction) ." colonia ". strToUri($suburb) ." " . strToUri($municipality) ." " . strToUri($id);
        
        return base_url() . "detail/" . url_title($concatinate);
    }
}

/**
*  uriDetail 
* 
*  @author Eduardo Hernandez Arenas <eduardo.hernandez@casasyterrenos.com>
*  @access public
*  @param  string $state
*  @param  string $fracc
*  @param  int $id
*  @return string 
*/
if ( ! function_exists('uriName'))
{
    function uriName($state, $fracc, $id)
    {
        $concatinate = strToUri($state) ." " . strToUri($fracc) ." " . strToUri($id);
        
        return url_title($concatinate);
    }
}

/**
*  uriArticle
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $type
*  @param  string $transaction
*  @param  string $suburb
*  @param  string $municipality
*  @param  int $id
*  @return string 
*/
if ( ! function_exists('uriArticle'))
{
    function uriArticle($title, $id)
    {
        $concatinate = strToUri($title) ." " . strToUri($id);
        
        return base_url() . "article/" . url_title($concatinate);
    }
}



/**
*  strToUri 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $string
*  @return string 
*/
if ( ! function_exists('strToUri'))
{
    function strToUri($string)
    {
       $CI =& get_instance();
       $string = accentuation($string);
       
       return strtolower($CI->security->xss_clean($string));
    }
}


/**
*  accentuation 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $string
*  @return string 
*/
if ( ! function_exists('accentuation'))
{
    function accentuation($string)
    {
        $acentos_a = array('á', 'Á', 'é', 'É', 'í', 'Í', 'ó', 'Ó', 'ú', 'Ú', 'ñ', 'Ñ', '.', '"','%');
        $noacentos_a = array('a', 'A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U', 'n', 'N', '', '', '');
        $string = str_replace($acentos_a, $noacentos_a, $string);
        
        return $string;
    }
}

/**
*  cambiar mayusculas a minusculas 
* 
*  @author Eduardo Hernandez Arenas <eduardo.hernandez@casasyterrenos.com>
*  @access public
*  @param  string $string
*  @return string 
*/
if ( ! function_exists('mayMin'))
{
    function mayMin($string)
    {
       $string =  strtolower($string);
       $string =  ucwords($string);
       return $string;
    }
}

/**
*  uriDetailFracc 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $type
*  @param  string $transaction
*  @param  string $suburb
*  @param  string $municipality
*  @param  int $id
*  @return string 
*/
if ( ! function_exists('uriDetailFracc'))
{
    function uriDetailFracc($name, $id)
    {
        $concatinate = strToUri($name) ." " . strToUri($id);
        
        return base_url() . "vivienda-nueva/fraccionamiento/" . url_title($concatinate);
    }
}

/**
*  uriCalculator 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $type
*  @param  string $transaction
*  @param  string $suburb
*  @param  string $municipality
*  @param  int $id
*  @return string 
*/
if ( ! function_exists('uriCalculator'))
{
    function uriCalculator($id)
    {
        $concatinate = strToUri($id);
        
        return base_url() . "calculadora-hipotecaria/" . url_title($concatinate);
    }
}

/**
*  uriImgProp 
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $name
*  @return string 
*/
if ( ! function_exists('uriImgProp'))
{
    function uriImgProp($name)
    {        
        return base_url() . "uploads/img_casas/" . $name;
    }
}

/**
*  uriImgfracc
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $name
*  @return string 
*/
if ( ! function_exists('uriImgfracc'))
{
    function uriImgfracc($name)
    {        
        return base_url() . "uploads/vivienda_nueva/" . $name;
    }
}


/**
*  uriImgServicio
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $name
*  @return string 
*/
if ( ! function_exists('uriImgServicio'))
{
    function uriImgServicio($name)
    {        
        return base_url() . "uploads/logos_serv/" . $name;
    }
}

/**
*  uriImgArt
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $name
*  @return string 
*/
if ( ! function_exists('uriImgArt'))
{
    function uriImgArt($name)
    {        
        return base_url() . "uploads/img/articulos/" . $name;
    }
}

/**
*  uriServicio
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $name
*  @return string 
*/
if ( ! function_exists('uriServicio'))
{
    function uriServicio($cat = NULL, $string = NULL)
    {
        $concatinate = strToUri($cat);
        
        if(!is_null($cat) && !is_null($string))
            $return      = base_url() . "directorio-servicios/" . url_title($concatinate) . '#' . url_title(strToUri($string));
        else
            $return = base_url() . "directorio-servicios/" . url_title($concatinate);
         
        return $return;
    }
}

/**
*  uriListArticles
* 
*  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
*  @access public
*  @param  string $type
*  @param  string $transaction
*  @param  string $suburb
*  @param  string $municipality
*  @param  int $id
*  @return string 
*/
if ( ! function_exists('uriListArticles'))
{
    function uriListArticles($tipo, $para)
    {
        $paras = array('inmobiliaria' => 'inmobiliarias', 'hogar' => 'hogar');
        
        $concatinate = 'A'==$tipo?$paras[$para]:'noticias';
        
        return base_url() . "article/" . url_title($concatinate) . '/listado';
    }
}

  
 // END uri helper

/* End of file uri_helper.php */
/* Location: ./application/helper/uri_helper.php */