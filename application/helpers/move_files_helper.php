<?php
function mover($origen,$destino)
{
	if(preg_match("/lin/i", PHP_OS))
	{
		if(!empty($origen) && ""!=trim($origen))
		{
			if(exec("mv  ".$origen." ".$destino))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(copy($origen,$destino))
		{
			if(unlink($origen))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
}

function existe($archivo)
{
	if(preg_match("/lin/i", PHP_OS))
	{
		exec("find ".$archivo,$salida,$bool);
		if($salida[0]==$archivo)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(file_exists($archivo))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function elimina_archivo($archivo)
{
	if(preg_match("/lin/i", PHP_OS))
	{
		exec("rm ".$archivo,$salida,$bool);
		if(!$salida[0])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(unlink($archivo))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

function elimina_carpeta($carpeta)
{
	if(preg_match("/lin/i", PHP_OS))
	{
		exec("rm -R ".$carpeta,$salida,$bool);
		if(!$salida[0])
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		if(rmdir($carpeta))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>