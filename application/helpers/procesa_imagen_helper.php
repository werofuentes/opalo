<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function resize_image($img, $width = 327, $height = 273, $copy = FALSE)
{
	$CI =& get_instance();
	
	$CI->load->library('image_lib');	
	
	$config['image_library']  = 'gd2';
	$config['source_image']   = $img;
	$config['maintain_ratio'] = TRUE;
	
	if($copy)
	{
		// $config['create_thumb'] = TRUE;
		$config['new_image'] = $copy;
	}
	
	$config['width']  = $width;
	$config['height'] = $height;
	
	$CI->image_lib->initialize($config);
	$CI->image_lib->resize();
	
	unset($config);
	$CI->image_lib->clear(); 
}

#@ add background white image
function base_image($imagen,$fondo)
{
	$CI =& get_instance();
	
	$CI->load->library('image_lib');
	
	
	$config = array();
	
	$config['wm_type'] = 'overlay';
	$config['source_image'] = $fondo;
	$config['wm_overlay_path']  = $imagen;
	$config['wm_opacity'] = 100;
	$config['wm_vrt_alignment'] = 'middle';
	$config['wm_hor_alignment'] = 'center';
	$config['new_image']		= $imagen;
	
	$CI->image_lib->initialize($config); 
	
	$CI->image_lib->watermark();
	
	unset($config);
	$CI->image_lib->clear();
	
}

#@ upload files
function upfiles($name, $dir)
{
	$CI = get_instance();
	$CI->load->library('upload');
	
	$error = false;
	
	$config['upload_path'] 	 = realpath('./uploads/img/' . $dir);
	$config['allowed_types'] = '*';
	$config['overwrite'] 	 = false;
	$config['encrypt_name']  = true;
	$config['remove_space']  = true;
	
	$CI->upload->initialize($config);

	// subimos el archivo
	if (!$CI->upload->do_upload("file_" . $name)){
		$files = array('error' => $CI->upload->display_errors());
	
		// print_r($files); echo $config['upload_path'];
	}
	else
	{
		$files = array('upload_data' => $CI->upload->data());
		chmod($files['upload_data']['file_path'].$files['upload_data']['file_name'],0777);
		return  $files; //print_r($files);
	}
	
	if(array_key_exists('error',$files)):
		@unlink($files['full_path']);
		// return false; 
		print_r($error); //return false;
	else:
		return $files; //print_r($files); echo "<br />" . $config['upload_path'];
	endif;

}

#@ move files
function move_files_images($imagen, $newimage, $type = FALSE)
{
	//echo $type;

	// validamos para la configuracion
	// switch($type)
	// {
		// case 'roomie':
			// $source1 = "./uploads/roomies/";
			// $source2 = "./uploads/roomies/thumb/thumb_";
// 			
			// if( !rename( ($source1 . $imagen), ($source1 . $newimage) ) && !rename( ($source2 . $imagen), ($source2 . $newimage) ) )
				// return false;
			// else
				// return true;
// 
		// break;
// 	
		// case 'room':
			// $source1 = "./uploads/cuartos/";
			// $source2 = "./uploads/cuartos/thumb/thumb_";
// 			
			// if( !rename( ($source1 . $imagen), ($source1 . $newimage) ) && !rename( ($source2 . $imagen), ($source2 . $newimage) ) )
				// return false;
			// else
				// return true;
// 
		// break;
// 	
		// case 'clientes':
			// $source1 = "./uploads/clientes/";
			// $source2 = "./uploads/clientes/thumb/thumb_";
// 
			// if( !rename( ($source1 . $imagen), ($source1 . $newimage) ) && !rename( ($source2 . $imagen), ($source2 . $newimage) ) )
				// return false;
			// else
				// return true;
// 
		// break;
	// }
	
	if( !rename( ($imagen), ($newimage) ) )
				return false;
			else
				return true;
}

#@ change name file
function change_name_file($images, $string, $idProp, $type)
{
	
	$string_name = "";
		
	// cicle for string name
	reset($string);
	foreach ($string as $key => $value)
	{
		$string_name .= str_replace(" ", "-", $value);
		$string_name .= "-";
	}
	
	reset($images);
	foreach ($images as $k => $v)
	{
		$extencion = array_pop(explode(".", $v));
		$nueva 	   = ($string_name . ($k+1) . "." . $extencion);
		move_files_images($v, $nueva, $type);
		
		$new_images[] = array('img_nombre' => $nueva, 'img_id_propiedad' => $idProp, 'img_orden' => ($k+1));
	}
	
	reset($new_images);	
	return $new_images;
	
					
}