<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Casas y terrenos
 *
 * helper comentrios
 *
 * @Src:       /application/helper
 * @File:      comentarios_helper.php
 * @Link:      http://www.casasyterrneos.com
 * @Copyright: Copyright (c) 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Eduardo Hernandez Arenas (francisco.gonzalez@casasyterrenos.com)
 * @Create:    19-Septiembre-2012
 */

/**
 *  formComment
 *
 *  @author Francisco Gonnzalez N. <francisco.gonzalez@casasyterrenos.com>
 *  @access public
 *  @param  string $action
 *  @param  string $idform
 *  @return template
 */
if (!function_exists('formComment'))
{
    function formComment($action, $idform)
    {
        $CI = & get_instance();

        $attributes         = ' class="contacto" id="' . $idform . '"';

        $form['OPEN_FORM']  = form_open(base_url() . $action, $attributes);
        $form['NAME']       = form_input(array('name' => 'name', 'id' => 'name', 'placeholder' => 'José Fernando Gómez Rivas', 'class' => 'nombre campo'));
        $form['EMAIL']      = form_input(array('name' => 'email', 'id' => 'email', 'placeholder' => 'josefer@email.com', 'class' => 'nombre campo'));
        $form['TELEPHONE']  = form_input(array('name' => 'telephone', 'id' => 'telephone', 'placeholder' => '(33) 3344 4072', 'class' => 'nombre campo'));
        $form['MESSAGE']    = form_textarea(array('name' => 'message', 'id' => 'message', 'placeholder' => 'Necesito informes...', 'class' => 'mensaje', 'rows' => 6));
        $form['SUBMIT']     = form_submit(array('name' => 'send', 'class' => 'boton_enviar', 'value' => ''));
        $form['CLOSE_FORM'] = form_close();

        $html               =   $CI->parser->parse('site/comentarios/form', $form, TRUE);
        
        return $html;
        
    }

}
