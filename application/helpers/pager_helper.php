<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function create_pager($regtot=0,$link,$respp=10,$reset=TRUE)
{
	$CI =& get_instance();
	
	if($reset)
	{
		if(isset($_SESSION['PAGER']))
		{
			if($_SESSION['PAGER']['url']!=current_url())
			{
				unset($_SESSION['PAGER']);
			}
		}
	}
	
	if(isset($_SESSION['PAGER']))
	{
		if($regtot>0)
		{
			$res_pagtot=$regtot/$_SESSION['PAGER']['regpp'];
			
			if($res_pagtot<=1)
			{
				$pagtot = 1;
			}
			else
			{
				$res=explode('.',$res_pagtot);
				
				if(!empty($res[1]))
				{
					$pagtot = $res[0]+1;
				}
				else
				{
					$pagtot = $res[0];
				}
			}
		}
		else
		{
			$pagtot = 1;
		}
		
		$_SESSION['PAGER']['regtot'] = $regtot;
		$_SESSION['PAGER']['pagtot'] = $pagtot;
		$_SESSION['PAGER']['url'] = current_url();
			
	}
	else
	{
		if($regtot>0)
		{
			$res_pagtot=$regtot/$respp;
			
			if($res_pagtot<=1)
			{
				$pagtot = 1;
			}
			else
			{
				$res=explode('.',$res_pagtot);
				
				if(!empty($res[1]))
				{
					$pagtot = $res[0]+1;
				}
				else
				{
					$pagtot = $res[0];
				}
			}
		}
		else
		{
			$pagtot = 1;
		}
		
		$_SESSION['PAGER']['regtot'] = $regtot;
		$_SESSION['PAGER']['regpp']  = $respp;
		$_SESSION['PAGER']['pagtot'] = $pagtot;
		$_SESSION['PAGER']['curpag'] = 1;
		$_SESSION['PAGER']['offset'] = 0;
		$_SESSION['PAGER']['url']    = current_url();
	}
	
	$paginador['registros_totales'] = $_SESSION['PAGER']['regtot'];
	$paginador['registros_ppagina'] = $_SESSION['PAGER']['regpp'];
	$paginador['offset']            = $_SESSION['PAGER']['offset'];
	$paginador['paginas_totales']   = $_SESSION['PAGER']['pagtot'];
	$paginador['pagina_actual']     = $_SESSION['PAGER']['curpag'];
	
	
	/*Se crean links para el paginador*/ 
	
	$links['next_button'] 	= $paginador['pagina_actual']==$paginador['paginas_totales']?'<span class="'.$link['active_class'].'">'.$link['text_next'].'</span>':'<a href="javascript:void(0);" class="page_class" rel="page_next"><span class="color-1">'.$link['text_next'].'</span></a>';
	$links['end_button'] 	= $paginador['pagina_actual']==$paginador['paginas_totales']?'':'<a href="javascript:void(0);" class="page_class" rel="page_end"><span class="color-1">'.$link['text_end'].'</span></a>';
	$links['prev_button'] 	= $paginador['pagina_actual']==1?'<span class="'.$link['active_class'].'">'.$link['text_prev'].'</span>':'<a href="javascript:void(0);" class="page_class" rel="page_prev"><span class="color-1">'.$link['text_prev'].'</span></a>';
	$links['begin_button'] 	= $paginador['pagina_actual']==1?'<span class="'.$link['active_class'].'">'.$link['text_begin'].'</span>':'<a href="javascript:void(0);" class="page_class" rel="page_begin"><span class="color-1">'.$link['text_begin'].'</span></a>';
	
	if($paginador['paginas_totales']>=$link['page_limit'])
	{
		$media = ($link['page_limit']/2);
		if(($link['page_limit']%2)!=0)
		{
			$media = round($media);
		}
		
		if($paginador['pagina_actual']<=$media)
		{
			for($x=1;$x<=$link['page_limit'];$x++)
			{
				if($x==$paginador['pagina_actual'])
				{
					$links['number_button'][]='<span class="'.$link['active_class'].'">'.$x.'</span>';
				}
				else
				{
					$links['number_button'][]='<a href="javascript:void(0);" class="page_class" rel="page_numpage">'.$x.'</a>';
				}
			}
		}
		else if(($paginador['paginas_totales']-$paginador['pagina_actual'])<=$media)
		{
			for($x=(($paginador['paginas_totales'])-($link['page_limit']-1));$x<=$paginador['paginas_totales'];$x++)
			{
				if($x==$paginador['pagina_actual'])
				{
					$links['number_button'][]='<span class="'.$link['active_class'].'">'.$x.'</span>';
				}
				else
				{
					$links['number_button'][]='<a href="javascript:void(0);" class="page_class" rel="page_numpage">'.$x.'</a>';
				}
			}
		}
		else
		{
			for($x=($paginador['pagina_actual']-($media-1));$x<=($paginador['pagina_actual']+($media-1));$x++)
			{
				if($x==$paginador['pagina_actual'])
				{
					$links['number_button'][]='<span class="'.$link['active_class'].'">'.$x.'</span>';
				}
				else
				{
					$links['number_button'][]='<a href="javascript:void(0);" class="page_class" rel="page_numpage">'.$x.'</a>';
				}
			}
		}
	}
	else
	{
		for($x=1;$x<=$paginador['paginas_totales'];$x++)
		{
			if($x==$paginador['pagina_actual'])
			{
				$links['number_button'][]='<span class="'.$link['active_class'].'">'.$x.'</span>';
			}
			else
			{
				$links['number_button'][]='<a href="javascript:void(0);" class="page_class" rel="page_numpage">'.$x.'</a>';
			}
		}
	}
	
	$paginador['links'] = $links;
	
	return $paginador;
}

?>