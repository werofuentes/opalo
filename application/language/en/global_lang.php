<?php

$lang['home']     = "Home";
$lang['services'] = "Services";
$lang['contacus'] = "Contactus";
$lang['idioma']   = "Change to Spanish";
$lang['these']    = "You are looking at: ";
$lang['langg']    = "es";

/*
 * Labes for section contact us 
 * 
 */
$lang['contactus_title']    = "Contac Us";
$lang['contactus_street']   = "Street";
$lang['contactus_sales']    = "Sales";
$lang['contactus_write_us'] = "Write to us";
$lang['contactus_name']     = "Name";
$lang['contactus_email']    = "Email";
$lang['contactus_phone']    = "Telephone";
$lang['contactus_message']  = "Message";

/* End of file email_lang.php */
/* Location: ./system/language/spanish/email_lang.php */
