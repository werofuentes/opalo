<?php

$lang['home']      = "Inicio";
$lang['services']  = "Servicios";
$lang['contacus']  = "Contactanos";
$lang['idioma']    = "Cambiar a Inglés";
$lang['these']     = "Estás buscando en: ";
$lang['langg']     = "en";


/*
 * Labes for section contact us 
 * 
 */
$lang['contactus_title']    = "Contacto";
$lang['contactus_street']   = "Domicilio";
$lang['contactus_sales']    = "Ejecutivo de Ventas";
$lang['contactus_write_us'] = "Escríbenos";
$lang['contactus_name']     = "Nombre";
$lang['contactus_email']    = "Email";
$lang['contactus_phone']    = "Teléfono";
$lang['contactus_message']  = "Mensaje";



/* End of file email_lang.php */
/* Location: ./system/language/spanish/email_lang.php */
