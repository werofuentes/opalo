<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
* Controller: search
* @package		CodeIgniter
* @subpackage	Libraries
* @Copyright: Copyright 2011 - Marketing Digital Casas y Terrenos S.A. de C.V.
* @Developer:Eduardo Hernández Arenas (eduardo.hernandez@casasyterrenos.com)
* @Create: 09-07-2012
*
*/

class Search_box 
{
	private static $CI;
		
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('search_model');
		/* load and conf CI validation  */
		$this->CI->load->library('form_validation');
		$this->CI->form_validation->set_error_delimiters('<b class="form_error" htmlfor="nombre" generated="true">', '</b>');
	}
	
	public function filterSearch()
	{
		if(isset($_SESSION['search_params']))
		{
			
			$min_price = $_SESSION['search_params']['min'];
			$max_price = $_SESSION['search_params']['max'];
			$locations = $_SESSION['search_params']['locations'];
			$type_prop = $_SESSION['search_params']['type_prop'];
			$type_oper = $_SESSION['search_params']['oper'];
			$only_new  = $_SESSION['search_params']['only_new'];
			$bat	   = $_SESSION['search_params']['bat'];
			$bed 	   = $_SESSION['search_params']['bed'];
			$gar 	   = $_SESSION['search_params']['gar'];
			
			$min 		= !empty($min_price)?$min_price:0;
			$max 		= !empty($max_price)?$max_price:0;
			$type_prop 	= !empty($type_prop)?$type_prop:FALSE;
			$only_new 	= !empty($only_new)?$only_new:FALSE;
			
			$bat 		= is_numeric($bat)&&!empty($bat)?$bat:0;
			$bed 		= is_numeric($bed)&&!empty($bed)?$bed:0;
			$gar 		= is_numeric($gar)&&!empty($gar)?$gar:0;
			
			if(1 == $type_oper)
			{
				$type_oper_text = ' cos_costov ';
			}
			else if(2 == $type_oper)
			{
				$type_oper_text = ' cos_costor ';
			}
			
			/* SETTING VALUES FOR WHERE */
			$conds 		= array();
			$conds[] 	= $this->setRange($min,$max,$type_oper_text);
			//$conds[] 	= $this->setLocal($locations);
			$conds[] 	= $this->setOper($type_oper);
			$conds[]	= $this->setBat($bat);
			$conds[]	= $this->setBed($bed);
			$conds[]	= $this->setGar($gar);
			$cond_order	= $this->setOrder(array(array('field' => 'col_nombre','type' => 'ASC')));
			
			if($only_new)
			{
				$conds[] = " pfr_id_propiedad = prp_id_propiedad ";
			}
			
			if($type_prop)
			{
				$conds[] = " tpt_id_tipo = {$type_prop} ";
			}
			
			/*where conditions*/
			
			$cond_final = $this->setConds($conds);
			
			
			$locations 	= explode(',',$locations);
			$munis 		= $this->CI->search_model->byMunWords($locations);
			
			$cols  		= $this->CI->search_model->getColByMun($munis,$cond_final);
			//die($this->CI->db->last_query());
			
			/* DROPDOWN COLONIAS */
			$colonias = array('' => 'Otras Colonias');
			
			if($cols)
			{
				foreach ($cols as $value) 
				{
					$colonias[$value['col_id_colonia']] = $value['casas'];
				}
			}
			
			
			$data = array();
		
			$attributes = array('class' => 'filtro', 'id' => 'filter');
			
			$data['open_form']		= form_open(base_url() . 'searchengine/searchFilters',$attributes);
			$data['fil_min'] 		= form_input(array('name' => 'min', 'id' => 'min', 'value' => $min,'class' => 'rangos' ));
			$data['fil_max'] 		= form_input(array('name' => 'max', 'id' => 'max', 'value' => $max,'class' => 'rangos' ));
			$data['fil_bed'] 		= form_input(array('name' => 'bed', 'id' => 'bed', 'value' => $bed,'class' => 'rangos' ));
			$data['fil_colonias'] 	= form_dropdown("id_colonia", $colonias,NULL,'class="rangos" id="id_colonia"');
			$data['close_form']		= form_close();
			
			/* END DROPDOWN COLONIAS */
			
			$filtros = $this->CI->parser->parse('site/results3/filtros', $data, TRUE);
			
			return $filtros;
		}
	}
	
	public function setConds($conds = FALSE)
	{
		if($conds && is_array($conds))
		{
			$cond_final = '';
			
			foreach ($conds as $value) {
				if(0 < strlen(trim($value)))
				{
					$cond_final .= ' AND ' . $value;
				}
			}
			return $cond_final;
		}
		return FALSE;
	}
	
	public function getSearch($actions)
	{
		$tipo_prop 	= FALSE;
		$tipo_oper 	= 1;
		$location  	= '';
		$min 	   	= FALSE;
		$max		= FALSE;
		$only_new	= FALSE;
		$bed 		= '';
		$bat 		= '';
		$gar		= '';
		
		/* if exists session search_params then*/
		if(isset($_SESSION['search_params']))
		{
			$params 	= $_SESSION['search_params'];
			$tipo_prop 	= $params['type_prop'];
			$tipo_oper 	= $params['oper'];
			$location  	= $params['locations'];
			$min 	   	= $params['min'];
			$max		= $params['max'];
			$only_new	= $params['only_new'];
			
			/* IF EXIST LIMIT & OFFSET PAGINATOR */
			if(isset($params['limit']) && !empty($params['limit']))
			{
				$limit		= $params['limit'];
			}
			else 
			{
				$limit = 10;
			}
			
			if(isset($params['offset']) && !empty($params['offset']))
			{
				$offset		= $params['offset'];
			}
			else 
			{
				$offset = FALSE;
			}
			/************************************************/
			$bed 	   	= !isset($params['bed'])?'Hab':$params['bed'];
			$bat 	   	= !isset($params['bat'])?'Baño':$params['bat'];
			$gar		= !isset($params['gar'])?'Est.':$params['gar'];
		}
		
		$this->CI->load->model('tipospropiedades_model');
		
		$attributes 			= array( 'name' => 'form_search','id' => 'form_search');
		$data['open_form']		= form_open_multipart(base_url() . $actions,$attributes);
		                          
		$data['radio_oper_v']	= form_radio(array('name' => 'oper', 'value' => 1, 'class' => 'radios', 'checked' => $tipo_oper == 1?TRUE:FALSE));
		$data['radio_oper_r']	= form_radio(array('name' => 'oper', 'value' => 2, 'class' => 'radios', 'checked' => $tipo_oper == 2?TRUE:FALSE));
		                          
		$data['tipo_prop']		= form_dropdown("type_prop", $this->CI->tipospropiedades_model->getDropdowm(),!$tipo_prop?NULL:$tipo_prop, 'id="tipo_propiedad"');
		
		$data['inp_locations']  = form_input(array('name' => 'locations','id' => 'colonia', 'class' => 'inputs', 'value' => $location, 'placeholder' => 'Colonia'));
		
		$data['chk_onlynew']	= form_checkbox('only_new','1',!$only_new?FALSE:TRUE);
		
		$data['inp_min']		= form_input(array('name' => 'min','id' => 'min', 'class' => 'inputs precio', 'value' => $min, 'placeholder' => '$ Min', 'style' => 'vertical-align:middle;' ));
		$data['inp_max']		= form_input(array('name' => 'max','id' => 'max', 'class' => 'inputs precio', 'value' => $max, 'placeholder' => '$ Max', 'style' => 'vertical-align:middle;' ));
		
		$data['inp_bat']		= form_input(array('name' => 'bat','id' => 'bat', 'class' => 'inputs habitaciones', 'value' => $bat, 'placeholder' => 'Baños'));
		
		$data['inp_bed']		= form_input(array('name' => 'bed','id' => 'bed', 'class' => 'inputs habitaciones', 'value' => $bed, 'placeholder' => 'Cuartos'));
		
		$data['inp_gar']		= form_input(array('name' => 'gar','id' => 'gar', 'class' => 'gar_form', 'value' => $gar));
		
		
		
		$data['btn_submit']		= form_submit(array('name' => 'mysubmit','class' => 'boton_buscar_min boton_buscar_min-t'));
		
		$data['close_form']		= form_close();
		
		/* setting errors label */
		$data['err_inp_locations'] 	= form_error('inp_locations') == '' ? '' : form_error('inp_locations');
		$data['err_type_prop']		= form_error('type_prop') == '' ? '' : form_error('type_prop');
		$buscador = $this->CI->parser->parse('structure/search_engines/engine_1', $data, TRUE);
		
		return $buscador;
	}
	
	public function setBat($bat = NULL)
	{
		$cond = FALSE;
		
		if(!is_null($bat))
		{
			$cond = "  bathrooms >= {$bat} ";
		}
		return $cond;
	}

	public function setBed($bed = NULL)
	{
		$cond = FALSE;
		
		if(!is_null($bed))
		{
			$cond = "  bedrooms >= {$bed} ";
		}
		return $cond;
	}
	
	public function setGar($gar = NULL)
	{
		$cond = FALSE;
		
		if(!is_null($gar))
		{
			$cond = "  garage >= {$gar} ";
		}
		return $cond;
	}

	
	public function setOrder($fields = NULL)
	{
		if(!is_null($fields))
		{
			$pass 		= FALSE;
			$cond_order = '';
			
			if(is_array($fields))
			{
				foreach ($fields as $value) {
					if(!$pass)
					{
						$cond_order = " ORDER BY " . $value['field'] . ' ' . $value['type'];
						$pass 		= TRUE;
					}
					else
					{
						$cond_order .= ", " . $value['field'] . ' ' . $value['type'];
					}
				}
				return $cond_order;
			}
		}
		return FALSE;
	}
	
	public function setLimit($limit = 10, $offset = FALSE)
	{
		$cond_limit = "";
		
		$cond_limit = " limit ";
		
		if($offset && 0 < $offset)
		{
			$cond_limit .= "{$offset}, ";
		}
		
		$cond_limit .= " {$limit} ";
		
		
		
		return $cond_limit;
	}
	
	public function setOper($type = NULL)
	{
		if(!is_null($type))
		{
			$cond_oper = '';
			switch ($type) {
				case '1':
					$cond_oper = " prp_proposito in(1,4,5,7) ";
					break;
				
				default:
					$cond_oper = " prp_proposito in(2,4,6,7) ";
					break;
			}
			return $cond_oper;
		}
		return FALSE;
	}
	
	public function setRange($min = 0,$max = 0,$type = FALSE)
	{
		$cond_range = '';
		
		/*if(1000 > $min)
		{
			$min = 1000;
		}
		*/
		if($type)
		{
			if(0 < $max)
			{
				$cond_range = " {$type} BETWEEN {$min} AND {$max}  ";
			}
			else 
			{
				$cond_range = " {$type} >= {$min} ";
			}
		}
		return $cond_range;
	}

	public function setLocal($locations = NULL)
	{
		$cond_local = '';
		
		if(!is_null($locations))
		{
			$words = explode(',',$locations);
			$words = $this->CI->search_model->byLocalWords($words);
			
			if($words)
			{
				$pass_col = FALSE;
				$pass_mun = FALSE;
				$cond_loc = FALSE;
				
				foreach ($words as $value) 
				{
					switch ($value['tipo']) 
					{
						case 'colonia':
							if(!$pass_col)
							{
								$cond_col = " col_id_colonia in (" . $value['id'];
								$pass_col = TRUE;
							}
							else 
							{
								$cond_col .= "," . $value['id'];
							}
							break;
						
						case 'municipio':
							if(!$pass_mun)
							{
								$cond_mun = " mun_mun_inegi in (" . $value['id'];
								$pass_mun = TRUE;
							}
							else 
							{
								$cond_mun .= "," . $value['id'];
							}
							break;
					}
				}
				if($pass_col && $pass_mun)
				{
					$cond_loc = "( {$cond_col} ) OR {$cond_mun}))";
				}else if(!$pass_col && $pass_mun)				
				{
					$cond_loc = "  {$cond_mun}) ";
				}else if($pass_col && !$pass_mun)
				{
					$cond_loc = "  {$cond_col}) ";
				}
				return $cond_loc;
			}
			
			return FALSE;
		}
	}

	public function setColonia($colonia = NULL)
	{
		$cond_local = '';
		
		if(!is_null($colonia))
		{
			$cond_local = " col_id_colonia = " . $colonia;
		
			return $cond_local;
		}
		else 
		{
			return FALSE;
		}
	}

	public function setAttr($attr = array())
	{
		if(!empty($attr) && is_array($attr))
		{
			$result = '';
			
			
			foreach ($attrt as $key => $value) {
				switch ($key) {
					case 'banos':
						$result .= ' AND () ';
						break;
					case 'hab':
						
						break;	
					case 'est':
						
						break;
					case 'mts_t':
						
						break;
					case 'mts_const':
						
						break;
				}
			}
		}
		return FALSE;
	}
}

// END Search_box class

/* End of file Search_box.php */
/* Location: ./application/libraries/Search_box.php */