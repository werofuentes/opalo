<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Buscoroomies.com
* Controller: paginador
* @package		CodeIgniter
* @subpackage	Libraries
* @Copyright: Copyright 2011 - Marketing Digital Casas y Terrenos S.A. de C.V.
* @Developer:Eduardo Hernández Arenas (eduardo.hernandez@casasyterrenos.com)
* @Create: 14-febrero-2012
*
*/

class Paginator 
{
	public 	$limit;
	public 	$offset;
	public 	$query;
	private $CI;
	
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->config('paginator');	
		$this->limit = $this->CI->config->item('limit');
		$this->offset =$this->CI->config->item('offset');
		
	}
	
	public function drawButtonsBed($baselink = '',$total_rows = 0, $current_page = 1)
	{
		if(0 < $total_rows)
		{
			//last page button
			$last_page = $total_rows/$this->limit;
			
			//params of search
			$params = $_SESSION['sess_search_bed'];
			
			//find the key 'page' drop
			foreach ($params as $key => $value) {
				if('page' == $key){
					$current_page = $value;
					unset($params['page']);
				}
				else
				{
					$current_page = 1;	
				}
			}
			
			//calculating total pages
			if($this->limit < $total_rows)
			{
				$total_page = ceil($total_rows/$this->limit);
			}
			else 
			{
				$total_page = 1;
			}
			
			//
			$botones 		= '';
			$disabled_page 	= '';		
			$first			= FALSE;
			
			$base_link = base_url().$baselink.$this->CI->uri->assoc_to_uri($params);
			
			if(1 == $current_page)
			{
				$disabled_page 	= "disabled_page";
				$first 			= TRUE;
			}									
			
			//show the paginator if total rows > limit
			if(1 < $total_page)
			{
				//first buttons
				$botones  = '<li><a href="'.$base_link.'/page/1" class="css3 '.$disabled_page.'">&laquo; primera</a></li>';
					
				if(0 <= $current_page-1 && $first === FALSE)
				{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page-1).'" class="css3 ">'.($current_page - 1).'</a></li>';
				}
					
				//current button
				$botones .= '<li><a href="javascript:void(0);" class="css3 selected_page">'.$current_page.'</a></li>';
					
				//
				if($current_page+1 <= ($total_page))
				{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+1).'" class="css3 ">'.($current_page + 1).'</a></li>';
				}
				
				//
				if($current_page+2 <= ($total_page) && $first === TRUE)
				{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+2).'" class="css3 ">'.($current_page + 2).'</a></li>';
				}
				
				//
				if($current_page+1 < $total_page)
				{
					if($current_page+8 <= ($total_page))
					{
						$botones .= '<li><span>...</span></li>';
						$botones .= '<li><a href="'.$base_link.'page/'.($current_page + 8).'" class="css3 ">'.($current_page + 8).'</a></li>';
					}
				}
	
				//last buttons
				if($total_page != $current_page)
				{	
					$botones .= '<li><a href="'.$base_link.'/page/'.$total_page.'" class="css3"  >última &raquo;</a></li></ul>';
				}
	
				// prev and next buttons
				$botones .= '<ul class="right_paginator">';
					
				$botones .= '<li><a href="'.$base_link.'/page/'.($current_page-1).'" class="css3 '.$disabled_page.'" title="Anterior">&laquo;</a></li>';
					
				if($current_page >= $total_page)
				{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+1).'" class="css3 disabled_page" title="Siguiente">&raquo;</a></li>';
				}
				else 
				{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+1).'" class="css3" title="Siguiente">&raquo;</a></li>';
				}				
			}		
				
				
			return $botones;
		}
	}
	
	public function drawButtonsPeople($baselink = '',$total_rows = 0, $current_page = 1)
	{
		if(0 < $total_rows)
		{
			//last page button
			$last_page = $total_rows/$this->limit;
	
			//params of search
			$params = $_SESSION['sess_search'];
	
			//find the key 'page' drop
			foreach ($params as $key => $value) {
				if('page' == $key){
					$current_page = $value;
					unset($params['page']);
				}
				else
				{
					$current_page = 1;
				}
			}
	
			//calculating total pages
			if($this->limit < $total_rows)
			{
				$total_page = ceil($total_rows/$this->limit);
			}
			else
			{
				$total_page = 1;
			}
	
			//
			$botones 		= '';
			$disabled_page 	= '';
			$first			= FALSE;
	
			$base_link = base_url().$baselink.$this->CI->uri->assoc_to_uri($params);
	
			if(1 == $current_page)
			{
			$disabled_page 	= "disabled_page";
			$first 			= TRUE;
		}
	
			//show the paginator if total rows > limit
			if(1 < $total_page)
			{
			//first buttons
			$botones  = '<li><a href="'.$base_link.'/page/1" class="css3 '.$disabled_page.'">&laquo; primera</a></li>';
	
			if(0 <= $current_page-1 && $first === FALSE)
			{
			$botones .= '<li><a href="'.$base_link.'/page/'.($current_page-1).'" class="css3 ">'.($current_page - 1).'</a></li>';
			}
	
			//current button
			$botones .= '<li><a href="javascript:void(0);" class="css3 selected_page">'.$current_page.'</a></li>';
	
			//
			if($current_page+1 <= ($total_page))
			{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+1).'" class="css3 ">'.($current_page + 1).'</a></li>';
			}
	
			//
			if($current_page+2 <= ($total_page) && $first === TRUE)
					{
					$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+2).'" class="css3 ">'.($current_page + 2).'</a></li>';
			}
	
			//
			if($current_page+1 < $total_page)
			{
			if($current_page+8 <= ($total_page))
			{
			$botones .= '<li><span>...</span></li>';
			$botones .= '<li><a href="'.$base_link.'page/'.($current_page + 8).'" class="css3 ">'.($current_page + 8).'</a></li>';
			}
			}
	
			//last buttons
			if($total_page != $current_page)
			{
					$botones .= '<li><a href="'.$base_link.'/page/'.$total_page.'" class="css3"  >última &raquo;</a></li></ul>';
			}
	
			// prev and next buttons
			$botones .= '<ul class="right_paginator">';
	
			$botones .= '<li><a href="'.$base_link.'/page/'.($current_page-1).'" class="css3 '.$disabled_page.'" title="Anterior">&laquo;</a></li>';
	
			if($current_page >= $total_page)
			{
			$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+1).'" class="css3 disabled_page" title="Siguiente">&raquo;</a></li>';
			}
			else
			{
			$botones .= '<li><a href="'.$base_link.'/page/'.($current_page+1).'" class="css3" title="Siguiente">&raquo;</a></li>';
	}
	}
	
	
	return $botones;
	}
	}
}