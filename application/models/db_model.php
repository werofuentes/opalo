<?php

/*
 * Buscoroomies.com
 * Model: Db
 * 
 * @Src: /application/models/
 * @Copyright: Copyright 2011 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Francisco González N (francisco.gonzalez@casasyterrenos.com)
 * @Create: 24-Noviembre-2011
 * 
*/

class Db_model extends CI_Model
{
	
	#@ List records on table
	function listsRows($selects = NULL, $joins = NULL, $table, $where = NULL, $order_by = NULL, $limit = NULL, $offset = 0, $groupby = NULL)
	{
		#@ agregamos select values
		if($selects != NULL)
		{
			$query = $this->db->select($selects, FALSE);	
		}
		
		#@ si el where es diferente de null agregamos condición al query
		if($where != NULL)
		{
			$query = $this->db->where($where);	
		}
		
		#@ si los campos limit y offset son diferentes de null entonces limitamos nuestro query
		if($limit != NULL && $offset != NULL)
		{
			$query 	= $this->db->get($table, $limit, $offset);
		}
		
		#@ si tenemos un order
		if($order_by != NULL)
		{
			foreach($order_by as $field => $value)
			{
				$query 	= $this->db->order_by($field, $value);
			}
		}
		
		#@ Existe joins
		if(is_array($joins))
		{
			foreach($joins as $key => $valorArray)
			{
				$query = $this->db->join($valorArray['tabla'], $valorArray['sentencia'], $valorArray['tipo']);
			}
		}
		
		#@ Group by
		if(!is_null($groupby))
		{
			$query = $this->db->group_by($groupby); 
		}
		
		#@ si el limit y offset son nulos mandamos query simple
		if(is_null($limit))
		{
			$query 	= $this->db->get($table);
		}else{
			$query 	= $this->db->get($table,$limit,$offset);
		}
		
		$result	= $query->result_array();
				
		$query->free_result();
		$this->db->close();
		
		return $result;

	}
	
	#@ extract one record on table
	function oneRow($table, $where, $select = NULL, $join = NULL)
	{
		
		#@ agregamos select values
		if($select != NULL)
		{
			$query = $this->db->select($select, FALSE);	
		}
				
		#@ Existe joins
		if(is_array($join))
		{
			foreach($join as $key => $valorArray)
			{
				$query = $this->db->join($valorArray['tabla'], $valorArray['sentencia'], $valorArray['tipo']);
			}
		}
		
		$query  = $this->db->where($where);	
		$query 	= $this->db->get($table, 1);
		
		$result = $query->row_array();	
		
		$query->free_result();
		$this->db->close();
		
		if(is_array($result) && count($result)>0)
			return $result;
		else
			return false;
	}
	
	#@ Insert data on table
	function insertRecord($table, $fields, $type = NULL)
	{
		switch ($type)
		{
			case 'multi':
				
				if($this->db->insert_batch($table, $fields))
					return $this->db->insert_id();			
				else
					return false;
								
				break;
				
			default:

				if($this->db->insert($table, $fields))
					return $this->db->insert_id();			
				else
					return false;
				
				break;
			
		}

	}
	
	#@ Update data on table
	function updateRecord($table, $fields, $where, $type = NULL)
	{
		
		switch ($type)
		{
			case 'multi':
				
				if($this->db->update_batch($table, $fields, $where))
					return true;
				else
					return false;
								
				break;
			
			default:
				
				if($this->db->update($table, $fields, $where))
					return true;
				else
					return false;				
				break;
		}

	}
	
	#@ delete record on table
	function deteleRecord($table, $where, $type = NULL)
	{
		$control = array();
		switch ($type)
		{
			case 'multi':
				
				foreach ($where as $key => $arreglo)
				{
					if($this->db->delete($table, $arreglo))
						$control['good_query']  += 1;
					else
						$control['error_query'] += 1;
				}
				
				return $control;
				
				break;
			
			default:
				
				if($this->db->delete($table, $where))
					return true;
				else
					return false;
				
				break;
			
		}
	}

	
}