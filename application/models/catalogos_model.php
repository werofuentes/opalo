<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		Jessep Guillermo Martinez Barba
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

class Catalogos_model extends MY_Model 
{
	public function __construct()
	{
		parent::__construct();
	}





	//--------------Empresa Primaria-------------------------//

	public function getAllEmpresaPrimaria($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('EmpresaPrimaria', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorEmpresaPrimaria($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('EmpresaPrimaria', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneEmpresaPrimaria($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('EmpresaPrimaria', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addEmpresaPrimaria($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('EmpresaPrimaria', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropEmpresaPrimaria($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('EmpresaPrimaria', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updEmpresaPrimaria($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('EmpresaPrimaria', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



	//--------------Empresa Secundaria-------------------------//

	public function getAllEmpresaSecundaria($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('EmpresaSecundaria', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorEmpresaSecundaria($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('EmpresaSecundaria', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneEmpresaSecundaria($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('EmpresaSecundaria', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addEmpresaSecundaria($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('EmpresaSecundaria', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropEmpresaSecundaria($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('EmpresaSecundaria', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updEmpresaSecundaria($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('EmpresaSecundaria', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



		//--------------Servicios -------------------------//

	public function getAllServicios($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Servicios', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorServicios($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Servicios', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneServicios($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Servicios', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addServicios($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Servicios', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropServicios($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Servicios', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updServicios($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Servicios', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



		//--------------Vendedores -------------------------//

	public function getAllVendedores($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Vendedores', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorVendedores($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Vendedores', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneVendedores($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Vendedores', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addVendedores($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Vendedores', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropVendedores($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Vendedores', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updVendedores($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Vendedores', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


			//--------------Estatus -------------------------//

	public function getAllEstatus($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('EstatusEvaluado', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}



			//--------------Credencial -------------------------//
	public function getAllCredencial($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Credencial', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorCredencial($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Credencial', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneCredencial($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Credencial', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addCredencial($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Credencial', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropCredencial($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Credencial', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updCredencial($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Credencial', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



				//--------------Credencial -------------------------//
	public function getAllMovimientos($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Movimientos', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorMovimientos($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Movimientos', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneMovimientos($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Movimientos', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addMovimientos($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Movimientos', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropMovimientos($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Movimientos', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updMovimientos($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Movimientos', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



	//-------------- Certificaciones -------------------------//

	public function getAllCertificaciones($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Certificaciones', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorCertificaciones($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Certificaciones', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneCertificaciones($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Certificaciones', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addCertificaciones($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Certificaciones', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropCertificaciones($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Certificaciones', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updCertificaciones($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Certificaciones', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



		//-------------- Sucursal -------------------------//

	public function getAllSucursal($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Sucursales', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorSucursal($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Sucursales', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneSucursal($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Sucursales', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addSucursal($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Sucursales', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropSucursal($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Sucursales', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updSucursal($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Sucursales', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}



	//-------------- Freelance -------------------------//

	public function getAllFreelance($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Freelance', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorFreelance($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Freelance', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOneFreelance($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Freelance', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addFreelance($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Freelance', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropFreelance($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Freelance', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updFreelance($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Freelance', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	//-------------- Freelance -------------------------//

	public function getAllPsicologa($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('Psicologa', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getAllContadorPsicologa($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('Psicologa', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}


	public function getOnePsicologa($inWhere = NULL, $join = NULL)
	{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('Psicologa', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
	}


	public function addPsicologa($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('Psicologa', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}

	public function dropPsicologa($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('Psicologa', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}


	public function updPsicologa($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('Psicologa', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}






}


