<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Sysmaster - casasyterrenos.com
 * Model: Login
 * 
 * @Src: /application/models/
 * @Copyright: Copyright 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Jessep Martinez Barba (jessep.martinez@casasyterrenos.com)
 * @Create: 26-Noviembre-2012
 * 
 * 
*/


class Login_model extends CI_Model
{
	
	#@ create session user
	function loginUser($user, $password)
	{
		//, "usu_password" => $password
		$this->load->model('db_model');
		$condicion = array("use_login" => $user, "use_active" => '1');
		
		
		$query = $this->db_model->oneRow("users", $condicion);

	    $result_count = count($query);
		if($result_count == 0)
		{
			$resultado['error'] = 'El usuario no existe';
		}
		else
		{
			

			$pw = hash('sha256',$password);
			//if(strcmp($password, $this->encrypt->decode($query['use_key'])) == 0)
			if(strcmp($pw, $query['use_key']) == 0)
			{
	
	            $parametros = array(
		            'use_lastaccess'    => date('Y-m-d H:m:s')
	            );
	
	            $this->db_model->updateRecord("users", $parametros, array("use_id" => $query['use_id']));
				$resultado = $this->db_model->oneRow("users", array("use_id" => $query['use_id']));

			}
			else
			{

				$resultado['error'] = 'Login Incorrecto';
			}
	
       	}
		
		return $resultado;
	}
	
	
	
	
	#@ add new user for login
	function addUser($fields)
	{
		$id_usuario = $this->db_model->insertRecord("usuarios", $fields);
		
		return $id_usuario;
		
	}
	
	#@ delete or destroy session exists
	function destroyLogin($name)
	{
		unset($_SESSION[$name]);
		//session_destroy();
	}
	
	#@ return password user login
	function returnPasswd($id)
	{
		$condicion = array("usu_id" => $id);
		$query = $this->db_model->oneRow("usuarios", $condicion);
		
		return $query;
	}
	
	#@ update password login
	function updatePasswdLogin($passwd, $id)
	{
		$fields = array("usu_password" => $passwd);
		$where 	= array("usu_id" => $id);
		
		$query  = $this->db_model->updateRecord("usuarios", $fields, $where);
		
		return $query;
	}
	
	#@ data user
	function getUser($id)
	{
		$where 	= array("usu_id" => $id);
		$query  = $this->db_model->oneRow("usuarios", $where);
		
		return $query;
	}
	
}
	