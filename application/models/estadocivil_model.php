<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		Jessep Guillermo Martinez Barba
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

class Estadocivil_model extends MY_Model 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	//private $tabla = 'nombre_tabla';
	
	/**
	* Gets all Guides 
	* 
	*  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	*  @access public
	*  @param  array
	*  @param  string
	*  @param  string
	*  @param  integer
	*  @param  integer
	*  @return array 
	*/
	
	public function getAllEstadoCivil($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('EdoCivil', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}



	/*
	 * Get one Guides
	 * 
 	 *  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @return recordset 
	 * 
	 */

	public function getOneEstadoCivil($inWhere = NULL, $join = NULL)
		{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('EdoCivil', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
		}
		
		
	
	
	 /*
	 * add Guides
	 * 
 	 *  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @return array 
	 * 
	 */
	
	public function addEstadoCivil($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('EdoCivil', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}
	
	
	
	
	
	
	/*
	 * drop Guides
	 * 
 	 *  @author Jessep Guillermo Martinez BArba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  string
	 *  @return array 
	 * 
	 */
	public function dropEstadoCivil($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('EdoCivil', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	
		/*
	 * update Guides
	 * 
 	 *  @author Jessep Guillermo Martinez BArba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @param  array
	 *  @return TRUE or FALSE
	 * 
	 */
	public function updEstadoCivil($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('EdoCivil', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	
	
}