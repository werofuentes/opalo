<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		Jessep Guillermo Martinez Barba
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

class Type_model extends MY_Model 
{
	public function __construct()
	{
		parent::__construct();
	}
	
	//private $tabla = 'nombre_tabla';
	
	/**
	* Gets all type 
	* 
	*  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	*  @access public
	*  @param  array
	*  @param  string
	*  @param  string
	*  @param  integer
	*  @param  integer
	*  @return array 
	*/
	
	public function getAllTypes($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArray('type', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}

    public function getAllContadorTypes($inWhere = NULL, $inSelect = NULL, $join = NULL, $inOrderBy = NULL, $inLimit = NULL, $inOffset = 0)
	{
		$query 	 = '';
		$result  = '';		
		
		$query = $this->getArrayCount('type', $inWhere, $inSelect, $join, $inOrderBy, NULL, $inLimit, $inOffset);
		
		if(0 < count($query))
		{
			return $query;
		}
		return FALSE;
	}

	/*
	 * Get one type
	 * 
 	 *  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @return recordset 
	 * 
	 */

	public function getOneType($inWhere = NULL, $join = NULL)
		{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('type', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
		}
		
		
		/*
	 * Get one Permisos
	 * 
 	 *  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @return recordset 
	 * 
	 */

	public function getOneTypePermisos($inWhere = NULL, $join = NULL)
		{		
			$result  = '';		
			if(!is_null($inWhere))
			{
				$result = $this->getRow('permisos', $inWhere, $join);
			
				if(0 < count($result))
				{
					return $result;
				}
			}
			
			return FALSE;
		}


	
	 /*
	 * add type
	 * 
 	 *  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @return array 
	 * 
	 */
	
	public function addType($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('type', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}
	
	
	 /*
	 * add permisos
	 * 
 	 *  @author Jessep Guillermo Martinez Barba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @return array 
	 * 
	 */
	
	public function addTypePermisos($data = NULL)
	{
		$result = '';
		
		if(!is_null($data))
		{
			$result = $this->addRows('permisos', $data);
			
			if($result)
			{
				return $result;
			}
		}
		return FALSE;
	}
	
	/*
	 * drop type
	 * 
 	 *  @author Jessep Guillermo Martinez BArba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  string
	 *  @return array 
	 * 
	 */
	public function dropType($id = NULL)
	{
		$result = '';
		
		if(!is_null($id))
		{
			$result = $this->delRows('type', $id);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	
		/*
	 * update type
	 * 
 	 *  @author Jessep Guillermo Martinez BArba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @param  array
	 *  @return TRUE or FALSE
	 * 
	 */
	public function updType($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('type', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	
	/*
	 * update Permisos
	 * 
 	 *  @author Jessep Guillermo Martinez BArba <jessep.martinez@casasyterrenos.com>
	 *  @access public
	 *  @param  array
	 *  @param  array
	 *  @return TRUE or FALSE
	 * 
	 */
	public function updTypePermisos($data = NULL, $where = NULL)
	{
		$result = '';
		
		if(!is_null($where))
		{
			$result = $this->updateRows('permisos', $data, $where);
			
			if($result)
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	
	
}