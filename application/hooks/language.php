<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Casas y terrenos
 *
 * Hook Language
 *
 * @Src:       /application/hooks
 * @File:      language.php
 * @Link:      http://www.casasyterrneos.com
 * @Copyright: Copyright (c) 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Francisco Gonzalez (francisco.gonzalez@casasyterrenos.com)
 * @Create:    19-Julio-2012
 */
 
class Language
{      
    function lang()
    {
       
        
        $CI =& get_instance();

        if(!isset($_SESSION[LOCATION]['language']))
        {
            $lang = "es";
            $_SESSION[LOCATION]['language'] = $lang;
            
        }
        else
            $lang = $_SESSION[LOCATION]['language'];
        
        $CI->lang->load('global', $lang);
        
    }
}
// END language hook

/* End of file language.php */
/* Location: ./application/hooks/language.php */