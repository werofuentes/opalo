<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Casas y terrenos
 *
 * Hook Language
 *
 * @Src:       /application/hooks
 * @File:      geolocation.php
 * @Link:      http://www.casasyterrneos.com
 * @Copyright: Copyright (c) 2012 - Marketing Digital Casas y Terrenos S.A. de C.V.
 * @Developer: Francisco Gonzalez (francisco.gonzalez@casasyterrenos.com)
 * @Create:    19-Julio-2012
 */
 
class Geolocation
{      
    function location()
    {
       
        $locations = FALSE;
        $CI =& get_instance();
        $CI->load->library('geoip_lib');
        $CI->load->model('estados_model');
        
		$URL = $CI->uri->segment_array();
		
		if('home' == $CI->router->class && 'zp' != $URL[1] && 'vivienda-nueva' != $URL[1])
		{
			if(isset($URL[2]))
			{
				$url_name_state 		= urldecode($URL[2]);

				$where 					= array('est_id_pais' => 42,'est_alias LIKE'=> "%{$url_name_state}%");
                $state 					= $CI->estados_model->getOneState($where);
				$estado['est_nombre'] 	= $state['est_alias'];
	            $estado['est_num']    	= $state['est_num_estado'];
					
				$this->creaSesion($estado);
			}
			else if(get_cookie('patilla'))
			{
				$estado = json_decode(get_cookie('patilla'), TRUE);
				
				if(empty($estado['est_nombre']))
				{
					delete_cookie('patilla');
					$locations = base_url() . "home/jalisco";
				}
				else 
				{
					$this->creaSesion($estado);
					$locations = base_url() . "home/" . strtolower(urldecode($estado['est_nombre']));
				}

			}
			else if(!empty($_SESSION[LOCATION]['geoip']['est_nombre']))
			{
				$this->creaSesion($_SESSION[LOCATION]['geoip']);
				
				$locations = base_url() . "home/" . strtolower(urldecode($_SESSION[LOCATION]['geoip']['est_nombre']));

			}
			else 
			{
				if($CI->geoip_lib->InfoIP())
	            {
	                $geoip 	= $CI->geoip_lib->result_array();

					if('MX' == $geoip['country_code'])
					{
						$region = $geoip['region_name'];
		                $where 	= array('est_id_pais' => 42,'est_nombre LIKE'=> "%{$region}%");
		                $state 	= $CI->estados_model->getOneState($where);
						
						if(!$state)
						{
							$estado['est_nombre'] = "Jalisco";
		                	$estado['est_num']    = 14;
						}
						else 
						{
							$estado['est_nombre'] = $state['est_alias'];
		                	$estado['est_num']    = $state['est_num_estado'];
						}
					}
					else 
					{
						$estado['est_nombre'] = "Jalisco";
	                	$estado['est_num']    = 14;
					}

	            } 
	            else
	            {
	                $estado['est_nombre'] = "Jalisco";
	                $estado['est_num']    = 14;
	            }
				
				$this->creaSesion($estado);
				
				$locations = base_url() . "home/" . strtolower(urldecode($estado['est_nombre']));
			}
			if($locations)  
			{
			 	redirect($locations);
			}
		}
		else 
		{
			if(get_cookie('patilla'))
			{
				$estado = json_decode(get_cookie('patilla'),TRUE);
				$this->creaSesion($estado);
				
				$locations = base_url() . "home/" . strtolower(urldecode($estado['est_nombre']));
			}
			else if(!empty($_SESSION[LOCATION]['geoip']['est_nombre']))
			{
				$this->creaSesion($_SESSION[LOCATION]['geoip']);
				
				$locations = base_url() . "home/" . strtolower(urldecode($_SESSION[LOCATION]['geoip']['est_nombre']));
			}
			else 
			{
				if($CI->geoip_lib->InfoIP())
	            {
	                $geoip 	= $CI->geoip_lib->result_array();
                    
                    if('MX' == $geoip['country_code'])
                    {
                        
    	                $region = $geoip['region_name'];
    	                $where 	= array('est_id_pais' => 42,'est_alias LIKE'=> "%{$region}%");
    	                $state 	= $CI->estados_model->getOneState($where);
    	                
    	                $estado['est_nombre'] = $state['est_alias'];
    	                $estado['est_num']    = $state['est_num_estado'];
                    }
                    else
                    {
                        $estado['est_nombre'] = "Jalisco";
                        $estado['est_num']    = 14;
                    }
	            } 
	            else
	            {
	                $estado['est_nombre'] = "Jalisco";
	                $estado['est_num']    = 14;
	            }
				
				$this->creaSesion($estado);
			}
			

		}
    }

	/* crea la sesion y la cookies de estado */
	function creaSesion($estado = NULL)
	{
		if(!is_null($estado) && is_array($estado))
		{
			/* delete session & cookie */	
			unset($_SESSION[LOCATION]);
			delete_cookie('patilla');
			/***************************/
			
			$json 						 = json_encode($estado);
			$_SESSION[LOCATION]['geoip'] = $estado;
			$cookie = array(
						'name'   => 'patilla',
						'value'  => $json,
						'expire' => time()+3600*24*365
					);

			set_cookie($cookie);
		}
	}
}
// END language hook

/* End of file geolocation.php */
/* Location: ./application/hooks/geolocation.php */