<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		
 * @copyright	Copyright (c) 2012 - 2024, 
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * Provides get arrays or get row 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		
 */
class MY_Model extends CI_Model {
	
	/**
	 * Constructor
	 *
	 * Simply determines whether the mcrypt library exists.
	 *
	 */	
	public function __construct()
	{
	    parent::__construct();
	}
	// --------------------------------------------------------------------

	/**
	 * Set Joins 
	 *
	 * Allow set joins tables need for query
	 *
	 * @access	public
	 * @param	array("table" => "tablename", "cond" => "conditions_fields", "type" => "inner")
	 */
	public function setJoins($joins = NULL)
	{
		if(!is_null($joins) && is_array($joins))
		{
			foreach ($joins as $key => $value) 
			{
				$query = $this->db->join($value['table'], $value['cond'], $value['type']);	
			}
		}
	}
	// --------------------------------------------------------------------

	/**
	 * Set the group condition need for query 
	 *
	 * @access	public
	 * @param	string
	 */
	public function setGroup($groupBy = NULL)
	{
		if(!is_null($groupBy))
		{			
			$query = $this->db->group_by($groupBy);	
		}
	}
	// --------------------------------------------------------------------

	/**
	 * Set the order fields need fro query
	 *
	 * @access	public
	 * @param	array
	 */
	public function setOrder($orderBy = NULL)
	{
		if(!is_null($orderBy) && is_array($orderBy))
		{
			foreach ($orderBy as $key => $value) {
				$query = $this->db->order_by($key,$value);
			}
		}
	}
	// --------------------------------------------------------------------

	/**
	 * Set where conditions fro query
	 *
	 * @access	public
	 * @param	string
	 */
	public function setWhere($where = NULL)
	{
		if(!is_null($where))
		{
			$query = $this->db->where($where, NULL, TRUE);

		}
	}
	
	/**
	 * Set where conditions fro query
	 *
	 * @access	public
	 * @param	string
	 */
	public function setWhere2($where = NULL)
	{
		if(!is_null($where))
		{
			$query = $this->db->where($where, NULL, FALSE);

		}
	}
	
	/**
	 * Set where conditions from array
	 *
	 * @access	public
	 * @param	string
	 */
	public function setWhereFromArray($where = NULL)
	{
		if(!is_null($where) && is_array($where))
		{
			$condicion 	= '';
			$pass 		= FALSE;
			
			foreach ($where as $key => $value) 
			{
				if ( ! $this->_has_operator($key))
				{
					$key .= ' = ';
				}
				
				if($pass)
				{
					$condicion  = $key . $value;
					$pass		= TRUE;
				}
				else
				{
					$condicion  .= " AND " . $key . $value;
				}
			}
			return $condicion;
		}
		return FALSE;
	}
	// --------------------------------------------------------------------

	/**
	 * Tests whether the string has an SQL operator
	 *
	 * @access	private
	 * @param	string
	 * @return	bool
	 */
	function _has_operator($str)
	{
		$str = trim($str);
		if ( ! preg_match("/(\s|<|>|!|=|is null|is not null)/i", $str))
		{
			return FALSE;
		}

		return TRUE;
	}
	
	/**
	 * Set fields selected fro query
	 *
	 * @access	public
	 * @param	string
	 */
	public function setSelect($selects = NULL)
	{
		if(!is_null($selects))
		{
			$query = $this->db->select($selects, FALSE);
		}
	}
	// --------------------------------------------------------------------

	/**
	 * Get one row 
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	array
	 */
	public function getRow($table = NULL, $where = NULL, $joins = NULL)
	{
		if(!is_null($table) && !is_null($where))
		{
			$this->setWhere($where);
			
			if(!is_null($joins))
			{
				$this->setJoins($joins);
			}
			
			$query 	= $this->db->get($table, 1);
		
			$result = $query->row_array();	
			
			$query->free_result();
			
			$this->db->close();
			
			if(is_array($result) && count($result)>0)
			{
				return $result;
			}
		}
		
		return FALSE;
	}
	// --------------------------------------------------------------------
	
	public function getRowESE($table = NULL, $where = NULL, $joins = NULL)
	{
		if(!is_null($table) && !is_null($where))
		{
			$this->setWhere($where);
			
			if(!is_null($joins))
			{
				$this->setJoins($joins);
			}
			
			$query 	= $this->db->get($table);
		
			$result = $query->row_array();	
			
			$query->free_result();
			
			$this->db->close();
			
			if(is_array($result) && count($result)>0)
			{
				return $result;
			}
		}
		
		return FALSE;
	}
	// --------------------------------------------------------------------

	/**
	 * Get one row 
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	string
	 * @param	array
	 * @param	array
	 * @param	string
	 * @param	integer
	 * @param	integer
	 */
	public function getArray($table = NULL, $where = NULL, $selects = NULL,  $joins = NULL, $orderBy = NULL, $groupBy = NULL, $limit = NULL, $offset = 0)
	{
		if(!is_null($table))
		{
			$this->setWhere($where);
			$this->setJoins($joins);
			$this->setOrder($orderBy);
			$this->setGroup($groupBy);
			$this->setSelect($selects);
			
			if(!is_null($limit))
			{
				$query 	= $this->db->get($table,$limit,$offset);
			}
			else 
			{
				$query 	= $this->db->get($table);
			}
			
			$result	= $query->result_array();
				
			$query->free_result();
			
			$this->db->close();
			
			return $result;
		}
		
		return FALSE;
	}
	
	public function getArrayCount($table = NULL, $where = NULL, $selects = NULL,  $joins = NULL, $orderBy = NULL, $groupBy = NULL, $limit = NULL, $offset = 0)
	{
		if(!is_null($table))
		{
            $this->setWhere($where);
		    $query 	= $this->db->get($table);
			$result	= $query->num_rows();
				
			$query->free_result();
			
			$this->db->close();
			
			return $result;
		}
		
		return FALSE;
	}
	// --------------------------------------------------------------------

	/**
	 * Get one row 
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	string
	 * @param	array
	 * @param	array
	 * @param	string
	 * @param	integer
	 * @param	integer
	 */
	public function getArray2($table = NULL, $where = NULL, $selects = NULL,  $joins = NULL, $orderBy = NULL, $groupBy = NULL, $limit = NULL, $offset = 0)
	{
		if(!is_null($table))
		{
			$this->setWhere2($where);
			$this->setJoins($joins);
			$this->setOrder($orderBy);
			$this->setGroup($groupBy);
			$this->setSelect($selects);
			
			if(!is_null($limit))
			{
				$query 	= $this->db->get($table,$limit,$offset);
			}
			else 
			{
				$query 	= $this->db->get($table);
			}
			
			$result	= $query->result_array();
				
			$query->free_result();
			
			$this->db->close();
			
			return $result;
		}
		
		return FALSE;
	}
	// --------------------------------------------------------------------
	
	
	
	/**
	 * delete one row 
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 */
	public function delRows($table = NULL, $where = NULL)
	{
		if(!is_null($table) && !is_null($where))
		{
			if($this->db->delete($table, $where))
			{
				return TRUE;
			}			
		}
		return FALSE;
	}
	// --------------------------------------------------------------------

	/**
	 * add row 
	 *
	 * @access	public
	 * @param	string
	 * @param	string or array
	 */
	public function addRows($table = NULL, $fields = NULL,$multi = FALSE)
	{
		if(!is_null($table) && !is_null($fields) && is_array($fields))
		{
			if($multi)
			{
				if($this->db->insert_batch($table, $fields))
				{
					return !$this->db->insert_id()?TRUE:$this->db->insert_id();
				}
			}
			else 
			{
				if($this->db->insert($table, $fields))
				{
					return !$this->db->insert_id()?TRUE:$this->db->insert_id();
				}
			}
		}
		return FALSE;
	}
	// --------------------------------------------------------------------

	/**
	 * update rows 
	 *
	 * @access	public
	 * @param	string
	 * @param	string or array
	 * @param	string
	 */
	public function updateRows($table = NULL, $fields = NULL, $where = NULL,$multi = FALSE)
	{
		if(!is_null($table) && !is_null($fields) && !is_null($where))
		{
			if($multi)
			{
				if($this->db->update_batch($table, $fields, $where))
				{
					return TRUE;
				}
			}
			else 
			{
				if($this->db->update($table, $fields, $where))
				{
					return TRUE;
				}
			}
		}
		
		return FALSE;
	}
}
// END My_Model class

/* End of file My_Model.php */
/* Location: ./application/core/My_Model.php */