<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		Hormigasystems Team
 * @copyright	Copyright (c) 2012 - 2024, Hormigasystems
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Model Class
 *
 * Provides get arrays or get row 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Hormigasystems Dev Team
 */
class MY_Loader extends CI_Loader {
	// All these are set automatically. Don't mess with them.
	public $_ci_ob_level;
	public $_ci_view_path		= '';
	public $_ci_library_paths	= array();
	public $_ci_model_paths	= array();
	public $_ci_helper_paths	= array();
	public $_base_classes		= array(); // Set by the controller class
	public $_ci_cached_vars	= array();
	public $_ci_classes		= array();
	public $_ci_loaded_files	= array();
	public $_ci_models			= array();
	public $_ci_helpers		= array();
	public $_ci_varmap			= array('unit_test' => 'unit', 'user_agent' => 'agent');


	/**
	 * Constructor
	 *
	 * Sets the path to the view files and gets the initial output buffering level
	 *
	 * @access	public
	 */
	function __construct()
	{
		$this->_ci_view_path = APPPATH.'views/';
		$this->_ci_ob_level  = ob_get_level();
		$this->_ci_library_paths = array(APPPATH, BASEPATH);
		$this->_ci_helper_paths = array(APPPATH, BASEPATH);
		$this->_ci_model_paths = array(APPPATH);

		log_message('debug', "Loader Class Initialized");
	}
}
// END My_Exceptions class

/* End of file My_Exceptions.php */
/* Location: ./application/core/My_Exceptions.php */