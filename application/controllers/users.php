<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Users extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->model('user_model');
		$this->load->model('type_model');
		$this->load->model('catalogos_model');
		$this->load->library('email');
		
		//echo $this->db->last_query();
	}
	
		
	public function index()
	{

		$data             = array();
		
		$pages=10; //Número de registros mostrados por páginas
        $this->load->library('pagination'); //Cargamos la librería de paginación
        $config['base_url'] = base_url().'users/index'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
        $config['total_rows'] = $this->user_model->getAllContadorUsers();//calcula el número de filas  
        $config['per_page'] = $pages; //Número de registros mostrados por páginas
        $config['num_links'] = 20; //Número de links mostrados en la paginación
        $config['first_link'] = 'Primera';//primer link
        $config['last_link'] = 'Última';//último link
        $config["uri_segment"] = 3;//el segmento de la paginación
        $config['next_link'] = 'Siguiente';//siguiente link
        $config['prev_link'] = 'Anterior';//anterior link
        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
        $this->pagination->initialize($config); //inicializamos la paginación        
		
		
		$usuarios         = $this->user_model->getAllUsers(null,null,null,null,$config['per_page'],$this->uri->segment(3));
		$data['usuarios'] = $usuarios;
		$data['NumPaginas'] = $this->pagination->create_links();
		$this->parser->parse('configuracion/users', $data);
      }

	
	public function formAddUSers()
	{

		$where                = array (
		                        'type.typ_active' => '1'
		                        );
 
		$types                = $this->type_model->getAllTypes($where);
		$dataTodos['types']   = $types;
		

		$Empresa                = $this->catalogos_model->getAllEmpresaSecundaria();
		$dataTodos['empresa']   = $Empresa;


		$Psicologa = $this->catalogos_model->getAllPsicologa();
		$dataTodos['psicologa']   = $Psicologa;


		$Freelance = $this->catalogos_model->getAllFreelance();
		$dataTodos['freelance']   = $Freelance;

		//echo $this->db->last_query();	
		//die;

		$this->parser->parse('configuracion/users_form_add', $dataTodos);
	}
	

	
	public function addUsers()
	{
			
		$data = array();
		$data['use_login']                = $this->input->post('login', TRUE);
		//$data['use_key']                  = $this->encrypt->encode($this->input->post('keys', TRUE));
		$data['use_key']                  = hash('sha256',$this->input->post('keys', TRUE));
		$data['use_session']              = $this->input->post('session', TRUE);
		$data['use_active']               = $this->input->post('activo', TRUE);
		
		if($data['use_active'] == 1){
			$asunto = 'Usuario y contraseña OPALO';
			$mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/logo.png" width="178" height="145" /><td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) usuario tus accesos al sistema son los siguientes</td>
				  </tr>
				  <tr>
				     <td colspan="2" align="center"><a href="'.base_url().'" >'.base_url().'</a></td>
				  </tr>
				  
				  <tr>
				     <td >Usuario:</td>
				     <td align="center">'.$data['use_login'].'</td>
				  </tr>
				  <tr>
				     <td >Password:</td>
				     <td align="center">'.$this->input->post('keys', TRUE).'</td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				</tablet>
				';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);
				$email = $this->input->post('email', TRUE);
				
				$this->email->from('no-replay@sistemaopalo.com', 'SISTEMA');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
		}
		
		
		$data['use_datecreate']           =  date("Y-m-d H:i:s");
		$data['use_typ_id']               = $this->input->post('tipo_usuario', TRUE);	
		$data['use_email']                = $this->input->post('email', TRUE);	
		
		 if($data['use_typ_id'] == 7){
		   	  $data['id_empresa']               = $this->input->post('empresa', TRUE);	
			
		 }elseif($data['use_typ_id'] == 8){
		 		 $data['id_psicologa']          = $this->input->post('psicologa', TRUE);	

		 }elseif($data['use_typ_id'] == 9){
		 		 $data['id_freelance']          = $this->input->post('freelance', TRUE);	

		 }else{
		 	
			$data['id_empresa']                 = null;
			$data['id_psicologa']               = null;
			$data['id_freelance']               = null;
		 }
		
		
		$users                            = $this->user_model->addUser($data);
		
		$dataTodos                        = array();	
		
        redirect('users');
	}

    public function formEditUSers($id = null)
	{

		
		 
		$where['use_id']      = $id;
		$usuarios             = $this->user_model->getOneUser($where);
		$datas['usuarios']     = $usuarios;




		$where1                = array (
		                        'type.typ_active' => '1'
		                        );
 
		
		$types                = $this->type_model->getAllTypes($where1);
		$datas['types']   = $types;



    
		$Empresa                = $this->catalogos_model->getAllEmpresaSecundaria($where2);
		$datas['empresa']   = $Empresa;


		$Psicologa = $this->catalogos_model->getAllPsicologa();
		$datas['psicologa']   = $Psicologa;


		$Freelance = $this->catalogos_model->getAllFreelance();
		$datas['freelance']   = $Freelance;
      
		$this->parser->parse('configuracion/users_form_edit', $datas);
	
	}
	
	
	public function editUsers()
	{
			
			
			
		$id                       = $this->input->post('id', TRUE);
		$wheres['use_id']         = $id;
		$data['use_login']        = $this->input->post('login', TRUE);
		//$data['use_key']          = $this->encrypt->encode($_POST['keys']);
		$cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$longitudCadena=strlen($cadena);
		$longitudPass=10;
		
		for($i=1 ; $i<=$longitudPass ; $i++){
          $pos=rand(0,$longitudCadena-1);
          $pass .= substr($cadena,$pos,1);
         }


		$data['use_key']          = hash('sha256',$pass);
		$data['use_session']      = $this->input->post('session', TRUE);
		$data['use_active']       = $this->input->post('activo', TRUE);
		
		if($data['use_active'] == 1){
			$asunto = 'Usuario y contraseña OPALO';
			$mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/logo.png" width="178" height="145" /><td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) usuario tus accesos al sistema son los siguientes</td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				  <tr>
				     <td colspan="2" align="center"><a href="'.base_url().'" >'.base_url().'</a></td>
				  </tr>
				  <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  
				  <tr>
				     <td >Usuario:</td>
				     <td align="center">'.$data['use_login'].'</td>
				  </tr>
				     <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td >Password:</td>
				     <td align="center">'.$pass.'</td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				</tablet>
				';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				
				$this->email->initialize($config);
				$email = $this->input->post('email', TRUE);
				
				$this->email->from('no-replay@sistemaopalo.com', 'SISTEMA');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
		}
		
		
		
		$data['use_datecreate']   =  date("Y-m-d H:i:s");
		$data['use_typ_id']       = $this->input->post('tipo_usuario', TRUE);
		$data['use_email']        = $this->input->post('email', TRUE);

		if($data['use_typ_id'] == 7){
		   	  $data['id_empresa']               = $this->input->post('empresa', TRUE);	
			
		 }elseif($data['use_typ_id'] == 8){
		 		 $data['id_psicologa']          = $this->input->post('psicologa', TRUE);	

		 }elseif($data['use_typ_id'] == 9){
		 		 $data['id_freelance']          = $this->input->post('freelance', TRUE);	

		 }else{
		 	
			$data['id_empresa']                 = null;
			$data['id_psicologa']               = null;
			$data['id_freelance']               = null;
		 }
		
	
		
		$usersUpdate              = $this->user_model->updUser($data,$wheres);
		
	    redirect('users');
		  
		
	}


	public function deleteUsers($id = null)
	{
       
       	$whereEliminar['use_id']      = $id;
		$users                        = $this->user_model->dropUser($whereEliminar);
		redirect('users');
	}
	
	
	public function contrasenaV($value='')
	{
		
		$data  = array();
		$this->parser->parse('cambiar_contrasena/cambiar_contasena', $data);
		
	}

	public function cambiaContrasena()
	{
		$keys      = $this->input->post('keys', TRUE);	
		$data['use_key']          = hash('sha256',$keys);
		$where['use_id'] = $_SESSION['SEUS']['use_id'];
	
		$usersUpdate              = $this->user_model->updUser($data,$where);
		//die($this->db->last_query());	
		$this->login_model->destroyLogin(SEUS);
		redirect(base_url() . "login");
		
	}
	
	
}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
