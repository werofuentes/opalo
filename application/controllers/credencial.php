<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * it4pymes.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Credencial extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->model('catalogos_model');
		$this->load->model('documentos_model');
	}
	
		
	public function index()
	{	
	}


	public function generar(){
			$pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'credencial/generar'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorCredencial();

	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//

	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        


	        $join   = array(
			                      
						array(
							'table' => 'EmpresaSecundaria',
							'cond'  => 'EmpresaSecundaria.id = Credencial.idEmprea',
							'type'  => 'inner' 
							),
							array(
							'table' => 'Evaluados',
							'cond'  => 'Evaluados.id = Credencial.idEvaluado',
							'type'  => 'inner' 
							)			   
		                );

	        $select = "Credencial.idCredencial, Credencial.CodigoEmpleado, EmpresaSecundaria.RazonSocial, Evaluados.Nombre ";

	        $where['Evaluados.EstatusEvaluado'] = 1;

			$credencial = $this->catalogos_model->getAllCredencial($where,$select,$join,null,$config['per_page'],$this->uri->segment(3));
			//echo $this->db->last_query();
			//die;

			// var_dump($credencial);

			$data['Credencial'] = $credencial;
			

			$data['NumPaginas'] = $this->pagination->create_links();

			$this->parser->parse('Credencial/GridCredencial', $data);
	}

	public function NuevaCredencial(){

		$orderBy                       = array (
		                                   'EmpresaSecundaria.RazonSocial' => 'ASC'
		                                 );	

			$Empresas         = $this->catalogos_model->getAllEmpresaSecundaria(null,null,null,$orderBy);
			$data['Empresa'] = $Empresas;

			$this->parser->parse('Credencial/credencialNueva', $data);

	}

	function CregarCredencial(){

			$where['id'] = $_POST['idEvaluado'];


			$evaluados         = $this->documentos_model->getOneDocumentoEvaluacion($where);
			

			 $Curp = substr($evaluados['Curp'], 0, 3);
		

			$whereSucursal['idSucursal'] = $evaluados['Sucursal'];
			$sucursal        = $this->catalogos_model->getOneSucursal($whereSucursal);
			
			
			$whereCertificacion['id'] = $evaluados['idCertificado'];
			$Cerificacion        = $this->catalogos_model->getOneCertificaciones($whereCertificacion);

			
			


			 $codigoVendedor = $sucursal['AbreSucursal'].'-'.$Cerificacion['servicioss_abrev'].'-'.$evaluados['id'].$Curp.$evaluados['Edad'].$evaluados['Peso'].$evaluados['Estatura'];

		


			$where2['id'] = $_POST['idEmpresaSec'];
			$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where2);

			
			$codigoEmpleado = $codigoVendedor;
			$this->load->library('ciqrcode');

			$params['data'] = base_url().'credencialqr/registroMovimiento/'.$codigoEmpleado;
			
			//print_r($params['data'] );
			//die;
			$params['level'] = 'H';
			$params['size'] = 10;
			$params['savename'] = FCPATH.'uploads/qr/'.$codigoEmpleado.'.png';
			
			$this->ciqrcode->generate($params);

			$codigoQr = '<img src="'.base_url().'uploads/qr/'.$codigoEmpleado.'.png" style="width:75%"/>';

			//$fecha = date('d-m-Y');
			if($evaluados['EstatusFecha'] != ''){
				$fecha =  $evaluados['EstatusFecha'];
			}else{
				$fecha =  $evaluados['FechaRegistro'];
			}

			$nuevafecha = strtotime ( '+1 year' , strtotime ( $fecha ) ) ;
			$nuevafecha = date ( 'd-m-Y' , $nuevafecha );
			 
			 if($evaluados['Foto'] != ''){
			 	$urlImagen = base_url().$evaluados['Foto'];
			 }else{
			 	$urlImagen = base_url().'inc/images_apro/facebook350.jpg';
			 }


		 $htmlCredencial =  "
		  <div style='width:100%;' align='center'>
		  	<input type='hidden' id='codigoUser' name='codigoUser' value='".$codigoEmpleado."'>
				<input type='hidden' id='FechaExpira' name='FechaExpira' value='".$nuevafecha."'>
			<div style='50%'>
				<img src='".base_url()."/inc/frontal.jpg' alt='Credencial' style='width:100%;z-index:-1' >
				<div style='margin-top:-437px;'>
					 <img src='".$urlImagen."' alt='Credencial' style='width:80px;height:100px;'>
				</div>
				<div style='margin-top:5px;'>

					<font size='4'>".$evaluados['Nombre']."</font>
				</div>
				<div style='margin-top:24px;'>

					<font size='4'>".$EmpresasSec['RazonSocial']."</font>
				</div>

				<div style='margin-top:78px;'>
					<table width='80%'>
					<tr style=' height: 20px;'>
						<td align='left' > ".$codigoVendedor."</td>
						<td align='right' > Vigencia <br> ".$nuevafecha."</td>
					</tr>
					</table>
				</div>
				<div style='margin-top:20px;'>

					<font color='white' size='4'><B>".$evaluados['Puesto']."</font><B>
				</div>
			</div>
		  </div>
		  <br> <br> <br>  <br> <br> <br> <br>  <br>
		   <div style='width:100%;' align='center'>
		   		<div style='50%'>
		   			<img src='".base_url()."/inc/posterior.jpg' alt='Credencial' style='width:100%;z-index:-1' >

		   			<div style='margin-top:-412px;'>
		   			".$codigoQr."
				
					</div>

		   		</div>
		   </div>

		   <br>  <br>  <br>  <br>  <br>  <br>  <br>  <br>  <br> <br>  <br>
			     <button type='button' class='btn btn-primary' onclick='GuardarCredencial();'>
						   <i class='icon-only bigger-120'></i>
							 Guardar
						  </button>
		";
			
			/*
			$htmlCredencial =  "
				<div class='card' align='center'>
				<input type='hidden' id='codigoUser' name='codigoUser' value='".$codigoEmpleado."'>
				<input type='hidden' id='FechaExpira' name='FechaExpira' value='".$nuevafecha."'>
				<table border='1' style=' border: #0431B4 5px solid; max-width:300px;' width='100%'>
						<tr style=' height: 20px;'>
							<td>
							 <div align='center'>
								<img src='".base_url()."/inc/logo.jpg' alt='Credencial' style='width:70%'>
							 </div>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr style=' height: 20px;'>
										 <td align='center' colspan='2'>
										 	 <img src='".base_url().$evaluados['Foto']."' alt='Credencial' style='width:50%'>
										 </td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>
									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											<font size='4'>".$evaluados['Nombre']."</font>
										</td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											".$EmpresasSec['RazonSocial']."
										</td>
									</tr>

									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
									  <td >
									  		".$codigoVendedor."
									  </td>

									   <td align='right'>
									  		".$nuevafecha."
									  </td>
									</tr>

									<tr bgcolor='#0431B4' style=' height: 20px;'>
									  <td colspan='2'>
									  		
									  		<br>
									  		<br>
									  		<br>
									  		<div align='center'>
									  			<font color='white' size='6'><B>".$evaluados['Puesto']."</font><B>
									  		</div>
									  		<br>
									  		<br>
									  		<br>
									  		<br>
									  	
									  </td>

									</tr>

								</table>

							</td>

						</tr>
			     </table>
			     </div>
			     <br>
			     <div class='card'  align='center'>
				<table border='1' style=' border: #0431B4 5px solid; max-width:300px;' width='100%' >

						<tr style=' height: 20px;'>
							<td>
							 <div align='center'>
								<img src='".base_url()."/inc/logo.jpg' alt='Credencial' style='width:70%'>
							 </div>
							</td>
						</tr>
						<tr  style=' height: 20px;'>
							<td>
								<table>
									<tr style=' height: 20px;'>
										 <td align='center' colspan='2'>
										 	
										 </td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>
									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											".$codigoQr."
										</td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
												Esta credencial es intransferible.
No es valida si presenta tachaduras y tiene vigencia solamente por un año. El propietario ha sido certificado por Sistema Opalo Occidente
su información se encuentra en un baco de datos
										</td>
									</tr>

									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
									  <td >
									  	
									  </td>

									   <td align='right'>
									  	
									  </td>
									</tr>

									<tr bgcolor='#0431B4' style=' height: 20px;'>
									  <td colspan='2'>
									  	<div align='center'>
									  		<font color='white' size='3'><B>
									  		Tel: 33 15 94 56 44<br>
									  		contacto@sistemaopalo.com.mx
									  		</font><B>
									  	</div>
									  </td>

									</tr>

								</table>

							</td>

						</tr>
			     </table>
			     </div>
			     <br>
			     <button type='button' class='btn btn-primary' onclick='GuardarCredencial();'>
						   <i class='icon-only bigger-120'></i>
							 Guardar
						  </button>
			 ";*/



			

		 $this->output
	        ->set_content_type('application/html')
	        ->set_output($htmlCredencial);
	}


	function guardarCredencial(){

		

			$data2 = array(
					               'idEmprea' => $_POST['idEmpresaSec'],
					               'idEvaluado' => $_POST['idEvaluado'],
					               'CodigoEmpleado' => $_POST['codigoUser'],
					               'FechaExpira' => $_POST['FechaExpira']
					            );


							$this->db->insert('Credencial', $data2);

							redirect('credencial/generar');
	}


	function verCredencial($id){

		$where['idCredencial'] = $id;
		$credencial = $this->catalogos_model->getOneCredencial($where);

	

		$where2['id'] = $credencial['idEvaluado'];
		$evaluados         = $this->documentos_model->getOneDocumentoEvaluacion($where2);

		$where3['id'] = $credencial['idEmprea'];
		$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where3);

		//print_r($EmpresasSec);

		$data['evaluados'] = $evaluados;
		$data['EmpresasSec'] = $EmpresasSec;


		/*
		 $htmlCredencial =  "
				<div class='card' align='center'>
				<table border='1' style=' border: #0431B4 5px solid; max-width:300px;' width='100%'>
						<tr style=' height: 20px;'>
							<td>
							 <div align='center'>
								<img src='".base_url()."/inc/logo.jpg' alt='Credencial' style='width:70%'>
							 </div>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr style=' height: 20px;'>
										 <td align='center' colspan='2'>
										 	 <img src='".base_url().$evaluados['Foto']."' alt='Credencial' style='width:50%'>
										 </td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>
									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											<font size='4'>".$evaluados['Nombre']."</font>
										</td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											".$EmpresasSec['RazonSocial']."
										</td>
									</tr>

									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
									  <td >
									  		".$credencial['CodigoEmpleado']."
									  </td>

									   <td align='right'>
									  		".$credencial['FechaExpira']."
									  </td>
									</tr>

									<tr bgcolor='#0431B4' style=' height: 20px;'>
									  <td colspan='2'>
									  		
									  		<br>
									  		<br>
									  		<br>
									  		<div align='center'>
									  			<font color='white' size='6'><B>".$evaluados['Puesto']."</font><B>
									  		</div>
									  		<br>
									  		<br>
									  		<br>
									  		<br>
									  	
									  </td>

									</tr>

								</table>

							</td>

						</tr>
			     </table>
			     </div>
			     <br>
			     <div class='card'  align='center'>
				<table border='1' style=' border: #0431B4 5px solid; max-width:300px;' width='100%' >

						<tr style=' height: 20px;'>
							<td>
							 <div align='center'>
								<img src='".base_url()."/inc/logo.jpg' alt='Credencial' style='width:70%'>
							 </div>
							</td>
						</tr>
						<tr  style=' height: 20px;'>
							<td>
								<table>
									<tr style=' height: 20px;'>
										 <td align='center' colspan='2'>
										 	
										 </td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>
									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
												<img src='".base_url()."/uploads/qr/".$credencial['CodigoEmpleado'].".png' alt='Credencial' style='width:70%''>
										</td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
												Esta credencial es intransferible.
No es valida si presenta tachaduras y tiene vigencia solamente por un año. El propietario ha sido certificado por Sistema Opalo Occidente
su información se encuentra en un baco de datos
										</td>
									</tr>

									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
									  <td >
									  	
									  </td>

									   <td align='right'>
									  	
									  </td>
									</tr>

									<tr bgcolor='#0431B4' style=' height: 20px;'>
									  <td colspan='2'>
									  	<div align='center'>
									  		<font color='white' size='3'><B>
									  		Tel: 33 15 94 56 44<br>
									  		contacto@sistemaopalo.com.mx
									  		</font><B>
									  	</div>
									  </td>

									</tr>

								</table>

							</td>

						</tr>
			     </table>
			     </div>
			 ";

			*/

			  if($evaluados['Foto'] != ''){
			 	$urlImagen = base_url().$evaluados['Foto'];
			 }else{
			 	$urlImagen = base_url().'inc/images_apro/facebook350.jpg';
			 }



		 $htmlCredencial =  "
		  <div style='width:100%;' align='center'>
			<div style='50%'>
				<img src='".base_url()."/inc/frontal.jpg' alt='Credencial' style='width:100%;z-index:-1' >
				<div style='margin-top:-437px;'>
					 <img src='".$urlImagen."' alt='Credencial' style='width:80px;height:100px;'>
				</div>
				<div style='margin-top:28px;'>

					<font size='4'>".$evaluados['Nombre']."</font>
				</div>
				<div style='margin-top:24px;'>

					<font size='4'>".$EmpresasSec['RazonSocial']."</font>
				</div>

				<div style='margin-top:50px;'>
					<table width='80%'>
					<tr style=' height: 20px;'>
						<td align='left' > ".$credencial['CodigoEmpleado']."</td>
						<td align='right' > Vigencia <br>".$credencial['FechaExpira']."</td>
					</tr>
					</table>
				</div>
				<div style='margin-top:30px;'>

					<font color='white' size='4'><B>".$evaluados['Puesto']."</font><B>
				</div>
			</div>
		  </div>
		   <br> <br> <br>  <br> <br> <br> <br>  <br><br> <br> <br>  <br>
		   <div style='width:100%;' align='center'>
		   		<div style='50%'>
		   			<img src='".base_url()."/inc/posterior.jpg' alt='Credencial' style='width:100%;z-index:-1' >

		   			<div style='margin-top:-412px;'>
					<img src='".base_url()."/uploads/qr/".$credencial['CodigoEmpleado'].".png' alt='Credencial' style='width:75%''>
					</div>

		   		</div>
		   </div>
		";
	
	
	


		$data['htmlCredencial'] = $htmlCredencial;

		$this->parser->parse('Credencial/credencialEditar', $data);
	}


	function generarImagenCredencial($id){


				require_once('./application/third_party/dompdf/dompdf_config.inc.php');

					$where['idCredencial'] = $id;
		$credencial = $this->catalogos_model->getOneCredencial($where);

		//print_r($credencial);

		$where2['id'] = $credencial['idEvaluado'];
		$evaluados         = $this->documentos_model->getOneDocumentoEvaluacion($where2);

		$where3['id'] = $credencial['idEmprea'];
		$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where3);

		//print_r($EmpresasSec);

		$data['evaluados'] = $evaluados;
		$data['EmpresasSec'] = $EmpresasSec;

		  if($evaluados['Foto'] != ''){
			 	$urlImagen = base_url().$evaluados['Foto'];
			 }else{
			 	$urlImagen = base_url().'inc/images_apro/facebook350.jpg';
			 }

		echo $htmlCredencial =  "
		  <div style='width:100%;' align='center'>
			<div style='50%'>
				<img src='".base_url()."/inc/frontal.jpg' alt='Credencial' style='width:28%;z-index:-1' >
				<div style='margin-top:-500px;'>
					 <img src='".$urlImagen."' alt='Credencial' style='width:100px;height:120px;'>
				</div>
				<div style='margin-top:5px;width:20%'>

					<font size='4'>".$evaluados['Nombre']."</font>
				</div>
				<div style='margin-top:45px;width:30%'>

					<font size='4'>".$EmpresasSec['RazonSocial']."</font>
				</div>

				<div style='margin-top:54px;width:24%'>
					<table width='100%'>
					<tr style=' height: 20px;'>
						<td align='left' > ".$credencial['CodigoEmpleado']."</td>
						<td align='right' > Vigencia <br> ".$credencial['FechaExpira']."</td>
					</tr>
					</table>
				</div>
				<div style='margin-top:30px;width:24%'>

					<font color='white' size='4'><B>".$evaluados['Puesto']."</font><B>
				</div>
			</div>
		  </div>
		  <br> <br> <br>  <br> <br> <br><br> <br> <br>
		   <div style='width:100%;' align='center'>
		   		<div style='50%'>
		   			<img src='".base_url()."/inc/posterior.jpg' alt='Credencial' style='width:28%;z-index:-1' >

		   			<div style='margin-top:-497px;'>
					<img src='".base_url()."/uploads/qr/".$credencial['CodigoEmpleado'].".png' alt='Credencial' style='width:20%''>
					</div>

		   		</div>
		   </div>
		";


		/*
	  $htmlCredencial =  "
				<div class='card' align='center'>
				<table border='1' style=' border: #0431B4 5px solid; max-width:300px;' >
						<tr style=' height: 20px;'>
							<td>
							 <div align='center'>
								<img src='".base_url()."/inc/logo.jpg' alt='Credencial' style='width:70%'>
							 </div>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr style=' height: 20px;'>
										 <td align='center' colspan='2'>
										 	 <img src='".base_url().$evaluados['Foto']."' alt='Credencial' style='width:50%'>
										 </td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>
									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											<font size='4'>".$evaluados['Nombre']."</font>
										</td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
											".$EmpresasSec['RazonSocial']."
										</td>
									</tr>

									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
									  <td >
									  		".$credencial['CodigoEmpleado']."
									  </td>

									   <td align='right'>
									  		".$credencial['FechaExpira']."
									  </td>
									</tr>

									<tr  style='background-color:#0431B4;height: 20px;'>
									  <td colspan='2'>
									  		
									  		<br>
									  		<br>
									  		<br>
									  		<div align='center'>
									  			<font color='white' size='6'><B>".$evaluados['Puesto']."</font><B>
									  		</div>
									  		<br>
									  		<br>
									  		<br>
									  		<br>
									  	
									  </td>

									</tr>

								</table>

							</td>

						</tr>
			     </table>
			     </div>
			     <br>
			     <div class='card'  align='center'>
				<table border='1' style=' border: #0431B4 5px solid; max-width:300px;' width='100%' >

						<tr style=' height: 20px;'>
							<td>
							 <div align='center'>
								<img src='".base_url()."/inc/logo.jpg' alt='Credencial' style='width:70%'>
							 </div>
							</td>
						</tr>
						<tr  style=' height: 20px;'>
							<td>
								<table>
									<tr style=' height: 20px;'>
										 <td align='center' colspan='2'>
										 	
										 </td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>
									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
												<img src='".base_url()."/uploads/qr/".$credencial['CodigoEmpleado'].".png' alt='Credencial' style='width:70%''>
										</td>
									</tr>
									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
										<td align='center' colspan='2'>
												Esta credencial es intransferible.
No es valida si presenta tachaduras y tiene vigencia solamente por un año. El propietario ha sido certificado por Sistema Opalo Occidente
su información se encuentra en un baco de datos
										</td>
									</tr>

									<tr style=' height: 20px;'>
									  <td colspan='2'>
									  		<br>
									  </td>
									</tr>

									<tr style=' height: 20px;'>
									  <td >
									  	
									  </td>

									   <td align='right'>
									  	
									  </td>
									</tr>

									<tr bgcolor='#0431B4' style=' height: 20px;'>
									  <td colspan='2'>
									  	<div align='center'>
									  		<font color='white' size='3'><B>
									  		Tel: 33 15 94 56 44<br>
									  		contacto@sistemaopalo.com.mx
									  		</font><B>
									  	</div>
									  </td>

									</tr>

								</table>

							</td>

						</tr>
			     </table>
			     </div>
			 ";*/
			

		
				 

	}	


	function movimientos(){
			$pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'credencial/movimientos'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorMovimientos();

	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

	        $join   = array(
			                      
						array(
							'table' => 'Evaluados',
							'cond'  => 'Evaluados.id = Movimientos.idEmpleado',
							'type'  => 'inner' 
							)
										   
		                );
	      	
	      	$select = "Evaluados.Nombre, Movimientos.id, Movimientos.Fecha, Movimientos.Empresa, Movimientos.Vehiculo, Movimientos.Placas, Movimientos.Comentarios";
			$movimientos = $this->catalogos_model->getAllMovimientos(null,$select,$join,null,$config['per_page'],$this->uri->segment(3));
	

			$data['movimientos'] = $movimientos;
			

			$data['NumPaginas'] = $this->pagination->create_links();

			$this->parser->parse('Credencial/GridMovimientos', $data);

	} 

	public function registrarMovimientos(){
		$this->parser->parse('Credencial/movimientos', $data);

	}

	public function buscarEmpleado(){


		   $query = "SELECT * FROM Credencial INNER JOIN Evaluados ON Evaluados.id = Credencial.idEvaluado WHERE CodigoEmpleado = '".$_POST['CodigoEmpleado']."'";
		   $credencial = $this->db->query( $query )->result_array(); 

		   //print_r($credencial);

		   $data['Nombre'] = $credencial[0]['Nombre'];
		   $data['idEmpleado'] = $credencial[0]['id'];
		   $data['Hora'] = date('Y-m-d h:m:s');
		

		   echo json_encode($data);
	}

	function GuardarMovimiento(){


					$data2 = array(
					               'idEmpleado' => $_POST['idEmpleado'],
					               'Fecha' 		=> $_POST['fecha'],
					               'Empresa' 	=> $_POST['Empresa'],
					               'Vehiculo'	=> $_POST['vehiculo'],
					               'Placas' 	=> $_POST['placas'],
					               'Comentarios' => $_POST['comentario']

					            );


							$this->db->insert('Movimientos', $data2);

							redirect('credencial/movimientos');

	}


	function verMovimiento($id){


		$select = "Evaluados.Nombre, Movimientos.id, Movimientos.idEmpleado, Movimientos.Fecha, Movimientos.Empresa, Movimientos.Vehiculo, Movimientos.Placas, Movimientos.Comentarios";

		 $join   = array(
			                      
						array(
							'table' => 'Evaluados',
							'cond'  => 'Evaluados.id = Movimientos.idEmpleado',
							'type'  => 'inner' 
							)
										   
		                );

		$Where['Movimientos.id'] = $id;
			$movimientos = $this->catalogos_model->getAllMovimientos($Where,$select,$join,null,$config['per_page'],$this->uri->segment(3));

			$data['movimientos'] = $movimientos[0];

			$this->parser->parse('Credencial/movimientosVer', $data);


	}



	public function deleteCredencial($id){

	

			$where['idCredencial'] = $_POST['eliminarID'];
			$this->catalogos_model->dropCredencial($where);
			redirect('credencial/generar');
	}
	

}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
