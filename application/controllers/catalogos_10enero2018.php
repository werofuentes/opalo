<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * it4pymes.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Catalogos extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->model('catalogos_model');
	}
	
		
	public function index()
	{	
	}


	public function primarias(){

			$data = array();



			if (isset($_POST['palabra_clave']) && $_POST['palabra_clave'] != '') {
				$clave = $_POST['palabra_clave'];
			
			 $where = "Nombre like '%$clave%' OR NombreContacto like '%$clave%'";
						
						 
			}else{
				$where = NULL;
			}

			$orderBy                       = array (
		                                   'EmpresaPrimaria.Nombre' => 'ASC'
		                                 );	
		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/primarias'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorEmpresaPrimaria($where,NULL,NULL,$orderBy);



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			$EmpresasPri         = $this->catalogos_model->getAllEmpresaPrimaria($where,null,null,$orderBy,$config['per_page'],$this->uri->segment(3));



			$data['EmpresasPrimarias'] = $EmpresasPri;
			

			$data['NumPaginas'] = $this->pagination->create_links();




			$this->parser->parse('Catalogos/GridempresaPrimaria', $data);
		

	}

	public function primariasNueva(){

		$data = array();


		$data['Vendedores'] = $this->catalogos_model->getAllVendedores();
		$this->parser->parse('Catalogos/empresaPrimaria', $data);

	}


	public function primariasGuardar(){

		
		
		$data['Nombre']    = $_POST['Nombres'];	
		$data['Domicilio']  = $_POST['Domicilio'];	
		$data['NombreContacto']  = $_POST['NombreContacto'];	
		$data['CorreoElectronico']  = $_POST['correo'];	
		$data['Telefono']  = $_POST['Telefono'];	
		$data['Celular']  = $_POST['Celular'];	
		$data['Medio']  = $_POST['medio'];	
		$data['Vendedor']  = $_POST['vendedor'];	

		
			
		$this->catalogos_model->addEmpresaPrimaria($data);
		
	  	
		redirect('catalogos/primarias');

	}

	public function primariasUpdate(){

		$data['Nombre']    = $_POST['Nombres'];	
		$data['Domicilio']  = $_POST['Domicilio'];	
		$data['NombreContacto']  = $_POST['NombreContacto'];	
		$data['CorreoElectronico']  = $_POST['correo'];	
		$data['Telefono']  = $_POST['Telefono'];	
		$data['Celular']  = $_POST['Celular'];	
		$data['Medio']  = $_POST['medio'];	
		$data['Vendedor']  = $_POST['vendedor'];	


		$where['id'] = $_POST['id'];

		$this->catalogos_model->updEmpresaPrimaria($data, $where);

		redirect('catalogos/primarias');

	}


	public function deletePrimarias(){

		$where['id'] = $_POST['eliminarID'];
		$this->catalogos_model->dropEmpresaPrimaria($where);
		redirect('catalogos/primarias');

	}


	public function primariasEditar($id){

		$data = array();
		$where['id'] = $id;

		$EmpresasPri         = $this->catalogos_model->getOneEmpresaPrimaria($where);
		$data = $EmpresasPri;

		$data['Vendedores'] = $this->catalogos_model->getAllVendedores();


		$this->parser->parse('Catalogos/empresaPrimariaEditar', $data);

	}


	public function secundaria(){


		$data = array();


			if (isset($_POST['palabra_clave']) && $_POST['palabra_clave'] != '') {
			$clave = $_POST['palabra_clave'];
			
			$where = "RazonSocial like '%$clave%' OR Rfc like '%$clave%'";
							
						 
			}else{
				$where = NULL;
			}

			$orderBy                       = array (
		                                   'EmpresaSecundaria.RazonSocial' => 'ASC'
		                                 );	
		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/secundaria'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorEmpresaSecundaria($where,NULL,NULL,$orderBy);



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			$EmpresasPri         = $this->catalogos_model->getAllEmpresaSecundaria($where,null,null,$orderBy,$config['per_page'],$this->uri->segment(3));



			$data['EmpresasSecundaria'] = $EmpresasPri;
			

			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridempresaSecundaria', $data);

		

	}

	public function secundariasNueva(){

		$data = array();
		$EmpresasPri         = $this->catalogos_model->getAllEmpresaPrimaria();
		$data['EmpresasPrimarias'] = $EmpresasPri;

		$data['Vendedores'] = $this->catalogos_model->getAllVendedores();
			
		$this->parser->parse('Catalogos/empresaSecundaria', $data);

	}




	public function filasCertificacion(){
		$NumFila = $_POST['NumFila'];

		if($_POST['NumFila'] == '1' ){
			$ButoonMenos = ' onclick="MenosFilaCertificacion('.$_POST['NumFila'].'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaCertificacion('.$_POST['NumFila'].'); return false ;" ' ;	
			}
			if( ($_POST['NumFila']%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
			

			echo '
	  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$_POST ['NumFila'].'" >
	     
	     	<td  style="text-align: center" >
	     		<img alt="Buscar" title="Buscar" src="'.base_url().'inc/images/lupa.png" style="cursor:pointer" onclick="renglonCertificacion('.$_POST ['NumFila'].'); return false;">
			  <input type="text" class="span11"  id="servicio_'.$_POST ['NumFila'].'"  name="servicio_'.$_POST ['NumFila'].'" value="" />
			   <input type="hidden" class="span11"  id="id_servicio_'.$_POST ['NumFila'].'"  name="id_servicio_'.$_POST ['NumFila'].'" value="" />
	     	</td>

	     	
	     	<td  style="text-align: center" >

			  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$_POST ['NumFila'].');" id="precio_'.$_POST ['NumFila'].'"  name="precio_'.$_POST ['NumFila'].'" value="" />
	     	</td>
	     	
	     	<td style="text-align: center">&nbsp;
            <input style="width : 18px;" type="button" value="-" '.$ButoonMenos.'>&nbsp;&nbsp;
            <input style="width : 18px;" type="button" value="+" onclick="MasFilaCertificacion('.$_POST ['NumFila'].'); return false ;">&nbsp;&nbsp;
      		  </td>
	 
	      </tr> 	
		' ;
	}




	public function secundariasGuardar(){


		
		$data['IdEmpresaPrimaria']    = $_POST['idEmpresaPrimaria'];	
		$data['Nombre']  			  = $_POST['Nombres'];	
		$data['RazonSocial']  		  = $_POST['RazonSocial'];	
		$data['Rfc']  				  = $_POST['Rfc'];	
		$data['CorreoFactuas']        = $_POST['CorreoFactuas'];	
		$data['DomicilioFiscal']      = $_POST['DomicilioFiscal'];	
		$data['DomicilioEntregaCredencial']  = $_POST['DomicilioEntregaCredencial'];	
		$data['NombreContacto']       = $_POST['NombreContacto'];	
		$data['CorreoElectronico']    = $_POST['correo'];	
		$data['Telefono']             = $_POST['Telefono'];	
		$data['Celular']             = $_POST['Celular'];	
		$data['CorreoResultados']    = $_POST['CorreoResultados'];	
		$data['Vendedor']   		 = $_POST['vendedor'];	


		$data['calleFiscal']       = $_POST['calleFiscal'];	
		$data['numroExtFiscal']    = $_POST['numroExtFiscal'];	
		$data['numeroIntFiscal']   = $_POST['numeroIntFiscal'];	
		$data['ColoniaFiscal']     = $_POST['ColoniaFiscal'];	
		$data['municipioFiscal']   = $_POST['municipioFiscal'];	
		$data['estadoFiscal']      = $_POST['estadoFiscal'];	
		$data['codigoPostal']      = $_POST['codigoPostal'];	


		$this->catalogos_model->addEmpresaSecundaria($data);
		$idEmpresaSec = $this->db->insert_id();

		foreach($_POST as $key => $value){
				if(strstr($key , 'id_servicio_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
		}



		for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEmpresaSec']    = $idEmpresaSec;
					$dataDetalle['idExamenCer']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['NombreExamen']    = $_POST['servicio_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];


					$this->db->insert('PreciosClientesCertificacion', $dataDetalle);


		    }
			

		
	  	
		redirect('catalogos/secundaria');

	}

	function mostrarClientesGrid(){

		$where['id'] = $_POST['id'];
		$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where);
		$data['datosSec'] = $EmpresasSec;




		$salida = "
					<table width='100%'>
						<tr>
							<td width='100%' colspan='2'>
							<div class='table-header'> Datos del cliente</div>
							
							</td>
							
						</tr>
						<tr>
							<td width='50%'>
									<B>Cliente:</B>
							
							</td>
							<td width='50%'>
								".$data['datosSec']['RazonSocial']."
							</td>
							
						</tr>
						<tr>
							<td width='100%' colspan='2'>
									<br>
							
							</td>
						</tr>
						<tr>
							<td width='50%'>
									<B>Domicilio:</B>
							
							</td>
							<td width='50%'>
								Calle".$data['datosSec']['calleFiscal']." #".$data['datosSec']['numroExtFiscal'].", colonia".$data['datosSec']['ColoniaFiscal'].', municipio'.$data['datosSec']['municipioFiscal'].", estado ".$data['datosSec']['estadoFiscal']."
							</td>
							
						</tr>
						<tr>
							<td width='50%'>
									<B>Telefono:</B>
							
							</td>
							<td width='50%'>
								".$data['datosSec']['Telefono']."
							</td>
							
						</tr>
					</table>
			";

		$this->output
	        ->set_content_type('application/html')
	        ->set_output($salida);

	}


	public function secundariasEditar($id){

		$data = array();
		$where['id'] = $id;

		$EmpresasPri         = $this->catalogos_model->getAllEmpresaPrimaria();
		$data['EmpresasPrimarias'] = $EmpresasPri;

		$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where);
		$data['datosSec'] = $EmpresasSec;


		$data['Vendedores'] = $this->catalogos_model->getAllVendedores();


		/*
			$query = "SELECT * FROM Evaluados INNER JOIN FacturaEvaluado ON FacturaEvaluado.idEvaluado =  Evaluados.id WHERE FacturaEvaluado.idFactura = '".$id."'";
		    $evaluados = $this->db->query( $query )->result_array(); 

		*/

		 $query = "SELECT * FROM PreciosClientesCertificacion WHERE idEmpresaSec = '".$id."'";
		 $CertificaEvaluados = $this->db->query( $query )->result_array(); 

		


		$totalFilas = count($CertificaEvaluados);
		$data['totalFilas'] = $totalFilas;
			
		$contador = 1;
		foreach ($CertificaEvaluados as $key => $value) {
				
			if($contador == 1 ){
			$ButoonMenos = ' onclick="MenosFilaCertificacion('.$contador.'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaCertificacion('.$contador.'); return false ;" ' ;	
			}


			if( ($contador%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
		
				 $renglones .= '
				  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		<img alt="Buscar" title="Buscar" src="'.base_url().'inc/images/lupa.png" style="cursor:pointer" onclick="renglonCertificacion('.$contador.'); return false;">
						  <input type="text" class="span11"  id="servicio_'.$contador.'"  name="servicio_'.$contador.'" value="'.$value['NombreExamen'].'" />
						   <input type="hidden" class="span11"  id="id_servicio_'.$contador.'"  name="id_servicio_'.$contador.'" value="'.$value['idExamenCer'].'" />
				     	</td>
				     	<td  style="text-align: center" >
						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="precio_'.$contador.'"  name="precio_'.$contador.'" value="'.$value['Precio'].'" />
				     	</td>
				     	<td style="text-align: center">&nbsp;
			            <input style="width : 18px;" type="button" value="-" '.$ButoonMenos.'>&nbsp;&nbsp;
			            <input style="width : 18px;" type="button" value="+" onclick="MasFilaCertificacion('.$contador.'); return false ;">&nbsp;&nbsp;
			      		  </td>
				 
				      </tr> 	
					' ;

					$contador++;

			}

			$data['DetalleCertificaciones'] =  $renglones;

		

		$this->parser->parse('Catalogos/empresaSecundariaEditar', $data);

	}

	public function secundariasUpdate(){

		$data['IdEmpresaPrimaria']    = $_POST['idEmpresaPrimaria'];	
		$data['Nombre']  			  = $_POST['Nombres'];	
		$data['RazonSocial']  		  = $_POST['RazonSocial'];	
		$data['Rfc']  				  = $_POST['Rfc'];	
		$data['CorreoFactuas']        = $_POST['CorreoFactuas'];	
		$data['DomicilioFiscal']      = $_POST['DomicilioFiscal'];	
		$data['DomicilioEntregaCredencial']  = $_POST['DomicilioEntregaCredencial'];	
		$data['NombreContacto']       = $_POST['NombreContacto'];	
		$data['CorreoElectronico']    = $_POST['correo'];	
		$data['Telefono']             = $_POST['Telefono'];	
		$data['Celular']             = $_POST['Celular'];	
		$data['CorreoResultados']    = $_POST['CorreoResultados'];	
		$data['Vendedor']    = $_POST['vendedor'];	

		$data['calleFiscal']       = $_POST['calleFiscal'];	
		$data['numroExtFiscal']    = $_POST['numroExtFiscal'];	
		$data['numeroIntFiscal']   = $_POST['numeroIntFiscal'];	
		$data['ColoniaFiscal']     = $_POST['ColoniaFiscal'];	
		$data['municipioFiscal']   = $_POST['municipioFiscal'];	
		$data['estadoFiscal']      = $_POST['estadoFiscal'];	
		$data['codigoPostal']      = $_POST['codigoPostal'];


		$where['id'] = $_POST['id'];
		$this->catalogos_model->updEmpresaSecundaria($data, $where);


		$idEmpresaSec = $_POST['id'];


		$this->db->where('idEmpresaSec', $idEmpresaSec);
		$this->db->delete('PreciosClientesCertificacion');


		foreach($_POST as $key => $value){
				if(strstr($key , 'id_servicio_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
		}



		for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEmpresaSec']    = $idEmpresaSec;
					$dataDetalle['idExamenCer']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['NombreExamen']    = $_POST['servicio_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];


					$this->db->insert('PreciosClientesCertificacion', $dataDetalle);


		    }




		redirect('catalogos/secundaria');

	}


	public function deleteSecundarias(){

		$where['id'] = $_POST['eliminarID'];
		$this->catalogos_model->dropEmpresaSecundaria($where);
		redirect('catalogos/secundaria');

	}



	public function servicios(){


		$data = array();





		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/servicios'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorServicios();



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			
	        $join   = array(
			                      
						array(
							'table' => 'Certificaciones',
							'cond'  => 'Certificaciones.id = Servicios.idCertificacion',
							'type'  => 'inner' 
							)		   
		                );

	        $select = "Servicios.Servicio, Servicios.id, Certificaciones.Certificaciones";
			$Servicios = $this->catalogos_model->getAllServicios(null,$select,$join,null,$config['per_page'],$this->uri->segment(3));


			$data['Servicios'] = $Servicios;
			//print_r($data['Servicios']);


			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridServicios', $data);



	}


	public function serviciosNueva(){

			$data = array();

			$Certificaciones = $this->catalogos_model->getAllCertificaciones();
			$data['Certificaciones'] = $Certificaciones;
		

			
		$this->parser->parse('Catalogos/servicio', $data);

	}


	public function serviciosGuardar(){

		$data['Servicio']    = $_POST['servicioss'];	
		$data['idCertificacion']    = $_POST['certificacionId'];			
		$this->catalogos_model->addServicios($data);
		
	  	
		redirect('catalogos/servicios');

	}



	public function deleteServicios($id){

		$where['id'] = $id;
		$this->catalogos_model->dropServicios($where);
		redirect('catalogos/servicios');

	}



	public function serviciosEditar($id){

		$data = array();
		$where['id'] = $id;

		$Servicios        = $this->catalogos_model->getOneServicios($where);
		$data['datosServicios'] = $Servicios;

		$Certificaciones = $this->catalogos_model->getAllCertificaciones();
		$data['Certificaciones'] = $Certificaciones;


		$this->parser->parse('Catalogos/servicioEditar', $data);

	}



	public function servicioUpdate(){

		$data['Servicio']    = $_POST['servicioss'];
		$data['idCertificacion']    = $_POST['certificacionId'];

		$where['id'] = $_POST['id'];

		$this->catalogos_model->updServicios($data, $where);

		redirect('catalogos/servicios');

	}

	public function freelance(){


		$data = array();





		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/freelance'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorFreelance();



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			$Freelance = $this->catalogos_model->getAllFreelance(null,null,null,null,$config['per_page'],$this->uri->segment(3));


			$data['Freelance'] = $Freelance;
			

			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridFreelance', $data);


	}

	public function freelanceNuevo(){

		$data = array();

			
		$this->parser->parse('Catalogos/freelance', $data);

	}


	public function freelanceGuardar(){

		$data['Nombre']    = $_POST['name'];
		$data['Costo']    = $_POST['costo'];			
		$this->catalogos_model->addFreelance($data);
		
	  	
		redirect('catalogos/freelance');

	}


	public function freelanceEditar($id){

		$data = array();
		$where['id'] = $id;

		$Freelance        = $this->catalogos_model->getOneFreelance($where);
		$data['datosFreelance'] = $Freelance;


		$this->parser->parse('Catalogos/freelanceEditar', $data);

	}


	public function freelanceUpdate(){

		$data['Nombre']    = $_POST['name'];
		$data['Costo']    = $_POST['costo'];

		$where['id'] = $_POST['id'];

		$this->catalogos_model->updFreelance($data, $where);

		redirect('catalogos/freelance');

	}


	public function deleteFreelance($id){

		$where['id'] = $_POST['eliminarID'];
		$this->catalogos_model->dropFreelance($where);
		redirect('catalogos/freelance');

	}

	//////// PSICOLOFAS //////////
	public function psicologas(){


		$data = array();





		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/psicologas'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorPsicologa();



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			$Psicologa = $this->catalogos_model->getAllPsicologa(null,null,null,null,$config['per_page'],$this->uri->segment(3));


			$data['Psicologa'] = $Psicologa;
			

			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridPsicologa', $data);


	}

	public function psicologasNuevo(){

		$data = array();

			
		$this->parser->parse('Catalogos/psicologo', $data);

	}


	public function psicologasGuardar(){

		$data['NombrePsicologa']    = $_POST['name'];
		$data['CostoPsicologa']    = $_POST['costo'];			
		$this->catalogos_model->addPsicologa($data);
		
	  	
		redirect('catalogos/psicologas');

	}


	public function psicologasEditar($id){

		$data = array();
		$where['id'] = $id;

		$Freelance        = $this->catalogos_model->getOnePsicologa($where);
		$data['datosPsicologa'] = $Freelance;


		$this->parser->parse('Catalogos/psicologaEditar', $data);

	}


	public function psicologasUpdate(){

		$data['NombrePsicologa']    = $_POST['name'];
		$data['CostoPsicologa']    = $_POST['costo'];

		$where['id'] = $_POST['id'];

		$this->catalogos_model->updPsicologa($data, $where);

		redirect('catalogos/psicologas');

	}


	public function deletePsicologas($id){

		$where['id'] = $_POST['eliminarID'];
		$this->catalogos_model->dropPsicologa($where);
		redirect('catalogos/psicologas');

	}
	/////////////





	public	function vendedores(){


			$data = array();



			$orderBy                       = array (
		                                   'Vendedores.Nombre' => 'ASC'
		                                 );	

		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/vendedores'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorVendedores(NULL,NULL,BULL,$orderBy);



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			$Vendedores = $this->catalogos_model->getAllVendedores(null,null,null,$orderBy ,$config['per_page'],$this->uri->segment(3));


			$data['Vendedores'] = $Vendedores;
			

			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridVendedores', $data);


		
	}

	public function vendedorNuevo(){

		$data = array();

			
		$this->parser->parse('Catalogos/vendedor', $data);

	}


	public function vendedorGuardar(){

		$data['Nombre']    = $_POST['name'];
		$data['porcentaje']    = $_POST['porcentaje'];			
		$this->catalogos_model->addVendedores($data);
		
	  	
		redirect('catalogos/vendedores');

	}


	public function vendedorEditar($id){

		$data = array();
		$where['id'] = $id;

		$Vendedores        = $this->catalogos_model->getOneVendedores($where);
		$data['datosVendedor'] = $Vendedores;


		$this->parser->parse('Catalogos/vendedorEditar', $data);

	}


	public function vendedorUpdate(){

		$data['Nombre']    = $_POST['name'];
		$data['porcentaje']    = $_POST['porcentaje'];

		$where['id'] = $_POST['id'];

		$this->catalogos_model->updVendedores($data, $where);

		redirect('catalogos/vendedores');

	}


	public function deleteVendedor($id){

		$where['id'] = $_POST['eliminarID'];
		$this->catalogos_model->dropVendedores($where);
		redirect('catalogos/vendedores');

	}


	/*******CERTIFICACIONES**********/

	public function certificaciones(){


		$data = array();



			$orderBy                       = array (
		                                   'Certificaciones.Certificaciones' => 'ASC'
		                                 );	

		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/certificaciones'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorCertificaciones(NULL,NULL,NULL,$orderBy );



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			$Servicios = $this->catalogos_model->getAllCertificaciones(null,null,null,$orderBy ,$config['per_page'],$this->uri->segment(3));


			$data['Servicios'] = $Servicios;
			

			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridCertificaciones', $data);



	}


	public function certificacionesNueva(){

		$data = array();
		$html = "";


		$html .= '
			<table >
		';

		$blockNone = '';

		for($num = 1; $num <= 30; $num++){

			if($num == 1 || $num == 30){

				$disabled = 'disabled';
				
			}else{
				$disabled = '';

			}

			if($num == 30){

				$disabled2 = 'disabled';
				
			}else{
				$disabled2 = '';

			}

			if($num > 1 ){
				$blockNone = 'display : none;';
			}


			$plus= 'plus';

			$html .= '<tr id="item_detalle_'.$num.'" style="'.$blockNone.'">
						<td>
							<input type="text"   name="nombre_'.$num.'" id="nombre_'.$num.'" size="30"/>
						</td>
						<td>
							<input style="width : 18px;" class="buttom" type="button" readonly="readonly" value="-" '.$disabled.'  onclick="display_item(1,'.($num).');">
						</td>
						<td>
							<input style="width : 18px;" class="buttom" type="button" readonly="readonly" value="+"   onclick="display_item(2,'.($num+1).');"   '.$disabled2.'>
						</td>
					</tr>';



		}

		$html .= '
			</table>
		';

		$data['mostraRenglones'] = $html;
			
		$this->parser->parse('Catalogos/certificaciones', $data);

	}


	public function certificacionesGuardar(){



		$data['Certificaciones']    = $_POST['servicioss'];	
		$data['servicioss_abrev']    = $_POST['servicioss_abrev'];	
		$id = $this->catalogos_model->addCertificaciones($data);
		

		for($num = 0; $num <= 30; $num++){
				if($_POST['nombre_'.$num] != ''){

					$data2['Servicio']    	  = $_POST['nombre_'.$num];	
					$data2['idCertificacion']  = $id;			
					$this->catalogos_model->addServicios($data2);

				}
		}


	  	
		redirect('catalogos/certificaciones');

	}



	public function deletecertificaciones($id){

		$where['id'] = $_POST['eliminarID'];
		$this->catalogos_model->dropCertificaciones($where);
		redirect('catalogos/certificaciones');

	}



	public function certificacionesEditar($id){

		$data = array();
		$where['id'] = $id;

		$Servicios        = $this->catalogos_model->getOneCertificaciones($where);
		$data['datosServicios'] = $Servicios;

		//print_r($data['datosServicios']);
		//die;

		$where2['idCertificacion'] = $id;
		$Examenes = $this->catalogos_model->getAllServicios($where2);


		$html = "";


		$html .= '
			<table >
		';

		$blockNone = '';

		for($num = 1; $num <= count($Examenes); $num++){

			if($num == 1 || $num == 30){

				$disabled = 'disabled';
				
			}else{
				$disabled = '';

			}

			if($num == 30){

				$disabled2 = '';
				
			}else{
				$disabled2 = '';

			}

			if($num > 1 ){
				$blockNone = '';
			}


			$plus= 'plus';

			$html .= '<tr id="item_detalle_'.$num.'" style="'.$blockNone.'">
						<td>
							<input type="text"  value="'.$Examenes[($num-1)]['Servicio'].'"  name="nombre_'.$num.'" id="nombre_'.$num.'" size="30"/>
						</td>
						<td>
							<input style="width : 18px;" class="buttom" type="button" readonly="readonly" value="-" '.$disabled.'  onclick="display_item(1,'.($num).');">
						</td>
						<td>
							<input style="width : 18px;" class="buttom" type="button" readonly="readonly" value="+"   onclick="display_item(2,'.($num+1).');"   '.$disabled2.'>
						</td>
					</tr>';



		}


		$blockNone = '';

		for($num = $num; $num <= 30; $num++){

			if($num == 1 || $num == 30){

				$disabled = 'disabled';
				
			}else{
				$disabled = '';

			}

			if($num == 30){

				$disabled2 = 'disabled';
				
			}else{
				$disabled2 = '';

			}

			if($num > 1 ){
				$blockNone = 'display : none;';
			}


			$plus= 'plus';

			$html .= '<tr id="item_detalle_'.$num.'" style="'.$blockNone.'">
						<td>
							<input type="text"   name="nombre_'.$num.'" id="nombre_'.$num.'" size="30"/>
						</td>
						<td>
							<input style="width : 18px;" class="buttom" type="button" readonly="readonly" value="-" '.$disabled.'  onclick="display_item(1,'.($num).');">
						</td>
						<td>
							<input style="width : 18px;" class="buttom" type="button" readonly="readonly" value="+"   onclick="display_item(2,'.($num+1).');"   '.$disabled2.'>
						</td>
					</tr>';



		}



		$html .= '
			</table>
		';

		$data['mostraRenglones'] = $html;


		$this->parser->parse('Catalogos/certificacionesEditar', $data);

	}



	public function certificacionesUpdate(){

		$data['Certificaciones']    = $_POST['servicioss'];
		$data['servicioss_abrev']    = $_POST['servicioss_abrev'];


		$where['id'] = $_POST['id'];

		$this->catalogos_model->updCertificaciones($data, $where);

		
		$where2['idCertificacion'] = $_POST['id'];
		$this->catalogos_model->dropServicios($where2);


		for($num = 0; $num <= 30; $num++){
				if($_POST['nombre_'.$num] != ''){

					$data2['Servicio']    	  = $_POST['nombre_'.$num];	
					$data2['idCertificacion']  = $_POST['id'];			
					$this->catalogos_model->addServicios($data2);

				}
		}



		redirect('catalogos/certificaciones');

	}


	/***SUCURSALES***/
	public function sucursales(){


		$data = array();





		
	        $pages=10; //Número de registros mostrados por páginas
	        $this->load->library('pagination'); //Cargamos la librería de paginación
	        $config['base_url'] = base_url().'catalogos/servicios'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
	        $config['total_rows'] = $this->catalogos_model->getAllContadorSucursal();



	        $config['per_page'] = $pages; //Número de registros mostrados por páginas
	        $config['num_links'] = 20; //Número de links mostrados en la paginación
	        $config['first_link'] = 'Primera';//primer link
	        $config['last_link'] = 'Última';//último link
	        $config["uri_segment"] = 3;//el segmento de la paginación
	        $config['next_link'] = 'Siguiente';//siguiente link
	        $config['prev_link'] = 'Anterior';//anterior link
	        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
	        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
	        $this->pagination->initialize($config); //inicializamos la paginación        

			


	
			$Sucursales = $this->catalogos_model->getAllSucursal(null,null,null,null,$config['per_page'],$this->uri->segment(3));


			$data['Sucursales'] = $Sucursales;
			//print_r($data['Servicios']);


			$data['NumPaginas'] = $this->pagination->create_links();


			$this->parser->parse('Catalogos/GridSucursales', $data);



	}


	public function sucursalesNueva(){

			$data = array();

		

			
		$this->parser->parse('Catalogos/sucursal', $data);

	}


	public function sucursalesGuardar(){

		$data['Sucursal']    = $_POST['sucursal'];	
		$data['AbreSucursal']    = $_POST['abreviatura'];			
		$this->catalogos_model->addSucursal($data);
		
	  	
		redirect('catalogos/sucursales');

	}



	public function deletesucursales($id){

		$where['idSucursal'] = $_POST['eliminarID'];
		$this->catalogos_model->dropSucursal($where);
		redirect('catalogos/sucursales');

	}



	public function sucursalesEditar($id){

		$data = array();
		$where['idSucursal'] = $id;

		$sucursal        = $this->catalogos_model->getOneSucursal($where);
		$data['datosServicios'] = $sucursal;



		$this->parser->parse('Catalogos/sucursalEditar', $data);

	}



	public function sucursalesUpdate(){

		$data['Sucursal']    = $_POST['sucursal'];	
		$data['AbreSucursal']    = $_POST['abreviatura'];

		$where['idSucursal'] = $_POST['id'];

		$this->catalogos_model->updSucursal($data, $where);

		redirect('catalogos/sucursales');

	}



	

}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
