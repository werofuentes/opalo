<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Elevator extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		//$this->load->helper('procesa_imagen_helper');
		//$this->load->model('photos_model');
		//$this->load->model('photos2_model');
		//$this->load->model('baner_model');
		//$this->load->model('employees_model');
	}
	
	public function index()
	{
		$data = array();

	}
	
	public function cargarClientes(){

		 $this->parser->parse('cargarClientes');
	}
	

	public function guardarCargaClientes(){

		 $this->load->library('excel');
		 $this->load->database();
         $dataInsert = array();

		 $tmp_fichero = $_FILES['excelClientes']['tmp_name'];
         $this->excel = PHPExcel_IOFactory::load($tmp_fichero);

  
		 
         foreach ( $this->excel->getWorksheetIterator() as $worksheet )
         {

         			$worksheetTitle     = $worksheet->getTitle();
                    $highestRow         = $worksheet->getHighestRow();
                    $highestColumn      = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

         	 for ($row = 2; $row <= $highestRow; ++ $row)
                    {
                            $val = array();
        
                            for ($col = 0; $col < $highestColumnIndex; ++ $col)
                            {
                                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                                

               

                                    if($col == 0){

                                        $Nombre =  $cell->getValue();
                                        $dataInsert['Nombre'] = $Nombre;
                                    }


                                    if($col == 1){

                                        $idEmpresa =  $cell->getValue();
                                        $dataInsert['idEmpresa'] = $idEmpresa;
                                    }


                                    if($col == 2){

                                        $idCertificado =  $cell->getValue();
                                        $dataInsert['idCertificado'] = $idCertificado;
                                    }


                                    if($col == 3){

                                        $idPsicologa =  $cell->getValue();
                                        $dataInsert['idPsicologa'] = $idPsicologa;
                                    }


                                    if($col == 4){

                                        $Estatus =  $cell->getValue();
                                        $dataInsert['Estatus'] = $Estatus;
                                    }

                                    if($col == 5){

                                        $Fech =  $cell->getFormattedValue();
                                        $porciones = explode("-", $Fech);
                                        ///print_r($porciones);

                                        $fecha = $porciones[2].'-'.$porciones[0].'-'.$porciones[1];
                                        $dataInsert['FechaRegistro'] = $fecha;
                                    }

                                  
                                  
                                  
                            }

                         
                            	
                            //print_r($dataInsert);
                           	//die;
                           	//break; 	
                            $this->db->insert('Evaluados', $dataInsert); 

                               
                    }

         } 


		 //print_r($_FILES);

		//die;
	}


	public function guardarCargaClientes2(){

		 $this->load->library('excel');
		 $this->load->database();
         $dataInsert = array();

		 $tmp_fichero = $_FILES['excelClientes']['tmp_name'];
         $this->excel = PHPExcel_IOFactory::load($tmp_fichero);

  
		 
         foreach ( $this->excel->getWorksheetIterator() as $worksheet )
         {

         			$worksheetTitle     = $worksheet->getTitle();
                    $highestRow         = $worksheet->getHighestRow();
                    $highestColumn      = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

         	 for ($row = 2; $row <= $highestRow; ++ $row)
                    {
                            $val = array();
        
                            for ($col = 0; $col < $highestColumnIndex; ++ $col)
                            {
                                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                                

               

                                    if($col == 1){

                                        $Nombre =  $cell->getValue();
                                        $dataInsert['Nombre'] = $Nombre;
                                    }


                                    if($col == 2){

                                        $RazonSocial =  $cell->getValue();      
                                        $dataInsert['RazonSocial'] = $RazonSocial;
                                    }


                                    if($col == 0){

                                        $Rfc =  $cell->getValue();      
                                        $dataInsert['Rfc'] = $Rfc;
                                    }


                                  // 	DomicilioFiscal


                                     if($col >= 3 && $col <= 10){

                                        $Domicilio .= $cell->getValue().' ';      
                                        
                                    }

                                    if($col == 11){

                                        $CorreoFactuas =  $cell->getValue();      
                                        $dataInsert['CorreoFactuas'] = $CorreoFactuas;
                                        $dataInsert['CorreoResultados'] = $CorreoFactuas;
                                        $dataInsert['CorreoElectronico'] = $CorreoFactuas;
                                    }


                                    if($col == 12){

                                        $NombreContacto =  $cell->getValue();      
                                        $dataInsert['NombreContacto'] = $NombreContacto;
                                    }


                                  
                            }

                            $dataInsert['DomicilioFiscal'] = $Domicilio;
                            $dataInsert['DomicilioEntregaCredencial'] = $Domicilio;
                            	
                            //print_r($dataInsert);
                           	//break; 	
                            $this->db->insert('EmpresaSecundaria', $dataInsert); 

                               
                    }

         } 


		 //print_r($_FILES);

		//die;
	}

	public function guardarCargaClientes3(){

		 $this->load->library('excel');
		 $this->load->database();
         $dataInsert = array();

		 $tmp_fichero = $_FILES['excelClientes']['tmp_name'];
         $this->excel = PHPExcel_IOFactory::load($tmp_fichero);

  
		 
         foreach ( $this->excel->getWorksheetIterator() as $worksheet )
         {

         			$worksheetTitle     = $worksheet->getTitle();
                    $highestRow         = $worksheet->getHighestRow();
                    $highestColumn      = $worksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

         	 for ($row = 2; $row <= $highestRow; ++ $row)
                    {
                            $val = array();
        					$Domicilio = '';
                            for ($col = 0; $col < $highestColumnIndex; ++ $col)
                            {
                                    $cell = $worksheet->getCellByColumnAndRow($col, $row);
                                

               


                                     if($col == 0){

                                        $Rfc =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }

                           

                                    if($col == 3){

                                        $calle =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }

                                    if($col == 4){

                                        $NumExterior =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }

                                     if($col == 5){

                                        $NumInterior =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }


                                    if($col == 6){

                                        $Colonia =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }


                                    if($col == 7){

                                        $Municipio =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }


                                    if($col == 8){

                                        $Estado =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }

                                    if($col == 10){

                                        $CP =  $cell->getValue();      
                                        //$dataInsert['Rfc'] = $Rfc;
                                    }
                                   
                                    /*
                                     if($col >= 3 && $col <= 10){

                                        $Domicilio .= $cell->getValue().' ';      
                                        
                                    }*/

                                 


                                  
                            }

                          	    $dataInsert['calleFiscal']       = $calle;	
								$dataInsert['numroExtFiscal']    = $NumExterior;	
								$dataInsert['numeroIntFiscal']   = $NumInterior;	
								$dataInsert['ColoniaFiscal']     = $Colonia;	
								$dataInsert['municipioFiscal']   = $Municipio;	
								$dataInsert['estadoFiscal']      = $Estado ;	
								$dataInsert['codigoPostal']      =  $CP;	
                 
                            
                            echo "<pre>";	
                            print_r($dataInsert);
                           	//break; 	
                            //$this->db->insert('EmpresaSecundaria', $dataInsert); 

                            $this->db->where('Rfc', $Rfc);
							$this->db->update('EmpresaSecundaria', $dataInsert);

                               
                    }

         } 


		 //print_r($_FILES);

		//die;
	}

	public function sube_foto_empleado($nombre,$carpeta,$ancho,$alto)
	{

			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);
				$foto = $fotox['upload_data'];

				
				$foto['image_width'];
				$foto['image_height'];
				$foto['file_ext'];
	
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);


				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;

			 
			 
			 }
			 else
			 {
				echo "error";
			}
	}
	
	
	public function borra_archivo_empleado()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				echo "ok";
			}
		}
	}


	public function borra_archivo_empleado2()
	{
		

		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
				
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['idEmpleado'] = $id;
				$datos['Fotografia'] = '';
				$photos = $this->employees_model->updEmpleados($datos,$where);
				echo "ok";
			}
		}
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	
	
	// upload image
	public function sube_fotos($nombre,$carpeta,$ancho,$alto)
	{
		//if((isset($_POST['cuenta'])) &&  (isset($_POST['limite']))) // Si viene POST de cuenta y limite, quiere decir que es un numero limitado de archivos a subir
		//{
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);
				//print_r($fotox);
				$foto = $fotox['upload_data'];
				chmod($foto['file_path'].$foto['file_name'],0777);
				
				
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				
				
				base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';

		
             echo $item;
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}


	public function sube_fotos_article($nombre,$carpeta,$ancho,$alto)
	{
		
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
			
				
				if($foto['image_width'] <= 640 && $foto['image_height'] <= 434){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				}else{
					
					if($foto['image_width'] >= 640 && $foto['image_height'] >= 434){
						
						echo 'EsMayor ';
						
					}
					
                  
               }
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}


	public function sube_fotos_mapa($nombre,$carpeta,$ancho,$alto)
	{
		
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
			
				
				// if($foto['image_width'] <= 640 && $foto['image_height'] <= 434){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 <input type="hidden" name="fotos_mapa[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo_mapa[]" value="0" id="id_photo_mapa[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto_mapa(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				// }else{
// 					
					// if($foto['image_width'] >= 640 && $foto['image_height'] >= 434){
// 						
						// echo 'EsMayor ';
// 						
					// }
// 					
//                   
               // }
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}



	
	public function sube_fotos_categoria($nombre,$carpeta,$ancho,$alto)
	{
		//if((isset($_POST['cuenta'])) &&  (isset($_POST['limite']))) // Si viene POST de cuenta y limite, quiere decir que es un numero limitado de archivos a subir
		//{
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
				$foto['file_ext'];
				
				
				
				//if($foto['image_width'] <= 82 && $foto['image_height'] <= 82 && $foto['file_ext'] == '.png'){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				
				//}else{
					
					// if($foto['file_ext'] != '.png'){
						// echo 'NoPng ';	
					// }
					// if($foto['image_width'] >= 82 && $foto['image_height'] >= 82){
// 						
						// echo 'EsMayor ';
// 						
					// }
// 					
//                   
               // }
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}


public function sube_fotos_categoria_es($nombre,$carpeta,$ancho,$alto)
	{
		//if((isset($_POST['cuenta'])) &&  (isset($_POST['limite']))) // Si viene POST de cuenta y limite, quiere decir que es un numero limitado de archivos a subir
		//{
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
				$foto['file_ext'];
				
				
				
				//if($foto['image_width'] <= 82 && $foto['image_height'] <= 82 && $foto['file_ext'] == '.png'){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				//}else{
					
					//if($foto['file_ext'] != '.png'){
						//echo 'NoPng ';	
					//}
					//if($foto['image_width'] >= 82 && $foto['image_height'] >= 82){
						
						//echo 'EsMayor ';
						
					//}
					
                  
               //}
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}


     public function sube_fotos_guias($nombre,$carpeta,$ancho,$alto)
	{
		//if((isset($_POST['cuenta'])) &&  (isset($_POST['limite']))) // Si viene POST de cuenta y limite, quiere decir que es un numero limitado de archivos a subir
		//{
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
			
				
				if($foto['image_width'] <= 640 && $foto['image_height'] <= 434){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				}else{
					
					if($foto['image_width'] >= 640 && $foto['image_height'] >= 434){
						
						echo 'EsMayor ';
						
					}
					
                  
               }
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}


 public function sube_fotos_baner($nombre,$carpeta,$ancho,$alto)
	{
		//if((isset($_POST['cuenta'])) &&  (isset($_POST['limite']))) // Si viene POST de cuenta y limite, quiere decir que es un numero limitado de archivos a subir
		//{
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
			
				
				if($foto['image_width'] <= 640 && $foto['image_height'] <= 100){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
								 LINK: <input type="text" name="link[]" value="" id="link[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				}else{
					
					if($foto['image_width'] >= 640 && $foto['image_height'] >= 100 || $foto['image_width'] >= 640 || $foto['image_height'] >= 100){
						
						echo 'EsMayor ';
						
					}
               }
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}

public function sube_fotos_objetos($nombre,$carpeta,$ancho,$alto)
	{
		//if((isset($_POST['cuenta'])) &&  (isset($_POST['limite']))) // Si viene POST de cuenta y limite, quiere decir que es un numero limitado de archivos a subir
		//{
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);

				$foto = $fotox['upload_data'];
				//print_r($foto);
				
				$foto['image_width'];
				$foto['image_height'];
			
				
				if($foto['image_width'] <= 640 && $foto['image_height'] <= 434){
					
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item;
					
				}else{
					
					if($foto['image_width'] >= 640 && $foto['image_height'] >= 434){
						
						echo 'EsMayor ';
						
					}
					
                  
               }
			 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}


public function sube_fotos_galeriaGeneral($nombre,$carpeta,$ancho,$alto)
	{
		
			if($_POST['cuenta']<$_POST['limite']) // verifica que la cuenta sea menor al limite
			 {
				
				$fotox = upfiles($nombre, $carpeta);
				$foto = $fotox['upload_data'];
				//print_r($foto);
				$foto['image_width'];
				$foto['image_height'];
					
				chmod($foto['file_path'].$foto['file_name'],0777);
				resize_image($foto['file_path'].$foto['file_name'], $ancho, $alto);
				//base_image($foto['file_path'].$foto['file_name'],'./inc/images/fondoblanco.jpg');

				 $item =	'<li id="li_'.$foto['raw_name'].'">
							 <div class="drag">
								 <img src="'.base_url().'uploads/img/img_mient/'.$foto['file_name'].'" width="140px" /><br>
								 Descripcion<br>
								 <input type="text" name="descripcion[]" value="" id="descripcion[]" />
								 <input type="hidden" name="fotos[]" value="'.$foto['file_name'].'" id="hide_'.$foto['raw_name'].'" />
								 <input type="hidden" name="id_photo[]" value="0" id="id_photo[]" />
							 </div>
							 <span class="delicon" onclick="delete_foto(\''.$foto['raw_name'].'\')">
								 <img src="'.base_url().'inc/images/boton_borrar_small.png"/>
							 </span>
						 </li>';
               echo $item; 
			 
			 
			 }
			 else
			 {
				echo "error";
			}
		 //}
	}



	public function borra_archivo()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				echo "ok";
			}
		}
	}



    public function subcategorias(){
    	
		$this->load->model('subcategories_model');
		
 	
				
	        $where['subcat_cat_id']                    =$_POST['categoria'];
	        $subcategories                            = $this->subcategories_model->getAllSubcategories($where);
			echo '<option value="">Elige</option>';
			for( $x = 0; $x < count($subcategories); $x++){
            echo '<option value="'.$subcategories[$x]['subcat_id'].'">'.$subcategories[$x]['subcat_name'].'</option>';
              	
			}

    }
	
	
	
	public function statesMunicipios(){
    	
		    $this->load->model('municipalities_model');

	        $where['mun_sta_id']                      =$_POST['stados'];
	        $municipios                               = $this->municipalities_model->getAllMunicipalities($where);
			echo '<option value="">Elige</option>';
			for( $x = 0; $x < count($municipios); $x++){
            echo '<option value="'.$municipios[$x]['mun_id'].'">'.$municipios[$x]['mun_name'].'</option>';
              	
			}
    }
	
	
	public function tipoMostrar(){
		$this->load->model('tipo_candidato_model');
		$where['TipoCandidato.idTipoCandidato']                      =$_POST['tipo_usuario'];
		
		$where['CuestionarioTipo.idCuestionarioTipo !=']                      ='1';
		
		$joinEs                          = array(
			                             array(
											  'table' => 'CuestionarioTipo_has_TipoCandidato',
											  'cond'  => 'CuestionarioTipo_has_TipoCandidato.idTipoCandidato = TipoCandidato.idTipoCandidato',
											  'type'  => 'inner' 
											  ),
										 array(
											  'table' => 'CuestionarioTipo',
											  'cond'  => 'CuestionarioTipo.idCuestionarioTipo = CuestionarioTipo_has_TipoCandidato.idCuestionarioTipo',
											  'type'  => 'inner' 
											  )
		                                 );
		
		$inSelect = 'CuestionarioTipo.idCuestionarioTipo as idCuestionarioTipo, CuestionarioTipo.Descripcion';
		
	    $candidato                               = $this->tipo_candidato_model->getAllTipoCandidato($where, $inSelect, $joinEs);
	    //die($this->db->last_query());	
		   echo '<option value="">Elige</option>';
			for( $x = 0; $x < count($candidato); $x++){
            echo '<option value="'.$candidato[$x]['idCuestionarioTipo'].'">'.$candidato[$x]['Descripcion'].'</option>';
              	
			}
		
	}
	
	public function tipoMunicipio()
	{
		$this->load->model('municipalities_model');
		$where['idEstado'] = $_POST['idEstado'];
		
		$munucipio = $this->municipalities_model->getAllMunicipalities($where);
		
		 echo '<option value="">Elige</option>';
		 for( $x = 0; $x < count($munucipio); $x++){
            echo '<option value="'.$munucipio[$x]['idMunicipio'].'">'.$munucipio[$x]['Nombre'].'</option>';
              	
		}
		
		
	}


	public function evaluadoMostrar(){
		$this->load->model('documentos_model');

		//$where['idEmpresa'] = $_POST['idEvaluado'];
		//$where['idEmpresa'] = 1;
		
		$where = "idEmpresa = '".$_POST['idEvaluado']."' AND EstatusEvaluado = 1 AND id NOT IN (select idEvaluado FROM Credencial)";

		$evaluados = $this->documentos_model->getAllDocumentoEvaluacion($where);
		//die($this->db->last_query());	


		 echo '<option value="">Elige</option>';

		 for( $x = 0; $x < count($evaluados); $x++){
            echo '<option value="'.$evaluados[$x]['id'].'">'.$evaluados[$x]['Nombre'].'</option>';
              	
		}


	}

	public function borra_archivo_guia()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['pho_id'] = $id;
				$photos = $this->photos_model->dropPhotos($where);
				echo "ok";
			}
		}
	}


	public function borra_archivo_baner()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				//echo "entro";
				unlink($archivo);
				$id = $_POST['id'];
				$where['id'] = $id;
				$photos = $this->baner_model->dropIMagenBaner($where);
				echo "ok";
			}
		}
	}
	
	
	public function borra_archivo_generalgaleriapho()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				//echo "entro";
				unlink($archivo);
				$id = $_POST['id'];
				$where['id'] = $id;
				$photos = $this->galeria_general_model->dropGaleriaGeneralImagenes($where);
				echo "ok";
			}
		}
	}


public function borra_archivo_objeto()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['pho_id'] = $id;
				$photos = $this->photos_model->dropPhotos($where);
				echo "ok";
			}
		}
	}


	public function borra_archivo_subcat()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['pho_id'] = $id;
				$photos = $this->photos_model->dropPhotos($where);
				echo "ok";
			}
		}
	}
	
	
	public function borra_archivo_cat()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['pho_id'] = $id;
				$photos = $this->photos_model->dropPhotos($where);
				echo "ok";
			}
		}
	}
	
	
	public function borra_archivo_articulo()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['pho_id'] = $id;
				$photos = $this->photos2_model->dropPhotos ($where);
				echo "ok";
			}else{
				
				$id = $_POST['id'];
				$where['pho_id'] = $id;
				$photos = $this->photos2_model->dropPhotos ($where);
				echo "ok";
				
				
			}
		}
	}
	
	
	public function borra_archivo_articulo_mapa()
	{
		if( (isset($_POST['archivo'])) && (!empty($_POST['archivo'])) && (''!=$_POST['archivo']) )
		{
			$archivo = $_POST['archivo'];
			
			if(file_exists($archivo))
			{
				unlink($archivo);
				$id = $_POST['id'];
				$where['id'] = $id;
				$photos = $this->photos2_model->dropPhotosMapas ($where);
				echo "ok";
			}else{
				
				$id = $_POST['id'];
				$where['id'] = $id;
				$photos = $this->photos2_model->dropPhotosMapas ($where);
				echo "ok";
				
				
			}
		}
	}


	public function tube_imagenes()
	{
		$url = $_POST['base'];
		$tube = parse_url($url);
		if ($tube["path"] == "/watch") {
			parse_str($tube["query"], $query);
			$id = $query["v"];
		} else {
			$id = "error";   
		}
		// echo '<img src="http://img.youtube.com/vi/'.$id.'/1.jpg"> <br>';
		echo $id;
	}

   public function encontrarEdad()
   {
   	
	     $fecha = $_POST['fecha'];
         list($Y,$m,$d) = explode("-",$fecha);
         $edad = ( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
		 
		 echo $edad;
   }
	
	
   public function validarExisteCorreo()
   {
   	   $this->load->model('user_model');
	   $email = $this->input->post('correo', TRUE);
		
	   $where['use_email']      = $email;
	   $usuarios             = $this->user_model->getOneUser($where);	
		//die($this->db->last_query());	

	   if(!empty($usuarios))
	   {
	   	 echo "Existe";
	   }else{
	   	  echo "No Existe";
	   }	
	   
   }

   public function tipoPersonal(){

   				 $this->load->model('documentos_model');
   				 $idEmpresa = $this->input->post('idEmpresa', TRUE);
		
	   			 $where['idEmpresa']    = $idEmpresa;
	  			 $usuarios              = $this->documentos_model->getAllDocumentoEvaluacion($where);


	  	echo '<option value="Todos">Todos</option>';

		 for( $x = 0; $x < count($usuarios); $x++){
            echo '<option value="'.$usuarios[$x]['id'].'">'.$usuarios[$x]['Nombre'].'</option>';
              	
		}	

   }


   public function cancelarFacturas(){


   			$wsFact = array(			
							'uuid' => "5C307881-2B01-4F58-8649-05761F7F34B4",
							'companyID' => 2,
							'rfc' => 'SOB151104AS6'
							);


   			require("./application/libraries/lib/nusoap.php");
			$client = new nusoap_client('http://162.221.187.226/stamping/stamping.asmx?wsdl','wsdl');
			$result = $client->call('cancelCFDI',$wsFact);
			echo "<pre>";
			print_r($result);
			echo "</pre>";
			die;
    }


}
