<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * it4pymes.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

//// ESTATUS EVALUADO GENERARL 1 ACEPTADO 2 RECHAZADO
//// ESTATUS SERVICIOS 1 APROBADO 2 REPROBADO 3 NO APLICA 

class Documentos extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->model('documentos_model');
		$this->load->model('catalogos_model');
		$this->load->model('estados_model');
		$this->load->model('municipalities_model');
		setlocale(LC_NUMERIC, 'C');
		

	}
	
		
	public function index()
	{	
	}


	public function ExcelPendientes(){
		$this->load->library('Excel');
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("FACTURA")
							 ->setLastModifiedBy("FACTURA")
							 ->setTitle("Factura")
							 ->setSubject("Factura")
							 ->setDescription("Listado de Facturas")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
		// Add some data
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LISTADO FACTURAS/RECIBO PENDIENTES DE COBRO')
				->setCellValue('A2', 'Folio')
				->setCellValue('B2', 'Fecha')
				->setCellValue('C2', 'Cliente')
				->setCellValue('D2', 'Total')
				->setCellValue('E2', 'Por pagar')
				->setCellValue('F2', 'Documento');


		$orderBy                       = array (
		                                   'DocumentoEncabezado.id' => 'DESC'
		                                 );

		$where = "Estatus ='1' OR Estatus ='2'";
		$select =  "DocumentoEncabezado.id, DocumentoEncabezado.NombreCliente, DocumentoEncabezado.Rfc, DocumentoEncabezado.FolioFactura, DocumentoEncabezado.Fecha, DocumentoEncabezado.Total, DocumentoEncabezado.PorPagar, DocumentoEncabezado.Estatus, DocumentoEncabezado.Cancelada";

		$documentos = $this->documentos_model->getAllDocumentoEncabezado($where, $select,null,$orderBy);


		$y = 3;
		for($x=0; $x < count($documentos); $x++){

			if($documentos[$x]['PorPagar'] >= 1){

				$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
				$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$y, $documentos[$x]['id'])
				->setCellValue('B'.$y, $documentos[$x]['Fecha'])
				->setCellValue('C'.$y, $documentos[$x]['NombreCliente'])
				->setCellValue('D'.$y, $documentos[$x]['Total'])
				->setCellValue('E'.$y, $documentos[$x]['PorPagar'])
				->setCellValue('F'.$y, 'Factura');
				
				
				$y++;
			}

		}	



		$orderBy2                       = array (
		                                   'DocumentoEncabezadoRecibo.id' => 'DESC'
		                                 );

		$where2 = "Estatus ='1' OR Estatus ='2'";
		$select2 =  "DocumentoEncabezadoRecibo.id, DocumentoEncabezadoRecibo.NombreCliente, DocumentoEncabezadoRecibo.Rfc, DocumentoEncabezadoRecibo.FolioFactura, DocumentoEncabezadoRecibo.Fecha, DocumentoEncabezadoRecibo.Total, DocumentoEncabezadoRecibo.PorPagar, DocumentoEncabezadoRecibo.Estatus, DocumentoEncabezadoRecibo.Cancelada";

		$documentos2 = $this->documentos_model->getAllDocumentoEncabezadoRecibo($where2, $select2,null,$orderBy2);



		for($x=0; $x < count($documentos2); $x++){

			if($documentos2[$x]['PorPagar'] >= 1){

				$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
				$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$y, $documentos2[$x]['id'])
				->setCellValue('B'.$y, $documentos2[$x]['Fecha'])
				->setCellValue('C'.$y, $documentos2[$x]['NombreCliente'])
				->setCellValue('D'.$y, $documentos2[$x]['Total'])
				->setCellValue('E'.$y, $documentos2[$x]['PorPagar'])
				->setCellValue('F'.$y, 'Recibo');
				
				
				$y++;
			}

		}

		// Rename worksheet
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Listado pendiente de cobro');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="listado_pendientes_cobro.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		

		
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');

	}

	public function ExcelFactura(){




		$this->load->library('Excel');
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("FACTURA")
							 ->setLastModifiedBy("FACTURA")
							 ->setTitle("Factura")
							 ->setSubject("Factura")
							 ->setDescription("Listado de Facturas")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

		// Add some data
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LISTADO FACTURAS')
				->setCellValue('A2', 'Folio')
				->setCellValue('B2', 'Fecha')
				->setCellValue('C2', 'Cliente')
				->setCellValue('D2', 'Total')
				->setCellValue('E2', 'Por pagar');


		$orderBy                       = array (
		                                   'DocumentoEncabezado.id' => 'DESC'
		                                 );

		$where = "Estatus ='1' OR Estatus ='2'";
		$select =  "DocumentoEncabezado.id, DocumentoEncabezado.NombreCliente, DocumentoEncabezado.Rfc, DocumentoEncabezado.FolioFactura, DocumentoEncabezado.Fecha, DocumentoEncabezado.Total, DocumentoEncabezado.PorPagar, DocumentoEncabezado.Estatus, DocumentoEncabezado.Cancelada";

		$documentos = $this->documentos_model->getAllDocumentoEncabezado($where, $select,null,$orderBy);



		$y = 3;
		for($x=0; $x < count($documentos); $x++){

			if($documentos[$x]['Cancelada'] != '' && $documentos[$x]['Cancelada'] != NULL){

				$EstatusDocumentoC = "Cancelada";
			}else{
					$EstatusDocumentoC = "Activa";
			}

			$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$y, $documentos[$x]['id'])
			->setCellValue('B'.$y, $documentos[$x]['Fecha'])
			->setCellValue('C'.$y, $documentos[$x]['NombreCliente'])
			->setCellValue('D'.$y, $documentos[$x]['Total'])
			->setCellValue('E'.$y, $documentos[$x]['PorPagar'])
			->setCellValue('F'.$y, $EstatusDocumentoC);
			
			
			$y++;

		}	
		

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Listado Facturas');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="listado_facturas_recibo.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		

		
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');


	}


	public function ExcelRecibos(){




		$this->load->library('Excel');
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("RECIBOS")
							 ->setLastModifiedBy("RECIBOS")
							 ->setTitle("Recibos")
							 ->setSubject("Recibos")
							 ->setDescription("Listado de Recibos")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

		// Add some data
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->getFont()->setBold(true);
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LISTADO RECIBOS')
				->setCellValue('A2', 'Folio')
				->setCellValue('B2', 'Fecha')
				->setCellValue('C2', 'Cliente')
				->setCellValue('D2', 'Total')
				->setCellValue('E2', 'Por pagar')
				->setCellValue('F2', 'Estatus');


		$orderBy                       = array (
		                                   'DocumentoEncabezadoRecibo.id' => 'DESC'
		                                 );

		$where = "Estatus ='1' OR Estatus ='3'";
		$select =  "DocumentoEncabezadoRecibo.id, DocumentoEncabezadoRecibo.NombreCliente, DocumentoEncabezadoRecibo.Rfc, DocumentoEncabezadoRecibo.FolioFactura, DocumentoEncabezadoRecibo.Fecha, DocumentoEncabezadoRecibo.Total, DocumentoEncabezadoRecibo.PorPagar, DocumentoEncabezadoRecibo.Estatus, DocumentoEncabezadoRecibo.Cancelada";

		$documentos = $this->documentos_model->getAllDocumentoEncabezadoRecibo($where, $select,null,$orderBy);



		$y = 3;
		for($x=0; $x < count($documentos); $x++){

		if($documentos[$x]['Cancelada'] != '' && $documentos[$x]['Cancelada'] != NULL){

				$EstatusDocumentoC = "Cancelada";
		}else{
				$EstatusDocumentoC = "Activa";
		}

			$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$y, $documentos[$x]['id'])
			->setCellValue('B'.$y, $documentos[$x]['Fecha'])
			->setCellValue('C'.$y, $documentos[$x]['NombreCliente'])
			->setCellValue('D'.$y, $documentos[$x]['Total'])
			->setCellValue('E'.$y, $documentos[$x]['PorPagar'])
			->setCellValue('F'.$y, $EstatusDocumentoC);
			
			
			$y++;

		}	
		

		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Listado Recibos');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="listado_recibo.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		

		
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');


	}


	public function cotizacion(){
		

		///COTIZACION 0, EVALUACION 1, FACTURA 2 , RECIBO 3, CANCELADO 4.   FACTURADA/REMISION 0 / 1

		if($_SESSION['SEUS']['use_typ_id'] > 2){
			
			$where = "Estatus ='0' AND idUsuarioRegistro = '".$_SESSION['SEUS']['use_id']."' ";
		}else{
			$where = "Estatus ='0'";
		}
		
		$select =  "DocumentoEncabezado_cot.id, DocumentoEncabezado_cot.NombreCliente, DocumentoEncabezado_cot.idCliente";
		$data['Documentos']        = $this->documentos_model->getAllDocumentoEncabezado_cot($where, $select);

		//print_r($data['Documentos']);
		//die;

		$this->parser->parse('Documentos/GridDocumentos', $data);
	}

	/*
	public function cotizacion(){
		

		///COTIZACION 0, EVALUACION 1, FACTURA 2 , RECIBO 3, CANCELADO 4.   FACTURADA/REMISION 0 / 1

		if($_SESSION['SEUS']['use_typ_id'] > 1){
			
			$where = "Estatus ='0' AND idUsuarioRegistro = '".$_SESSION['SEUS']['use_id']."' ";
		}else{
			$where = "Estatus ='0'";
		}
		
		$select =  "DocumentoEncabezado.id, DocumentoEncabezado.NombreCliente";
		$data['Documentos']        = $this->documentos_model->getAllDocumentoEncabezado($where, $select);


		$this->parser->parse('Documentos/GridDocumentos', $data);
	}

	*/



	public function cotizacionNueva(){


		$data['fecha'] = date('Y-m-d');
		$this->parser->parse('Documentos/cotizacionNueva', $data);
	}

	public function buscarClientes(){

		$id = $_POST['id'];
		$where['id'] = $id;
		$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where);
		$data['datosSec'] = $EmpresasSec;

		echo json_encode($data['datosSec']);




	}


	public function buscarServicios(){


	


		$id = $_POST['id'];
		$where['id'] = $id;
		$Certificacion        = $this->catalogos_model->getOneCertificaciones($where);
		$data['Certificacion'] = $Certificacion;

		if($_POST['idEmpresa'] != ''){

				$query = "SELECT * FROM PreciosClientesCertificacion WHERE idEmpresaSec = '".$_POST['idEmpresa']."' AND idExamenCer = '".$_POST['id']."'";
				$CertificaEvaluados = $this->db->query( $query )->result_array(); 
				$data['Certificacion']['Precio'] = $CertificaEvaluados[0]['Precio'];
				$data['Certificacion']['Cantidad'] = 1;
		}else{
				$data['Certificacion']['Precio'] = '';
				$data['Certificacion']['Cantidad'] = '';
		}


		echo json_encode($data['Certificacion']);




	}

	public function mostrarClientes(){

			$orderBy                       = array (
		                                   'EmpresaSecundaria.RazonSocial' => 'ASC'
		                                 );	
			$Empresas         = $this->catalogos_model->getAllEmpresaSecundaria(null,null,null,$orderBy);

			//echo "<pre>";
			//print_r($Empresas);
			//echo "</pre>";

			foreach ($Empresas as $key => $value) {
				$tr .= "
					<tr>
						<td style='cursor:pointer' class='Cliente' onclick='cargarClientes(".$value['id'].")' >
						".$value['RazonSocial']."
						</td>
						<td style='cursor:pointer'  onclick='cargarClientes(".$value['id'].")'>
						".$value['Rfc']."
						</td>
					</tr>
					";
			}

			


			$salida = "
					<script>
					$(document).ready(function() {
					$('#searchClientes').domsearch('#sample-table-2 tbody',{criteria: ['td.Cliente']})
					}) 
					</script>
					<table width='100%' id='sample-table-2' class='table table-striped table-bordered table-hover'>
						<thead>
						<tr>
							<td width='70%'>
							<div class='table-header'>  Clientes</div>
							
							</td>
							<td width='30%'>
							<div class='table-header'>  RFC</div>
							
							</td>
						</tr>
						</thead>
						<tbody>
						".$tr."
						</tbody>	
					</table>
			";


			$this->output
	        ->set_content_type('application/html')
	        ->set_output($salida);


	}

	public function mostrarServicios(){

		$Certificaciones = $this->catalogos_model->getAllCertificaciones();

		$numeroFila = $_POST['numeroFila'];


		foreach ($Certificaciones as $key => $value) {
				$tr .= "
					<tr>
						<td style='cursor:pointer' onclick='cargarServicios(".$value['id'].",".$numeroFila.")'>
						".$value['Certificaciones']."
						</td>

					</tr>
					";
			}

			


			$salida = "
					<table width='100%'>
						<tr>
							<td width='100%'>
							<div class='table-header'> Certificaciones</div>
							
							</td>
							
						</tr>
						".$tr."	
					</table>
			";

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($salida);


	}

		public function mostrarServiciosClientes(){

		$Certificaciones = $this->catalogos_model->getAllCertificaciones();

		$numeroFila = $_POST['numeroFila'];


		foreach ($Certificaciones as $key => $value) {
				$tr .= "
					<tr>
						<td style='cursor:pointer' onclick='cargarServiciosClientes(".$value['id'].",".$numeroFila.")'>
						".$value['Certificaciones']."
						</td>

					</tr>
					";
			}

			


			$salida = "
					<table width='100%'>
						<tr>
							<td width='100%'>
							<div class='table-header'> Certificaciones</div>
							
							</td>
							
						</tr>
						".$tr."	
					</table>
			";

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($salida);


	}






	public function filasCotizacion(){
		$NumFila = $_POST['NumFila'];

		if($_POST['NumFila'] == '1' ){
			$ButoonMenos = ' onclick="MenosFilaPedidos('.$_POST['NumFila'].'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaPedidos('.$_POST['NumFila'].'); return false ;" ' ;	
			}
			if( ($_POST['NumFila']%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
			

			echo '
	  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$_POST ['NumFila'].'" >
	     
	     	<td  style="text-align: center" >
	     		<img alt="Buscar" title="Buscar" src="'.base_url().'inc/images/lupa.png" style="cursor:pointer" onclick="renglon('.$_POST ['NumFila'].'); return false;">
			  <input type="text" class="span11"  id="servicio_'.$_POST ['NumFila'].'"  name="servicio_'.$_POST ['NumFila'].'" value="" />
			   <input type="hidden" class="span11"  id="id_servicio_'.$_POST ['NumFila'].'"  name="id_servicio_'.$_POST ['NumFila'].'" value="" />
	     	</td>

	     	<td  style="text-align: center" >

			  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$_POST ['NumFila'].');" id="cantidad_'.$_POST ['NumFila'].'"  name="cantidad_'.$_POST ['NumFila'].'" value="" />
	     	</td>

	     	<td  style="text-align: center" >

			  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$_POST ['NumFila'].');" id="precio_'.$_POST ['NumFila'].'"  name="precio_'.$_POST ['NumFila'].'" value="" />
	     	</td>
	     	<td  style="text-align: center" >

			  <input type="text" class="span10"  id="importe_'.$_POST ['NumFila'].'"  name="importe_'.$_POST ['NumFila'].'" value="" readonly/>
	     	</td>
	     	<td style="text-align: center">&nbsp;
            <input style="width : 18px;" type="button" value="-" '.$ButoonMenos.'>&nbsp;&nbsp;
            <input style="width : 18px;" type="button" value="+" onclick="MasFilaPedidos('.$_POST ['NumFila'].'); return false ;">&nbsp;&nbsp;
      		  </td>
	 
	      </tr> 	
		' ;
	}

	/*
	public function actualizarCotizacion(){
			

			$data['Comentarios']    	= $_POST['comentario'];

			$where['id'] = $_POST['idCotizacion'];
			$this->documentos_model->updDocumentoEncabezado($data,$where);
    	

    		$whereId['idEncabezado'] = $_POST['idCotizacion'];
			$this->documentos_model->dropDocumentoDetalle($whereId);


			foreach($_POST as $key => $value){
				if(strstr($key , 'cantidad_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

			for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEncabezado']    = $_POST['idCotizacion'];
					$dataDetalle['idServicio']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['Servicio']        = $_POST['servicio_'.$i];
					$dataDetalle['Cantidad']    	= $_POST['cantidad_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];
					$dataDetalle['Importe']    		= $_POST['importe_'.$i];

					$this->documentos_model->addDocumentoDetalle($dataDetalle);


		    }	

			redirect('documentos/cotizacion');

    }	*/

    public function actualizarCotizacion(){
			

			$data['Comentarios']    	= $_POST['comentario'];

			$where['id'] = $_POST['idCotizacion'];
			$this->documentos_model->updDocumentoEncabezado_cot($data,$where);
    	

    		$whereId['idEncabezado'] = $_POST['idCotizacion'];
			$this->documentos_model->dropDocumentoDetalle_cot($whereId);


			foreach($_POST as $key => $value){
				if(strstr($key , 'cantidad_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

			for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEncabezado']    = $_POST['idCotizacion'];
					$dataDetalle['idServicio']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['Servicio']        = $_POST['servicio_'.$i];
					$dataDetalle['Cantidad']    	= $_POST['cantidad_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];
					$dataDetalle['Importe']    		= $_POST['importe_'.$i];

					$this->documentos_model->addDocumentoDetalle_cot($dataDetalle);


		    }	

			redirect('documentos/cotizacion');

    }	

    /*

	public function guardarCotizacion(){

			//print_r($_POST);

			$data['idCliente']    		= $_POST['IdEmpresaSecundaria'];
			$data['NombreCliente']    	= $_POST['Nombres'];
			$data['DireccionCliente']   = $_POST['DomicilioFiscal'];
			$data['Rfc']    			= $_POST['Rfc'];
			$data['Comentarios']    	= $_POST['comentario'];
			$data['Estatus']    		= 0;	
			$data['Fecha']    			= date('Y-m-d');
			$data['idUsuarioRegistro']  = $_SESSION['SEUS']['use_id'];
		

			$Respuesta = $this->documentos_model->addDocumentoEncabezado($data);



			foreach($_POST as $key => $value){
				if(strstr($key , 'cantidad_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

		
			for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEncabezado']    = $Respuesta;
					$dataDetalle['idServicio']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['Servicio']        = $_POST['servicio_'.$i];
					$dataDetalle['Cantidad']    	= $_POST['cantidad_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];
					$dataDetalle['Importe']    		= $_POST['importe_'.$i];

					$this->documentos_model->addDocumentoDetalle($dataDetalle);


		    }		

			redirect('documentos/cotizacion');

	}*/

	public function guardarCotizacion(){

			//print_r($_POST);

			$data['idCliente']    		= $_POST['IdEmpresaSecundaria'];
			$data['NombreCliente']    	= $_POST['Nombres'];
			$data['DireccionCliente']   = $_POST['DomicilioFiscal'];
			$data['Rfc']    			= $_POST['Rfc'];
			$data['Comentarios']    	= $_POST['comentario'];
			$data['Estatus']    		= 0;	
			$data['Fecha']    			= date('Y-m-d');
			$data['idUsuarioRegistro']  = $_SESSION['SEUS']['use_id'];
		

			$Respuesta = $this->documentos_model->addDocumentoEncabezado_cot($data);



			foreach($_POST as $key => $value){
				if(strstr($key , 'cantidad_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

		
			for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEncabezado']    = $Respuesta;
					$dataDetalle['idServicio']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['Servicio']        = $_POST['servicio_'.$i];
					$dataDetalle['Cantidad']    	= $_POST['cantidad_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];
					$dataDetalle['Importe']    		= $_POST['importe_'.$i];

					$this->documentos_model->addDocumentoDetalle_cot($dataDetalle);


		    }		

			redirect('documentos/cotizacion');

	}

	/*
	function EditarCotizacion($id){

			$renglones = '';
					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado($where);

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle($where2);

			$totalFilas = count($DocumentosDetalle);
			$data['totalFilas'] = $totalFilas + 1 ;
			
		$contador = 1;
		foreach ($DocumentosDetalle as $key => $value) {
				
			if($contador == 1 ){
			$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" ' ;	
			}


			if( ($contador%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
		
				 $renglones .= '
				  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		<img alt="Buscar" title="Buscar" src="'.base_url().'inc/images/lupa.png" style="cursor:pointer" onclick="renglon('.$contador.'); return false;">
						  <input type="text" class="span11"  id="servicio_'.$contador.'"  name="servicio_'.$contador.'" value="'.$value['Servicio'].'" />
						   <input type="hidden" class="span11"  id="id_servicio_'.$contador.'"  name="id_servicio_'.$contador.'" value="'.$value['idServicio'].'" />
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="cantidad_'.$contador.'"  name="cantidad_'.$contador.'" value="'.$value['Cantidad'].'" />
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="precio_'.$contador.'"  name="precio_'.$contador.'" value="'.$value['Precio'].'" />
				     	</td>
				     	<td  style="text-align: center" >

						  <input type="text" class="span10"  id="importe_'.$contador.'"  name="importe_'.$contador.'" value="'.$value['Importe'].'" readonly/>
				     	</td>
				     	<td style="text-align: center">&nbsp;
			            <input style="width : 18px;" type="button" value="-" '.$ButoonMenos.'>&nbsp;&nbsp;
			            <input style="width : 18px;" type="button" value="+" onclick="MasFilaPedidosEditar('.$contador.'); return false ;">&nbsp;&nbsp;
			      		  </td>
				 
				      </tr> 	
					' ;

					$contador++;

			}


			$data['DocumentosDetalle'] = $renglones;


			$this->parser->parse('Documentos/cotizacionDetalle', $data);
	}*/

	function EditarCotizacion($id){

			$renglones = '';
					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado_cot($where);

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle_cot($where2);


			$totalFilas = count($DocumentosDetalle);
			$data['totalFilas'] = $totalFilas + 1 ;
			
		$contador = 1;
		foreach ($DocumentosDetalle as $key => $value) {
				
			if($contador == 1 ){
			$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" ' ;	
			}


			if( ($contador%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
		
				 $renglones .= '
				  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		<img alt="Buscar" title="Buscar" src="'.base_url().'inc/images/lupa.png" style="cursor:pointer" onclick="renglon('.$contador.'); return false;">
						  <input type="text" class="span11"  id="servicio_'.$contador.'"  name="servicio_'.$contador.'" value="'.$value['Servicio'].'" />
						   <input type="hidden" class="span11"  id="id_servicio_'.$contador.'"  name="id_servicio_'.$contador.'" value="'.$value['idServicio'].'" />
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="cantidad_'.$contador.'"  name="cantidad_'.$contador.'" value="'.$value['Cantidad'].'" />
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="precio_'.$contador.'"  name="precio_'.$contador.'" value="'.$value['Precio'].'" />
				     	</td>
				     	<td  style="text-align: center" >

						  <input type="text" class="span10"  id="importe_'.$contador.'"  name="importe_'.$contador.'" value="'.$value['Importe'].'" readonly/>
				     	</td>
				     	<td style="text-align: center">&nbsp;
			            <input style="width : 18px;" type="button" value="-" '.$ButoonMenos.'>&nbsp;&nbsp;
			            <input style="width : 18px;" type="button" value="+" onclick="MasFilaPedidosEditar('.$contador.'); return false ;">&nbsp;&nbsp;
			      		  </td>
				 
				      </tr> 	
					' ;

					$contador++;

			}


			$data['DocumentosDetalle'] = $renglones;


			$this->parser->parse('Documentos/cotizacionDetalle', $data);
	}


	/*
	public function deleteDocumentos($id){

			$data['Estatus']    	= 4;

			$where['id'] = $_POST['eliminarID'];
			$this->documentos_model->updDocumentoEncabezado($data,$where);

			redirect('documentos/cotizacion');
	}*/


	public function deleteDocumentos($id){

			$data['Estatus']    	= 4;

			$where['id'] = $_POST['eliminarID'];
			$this->documentos_model->updDocumentoEncabezado_cot($data,$where);

			redirect('documentos/cotizacion');
	}


	/*
	public function mostrarDocumentoAutorizar(){



				 $where['id'] = $_POST['id'];
				$Cotizacion         = $this->documentos_model->getOneDocumentoEncabezado($where);

				 $where2['idEncabezado'] = $_POST['id'];
				$CotizacionDetalle         = $this->documentos_model->getAllDocumentoDetalle($where2);

				$subtotal = 0;

				foreach ($CotizacionDetalle as $key => $value) {



				
				 $renglones .= '
				  <tr style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		'.$value['Servicio'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Cantidad'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Precio'].'
				     	</td>
				     	<td  style="text-align: center" >
				     		'.$value['Importe'].'

				     	</td>
				
				 
				      </tr> 	
					' ;

					$subtotal =  $subtotal + $value['Importe'];

				}


				$iva = ($subtotal * 0.16);
				$total = ($subtotal + $iva);
			

				$htmlCotizacion = '
					<div class="table-header">
													Información cotización
												</div>

						  <input type="hidden" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="idCotizacion"  name="idCotizacion" value="'.$_POST['id'].'" />
												 <br>
					<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa:
																     
																           '.$Cotizacion['NombreCliente'].'
																           </label>
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC:
																       	'.$Cotizacion['Rfc'].'
																       	</label>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal:
																         
																           	'.$Cotizacion['DireccionCliente'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios:
																         
																           	'.$Cotizacion['Comentarios'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle cotización
												</div>
												<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>						
															</tr>
														</thead>
															'.$renglones.'
													 </table>

													 <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Subtotal:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$subtotal.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Iva:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$iva.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$total.'</td>
													       </tr>
													 </table>
													  <input type="hidden" class="span10" id="total"  name="total" value="'.$total.'" />

				';

			

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($htmlCotizacion);


	}*/

	public function mostrarDocumentoAutorizar(){



				 $where['id'] = $_POST['id'];
				$Cotizacion         = $this->documentos_model->getOneDocumentoEncabezado_cot($where);

				 $where2['idEncabezado'] = $_POST['id'];
				$CotizacionDetalle         = $this->documentos_model->getAllDocumentoDetalle_cot($where2);

				$subtotal = 0;

				foreach ($CotizacionDetalle as $key => $value) {



				
				 $renglones .= '
				  <tr style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		'.$value['Servicio'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Cantidad'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Precio'].'
				     	</td>
				     	<td  style="text-align: center" >
				     		'.$value['Importe'].'

				     	</td>
				
				 
				      </tr> 	
					' ;

					$subtotal =  $subtotal + $value['Importe'];

				}


				$iva = ($subtotal * 0.16);
				$total = ($subtotal + $iva);
			

				$htmlCotizacion = '
					<div class="table-header">
													Información cotización
												</div>

						  <input type="hidden" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="idCotizacion"  name="idCotizacion" value="'.$_POST['id'].'" />

						  <br>
						  	<div class="form-group">
							  <label for="sel1">Seleccionar  Recibo o Facturacion:</label>
							  <select class="form-control" id="moduloSel" name="moduloSel">
							    <option value="">Elige</option>
							    <option value="Recibo">Modulo recibos</option>
							    <option value="Facturacion">Modulo facturacion</option>
							  </select>
							</div>		  

						
						
												 <br>
					<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa:
																     
																           '.$Cotizacion['NombreCliente'].'
																           </label>
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC:
																       	'.$Cotizacion['Rfc'].'
																       	</label>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal:
																         
																           	'.$Cotizacion['DireccionCliente'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios:
																         
																           	'.$Cotizacion['Comentarios'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle cotización
												</div>
												<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>						
															</tr>
														</thead>
															'.$renglones.'
													 </table>

													 <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Subtotal:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$subtotal.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Iva:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$iva.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$total.'</td>
													       </tr>
													 </table>
													  <input type="hidden" class="span10" id="total"  name="total" value="'.$total.'" />

				';

			

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($htmlCotizacion);


	}


		public function mostrarDocumentoFacturar(){



				 $where['id'] = $_POST['id'];
				$Cotizacion         = $this->documentos_model->getOneDocumentoEncabezado($where);

		

				 $where2['idEncabezado'] = $_POST['id'];
				$CotizacionDetalle         = $this->documentos_model->getAllDocumentoDetalle($where2);

				$subtotal = 0;

				foreach ($CotizacionDetalle as $key => $value) {



				
				 $renglones .= '
				  <tr style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		'.$value['Servicio'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Cantidad'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Precio'].'
				     	</td>
				     	<td  style="text-align: center" >
				     		'.$value['Importe'].'

				     	</td>
				
				 
				      </tr> 	
					' ;

					$subtotal =  $subtotal + $value['Importe'];

				}


				$iva = ($subtotal * 0.16);
				$total = ($subtotal + $iva);
			

				if($Cotizacion['Estatus'] == '2'){

					$htmlCotizacion = '
					<div class="table-header">
													Información Factura/Recibo
												</div>

						  <input type="hidden" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="idCotizacion"  name="idCotizacion" value="'.$_POST['id'].'" />
												 <br>
					<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa:
																     
																           '.$Cotizacion['NombreCliente'].'
																           </label>
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC:
																       	'.$Cotizacion['Rfc'].'
																       	</label>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal:
																         
																           	'.$Cotizacion['DireccionCliente'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios:
																         
																           	'.$Cotizacion['Comentarios'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle
												</div>
												<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>						
															</tr>
														</thead>
															'.$renglones.'
													 </table>

													 <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Subtotal:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$subtotal.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Iva:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$iva.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$total.'</td>
													       </tr>
													 </table>
													  <input type="hidden" class="span10" id="total"  name="total" value="'.$total.'" />

				';

				}else{
					$htmlCotizacion = '
					<div class="table-header">
													Información Factura/Recibo
												</div>

						  <input type="hidden" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="idCotizacion"  name="idCotizacion" value="'.$_POST['id'].'" />
												 <br>
					<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa:
																     
																           '.$Cotizacion['NombreCliente'].'
																           </label>
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC:
																       	'.$Cotizacion['Rfc'].'
																       	</label>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal:
																         
																           	'.$Cotizacion['DireccionCliente'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios:
																         
																           	'.$Cotizacion['Comentarios'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle
												</div>
												<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>						
															</tr>
														</thead>
															'.$renglones.'
													 </table>

													 <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$subtotal.'</td>
													       </tr>

		
													 </table>
													  <input type="hidden" class="span10" id="total"  name="total" value="'.$subtotal.'" />

				';
				}
				

			

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($htmlCotizacion);


	}


	public function mostrarDocumentoFacturarRecibo(){



				 $where['id'] = $_POST['id'];
				$Cotizacion         = $this->documentos_model->getOneDocumentoEncabezadoRecibo($where);

		

				 $where2['idEncabezado'] = $_POST['id'];
				$CotizacionDetalle         = $this->documentos_model->getAllDocumentoDetalleRecibo($where2);

				$subtotal = 0;

				foreach ($CotizacionDetalle as $key => $value) {



				
				 $renglones .= '
				  <tr style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
				     		'.$value['Servicio'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Cantidad'].'
				     	</td>

				     	<td  style="text-align: center" >
				     		'.$value['Precio'].'
				     	</td>
				     	<td  style="text-align: center" >
				     		'.$value['Importe'].'

				     	</td>
				
				 
				      </tr> 	
					' ;

					$subtotal =  $subtotal + $value['Importe'];

				}


				$iva = ($subtotal * 0.16);
				$total = ($subtotal + $iva);
			

				if($Cotizacion['Estatus'] == '2'){

					$htmlCotizacion = '
					<div class="table-header">
													Información Recibo
												</div>

						  <input type="hidden" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="idCotizacion"  name="idCotizacion" value="'.$_POST['id'].'" />
												 <br>
					<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa:
																     
																           '.$Cotizacion['NombreCliente'].'
																           </label>
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC:
																       	'.$Cotizacion['Rfc'].'
																       	</label>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal:
																         
																           	'.$Cotizacion['DireccionCliente'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios:
																         
																           	'.$Cotizacion['Comentarios'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle
												</div>
												<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>						
															</tr>
														</thead>
															'.$renglones.'
													 </table>

													 <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Subtotal:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$subtotal.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Iva:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$iva.'</td>
													       </tr>
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$total.'</td>
													       </tr>
													 </table>
													  <input type="hidden" class="span10" id="total"  name="total" value="'.$total.'" />

				';

				}else{
					$htmlCotizacion = '
					<div class="table-header">
													Información Recibo
												</div>

						  <input type="hidden" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="idCotizacion"  name="idCotizacion" value="'.$_POST['id'].'" />
												 <br>
					<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa:
																     
																           '.$Cotizacion['NombreCliente'].'
																           </label>
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC:
																       	'.$Cotizacion['Rfc'].'
																       	</label>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal:
																         
																           	'.$Cotizacion['DireccionCliente'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios:
																         
																           	'.$Cotizacion['Comentarios'].'
																           	</label>
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle
												</div>
												<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>						
															</tr>
														</thead>
															'.$renglones.'
													 </table>

													 <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">'.$subtotal.'</td>
													       </tr>

		
													 </table>
													  <input type="hidden" class="span10" id="total"  name="total" value="'.$subtotal.'" />

				';
				}
				

			

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($htmlCotizacion);


	}

	public function FacturarReciboMandar(){

			
			$where3 = "id =".$_POST['IDfactura'];
			$dataFact = array();
			$dataFactDetalle = array();
			$dataFact = $this->documentos_model->getOneDocumentoEncabezadoRecibo($where3);
			unset($dataFact['id']);
			$dataFact['Estatus'] = 1;
			$this->db->insert('DocumentoEncabezado', $dataFact); 
			$idDetalleE = $this->db->insert_id();


			$where2 = "idEncabezado =".$_POST['IDfactura'];
			$dataFactDetalle = $this->documentos_model->getAllDocumentoDetalleRecibo($where2);
			

			foreach ($dataFactDetalle as $key => $value) {
				

					$dataFactDetalle2 = array(
							'idEncabezado' =>$idDetalleE,
							'idServicio' => $value['idServicio'],
							'Servicio' => $value['Servicio'],
							'Cantidad' => $value['Cantidad'],
							'Precio' => $value['Precio'],
							'Importe' => $value['Importe']
							);
					$this->db->insert('DocumentoDetalle', $dataFactDetalle2); 	
			}
			

			$dataEvaluado = array(
               'Documento' => "Factura"
            );

			$this->db->where('idFactura', $_POST['IDfactura']);
			$this->db->where('Documento','Recibo');
			$this->db->update('FacturaEvaluado', $dataEvaluado);


			$updateEncabezadoRecibo = array(
               'Estatus' => 4
            );

			$this->db->where('id', $_POST['IDfactura']);
			$this->db->update('DocumentoEncabezadoRecibo', $updateEncabezadoRecibo);




			$updatePagos = array(
               'idDocumento' => $idDetalleE,
               'Documento' => 'Factura',
            );

			$this->db->where('idDocumento', $_POST['IDfactura']);
			$this->db->where('Documento','Recibo');
			$this->db->update('PagosCobranza', $updatePagos);

			redirect('documentos/facturacionRecibos');


	}	


	public function ActualizarAutorizacionCoti(){
			 
			 $where['id'] = $_POST['idCotizacion'];
			 $data['Estatus']    	= 1;
			 $data['Total']     	= $_POST['total'];
			 $data['PorPagar']    	= $_POST['total'];
			 $moduloSel    	= $_POST['moduloSel'];


			$this->documentos_model->updDocumentoEncabezado_cot($data,$where);

			$where3 = "id =".$_POST['idCotizacion'];
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado_cot($where3);
			 

			$where2 = "idEncabezado =".$_POST['idCotizacion'];
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle_cot($where2);




			if($moduloSel == 'Recibo'){

				$InserDataFactura = array(
					            'idCliente' => $data['Documentos']['idCliente'],
					            'NombreCliente' => $data['Documentos']['NombreCliente'],
					            'DireccionCliente' => $data['Documentos']['DireccionCliente'],
					            'Rfc' => $data['Documentos']['Rfc'],
					            'Comentarios' => $data['Documentos']['Comentarios'],
					            'Fecha' => $data['Documentos']['Fecha'],
					            'Estatus' => $data['Documentos']['Estatus'],
					            'Vendedor' => $data['Documentos']['Vendedor'],
					            'Total' => $data['Documentos']['Total'],
					            'PorPagar' => $data['Documentos']['PorPagar'],
					            'FolioFactura' => $data['Documentos']['FolioFactura'],
					            'SelloDigitalCFDI' => $data['Documentos']['SelloDigitalCFDI'],
					            'SelloSat' => $data['Documentos']['SelloSat'],
					            'CadenaComplemento' => $data['Documentos']['CadenaComplemento'],
					            'NoCertificacionSAT' => $data['Documentos']['NoCertificacionSAT'],
					            'FechaHoraCertificacion' => $data['Documentos']['FechaHoraCertificacion'],
					            'NoSerieCertificao' => $data['Documentos']['NoSerieCertificao'],
					            'idUsuarioRegistro' => $data['Documentos']['idUsuarioRegistro'],
					            'formaPago' => $data['Documentos']['formaPago']
					         	);

				$this->db->insert('DocumentoEncabezadoRecibo', $InserDataFactura);
				$idDetalle = $this->db->insert_id();

				foreach ($DocumentosDetalle as $key => $value) {
					$InserDataFacturaDetalle = array(
							            'idEncabezado' => $idDetalle,
							            'idServicio' => $value['idServicio'],
							            'Servicio' => $value['Servicio'],
							            'Cantidad' => $value['Cantidad'],
							            'Precio' => $value['Precio'],
							            'Importe' => $value['Importe']
					         	);


					$this->db->insert('DocumentoDetalleRecibo', $InserDataFacturaDetalle);
				}

			}else{


					$InserDataFactura = array(
					            'idCliente' => $data['Documentos']['idCliente'],
					            'NombreCliente' => $data['Documentos']['NombreCliente'],
					            'DireccionCliente' => $data['Documentos']['DireccionCliente'],
					            'Rfc' => $data['Documentos']['Rfc'],
					            'Comentarios' => $data['Documentos']['Comentarios'],
					            'Fecha' => $data['Documentos']['Fecha'],
					            'Estatus' => $data['Documentos']['Estatus'],
					            'Vendedor' => $data['Documentos']['Vendedor'],
					            'Total' => $data['Documentos']['Total'],
					            'PorPagar' => $data['Documentos']['PorPagar'],
					            'FolioFactura' => $data['Documentos']['FolioFactura'],
					            'SelloDigitalCFDI' => $data['Documentos']['SelloDigitalCFDI'],
					            'SelloSat' => $data['Documentos']['SelloSat'],
					            'CadenaComplemento' => $data['Documentos']['CadenaComplemento'],
					            'NoCertificacionSAT' => $data['Documentos']['NoCertificacionSAT'],
					            'FechaHoraCertificacion' => $data['Documentos']['FechaHoraCertificacion'],
					            'NoSerieCertificao' => $data['Documentos']['NoSerieCertificao'],
					            'idUsuarioRegistro' => $data['Documentos']['idUsuarioRegistro'],
					            'formaPago' => $data['Documentos']['formaPago']
					         	);

				$this->db->insert('DocumentoEncabezado', $InserDataFactura);
				$idDetalle = $this->db->insert_id();

				foreach ($DocumentosDetalle as $key => $value) {
					$InserDataFacturaDetalle = array(
							            'idEncabezado' => $idDetalle,
							            'idServicio' => $value['idServicio'],
							            'Servicio' => $value['Servicio'],
							            'Cantidad' => $value['Cantidad'],
							            'Precio' => $value['Precio'],
							            'Importe' => $value['Importe']
					         	);


					$this->db->insert('DocumentoDetalle', $InserDataFacturaDetalle);
				}

			}
			

			echo '1';
	}


	public function pdfCotizacion($id){

			require_once('./application/third_party/dompdf/dompdf_config.inc.php');


					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado_cot($where);

			$where2 = "id =".$data['Documentos']['idCliente'];
			$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where2);
			$data['Documentos']['Telefono'] = $EmpresasSec['Telefono'];

			if($EmpresasSec['Vendedor'] != ''){
				$where3 = "id =".$EmpresasSec['Vendedor'];
				$Vendedores        = $this->catalogos_model->getOneVendedores($where3);

				$data['Documentos']['VendedoresNombre'] = $Vendedores['Nombre'];
			}else{
				$data['Documentos']['VendedoresNombre'] = 'N/A';
			}
			
			
			

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle_cot($where2);

			$data['DocumentosDetalle'] =$DocumentosDetalle;

		
				 //echo $_SERVER["DOCUMENT_ROOT"];
				$data['DOCUMENT_ROOT'] = $_SERVER["DOCUMENT_ROOT"];
				
			


					//$codigo = $this->load->view('Documentos/1',$data);
				     $codigo = $this->parser->parse('Documentos/cotizacion_PDF', $data);
				
				
	   				$nombre = 'cotizacion'.$id.'.pdf';

					 $codigo = ($codigo);
				
					$dompdf = new DOMPDF();
					$dompdf->load_html(($codigo));
					ini_set('memory_limit', '-1');
					$dompdf->render();
					$dompdf->stream($nombre);
	}



	public function pdfFactura($id){

			require_once('./application/third_party/dompdf/dompdf_config.inc.php');


					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado($where);

			//echo "<pre>";
			//print_r($data['Documentos']);
			$queryEva = "SELECT Evaluados.* FROM FacturaEvaluado INNER JOIN Evaluados ON Evaluados.id = FacturaEvaluado.idEvaluado  WHERE  FacturaEvaluado.idFactura = '".$id."'";

			$evaluados = $this->db->query( $queryEva )->result_array(); 
			
			foreach ($evaluados as $key => $value) {
					$informacionHtml .= '
						<tr>
							<td width="100%">'.$value['Nombre'].'</td>				
						</tr>
					';
			}

			$data['informacionHtml'] = $informacionHtml;
			

			$where2 = "id =".$data['Documentos']['idCliente'];
			$EmpresasSecPDF        = $this->catalogos_model->getOneEmpresaSecundaria($where2);

			$data['Documentos']['Telefono'] = $EmpresasSecPDF['Telefono'];
			$data['Documentos']['Rfc'] = $EmpresasSecPDF['Rfc'];
			$data['Documentos']['NombreCliente'] = $EmpresasSecPDF['RazonSocial'];
			$data['Documentos']['DireccionCliente'] = $EmpresasSecPDF['calleFiscal'].' #'.$EmpresasSecPDF['numroExtFiscal'].', colonia '.$EmpresasSecPDF['ColoniaFiscal'].', '.$EmpresasSecPDF['municipioFiscal'].', '.$EmpresasSecPDF['estadoFiscal'].', CP. '.$EmpresasSecPDF['codigoPostal'];

	

			if($EmpresasSec['Vendedor'] != ''){
				$where3 = "id =".$EmpresasSecPDF['Vendedor'];
				$Vendedores        = $this->catalogos_model->getOneVendedores($where3);
				$data['Documentos']['VendedoresNombre'] = $EmpresasSecPDF['Vendedor'];
			}else{
				$data['Documentos']['VendedoresNombre'] = 'N/A';
			}
			
			 $data['Documentos']['SelloDigitalCFDI'];
				

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle($where2);

			$data['DocumentosDetalle'] =$DocumentosDetalle;

		
				 	//echo $_SERVER["DOCUMENT_ROOT"];
					$data['DOCUMENT_ROOT'] = $_SERVER["DOCUMENT_ROOT"];
				
					$arraryP = explode('/',$data['Documentos']['SelloDigitalCFDI']);
					$data['arrayCFDI'] =  $arraryP;



					//$data['Documentos']['CadenaComplemento'] = str_replace('+','', $data['Documentos']['CadenaComplemento']);
					$arraryComplementos = explode('/',$data['Documentos']['CadenaComplemento']);
					
					$data['arrayCompletos'] =  $arraryComplementos;

					//$codigo = $this->load->view('Documentos/1',$data);
				     $codigo = $this->parser->parse('Documentos/factura_PDF', $data);
			
				
					
					if($data['Documentos']['Estatus'] == 2){
						$nombre = 'factura_'.$id.'.pdf';
					}else{
						$nombre = 'recibo_'.$id.'.pdf';
					}
	   				

					 $codigo = ($codigo);
				
					$dompdf = new DOMPDF();
					$dompdf->load_html($codigo);
					ini_set('memory_limit', '-1');
					$dompdf->render();
					$dompdf->stream($nombre);
					//file_put_contents("archivo.pdf", $pdf);
					//file_put_contents('uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$Respuesta.'.xml', 
					//$dompdf->stream($nombre);
	}



	public function pdfRecibo($id){

			require_once('./application/third_party/dompdf/dompdf_config.inc.php');


					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezadoRecibo($where);

			//echo "<pre>";
			//print_r($data['Documentos']);
			$queryEva = "SELECT Evaluados.* FROM FacturaEvaluado INNER JOIN Evaluados ON Evaluados.id = FacturaEvaluado.idEvaluado  WHERE  FacturaEvaluado.idFactura = '".$id."' AND Documento='Recibo'";

			$evaluados = $this->db->query( $queryEva )->result_array(); 
			
			foreach ($evaluados as $key => $value) {
					$informacionHtml .= '
						<tr>
							<td width="100%">'.$value['Nombre'].'</td>				
						</tr>
					';
			}

			$data['informacionHtml'] = $informacionHtml;
			

			$where2 = "id =".$data['Documentos']['idCliente'];
			$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where2);
			$data['Documentos']['Telefono'] = $EmpresasSec['Telefono'];
			$data['Documentos']['Rfc'] = $EmpresasSec['Rfc'];
			$data['Documentos']['NombreCliente'] = $EmpresasSec['RazonSocial'];
			$data['Documentos']['DireccionCliente'] = $EmpresasSec['calleFiscal'].' #'.$EmpresasSecPDF['numroExtFiscal'].', colonia '.$EmpresasSecPDF['ColoniaFiscal'].', '.$EmpresasSecPDF['municipioFiscal'].', '.$EmpresasSecPDF['estadoFiscal'].', CP. '.$EmpresasSecPDF['codigoPostal'];

			if($EmpresasSec['Vendedor'] != ''){
				$where3 = "id =".$EmpresasSec['Vendedor'];
				$Vendedores        = $this->catalogos_model->getOneVendedores($where3);
				$data['Documentos']['VendedoresNombre'] = $EmpresasSec['Vendedor'];
			}else{
				$data['Documentos']['VendedoresNombre'] = 'N/A';
			}
			
			 $data['Documentos']['SelloDigitalCFDI'];
				

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalleRecibo($where2);

			$data['DocumentosDetalle'] =$DocumentosDetalle;

		
				 	//echo $_SERVER["DOCUMENT_ROOT"];
					$data['DOCUMENT_ROOT'] = $_SERVER["DOCUMENT_ROOT"];
				
					$arraryP = explode('/',$data['Documentos']['SelloDigitalCFDI']);
					$data['arrayCFDI'] =  $arraryP;



					//$data['Documentos']['CadenaComplemento'] = str_replace('+','', $data['Documentos']['CadenaComplemento']);
					$arraryComplementos = explode('/',$data['Documentos']['CadenaComplemento']);
					
					$data['arrayCompletos'] =  $arraryComplementos;

					//$codigo = $this->load->view('Documentos/1',$data);
				     $codigo = $this->parser->parse('Documentos/factura_PDF', $data);
			
					
					if($data['Documentos']['Estatus'] == 2){
						$nombre = 'factura_'.$id.'.pdf';
					}else{
						$nombre = 'recibo_'.$id.'.pdf';
					}
	   				

					 $codigo = ($codigo);
				
					$dompdf = new DOMPDF();
					$dompdf->load_html($codigo);
					ini_set('memory_limit', '-1');
					$dompdf->render();
					$dompdf->stream($nombre);
					//file_put_contents("archivo.pdf", $pdf);
					//file_put_contents('uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$Respuesta.'.xml', 
					//$dompdf->stream($nombre);
	}





	public function evaluacion(){



		//if($_SESSION['SEUS']['use_typ_id']  == 8){
		//	$where['idPsicologa'] = $_SESSION['SEUS']['id_psicologa'];
		//}elseif($_SESSION['SEUS']['use_typ_id']  == 9){
		//	$where['idFreelance'] = $_SESSION['SEUS']['id_freelance'];
		//}else{
			$where = null;
		//}

		///COTIZACION 0, EVALUACION 1, FACTURA 2 , RECIBO 3, CANCELADO 4.   FACTURADA/REMISION 0 / 1
		$select = "Evaluados.id, Evaluados.Nombre, Evaluados.FechaRegistro, EmpresaSecundaria.RazonSocial as nombreEmpresa, EstatusEvaluado.Descripcion, Evaluados.FechaLimite, IF(Evaluados.EstatusEvaluado=1,'Activo', 'Baja' ) as EstatusEvaluado, Psicologa.nombrePsicologa, FacturaEvaluado.idFactura, DocumentoEncabezado.Estatus";
		$join   = array(
			                      
						array(
							'table' => 'EmpresaSecundaria',
							'cond'  => 'EmpresaSecundaria.id = Evaluados.idEmpresa',
							'type'  => 'left' 
							),
							array(
							'table' => 'EstatusEvaluado',
							'cond'  => 'EstatusEvaluado.id = Evaluados.Estatus',
							'type'  => 'left' 
							),
							array(
							'table' => 'Psicologa',
							'cond'  => 'Psicologa.id = Evaluados.idPsicologa',
							'type'  => 'left' 
							),
							array(
							'table' => 'FacturaEvaluado',
							'cond'  => 'FacturaEvaluado.idEvaluado = Evaluados.id',
							'type'  => 'left' 
							),
							array(
							'table' => 'DocumentoEncabezado',
							'cond'  => 'DocumentoEncabezado.id = FacturaEvaluado.idFactura',
							'type'  => 'left' 
							)			   
		                );

		$orderBy                       = array (
		                                   'Evaluados.id' => 'DESC'
		                                 );	

		$data['Evaluados']        = $this->documentos_model->getAllDocumentoEvaluacion($where,$select,$join,$orderBy);

		


		$this->parser->parse('Documentos/GridDocumentosEvaluaciones', $data);

	}

	public function InsertarEValuadoPrimeraVez(){

		$InserDataEv = array(
					            'Nombre' => 'Pendiente'
					         );


		$this->db->insert('Evaluados', $InserDataEv);

		$idFolio = $this->db->insert_id();

		$_SESSION['idFolio'] = $idFolio;

		redirect('documentos/evaluadoNuevo');

	}


	public function evaluadoNuevo(){


		
		$data['idFolio'] = $_SESSION['idFolio'];



		$orderBy                       = array (
		                                   'EmpresaSecundaria.Nombre' => 'ASC'
		                                 );	
	  	$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria(null,null,null,$orderBy);
	  	$data['Psicologa']  = $this->catalogos_model->getAllPsicologa();
	    $data['Estados']    = $this->estados_model->getAllEstados();

	    //$data['Municipio']  = $this->municipalities_model->getAllMunicipalities();
	    $Certificaciones = $this->catalogos_model->getAllCertificaciones();
		$data['Certificaciones'] = $Certificaciones;

		$Sucursal = $this->catalogos_model->getAllSucursal();
		$data['Sucursal'] = $Sucursal;

		$data['Freelance']   = $this->catalogos_model->getAllFreelance();

		$this->parser->parse('Documentos/FormularioAltaEvaluado', $data);
	}

	
	function guardarAltaFormulario(){

			$data['idCertificado']    	= $_POST['certificacionId'];	
			$data['idEmpresa']    		= $_POST['idEmpresa'];
			$data['idPsicologa']    	= $_POST['idPsicologa'];
			$data['idFreelance']    	= $_POST['Freelance'];


			if($_POST['Freelance'] != $_POST['idFreelanceVer']){

				$this->load->library('email');
				$this->load->model('user_model');
				
				$whereUsers['id_freelance'] = $_POST['Freelance'];
				$usuarios             = $this->user_model->getOneUser($whereUsers);
				if(!empty($usuarios)){
					$asunto = 'Asignacion de evaluado OPALO';
					 $mensaje = '
					<tablet>
					  <tr>
					    <td colspan="2"><img src="'.base_url().'inc/logo.png" width="178" height="145" /><td>
					  </tr>
					  <tr>
					     <td colspan="2"> Estimado(a) trabajadora(or) social, se le asigno el siguiente evaluado: </td>
					  </tr>
					  <tr>
					     <td colspan="2" align="center">
					     <br><br>
					     </td>
					  </tr>
					  <tr>
					     <td colspan="2" align="center">
					     Nombre: '.$_POST['Nombres'].' <br>
					     Correo: '.$_POST['Mail'].'
					     </td>
					  </tr>
					</tablet>
					';
				}

				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);

				$email = $usuarios['use_email'];
				
				$this->email->from('no-replay@sistemaopalo.com', 'SISTEMA');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
		


			}


			$data['EstatusEvaluado']    	= $_POST['EstatusEvaluado'];

			

			
			$data['Sucursal']    		= $_POST['sucursal'];	
			$data['Curp']    			= $_POST['Curp'];
			$data['Edad']    			= $_POST['Edad'];
			$data['Peso']    			= $_POST['Peso'];
			$data['Estatura']    		= $_POST['Estatura'];	

			$data['Puesto']    			= $_POST['Puesto'];	
			 	
			$data['Sexo']    			= $_POST['tipo_sexo'];	
			$data['Nombre']    			= $_POST['Nombres'];	
			$data['Correo']    			= $_POST['Mail'];	
			$data['Domicilio']    		= $_POST['Domicilio12'];	
			$data['Colonia']    		= $_POST['Colonia'];
			$data['Ciudad']    		    = $_POST['Ciudad'];	
			$data['Estado']    			= $_POST['Estado'];	
			$data['Municipio']    		= $_POST['Municipio'];	
			$data['Cp']    		    	= $_POST['CodigoPost'];	
			$data['Telefono']    		= $_POST['TelCasa'];
			$data['Celular']    		= $_POST['Cel'];		
			$data['FechaRegistro']    	= date('Y-m-d');	
			$data['Estatus']    	    = 1;	

			//echo "<pre>";
			//print_r($_POST);
			//die;

			$where['id'] = $_POST['idFolio'];

			$this->documentos_model->updDocumentoEvaluacion($data,$where);
			//die($this->db->last_query());	
			$id = $_POST['idFolio'];
			//$id = $this->documentos_model->addDocumentoEvaluacion($data);

			$data2['idEvaluado']    	= $id;
			$data2['NombreEmpresa']    	= $_POST['NombreEmpresa'];
			$data2['Domicilio']    		= $_POST['Domicilio'];
			$data2['Telefono']    		= $_POST['Telefono'];
			$data2['FechaIngreso']    	= $_POST['FechaIngreso'];
			$data2['FechaSalida']    	= $_POST['FechaSalida'];
			$data2['JefeInmediato']    	= $_POST['JefeInmediato'];
			$data2['PuestoJefe']    	= $_POST['PuestoJefe'];
			$data2['MotivoSalida']    	= $_POST['MotivoSalida'];


			$data2['NombreEmpresa2']    = $_POST['NombreEmpresa2'];
			$data2['Domicilio2']    	= $_POST['Domicilio2'];
			$data2['Telefono2']    		= $_POST['Telefono2'];
			$data2['FechaIngreso2']    	= $_POST['FechaIngreso2'];
			$data2['FechaSalida2']    	= $_POST['FechaSalida2'];
			$data2['JefeInmediato2']    = $_POST['JefeInmediato2'];
			$data2['PuestoJefe2']    	= $_POST['PuestoJefe2'];
			$data2['MotivoSalida2']    	= $_POST['MotivoSalida2'];


			$data2['NombreEmpresa3']    = $_POST['NombreEmpresa3'];
			$data2['Domicilio3']    	= $_POST['Domicilio3'];
			$data2['Telefono3']    		= $_POST['Telefono3'];
			$data2['FechaIngreso3']    	= $_POST['FechaIngreso3'];
			$data2['FechaSalida3']    	= $_POST['FechaSalida3'];
			$data2['JefeInmediato3']    = $_POST['JefeInmediato3'];
			$data2['PuestoJefe3']    	= $_POST['PuestoJefe3'];
			$data2['MotivoSalida3']    	= $_POST['MotivoSalida3'];


			$data2['NombreEmpresa4']    = $_POST['NombreEmpresa4'];
			$data2['Domicilio4']    	= $_POST['Domicilio4'];
			$data2['Telefono4']    		= $_POST['Telefono4'];
			$data2['FechaIngreso4']    	= $_POST['FechaIngreso4'];
			$data2['FechaSalida4']    	= $_POST['FechaSalida4'];
			$data2['JefeInmediato4']    = $_POST['JefeInmediato4'];
			$data2['PuestoJefe4']    	= $_POST['PuestoJefe4'];
			$data2['MotivoSalida4']    	= $_POST['MotivoSalida4'];


			$data2['NombreEmpresa5']    = $_POST['NombreEmpresa5'];
			$data2['Domicilio5']    	= $_POST['Domicilio5'];
			$data2['Telefono5']    		= $_POST['Telefono5'];
			$data2['FechaIngreso5']    	= $_POST['FechaIngreso5'];
			$data2['FechaSalida5']    	= $_POST['FechaSalida5'];
			$data2['JefeInmediato5']    = $_POST['JefeInmediato5'];
			$data2['PuestoJefe5']    	= $_POST['PuestoJefe5'];
			$data2['MotivoSalida5']    	= $_POST['MotivoSalida5'];

			$this->documentos_model->addDocumentoReferencias($data2);
				


			$file_type = $_FILES['file-input']['type'];
			$file_name = sanear_string($_FILES['file-input']['name']); 

			if($_FILES['file-input']['name'] != ''){

					$directorioArchivos = "uploads/evaluados/".$id."/";

					if (!file_exists($directorioArchivos)) {
						mkdir('./uploads/evaluados/'.$id.'/', 0777, true);
					}

					$numRand = rand();
					$url = 'uploads/evaluados/'.$id.'/'.$numRand.'-'.utf8_encode($file_name);

					move_uploaded_file($_FILES['file-input']['tmp_name'], $url);

					$dataFoto = array(
					               'Foto' => $url
					            );

					$this->db->where('id', $id);
					$this->db->update('Evaluados', $dataFoto);

			}	


			redirect('documentos/evaluacion');

	}


	function editarEvaluado($id){

			$data = array();
			$where['id'] = $id;
			$evaluados         = $this->documentos_model->getOneDocumentoEvaluacion($where);
			//echo "<pre>";
			//print_r($evaluados);
			//die;

			$data['Evaluados']  = $evaluados;
			$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();
			$data['Psicologa']  = $this->catalogos_model->getAllPsicologa();
			$data['Freelance']   = $this->catalogos_model->getAllFreelance();

	    	$data['Estados']    = $this->estados_model->getAllEstados();
			
			$wheremun['idEstado'] = $data['Evaluados']['Estado'];
			$municipios = $this->municipalities_model->getAllMunicipalities($wheremun);

			$data['Municipio'] = $municipios;


			$whereReferencia['idEvaluado'] = $id;
			$referencias         = $this->documentos_model->getOneDocumentoReferencias($whereReferencia);


			$data['Referencias']  = $referencias;

			$Certificaciones = $this->catalogos_model->getAllCertificaciones();
			$data['Certificaciones'] = $Certificaciones;

			$Sucursal = $this->catalogos_model->getAllSucursal();
			$data['Sucursal'] = $Sucursal;
			$data['idFolio'] = $id;

			$this->parser->parse('Documentos/FormularioEditarEvaluado', $data);

	}


	function editarAltaFormulario(){

			$data['idCertificado']    	= $_POST['certificacionId'];
			$data['idEmpresa']    		= $_POST['idEmpresa'];
			$data['idPsicologa']    	= $_POST['idPsicologa'];
			$data['idFreelance']    	= $_POST['Freelance'];


			if($_POST['Freelance'] != $_POST['idFreelanceVer']){
				$this->load->library('email');
				$this->load->model('user_model');
				
				$whereUsers['id_freelance'] = $_POST['Freelance'];
				$usuarios             = $this->user_model->getOneUser($whereUsers);
				if(!empty($usuarios)){
					$asunto = 'Asignacion de evaluado OPALO';
				  $mensaje = '
					<tablet>
					  <tr>
					    <td colspan="2"><img src="'.base_url().'inc/logo.png" width="178" height="145" /><td>
					  </tr>
					  <tr>
					     <td colspan="2"> Estimado(a) trabajadora(or) social, se le asigno el siguiente evaluado: </td>
					  </tr>
					  <tr>
					     <td colspan="2" align="center">
					     <br><br>
					     </td>
					  </tr>
					  <tr>
					     <td colspan="2" align="center">
					     Nombre: '.$_POST['Nombres'].' <br>
					     Correo: '.$_POST['Mail'].'
					     </td>
					  </tr>
					</tablet>
					';
				}

				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);

				$email = $usuarios['use_email'];
				
				$this->email->from('no-replay@sistemaopalo.com', 'SISTEMA');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
		


			}


			$data['EstatusEvaluado']    	= $_POST['EstatusEvaluado'];

			$data['Sucursal']    		= $_POST['sucursal'];

			$data['Curp']    			= $_POST['Curp'];
			$data['Edad']    			= $_POST['Edad'];
			$data['Peso']    			= $_POST['Peso'];
			$data['Estatura']    		= $_POST['Estatura'];	




			$data['Puesto']    			= $_POST['Puesto'];	
			$data['Sexo']    			= $_POST['tipo_sexo'];	
			$data['Nombre']    			= $_POST['Nombres'];	
			$data['Correo']    			= $_POST['Mail'];	
			$data['Domicilio']    		= $_POST['Domicilio12'];	
			$data['Colonia']    		= $_POST['Colonia'];
			$data['Ciudad']    		    = $_POST['Ciudad'];	
			$data['Estado']    			= $_POST['Estado'];	
			$data['Municipio']    		= $_POST['Municipio'];	
			$data['Cp']    		    	= $_POST['CodigoPost'];	
			$data['Telefono']    		= $_POST['TelCasa'];
			$data['Celular']    		= $_POST['Cel'];


			if($_POST['tieneFecha'] == 'No'){
				$data['FechaRegistro'] = date('Y-m-d');
			}


			$where['id'] = $_POST['id'];
			//echo "<pre>";
			//print_r($_POST);
			//echo "</pre>";
			//die;
			$this->documentos_model->updDocumentoEvaluacion($data,$where);



			$data2['NombreEmpresa']    	= $_POST['NombreEmpresa'];
			$data2['Domicilio']    		= $_POST['Domicilio'];
			$data2['Telefono']    		= $_POST['Telefono'];
			$data2['FechaIngreso']    	= $_POST['FechaIngreso'];
			$data2['FechaSalida']    	= $_POST['FechaSalida'];
			$data2['JefeInmediato']    	= $_POST['JefeInmediato'];
			$data2['PuestoJefe']    	= $_POST['PuestoJefe'];
			$data2['MotivoSalida']    	= $_POST['MotivoSalida'];


			$data2['NombreEmpresa2']    = $_POST['NombreEmpresa2'];
			$data2['Domicilio2']    	= $_POST['Domicilio2'];
			$data2['Telefono2']    		= $_POST['Telefono2'];
			$data2['FechaIngreso2']    	= $_POST['FechaIngreso2'];
			$data2['FechaSalida2']    	= $_POST['FechaSalida2'];
			$data2['JefeInmediato2']    = $_POST['JefeInmediato2'];
			$data2['PuestoJefe2']    	= $_POST['PuestoJefe2'];
			$data2['MotivoSalida2']    	= $_POST['MotivoSalida2'];


			$data2['NombreEmpresa3']    = $_POST['NombreEmpresa3'];
			$data2['Domicilio3']    	= $_POST['Domicilio3'];
			$data2['Telefono3']    		= $_POST['Telefono3'];
			$data2['FechaIngreso3']    	= $_POST['FechaIngreso3'];
			$data2['FechaSalida3']    	= $_POST['FechaSalida3'];
			$data2['JefeInmediato3']    = $_POST['JefeInmediato3'];
			$data2['PuestoJefe3']    	= $_POST['PuestoJefe3'];
			$data2['MotivoSalida3']    	= $_POST['MotivoSalida3'];


			$data2['NombreEmpresa4']    = $_POST['NombreEmpresa4'];
			$data2['Domicilio4']    	= $_POST['Domicilio4'];
			$data2['Telefono4']    		= $_POST['Telefono4'];
			$data2['FechaIngreso4']    	= $_POST['FechaIngreso4'];
			$data2['FechaSalida4']    	= $_POST['FechaSalida4'];
			$data2['JefeInmediato4']    = $_POST['JefeInmediato4'];
			$data2['PuestoJefe4']    	= $_POST['PuestoJefe4'];
			$data2['MotivoSalida4']    	= $_POST['MotivoSalida4'];


			$data2['NombreEmpresa5']    = $_POST['NombreEmpresa5'];
			$data2['Domicilio5']    	= $_POST['Domicilio5'];
			$data2['Telefono5']    		= $_POST['Telefono5'];
			$data2['FechaIngreso5']    	= $_POST['FechaIngreso5'];
			$data2['FechaSalida5']    	= $_POST['FechaSalida5'];
			$data2['JefeInmediato5']    = $_POST['JefeInmediato5'];
			$data2['PuestoJefe5']    	= $_POST['PuestoJefe5'];
			$data2['MotivoSalida5']    	= $_POST['MotivoSalida5'];

			$where2['idEvaluado'] = $_POST['id'];
			$this->documentos_model->updDocumentoReferencias($data2,$where2);

			

			if($_FILES['file-input']['name'] != ''){

					$file_type = $_FILES['file-input']['type'];
					$file_name = sanear_string($_FILES['file-input']['name']); 

					$directorioArchivos = "uploads/evaluados/".$_POST['id']."/";

					if (!file_exists($directorioArchivos)) {
						mkdir('./uploads/evaluados/'.$_POST['id'].'/', 0777, true);
					}

					$numRand = rand();
					$url = 'uploads/evaluados/'.$_POST['id'].'/'.$numRand.'-'.utf8_encode($file_name);

					move_uploaded_file($_FILES['file-input']['tmp_name'], $url);

					$dataFoto = array(
					               'Foto' => $url
					            );

					$this->db->where('id', $_POST['id']);
					$this->db->update('Evaluados', $dataFoto);

			}



			if(isset($_FILES['file-input-socioEconomico']['name']) && $_FILES['file-input-socioEconomico']['name'] != ''){


						$file_type_poligrafos = $_FILES['file-input-socioEconomico']['type']; 
						$file_name_poligrafos = $_FILES['file-input-socioEconomico']['name']; 
						
						$directorioArchivos = "uploads/archivos/socioEconomicoDoc/".$_POST['id'];
						if (!file_exists($directorioArchivos)) {
							mkdir('./uploads/archivos/socioEconomicoDoc/'.$_POST['id'], 0777, true);
						}


						$url2 = 'uploads/archivos/socioEconomicoDoc/'.$_POST['id'].'/'.$file_name_poligrafos;
						move_uploaded_file($_FILES['file-input-socioEconomico']['tmp_name'], $url2);


						$dataDoc2 = array(
						               'socioEconomicoDoc' => $url2,
						               'estatusSocioDoc' => $_POST['Entregasocioeconomico']
						            );

						$this->db->where('id', $_POST['id']);
						$this->db->update('Evaluados', $dataDoc2);


					}else{


							$dataDoc2 = array(
						               'estatusSocioDoc' => $_POST['Entregasocioeconomico']
						            );

							$this->db->where('id', $_POST['id']);
							$this->db->update('Evaluados', $dataDoc2);



					}


			redirect('documentos/evaluacion');


	}

	public function deleteEvaluado(){

			$data['Estatus']    	= 8;

			$where['id'] = $_POST['eliminarID'];
			$this->documentos_model->updDocumentoEvaluacion($data,$where);

			redirect('documentos/evaluacion');
	}


	public function resultadosExamen($id){
				

				$data = array();
				$where['id'] = $id;
				$evaluados         = $this->documentos_model->getOneDocumentoEvaluacion($where);
				$data['Evaluados']  = $evaluados;
				//print_r($data['Evaluados']);

				$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();
			
				$where2['idEvaluado'] = $id;
				$resultados         = $this->documentos_model->getAllDocumentoResultados($where2);


				$data['EstatusGeneral']   = $this->catalogos_model->getAllEstatus();


				$data['Freelance']   = $this->catalogos_model->getAllFreelance();


				
				$Certificaciones = $this->catalogos_model->getAllCertificaciones();
				$data['Certificaciones'] = $Certificaciones;
			

				if($data['Evaluados']['Estatus'] == '' || $data['Evaluados']['Estatus'] == 1){
					$dataEstatus = array(
					               'Estatus' => 7
					            );

					$this->db->where('id', $id);
					$this->db->update('Evaluados', $dataEstatus);

					$evaluados         = $this->documentos_model->getOneDocumentoEvaluacion($where);
					$data['Evaluados']  = $evaluados;

				}	

					


				if(!empty($resultados)){

						$whereCertificado['idCertificacion'] = $data['Evaluados']['idCertificado'];
						$Servicios = $this->catalogos_model->getAllServicios($whereCertificado);
				


						$contador = 1;
						
						$arraryNoMostrar = array();

						foreach ($resultados as $key => $value) {

							if($contador == 1 ){
								$ButoonMenos = ' onclick="MenosFilaResultados('.$contador.'); return false ;" disabled="disabled" ' ;	
							} else {
									$ButoonMenos = ' onclick="MenosFilaResultados('.$contador.'); return false ;" ' ;	
							}



							
							$selectServicio = "";
							$selectServicio = "<option value=''> Elige</option>";

							


							foreach ($Servicios as $key2 => $value2) {
						 	 	
						 	 	if(!in_array($value2['id'], $arraryNoMostrar)){
						 	 	
							 	 	if($value2['id'] == $value['idServicio']){
							 	 		$selectServicio .= "
								 	     <option value='".$value2['id']."' selected> ".$value2['Servicio']."</option>
								 	     ";
							 	 	}else{
							 	 			$selectServicio .= "
									 	     <option value='".$value2['id']."'> ".$value2['Servicio']."</option>
									 	     ";
							 	 	}
							 	}


							 }

							 $arraryNoMostrar[$contador] = $value['idServicio'];


							 if($value['EstatusServicios'] == 1){

							 		$selectServicioEstatus = "
							 			<select id='statusServicio_".$contador."' name='statusServicio_".$contador."'>
							 		  		  <option value='0'>Elige</option>
							 		  		  <option value='1' selected>Aprobado</option>
							 		  		  <option value='2'>No Aprobado</option>
							 		  		  <option value='3'>Supervision</option>
							 		  		</select>
							 		";

							 }elseif($value['EstatusServicios'] == 2){
							 		$selectServicioEstatus = "
							 			<select id='statusServicio_".$contador."' name='statusServicio_".$contador."'>
							 		  		  <option value='0'>Elige</option>
							 		  		  <option value='1'>Aprobado</option>
							 		  		  <option value='2' selected>No Aprobado</option>
							 		  		   <option value='3'>Supervision</option>
							 		  		</select>
							 		";

							 }elseif($value['EstatusServicios'] == 3){
							 		$selectServicioEstatus = "
							 			<select id='statusServicio_".$contador."' name='statusServicio_".$contador."'>
							 		  		  <option value='0'>Elige</option>
							 		  		  <option value='1'>Aprobado</option>
							 		  		  <option value='2'>No Aprobado</option>
							 		  		  <option value='3' selected>Supervision</option>
							 		  		</select>
							 		";

							 }else{

							 	$selectServicioEstatus = "
							 			<select id='statusServicio_".$contador."' name='statusServicio_".$contador."'>
							 		  		  <option value='0' selected>Elige</option>
							 		  		  <option value='1'>Aprobado</option>
							 		  		  <option value='2' >No Aprobado</option>
							 		  		  <option value='3'>Supervision</option>
							 		  		</select>
							 		";

							 }




							 $html .= "
							 		<tr id='fila_cotizacion_".$contador."'>
							 		  <td>
							 		  		<select id='servicio_".$contador."' name='servicio_".$contador."'>
							 		  		".$selectServicio."
							 		  		</select>
							 		  </td>
							 		  <td>
							 		  		".$selectServicioEstatus."
							 		  </td>
							 		  <td>
							 		  <textarea id='comentario_".$contador."' class='span12' name='comentario_".$contador."'>".$value['Comentarios']."</textarea>
							 		  </td>
							 		  <td>
							 		  	<input style='width : 18px;' type='button' value='-' ".$ButoonMenos.">&nbsp;&nbsp;
					            		<input style='width : 18px;' type='button' value='+' onclick='MasFilaResultado(".$contador."); return false ;'>&nbsp;&nbsp;
							 		  </td>
							 		</tr>
							 	";

							 	$contador ++;

						}

						$data['htmlTr'] = $html;


				}else{

					$contador = 1;
				$ButoonMenos = ' onclick="MenosFilaResultados('.$contador.'); return false ;" disabled="disabled" ' ;
					
				
						$whereCertificado['idCertificacion'] = $data['Evaluados']['idCertificado'];
						$Servicios = $this->catalogos_model->getAllServicios($whereCertificado);
						$selectServicio = "<option value=''> Elige</option>";

					

						 foreach ($Servicios as $key => $value) {
						 	  $selectServicio .= "
						 	  <option value='".$value['id']."'> ".$value['Servicio']."</option>
						 	  ";
						 }

					 	 $html = "
					 		<tr id='fila_cotizacion_1'>
					 		  <td>
					 		  		<select id='servicio_1' name='servicio_1'>
					 		  		".$selectServicio."
					 		  		</select>
					 		  </td>
					 		  <td>
					 		  		<select id='statusServicio_1' name='statusServicio_1'>
					 		  		  <option value='0'>Elige</option>
					 		  		  <option value='1'>Aprobado</option>
					 		  		  <option value='2'>No Aprobado</option>
					 		  		  <option value='3' selected>Supervision</option>
					 		  		</select>
					 		  </td>
					 		  <td>
					 		  <textarea id='comentario_1' class='span12' name='comentario_1'></textarea>
					 		  </td>
					 		  <td>
					 		  	<input style='width : 18px;' type='button' value='-' ".$ButoonMenos.">&nbsp;&nbsp;
			            		<input style='width : 18px;' type='button' value='+' onclick='MasFilaResultado(".$contador."); return false ;'>&nbsp;&nbsp;
					 		  </td>
					 		</tr>
					 	";


					 	$data['htmlTr'] = $html;

				}

				$this->parser->parse('Documentos/resultados', $data);

	}	



		public function filasResultado(){
		$NumFila = $_POST['NumFila'];
		$contador = $_POST['NumFila'];
		$resultadosRenglon = $_POST['resultadosRenglon'];

		

		if(strpos($resultadosRenglon,'-')){
			$EsArrary = 'Si';
			$validarRenglon = explode('-', $resultadosRenglon);



		}else{
			$EsArrary = 'No';
			$validarRenglon = $resultadosRenglon;
		}


		if($_POST['NumFila'] == '1' ){
			$ButoonMenos = ' onclick="MenosFilaPedidos('.$_POST['NumFila'].'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaPedidos('.$_POST['NumFila'].'); return false ;" ' ;	
			}
			if( ($_POST['NumFila']%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
			
			$whereCertificado['idCertificacion'] = $_POST['certificacionId'];
			$Servicios = $this->catalogos_model->getAllServicios($whereCertificado);
						$selectServicio = "<option value=''> Elige</option>";

					

						 foreach ($Servicios as $key => $value) {

							 	
						 	if($EsArrary == 'Si'){

						 		if(!in_array($value['id'], $validarRenglon)){

							 	  $selectServicio .= "
							 	  <option value='".$value['id']."'> ".$value['Servicio']."</option>
							 	  ";

							 	}


						 	}else{

						 		if($validarRenglon != '' && $validarRenglon != $value['id']){

							 	  $selectServicio .= "
							 	  <option value='".$value['id']."'> ".$value['Servicio']."</option>
							 	  ";

							 	}

						 	}

							 	


						 }


			echo '
	  <tr style="color:#000;" id="fila_cotizacion_'.$_POST ['NumFila'].'" >
	    
	     	  <td>
					 <select id="servicio_'.$_POST['NumFila'].'" name="servicio_'.$_POST['NumFila'].'">
					 		  		'.$selectServicio.'
					 </select>
			  </td>
			 
	  
	 
	     	<td  style="text-align: center" >
	     		
					<select id="statusServicio_'.$_POST['NumFila'].'" name="statusServicio_'.$_POST['NumFila'].'">
					    <option value="0">Elige</option>
					    <option value="1">Aprobado</option>
					 	<option value="2">Reprobado</option>
					 	<option value="3">Supervision</option>
					</select>
					 		
			 
	     	</td>

	     	<td  style="text-align: center" >
	     			<textarea id="comentario_'.$_POST['NumFila'].'" class="span12" name="comentario_'.$_POST['NumFila'].'"></textarea>
			 
	     	</td>

	     	<td  style="text-align: center" >
	     		<input style="width : 18px;" type="button" value="-" '.$ButoonMenos.'>&nbsp;&nbsp;
			     <input style="width : 18px;" type="button" value="+"" onclick="MasFilaResultado('.$contador.'); return false ;">&nbsp;&nbsp;
			 
	     	</td>

	     
	 
	      </tr> 	
		' ;
	}

	public function guardarResultados(){

				
			$dataEstatus = array(
					               'Estatus' => $_POST['EstatusGeneral'],
					               'EstatusFecha' => date('Y-m-d')
					            );

			$this->db->where('id', $_POST['id']);
			$this->db->update('Evaluados', $dataEstatus);


			$whereId['idEvaluado'] = $_POST['id'];
			$this->documentos_model->dropDocumentoResultados($whereId);


			foreach($_POST as $key => $value){
				if(strstr($key , 'servicio_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

			$statusReprobado = "No";
			$contadorAprobado = 0;
			$contadorSupervisor = 0;

			for($i=1; $i<=count($_Data_); $i++){


					if($_POST['statusServicio_'.$i] == '2'){
							$statusReprobado = 'Si';
					}

					if($_POST['statusServicio_'.$i] == '1'){
						$contadorAprobado ++;
					}

					if($_POST['statusServicio_'.$i] == '3'){
						$contadorSupervisor ++;
					}


					$data['idEvaluado']    		= $_POST['id'];
					$data['idServicio']   		= $_POST['servicio_'.$i];
					$data['EstatusServicios']   = $_POST['statusServicio_'.$i];
					$data['Comentarios']    	= $_POST['comentario_'.$i];


					$this->documentos_model->addDocumentoResultados($data);


		    }


		    if($statusReprobado == 'Si'){
		    		$dataEstatus2 = array(
					               'Estatus' => 3
					            );

					$this->db->where('id', $_POST['id']);
					$this->db->update('Evaluados', $dataEstatus2);
		    }

		    if($contadorSupervisor > 0){

		    	$dataEstatus2 = array(
					               'Estatus' => 4
					            );

					$this->db->where('id', $_POST['id']);
					$this->db->update('Evaluados', $dataEstatus2);

		    }

		    if($contadorAprobado > 0 && $contadorAprobado == count($_Data_) && $contadorSupervisor == 0){

		    	$dataEstatus2 = array(
					               'Estatus' => 2
					            );

					$this->db->where('id', $_POST['id']);
					$this->db->update('Evaluados', $dataEstatus2);
		    }




		    redirect('documentos/evaluacion');
			
	}

	public function facturaNueva(){

			$data['fecha'] = date('Y-m-d');
		  $this->parser->parse('Documentos/facturaNueva', $data);

	}

	public function reciboNuevo(){

			$data['fecha'] = date('Y-m-d');
		  $this->parser->parse('Documentos/reciboNueva', $data);

	}

	public function guardarRecibos(){

		    $data['idCliente']    		= $_POST['IdEmpresaSecundaria'];
			$data['NombreCliente']    	= $_POST['Nombres'];
			$data['DireccionCliente']   = $_POST['DomicilioFiscal'];
			$data['Rfc']    			= $_POST['Rfc'];
			$data['Comentarios']    	= $_POST['comentario'];
			$data['Estatus']    		= $_POST['estatusDocumento'];	
			$data['Fecha']    			= date('Y-m-d');
			$data['idUsuarioRegistro']  = $_SESSION['SEUS']['use_id'];
			$data['formaPago']    		= $_POST['formaPago'];
			$data['CuentaBancariaCliente']    		= $_POST['CuentaBancariaCliente'];
			$data['bancoCliente']    		= $_POST['bancoCliente'];

		

			$Respuesta = $this->documentos_model->addDocumentoEncabezadoRecibo($data);



			foreach($_POST as $key => $value){
				if(strstr($key , 'cantidad_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

			$total = 0;
		
			for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEncabezado']    = $Respuesta;
					$dataDetalle['idServicio']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['Servicio']        = $_POST['servicio_'.$i];
					$dataDetalle['Cantidad']    	= $_POST['cantidad_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];
					$dataDetalle['Importe']    		= $_POST['importe_'.$i];

					$total += ($_POST['cantidad_'.$i] * $_POST['precio_'.$i]);

					$this->documentos_model->addDocumentoDetalleRecibo($dataDetalle);


		    }		

		    		
		    		 $MegaTotal = ($total  * 0.16) + $total;
		   			 //$MegaTotal = ($total);
		    	
		    	
		    		$data2 = array(
					               'Total' => $MegaTotal,
					               'PorPagar' => $MegaTotal
					            );

		    		$this->db->where('id',$Respuesta);
					$this->db->update('DocumentoEncabezadoRecibo', $data2);
				
		  

					$evaluados = explode(',',$_POST['evaluadosCheck']);

					foreach ($evaluados as $key => $value) {
					
						if($value != 0){

							$data2 = array(
					               'idEvaluado' => $value,
					               'idFactura' => $Respuesta,
					               'Documento' => 'Recibo'
					            );


							$this->db->insert('FacturaEvaluado', $data2);

						}
					}		
				
					

					redirect('documentos/Recibos');

	}

	public function guardarFacturacion(){
		
		    $data['idCliente']    		= $_POST['IdEmpresaSecundaria'];
			$data['NombreCliente']    	= $_POST['Nombres'];
			$data['DireccionCliente']   = $_POST['DomicilioFiscal'];
			$data['Rfc']    			= $_POST['Rfc'];
			$data['Comentarios']    	= $_POST['comentario'];
			$data['Estatus']    		= $_POST['estatusDocumento'];	
			$data['Fecha']    			= date('Y-m-d');
			$data['idUsuarioRegistro']  = $_SESSION['SEUS']['use_id'];
			$data['formaPago']    		= $_POST['formaPago'];
			$data['CuentaBancariaCliente']    		= $_POST['CuentaBancariaCliente'];
			$data['bancoCliente']    		= $_POST['bancoCliente'];

		

			$Respuesta = $this->documentos_model->addDocumentoEncabezado($data);



			foreach($_POST as $key => $value){
				if(strstr($key , 'cantidad_') ){
					$Explode = explode('_' , $key);
					$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
				}
			}

			$total = 0;
		
			for($i=1; $i<=count($_Data_); $i++){

					$dataDetalle['idEncabezado']    = $Respuesta;
					$dataDetalle['idServicio']    	= $_POST['id_servicio_'.$i];
					$dataDetalle['Servicio']        = $_POST['servicio_'.$i];
					$dataDetalle['Cantidad']    	= $_POST['cantidad_'.$i];
					$dataDetalle['Precio']    		= $_POST['precio_'.$i];
					$dataDetalle['Importe']    		= $_POST['importe_'.$i];

					$total += ($_POST['cantidad_'.$i] * $_POST['precio_'.$i]);

					$this->documentos_model->addDocumentoDetalle($dataDetalle);


		    }		

		    		
		    		 $MegaTotal = ($total  * 0.16) + $total;
		   			 //$MegaTotal = ($total);
		    	
		    	
		    		$data2 = array(
					               'Total' => $MegaTotal,
					               'PorPagar' => $MegaTotal
					            );

		    		$this->db->where('id',$Respuesta);
					$this->db->update('DocumentoEncabezado', $data2);
				
		  

					$evaluados = explode(',',$_POST['evaluadosCheck']);

					foreach ($evaluados as $key => $value) {
					
						if($value != 0){

							$data2 = array(
					               'idEvaluado' => $value,
					               'idFactura' => $Respuesta,
					               'Documento' => "Factura"

					            );


							$this->db->insert('FacturaEvaluado', $data2);

						}
					}		
				
					

				


							foreach($_POST as $key => $value){
								if(strstr($key , 'cantidad_') ){
									$Explode = explode('_' , $key);
									$_Data2_[] = (int) $Explode[(count($Explode)-1)] ;
								}
							}

					


							$total = 0;
							$subtotal = 0;
							$item = array();
							$detail = array();
							for($i=1; $i<=count($_Data2_); $i++){

									$item['iva']    		   = '16';
									$item['orderDetailID']   = rand(1,1000000);
									$item['orderID']    	   = rand(1,1000000);
									$item['price']    	   = $_POST['precio_'.$i];
									$item['productCode']     = 'N/A';
									$item['productName']     = $_POST['servicio_'.$i];
									$item['quantity']    	   = $_POST['cantidad_'.$i];
									$item['total']    	   = $_POST['importe_'.$i];
									$item['unitMeasure']     = 'SERVICIO';
									array_push($detail, $item);

									$subtotal += $_POST['importe_'.$i];
						    }

						    $total =  ($subtotal * 0.16) + $subtotal;
						    $iva   =  ($subtotal * 0.16);
						    $where['id'] = $_POST['IdEmpresaSecundaria'];
						    $EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where);

						    if($EmpresasSec['numeroIntFiscal'] != '' && $EmpresasSec['numeroIntFiscal'] != '-'){
						    	$numInterior = $EmpresasSec['numeroIntFiscal'];
						    }else{
						    	$numInterior = '';
						    }
							
							if($_POST['formaPago'] == 98){
								$varFormaPago = $_POST['formaPago'];
							}else{
								$varFormaPago = $_POST['formaPago'];
							
							}


						    $ap_param = 
						array(							
							'accountNumber' => 'NO IDENTIFICADO', 
							'authorID' => 0, 
							'customerAddress' => $EmpresasSec['calleFiscal'], 
							'customerCity' => $EmpresasSec['municipioFiscal'],
							'customerColony' => $EmpresasSec['ColoniaFiscal'],
							'customerCountry' => 'Mexico',
							'customerEmail' => $EmpresasSec['CorreoFactuas'],
							'customerExtNumber' => $EmpresasSec['numroExtFiscal'],
							'customerIntNumber' => $numInterior,
							'customerName' => $EmpresasSec['RazonSocial'],
							'customerRFC' => $EmpresasSec['Rfc'],
							'customerState' => $EmpresasSec['estadoFiscal'],
							'customerStreet' => $EmpresasSec['calleFiscal'],
							'customerZipCode' => $EmpresasSec['codigoPostal'],
							'detail' => ($detail),
							'folio' => $Respuesta,
							'invoiceID' => $Respuesta.rand(1,1000000),
							'iva' => $iva,
							'paymentCondition' => 'PAGO EN UNA SOLA EXHIBICIÓN',
							'paymentMethodSat' => $varFormaPago,
							'subTotal' => $subtotal,
							'total' => $total,
							);	

						
							//echo $ver  = json_encode($ap_param);
							//die;
							$wsFact = array(
										'invoice' => json_encode($ap_param),
										'companyID' => 2);

					
						//error_reporting(E_ALL);
						//ini_set('display_errors', 1);

						require("./application/libraries/lib/nusoap.php");
						$client = new nusoap_client('http://162.221.187.226/stamping/stamping.asmx?wsdl','wsdl');

						$result = $client->call('getStamping',$wsFact);
						
						$test = json_decode(utf8_encode($result['getStampingResult']));

						file_put_contents('uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$Respuesta.'.xml', print_r($test->xml, true));

						 $filenameXML = 'uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$Respuesta.'.xml';
						

						$dom = new DOMDocument('1.0','utf-8'); // Creamos el Objeto DOM
				         $dom->load('uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$Respuesta.'.xml'); // Definimos la ruta de nuestro XML
				         // Recorremos el XML Tag por Tag para encontrar los elementos buscados
				         // Obtenemos el Machote(Estructura) del XML desde la web de SAT 
				         foreach ($dom->getElementsByTagNameNS('http://www.sat.gob.mx/TimbreFiscalDigital', '*') as $elemento) {
				             $UUID             = $elemento->getAttribute('UUID'); 
				             $noCertificadoSAT = $elemento->getAttribute('noCertificadoSAT'); 
				             $FechaTimbrado    = $elemento->getAttribute('FechaTimbrado'); 
				             $selloCFD         = $elemento->getAttribute('selloCFD');
				             $selloSAT         = $elemento->getAttribute('selloSAT');
				             $version         = $elemento->getAttribute('version');
				         
				            
				            
				         }	

		

						$dataF = array(
					               'Estatus' => $_POST['estatusDocumento'],
					               'formaPago' => $_POST['formaPago'],
					               'FolioFactura' => $UUID,
					               'SelloDigitalCFDI' => $selloCFD,
					               'SelloSat' => $selloSAT,
					               'CadenaComplemento' => '||'.$version.'|'.$UUID.'|'.$FechaTimbrado.'|'.$selloCFD,
					               'NoCertificacionSAT' => $noCertificadoSAT,
					               'FechaHoraCertificacion' => $FechaTimbrado,
					            );

						$this->db->where('id', $Respuesta);
						$this->db->update('DocumentoEncabezado', $dataF);






						//// PDF
						require_once('./application/third_party/dompdf/dompdf_config.inc.php');
						$wherePDF = "id =".$Respuesta;
						$datapdf['Documentos']  = $this->documentos_model->getOneDocumentoEncabezado($wherePDF);

						$wherePDF2 = "id =".$datapdf['Documentos']['idCliente'];
						$EmpresasSecPDF        = $this->catalogos_model->getOneEmpresaSecundaria($wherePDF2);
						$datapdf['Documentos']['Telefono'] = $EmpresasSecPDF['Telefono'];
						$datapdf['Documentos']['Rfc'] = $EmpresasSecPDF['Rfc'];
						$datapdf['Documentos']['NombreCliente'] = $EmpresasSecPDF['RazonSocial'];

						if($EmpresasSecPDF['numeroIntFiscal'] != '' && $EmpresasSecPDF['numeroIntFiscal'] != '-'){
							$intNum = $EmpresasSecPDF['numeroIntFiscal'].', ';
						}else{
							$intNum = '';
						}

						$datapdf['Documentos']['DireccionCliente'] = $EmpresasSecPDF['calleFiscal'].' #'.$EmpresasSecPDF['numroExtFiscal'].', '.$intNum.' colonia '.$EmpresasSecPDF['ColoniaFiscal'].', '.$EmpresasSecPDF['municipioFiscal'].', '.$EmpresasSecPDF['estadoFiscal'].', CP. '.$EmpresasSecPDF['codigoPostal'];



						if($EmpresasSecPDF['Vendedor'] != '' && $EmpresasSecPDF['Vendedor'] != 0){
							$wherePDF3 = "id =".$EmpresasSecPDF['Vendedor'];
							$Vendedores        = $this->catalogos_model->getOneVendedores($wherePDF3);
							$datapdf['Documentos']['VendedoresNombre'] = $EmpresasSecPDF['Vendedor'];
						}else{
							$datapdf['Documentos']['VendedoresNombre'] = 'N/A';
						}

						$datapdf['Documentos']['SelloDigitalCFDI'];

						$queryEva = "SELECT Evaluados.* FROM FacturaEvaluado INNER JOIN Evaluados ON Evaluados.id = FacturaEvaluado.idEvaluado  WHERE  FacturaEvaluado.idFactura = '".$Respuesta."'";

						$evaluados = $this->db->query( $queryEva )->result_array(); 
						
						foreach ($evaluados as $key => $value) {
								$informacionHtml .= '
									<tr>
										<td width="100%">'.$value['Nombre'].'</td>				
									</tr>
								';
						}

						$datapdf['informacionHtml'] = $informacionHtml;

						$wherePDF4 = "idEncabezado =".$Respuesta;
						$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle($wherePDF4);

						$datapdf['DocumentosDetalle'] =$DocumentosDetalle;

						$datapdf['DOCUMENT_ROOT'] = $_SERVER["DOCUMENT_ROOT"];

						$arraryP = explode('/',$datapdf['Documentos']['SelloDigitalCFDI']);
					
						$datapdf['arrayCFDI'] =  $arraryP;

				   		$codigo = $this->parser->parse('Documentos/factura_PDF', $datapdf);
				   		$dompdf = new DOMPDF();
						$dompdf->load_html(($codigo));
						ini_set('memory_limit', '-1');
						$dompdf->render();

						$pdf = $dompdf->output();
						file_put_contents('uploads/facturas/'.$EmpresasSecPDF['Rfc'].'-'.$Respuesta.'.pdf', $pdf);

					
						sleep(15);
						error_reporting(E_ALL);
						ini_set('display_errors', 1);
						$this->load->library('email');
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = "html";
						$this->email->initialize($config);
						$filenamePDF = 'uploads/facturas/'.$EmpresasSecPDF['Rfc'].'-'.$Respuesta.'.pdf';
						$this->email->attach($filenamePDF);
						$this->email->attach($filenameXML);

						$asunto = 'CFDI Factura FA '.$Respuesta;
						$mensaje = 'Agradecemos su preferencia.	
							<br>
							En este correo se adjuntan los archivos PDF y XML de su factura
						';
					    $email = $EmpresasSec['CorreoFactuas'];
						
						$this->email->from('no-replay@sistemaopalo.com', 'SISTEMA FACTURACION OPALO');
				        $this->email->to($email);
				        $this->email->bcc('administracion@sistemaopalo.com.mx');
				        $this->email->reply_to('jessep.barba@gmail.com', 'Jessep');
				        $this->email->subject($asunto);
				        $this->email->message($mensaje);
				        
				        $this->email->send();

		
						////
				

				





					redirect('documentos/facturacionRecibos');

	}

	public function facturacionRecibos(){


		$orderBy                       = array (
		                                   'DocumentoEncabezado.id' => 'DESC'
		                                 );

		//$where = "Estatus ='1' OR Estatus ='2' OR Estatus ='3'";
		$where = "Estatus ='1' OR Estatus ='2'";
		$select =  "DocumentoEncabezado.id, DocumentoEncabezado.NombreCliente, DocumentoEncabezado.Rfc, DocumentoEncabezado.FolioFactura, DocumentoEncabezado.Fecha, DocumentoEncabezado.Total, DocumentoEncabezado.Estatus, DocumentoEncabezado.Cancelada";
		$data['Documentos']        = $this->documentos_model->getAllDocumentoEncabezado($where, $select,null,$orderBy);


		

		if(isset($_SESSION['FacturaCancelada'])){
				$data['FacturaCancelada2'] = '1';

				unset($_SESSION['FacturaCancelada']);
		}else{
				$data['FacturaCancelada2'] = '0';
		}

		

		$this->parser->parse('Documentos/GridDocumentosFactura', $data);
	}


	public function Recibos(){


		$orderBy                       = array (
		                                   'DocumentoEncabezadoRecibo.id' => 'DESC'
		                                 );

		$where = "Estatus ='1' OR Estatus ='3'";
		$select =  "DocumentoEncabezadoRecibo.id, DocumentoEncabezadoRecibo.NombreCliente, DocumentoEncabezadoRecibo.Rfc, DocumentoEncabezadoRecibo.FolioFactura, DocumentoEncabezadoRecibo.Fecha, DocumentoEncabezadoRecibo.Total, DocumentoEncabezadoRecibo.Estatus, DocumentoEncabezadoRecibo.Cancelada";
		$data['Documentos']        = $this->documentos_model->getAllDocumentoEncabezadoRecibo($where, $select,null,$orderBy);


		

		if(isset($_SESSION['FacturaCancelada'])){
				$data['FacturaCancelada2'] = '1';

				unset($_SESSION['FacturaCancelada']);
		}else{
				$data['FacturaCancelada2'] = '0';
		}

		

		$this->parser->parse('Documentos/GridDocumentosFacturaRecibo', $data);
	}

	public function verDocumento($id){

			$renglones = '';
					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado($where);

		

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle($where2);

			$totalFilas = count($DocumentosDetalle);
			$data['totalFilas'] = $totalFilas + 1 ;
			
		$contador = 1;
		foreach ($DocumentosDetalle as $key => $value) {
				
			if($contador == 1 ){
			$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" ' ;	
			}


			if( ($contador%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
		
				 $renglones .= '
				  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
						  <input type="text" class="span11"  id="servicio_'.$contador.'"  name="servicio_'.$contador.'" value="'.$value['Servicio'].'" readonly/>
						   <input type="hidden" class="span11"  id="id_servicio_'.$contador.'"  name="id_servicio_'.$contador.'" value="'.$value['idServicio'].'" readonly/>
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="cantidad_'.$contador.'"  name="cantidad_'.$contador.'" value="'.$value['Cantidad'].'" readonly/>
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="precio_'.$contador.'"  name="precio_'.$contador.'" value="'.$value['Precio'].'" readonly/>
				     	</td>
				     	<td  style="text-align: center" >

						  <input type="text" class="span10"  id="importe_'.$contador.'"  name="importe_'.$contador.'" value="'.$value['Importe'].'" readonly/>
				     	</td>
				 
				      </tr> 	
					' ;

					$contador++;

			}


			$query = "SELECT * FROM Evaluados INNER JOIN FacturaEvaluado ON FacturaEvaluado.idEvaluado =  Evaluados.id WHERE FacturaEvaluado.idFactura = '".$id."'  AND Documento = 'Factura' ";
		    $evaluados = $this->db->query( $query )->result_array(); 


	

		    if(!empty($evaluados)){

		    	$cadenaEvaluado = '';
		    	foreach ($evaluados as $key => $value) {

		    				$cadenaEvaluado .= $value['id'].",";

							$informacionHtml .= '
								<tr>
																	
									<td width="25%" align="center"> <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="'.base_url().$value['Foto'].'" /></td>
									<td width="50%">'.$value['Nombre'].'</td>
													
								</tr>
							';

						

					}


				$cadenaEvaluado .='0';

				$data['cadenaEvaluado'] = $cadenaEvaluado;

				 $html = '<form> <table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">	
				  		<input type="hidden" id="numeroEvaluados" name="numeroEvaluados" value="'.$contador.'">	
								<thead>
									<tr>
																	
									<th width="25%"> Foto</th>
										<th width="50%">Nombre evaluado</th>			
									</tr>
					</thead>
					'.$informacionHtml.'
					</table></form> ';

				$data['Evaluados'] = $html;

		    }else{
		    	$data['Evaluados'] = '';
		    	$data['cadenaEvaluado'] = '';

		    }


			$data['DocumentosDetalle'] = $renglones;


			$this->parser->parse('Documentos/facturacionReciboDetalle', $data);

	}



	public function verDocumentoRecibo($id){

			$renglones = '';
					
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezadoRecibo($where);

		

			$where2 = "idEncabezado =".$id;
			$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalleRecibo($where2);

			$totalFilas = count($DocumentosDetalle);
			$data['totalFilas'] = $totalFilas + 1 ;
			
		$contador = 1;
		foreach ($DocumentosDetalle as $key => $value) {
				
			if($contador == 1 ){
			$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" disabled="disabled" ' ;	
			} else {
				$ButoonMenos = ' onclick="MenosFilaPedidosEditar('.$contador.'); return false ;" ' ;	
			}


			if( ($contador%2) == 0 ){
				$color = 'ffffff';
			} else {
				$color = 'e4eff8';
			}	
		
				 $renglones .= '
				  <tr bgcolor="#'.$color.'" style="color:#000;" id="fila_cotizacion_'.$contador.'" >
				     
				     	<td  style="text-align: center" >
						  <input type="text" class="span11"  id="servicio_'.$contador.'"  name="servicio_'.$contador.'" value="'.$value['Servicio'].'" readonly/>
						   <input type="hidden" class="span11"  id="id_servicio_'.$contador.'"  name="id_servicio_'.$contador.'" value="'.$value['idServicio'].'" readonly/>
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="cantidad_'.$contador.'"  name="cantidad_'.$contador.'" value="'.$value['Cantidad'].'" readonly/>
				     	</td>

				     	<td  style="text-align: center" >

						  <input type="text" class="span10" onkeyup="CalcularFilaCotizacion('.$contador.');" id="precio_'.$contador.'"  name="precio_'.$contador.'" value="'.$value['Precio'].'" readonly/>
				     	</td>
				     	<td  style="text-align: center" >

						  <input type="text" class="span10"  id="importe_'.$contador.'"  name="importe_'.$contador.'" value="'.$value['Importe'].'" readonly/>
				     	</td>
				 
				      </tr> 	
					' ;

					$contador++;

			}


			$query = "SELECT * FROM Evaluados INNER JOIN FacturaEvaluado ON FacturaEvaluado.idEvaluado =  Evaluados.id WHERE FacturaEvaluado.idFactura = '".$id."' AND Documento = 'Recibo' ";
		    $evaluados = $this->db->query( $query )->result_array(); 


	

		    if(!empty($evaluados)){

		    	$cadenaEvaluado = '';
		    	foreach ($evaluados as $key => $value) {

		    				$cadenaEvaluado .= $value['id'].",";

							$informacionHtml .= '
								<tr>
																	
									<td width="25%" align="center"> <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="'.base_url().$value['Foto'].'" /></td>
									<td width="50%">'.$value['Nombre'].'</td>
													
								</tr>
							';

						

					}


				$cadenaEvaluado .='0';

				$data['cadenaEvaluado'] = $cadenaEvaluado;

				 $html = '<form> <table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">	
				  		<input type="hidden" id="numeroEvaluados" name="numeroEvaluados" value="'.$contador.'">	
								<thead>
									<tr>
																	
									<th width="25%"> Foto</th>
										<th width="50%">Nombre evaluado</th>			
									</tr>
					</thead>
					'.$informacionHtml.'
					</table></form> ';

				$data['Evaluados'] = $html;

		    }else{
		    	$data['Evaluados'] = '';
		    	$data['cadenaEvaluado'] = '';

		    }


			$data['DocumentosDetalle'] = $renglones;


			$this->parser->parse('Documentos/facturacionReciboDetalleRecibo', $data);

	}


	public function cancelFacturas(){
		

		 $where = "id ='".$_POST['eliminarFactura']."'";
		 $select =  "*";
		 $data['Documentos']        = $this->documentos_model->getAllDocumentoEncabezado($where, $select);


		 $wsFact = array(			
							'uuid' => strtoupper($data['Documentos'][0]['FolioFactura']) ,
							'companyID' => 2,
							'rfc' => 'SOB151104AS6'
							);


   			require("./application/libraries/lib/nusoap.php");
			$client = new nusoap_client('http://162.221.187.226/stamping/stamping.asmx?wsdl','wsdl');
			$result = $client->call('cancelCFDI',$wsFact);


				$data = array(
					               'Cancelada' => 'x',
					               
					            );

						$this->db->where('id', $_POST['eliminarFactura']);
						$this->db->update('DocumentoEncabezado', $data);

			session_start();
			$_SESSION['FacturaCancelada'] = 1;	


			$this->db->where('idFactura', $_POST['eliminarFactura']);
			$this->db->where('Documento','Factura');
			$this->db->delete('FacturaEvaluado'); 
			sleep(5);

			redirect('documentos/facturacionRecibos');

		  
	}


	public function mostrarEvaluados(){

		



			 $query = "SELECT * FROM Evaluados WHERE  idEmpresa = '".$_POST['IdEmpresaSecundaria']."' AND Evaluados.id NOT IN (SELECT idEvaluado FROM FacturaEvaluado)";
	
			$evaluados = $this->db->query( $query )->result_array(); 

			$informacionHtml = '';
			$contador = 0;
			foreach ($evaluados as $key => $value) {
					$informacionHtml .= '
						<tr>
															
							<td width="25%" align="center"> <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="'.base_url().$value['Foto'].'" /></td>
							<td width="50%">'.$value['Nombre'].'</td>
							<td width="10%"> <input style="opacity: 2.5;"  type="checkbox" name="seleccionaEvaluado_'.$contador.'" value="'.$value['id'].'" />
							</td>					
						</tr>
					';

					$contador ++;

			}

		  $html = '<form> <table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">	
		  		<input type="hidden" id="numeroEvaluados" name="numeroEvaluados" value="'.$contador.'">	
						<thead>
							<tr>
															
							<th width="25%"> Foto</th>
							<th width="50%">Nombre evaluado</th>
							<th width="10%"></th>					
							</tr>
			</thead>
			'.$informacionHtml.'
			</table></form> ';

			$this->output
	        ->set_content_type('application/html')
	        ->set_output($html);

	}


	public function ponerEvaluados(){

		

		 $query = "SELECT * FROM Evaluados WHERE id in (".$_POST['idEvaluados']."0)";
		 $evaluados = $this->db->query( $query )->result_array(); 


		 foreach ($evaluados as $key => $value) {
					$informacionHtml .= '
						<tr>
															
							<td width="25%" align="center"> <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="'.base_url().$value['Foto'].'" /></td>
							<td width="50%">'.$value['Nombre'].'</td>
											
						</tr>
					';

				

			}


		 $html = '<form> <table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">	
		  		<input type="hidden" id="numeroEvaluados" name="numeroEvaluados" value="'.$contador.'">	
						<thead>
							<tr>
															
							<th width="25%"> Foto</th>
								<th width="50%">Nombre evaluado</th>			
							</tr>
			</thead>
			'.$informacionHtml.'
			</table></form> ';


			$this->output
	        ->set_content_type('application/html')
	        ->set_output($html);

	}


	public function actualizadFacRec(){

		
					if($_POST['estatusDocumento'] == 2){

						$detail = array();

						

						//echo "<pre>";
						//print_r($_POST);
						//echo "</pre>";
						//die;
						foreach($_POST as $key => $value){
								if(strstr($key , 'cantidad_') ){
									$Explode = explode('_' , $key);
									$_Data_[] = (int) $Explode[(count($Explode)-1)] ;
								}
							}

							$total = 0;
							$subtotal = 0;
							$item = array();
							$detail = array();
							for($i=1; $i<=count($_Data_); $i++){

									$item['iva']    		   = '16';
									$item['orderDetailID']   = rand(1,1000000);
									$item['orderID']    	   = rand(1,1000000);
									$item['price']    	   = $_POST['precio_'.$i];
									$item['productCode']     = 'N/A';
									$item['productName']     = $_POST['servicio_'.$i];
									$item['quantity']    	   = $_POST['cantidad_'.$i];
									$item['total']    	   = $_POST['importe_'.$i];
									$item['unitMeasure']     = 'SERVICIO';
									array_push($detail, $item);

									$subtotal += $_POST['importe_'.$i];
						    }

						    $total =  ($subtotal * 0.16) + $subtotal;
						    $iva =  ($subtotal * 0.16);

						$where['id'] = $_POST['IdEmpresaSecundaria'];
						$EmpresasSec        = $this->catalogos_model->getOneEmpresaSecundaria($where);
						
			
					
						 $fecha = new DateTime();
						 $fecha->getTimestamp()-36000;

						error_reporting(E_ALL);
						ini_set('display_errors', 1);

						 
						 if($EmpresasSec['numeroIntFiscal'] != '' && $EmpresasSec['numeroIntFiscal'] != '-'){
						    	$numInterior = $EmpresasSec['numeroIntFiscal'];
						    }else{
						    	$numInterior = '';
						    }

							if($_POST['formaPago'] == 98){
								$varFormaPago = $_POST['formaPago'];
							}else{
								$varFormaPago = $_POST['formaPago'];
							
							}

						$ap_param = 
						array(							
							'accountNumber' => 'NO IDENTIFICADO', 
							'authorID' => 0, 
							'customerAddress' => $EmpresasSec['calleFiscal'], 
							'customerCity' => $EmpresasSec['municipioFiscal'],
							'customerColony' => $EmpresasSec['ColoniaFiscal'],
							'customerCountry' => 'Mexico',
							'customerEmail' => $EmpresasSec['CorreoFactuas'],
							'customerExtNumber' => $EmpresasSec['numroExtFiscal'],
							'customerIntNumber' => $numInterior,
							'customerName' => $EmpresasSec['RazonSocial'],
							'customerRFC' => $EmpresasSec['Rfc'],
							'customerState' => $EmpresasSec['estadoFiscal'],
							'customerStreet' => $EmpresasSec['calleFiscal'],
							'customerZipCode' => $EmpresasSec['codigoPostal'],
							'detail' => ($detail),
							'folio' => $_POST['idCotizacion'],
							'invoiceID' => $_POST['idCotizacion'].rand(1,1000000),
							'iva' => $iva,
							'paymentCondition' => 'PAGO EN UNA SOLA EXHIBICIÓN',
							'paymentMethodSat' => $varFormaPago,
							'subTotal' => $subtotal,
							'total' => $total,
							);	
							//echo $ver  = json_encode($ap_param);
							//die;
							//echo "<br>";
							//print_r($ap_param);
							$wsFact = array(
										'invoice' => json_encode($ap_param),
										'companyID' => 2);

					
						//error_reporting(E_ALL);
						//ini_set('display_errors', 1);

						require("./application/libraries/lib/nusoap.php");
						$client = new nusoap_client('http://162.221.187.226/stamping/stamping.asmx?wsdl','wsdl');

						$result = $client->call('getStamping',$wsFact);
						//var_dump($result);
						//die;
						$test = json_decode(utf8_encode($result['getStampingResult']));

						file_put_contents('uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$_POST['idCotizacion'].'.xml', print_r($test->xml, true));

						$filenameXML = 'uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$_POST['idCotizacion'].'.xml';
						$dom = new DOMDocument('1.0','utf-8'); // Creamos el Objeto DOM
				         $dom->load('uploads/facturas/'.$EmpresasSec['Rfc'].'-'.$_POST['idCotizacion'].'.xml'); // Definimos la ruta de nuestro XML
				         // Recorremos el XML Tag por Tag para encontrar los elementos buscados
				         // Obtenemos el Machote(Estructura) del XML desde la web de SAT 
				         foreach ($dom->getElementsByTagNameNS('http://www.sat.gob.mx/TimbreFiscalDigital', '*') as $elemento) {
				             $UUID             = $elemento->getAttribute('UUID'); 
				             $noCertificadoSAT = $elemento->getAttribute('noCertificadoSAT'); 
				             $FechaTimbrado    = $elemento->getAttribute('FechaTimbrado'); 
				             $selloCFD         = $elemento->getAttribute('selloCFD');
				             $selloSAT         = $elemento->getAttribute('selloSAT');
				             $version         = $elemento->getAttribute('version');
				         
				            
				            
				         }	

						if(isset($_POST['bancoCliente'])){
							$bancoCliente =  $_POST['bancoCliente'];
						}else{
							$bancoCliente =  '';
						}

						$data = array(
					               'Estatus' => $_POST['estatusDocumento'],
					               'formaPago' => $_POST['formaPago'],
					               'FolioFactura' => $UUID,
					               'SelloDigitalCFDI' => $selloCFD,
					               'SelloSat' => $selloSAT,
					               'CadenaComplemento' => '||'.$version.'|'.$UUID.'|'.$FechaTimbrado.'|'.$selloCFD,
					               'NoCertificacionSAT' => $noCertificadoSAT,
					               'FechaHoraCertificacion' => $FechaTimbrado,
					               'CuentaBancariaCliente' => $_POST['CuentaBancariaCliente'],
					               'bancoCliente' => $bancoCliente
					            );

						$this->db->where('id', $_POST['idCotizacion']);
						$this->db->update('DocumentoEncabezado', $data);


						$evaluados = explode(',',$_POST['evaluadosCheck']);

						
						$this->db->where('idFactura', $_POST['idCotizacion']);
						$this->db->delete('FacturaEvaluado'); 
						foreach ($evaluados as $key => $value) {
						

							if($value != 0){

								$data2 = array(
						               'idEvaluado' => $value,
						               'idFactura' => $_POST['idCotizacion'],
						               'Documento' => 'Factura'
						            );


								$this->db->insert('FacturaEvaluado', $data2);

							}
						}

						//// PDF
						require_once('./application/third_party/dompdf/dompdf_config.inc.php');
						$wherePDF = "id =".$_POST['idCotizacion'];
						$datapdf['Documentos']  = $this->documentos_model->getOneDocumentoEncabezado($wherePDF);

						$wherePDF2 = "id =".$datapdf['Documentos']['idCliente'];
						$EmpresasSecPDF        = $this->catalogos_model->getOneEmpresaSecundaria($wherePDF2);
						$datapdf['Documentos']['Telefono'] = $EmpresasSecPDF['Telefono'];
						$datapdf['Documentos']['Rfc'] = $EmpresasSecPDF['Rfc'];
						$datapdf['Documentos']['NombreCliente'] = $EmpresasSecPDF['RazonSocial'];

						if($EmpresasSecPDF['numeroIntFiscal'] != '' && $EmpresasSecPDF['numeroIntFiscal'] != '-'){
							$intNum = $EmpresasSecPDF['numeroIntFiscal'].', ';
						}else{
							$intNum = '';
						}

						$datapdf['Documentos']['DireccionCliente'] = $EmpresasSecPDF['calleFiscal'].' #'.$EmpresasSecPDF['numroExtFiscal'].', '.$intNum.' colonia '.$EmpresasSecPDF['ColoniaFiscal'].', '.$EmpresasSecPDF['municipioFiscal'].', '.$EmpresasSecPDF['estadoFiscal'].', CP. '.$EmpresasSecPDF['codigoPostal'];


						if($EmpresasSecPDF['Vendedor'] != '' && $EmpresasSecPDF['Vendedor'] != 0){
							$wherePDF3 = "id =".$EmpresasSecPDF['Vendedor'];
							$Vendedores        = $this->catalogos_model->getOneVendedores($wherePDF3);
							$datapdf['Documentos']['VendedoresNombre'] = $EmpresasSecPDF['Vendedor'];
						}else{
							$datapdf['Documentos']['VendedoresNombre'] = 'N/A';
						}

						$queryEva = "SELECT Evaluados.* FROM FacturaEvaluado INNER JOIN Evaluados ON Evaluados.id = FacturaEvaluado.idEvaluado  WHERE  FacturaEvaluado.idFactura = '".$_POST['idCotizacion']."'";

						$evaluados = $this->db->query( $queryEva )->result_array(); 
						

						$informacionHtml = '';

						foreach ($evaluados as $key => $value) {
								$informacionHtml .= '
									<tr>
										<td width="100%">'.$value['Nombre'].'</td>				
									</tr>
								';
						}

						$datapdf['informacionHtml'] = $informacionHtml;

						$datapdf['Documentos']['SelloDigitalCFDI'];

						$wherePDF4 = "idEncabezado =".$_POST['idCotizacion'];
						$DocumentosDetalle = $this->documentos_model->getAllDocumentoDetalle($wherePDF4);

						$datapdf['DocumentosDetalle'] =$DocumentosDetalle;

						$datapdf['DOCUMENT_ROOT'] = $_SERVER["DOCUMENT_ROOT"];


						$arraryP = explode('/',$datapdf['Documentos']['SelloDigitalCFDI']);
					
						$datapdf['arrayCFDI'] =  $arraryP;

						$arraryComplementos = explode('/',$datapdf['Documentos']['CadenaComplemento']);
						$datapdf['arrayCompletos'] =  $arraryComplementos;

				   		$codigo = $this->parser->parse('Documentos/factura_PDF', $datapdf);
				   		$dompdf = new DOMPDF();
						$dompdf->load_html(($codigo));
						ini_set('memory_limit', '-1');
						$dompdf->render();

						$pdf = $dompdf->output();
						file_put_contents('uploads/facturas/'.$EmpresasSecPDF['Rfc'].'-'.$_POST['idCotizacion'].'.pdf', $pdf);


						sleep(15);
						error_reporting(E_ALL);
						ini_set('display_errors', 1);
						$this->load->library('email');
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = "html";
						$this->email->initialize($config);
						$filenamePDF = 'uploads/facturas/'.$EmpresasSecPDF['Rfc'].'-'.$_POST['idCotizacion'].'.pdf';
						$this->email->attach($filenamePDF);
						$this->email->attach($filenameXML);

						$asunto = 'CFDI Factura FA '.$_POST['idCotizacion'];
						$mensaje = 'Agradecemos su preferencia.	
							<br>
							En este correo se adjuntan los archivos PDF y XML de su factura
						';
					    $email = $EmpresasSecPDF['CorreoFactuas'];
						
						$this->email->from('no-replay@sistemaopalo.com.mx', 'SISTEMA FACTURACION OPALO');
				        $this->email->to($email);
				        $this->email->bcc('administracion@sistemaopalo.com.mx');
				        $this->email->reply_to('jessep.barba@gmail.com', 'Jessep');
				        $this->email->subject($asunto);
				        $this->email->message($mensaje);
				        
				        $this->email->send();

		
						////



					}else{

						$data = array(
					               'Estatus' => $_POST['estatusDocumento'],
					               'CuentaBancariaCliente' => $_POST['CuentaBancariaCliente'],
					               'bancoCliente' => $_POST['bancoCliente']
					            );

						$this->db->where('id', $_POST['idCotizacion']);
						$this->db->update('documentoencabezado', $data);


						$evaluados = explode(',',$_POST['evaluadosCheck']);


						$this->db->where('idFactura', $_POST['idCotizacion']);
						$this->db->delete('FacturaEvaluado'); 

						foreach ($evaluados as $key => $value) {
						

							if($value != 0){

								$data2 = array(
						               'idEvaluado' => $value,
						               'idFactura' => $_POST['idCotizacion'],
						               'Documento' => 'Recibo'
						            );


								$this->db->insert('FacturaEvaluado', $data2);

							}
						}


					}

					


				redirect('documentos/facturacionRecibos');

	}



	public function actualizadRecibos(){

		
		

						$data = array(
					               'Estatus' => $_POST['estatusDocumento'],
					               'CuentaBancariaCliente' => $_POST['CuentaBancariaCliente'],
					               'bancoCliente' => $_POST['bancoCliente']
					            );

						$this->db->where('id', $_POST['idCotizacion']);
						$this->db->update('DocumentoEncabezadoRecibo', $data);


						$evaluados = explode(',',$_POST['evaluadosCheck']);


						$this->db->where('idFactura', $_POST['idCotizacion']);
						$this->db->where('Documento', 'Recibo');
						$this->db->delete('FacturaEvaluado'); 

						foreach ($evaluados as $key => $value) {
						

							if($value != 0){

								$data2 = array(
						               'idEvaluado' => $value,
						               'idFactura' => $_POST['idCotizacion'],
						               'Documento' => 'Recibo'
						            );


								$this->db->insert('FacturaEvaluado', $data2);

							}
						}


				redirect('documentos/Recibos');

	}

	public function cobranza(){

		$arrayName1 = array();
		$arrayName2 = array();

		$where = '(Estatus ="2")';

		if (isset($_POST['palabra_clave']) && $_POST['palabra_clave'] != '') {
				$clave = $_POST['palabra_clave'];
			
			 	$where .= " AND idCliente = '".$clave."'";
						
						 
		}else{
				$where .= '';
		}


		$select =  "DocumentoEncabezado.id, DocumentoEncabezado.Cancelada, DocumentoEncabezado.Fecha, DocumentoEncabezado.NombreCliente, DocumentoEncabezado.Total, DocumentoEncabezado.Estatus, DocumentoEncabezado.PorPagar";

		$orderBy                       = array (
		                                   'DocumentoEncabezado.id' => 'DESC'
		                                 );

		$arrayName1        = $this->documentos_model->getAllDocumentoEncabezado($where, $select,null, $orderBy);
		//die($this->db->last_query());	
		/************************/

		$where2 = '(Estatus ="3")';

		if (isset($_POST['palabra_clave']) && $_POST['palabra_clave'] != '') {
				$clave2 = $_POST['palabra_clave'];
			
			 	$where2 .= " AND idCliente = '".$clave2."'";
						
						 
		}else{
				$where2 .= '';
		}


		$select2 =  "DocumentoEncabezadoRecibo.id, DocumentoEncabezadoRecibo.Fecha, DocumentoEncabezadoRecibo.Cancelada, DocumentoEncabezadoRecibo.NombreCliente, DocumentoEncabezadoRecibo.Total, DocumentoEncabezadoRecibo.Estatus, DocumentoEncabezadoRecibo.PorPagar";

		$orderBy2                       = array (
		                                   'DocumentoEncabezadoRecibo.id' => 'DESC'
		                                 );

		$arrayName2        = $this->documentos_model->getAllDocumentoEncabezadoRecibo($where2, $select2,null, $orderBy2);



		/************************/

		$data['Documentos'] = array_merge($arrayName1, $arrayName2);



		$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();
		$this->parser->parse('Documentos/GridDocumentosCobranza', $data);
	}


	public function verDocumentoCobrar($id){
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezado($where);
			

			$query = "SELECT * FROM PagosCobranza WHERE PagosCobranza.idDocumento = '".$id."'  AND  	Documento = 'Factura'";
		    $pagos = $this->db->query( $query )->result_array(); 
		    

		    if(!empty($pagos)){
		    	 $data['pagos'] = $pagos;
		    }else{
		    	 $data['pagos'] = '';
		    }
		   



			//print_r($data['pagos']);
			//die;
			$this->parser->parse('Documentos/formPagos', $data);
	}


	public function verDocumentoCobrarRecibo($id){
			$where = "id =".$id;
			$data['Documentos']        = $this->documentos_model->getOneDocumentoEncabezadoRecibo($where);
			

			$query = "SELECT * FROM PagosCobranza WHERE PagosCobranza.idDocumento = '".$id."' AND  	Documento = 'Recibo' ";
		    $pagos = $this->db->query( $query )->result_array(); 
		    

		    if(!empty($pagos)){
		    	 $data['pagos'] = $pagos;
		    }else{
		    	 $data['pagos'] = '';
		    }
		   



			//print_r($data['pagos']);
			//die;
			$this->parser->parse('Documentos/formPagosRecibo', $data);
	}

	public function guardarPago(){


					$Residuo = $_POST['PorPagar'] - $_POST['pago_recibo'];


					$data = array(
					               'PorPagar' => $Residuo
					            );

					$this->db->where('id', $_POST['id']);
					$this->db->update('DocumentoEncabezado', $data);


					$data2 = array(
					               'NoRecibo' => $_POST['no_recibo'],
					               'Fecha' => $_POST['fecha_recibo'],
					               'idDocumento' => $_POST['id'],
					               'Monto' => $_POST['pago_recibo'],
					               'Documento' => 'Factura'
					            );


							$this->db->insert('PagosCobranza', $data2);

			redirect('documentos/cobranza');
	}


	public function guardarPagoRecibo(){


					$Residuo = $_POST['PorPagar'] - $_POST['pago_recibo'];


					$data = array(
					               'PorPagar' => $Residuo
					            );

					$this->db->where('id', $_POST['id']);
					$this->db->update('DocumentoEncabezadoRecibo', $data);


					$data2 = array(
					               'NoRecibo' => $_POST['no_recibo'],
					               'Fecha' => $_POST['fecha_recibo'],
					               'idDocumento' => $_POST['id'],
					               'Monto' => $_POST['pago_recibo'],
					               'Documento' => 'Recibo'
					            );


							$this->db->insert('PagosCobranza', $data2);

			redirect('documentos/cobranza');
	}
	
	

}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
