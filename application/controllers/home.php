<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Home extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('type_model');
	}
	
	
	
	public function index()
	{

		$data = array();
		$data['hola'] = 'hola';
		$this->parser->parse('contenido', $data);
	}
	
	
}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
