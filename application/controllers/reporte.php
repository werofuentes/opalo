<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Reporte extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('catalogos_model');
		
	}
	
		
	public function Movimientos()
	{
		
		$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();

		$this->parser->parse('Reportes/movimientosFiltros', $data);
	}


	public function movimientosreporte(){

			//print_r($_POST);
			
			if($_POST['idEmpresa'] != 'Todas'){
					$this->db->where('Evaluados.idEmpresa', $_POST['idEmpresa']);
			}


			if($_POST['Empleado'] != 'Todos'){
				$this->db->where('Evaluados.id', $_POST['Empleado']);
			}

			$this->db->where('Movimientos.Fecha >=', $_POST['FechaInicio']);
			$this->db->where('Movimientos.Fecha <=', $_POST['FechaFin']);

			$this->db->select('Evaluados.*, Movimientos.Fecha, Movimientos.Empresa, Movimientos.Comentarios,Credencial.CodigoEmpleado, Credencial.FechaExpira');
			$this->db->join('Evaluados', 'Movimientos.idEmpleado = Evaluados.id', 'inner');
			$this->db->join('Credencial', 'Credencial.idEvaluado = Evaluados.id', 'left');
			$resultado = $this->db->get('Movimientos')->result_array();
			
			$data['resultados'] = $resultado;



			$this->parser->parse('Reportes/movimientosReportes', $data);

	}

	
    

    public function EstatusPersonal(){

    		$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();
            $data['Estatus']   = $this->catalogos_model->getAllEstatus();


    		$this->parser->parse('Reportes/estatusPersonalFiltros', $data);
    }

    public function verreporteEvaluaciones(){


        $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
        $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);

        $this->db->select('Evaluados.*');    
        $totalEvaluaciones = $this->db->get('Evaluados')->result_array();
        $NumeroTotalEvaluaciones = count($totalEvaluaciones);
         $data['NumeroTotalEvaluaciones'] = $NumeroTotalEvaluaciones;


        $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
        $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);
        $this->db->join('Sucursales', 'Evaluados.Sucursal = Sucursales.idSucursal', 'inner');
        $this->db->select('count(*) as total, Sucursales.Sucursal');
        $this->db->group_by("Sucursales.Sucursal"); 
        $totalSucursales = $this->db->get('Evaluados')->result_array();
         $data['totalSucursales'] = $totalSucursales;

        


        $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
        $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);
        $this->db->join('Psicologa', 'Psicologa.id = Evaluados.idPsicologa', 'inner');
        $this->db->select('count(*) as total, Psicologa.NombrePsicologa');
        $this->db->group_by("Psicologa.NombrePsicologa"); 
        $totalPsicologas = $this->db->get('Evaluados')->result_array();
        $data['totalPsicologas'] = $totalPsicologas;


        $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
        $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);
        $this->db->join('Freelance', 'Freelance.id = Evaluados.idFreelance', 'inner');
        $this->db->select('count(*) as total, Freelance.Nombre');
        $this->db->group_by("Freelance.Nombre"); 
        $totalFreelance = $this->db->get('Evaluados')->result_array();
        $data['totalFreelance'] = $totalFreelance;

        //print_r($totalFreelance);

        $NombreFreelance = array();
        $TotalFreelance = array();
        $cont = 0;
        foreach ($totalFreelance as $key => $value) {
                if(!in_array($value['Nombre'], $NombreFreelance)){

                    $NombreFreelance[$cont] = ($value['Nombre']); 
                    $TotalFreelance[$cont] = ($value['total']); 
                    $cont ++;
                }
        }





        $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
        $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);
        $this->db->join('Certificaciones', 'Certificaciones.id = Evaluados.idCertificado', 'inner');
        $this->db->select('count(*) as total, Certificaciones.Certificaciones');
        $this->db->group_by("Certificaciones.Certificaciones"); 
        $tipoEvaluaciones = $this->db->get('Evaluados')->result_array();
         $data['tipoEvaluaciones'] = $tipoEvaluaciones;
        


        $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
        $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);
        $this->db->join('EstatusEvaluado', 'EstatusEvaluado.id = Evaluados.Estatus', 'inner');
        $this->db->select('count(*) as total, EstatusEvaluado.Descripcion');
        $this->db->group_by("EstatusEvaluado.Descripcion"); 
        $estatusEvaluado = $this->db->get('Evaluados')->result_array();
        $data['estatusEvaluado'] = $estatusEvaluado;
       
        $this->parser->parse('Reportes/evaluacionesReporte', $data);
    }


    public function evaluacionesRealizadas(){





            $this->parser->parse('Reportes/evaluacionesRealizadasFiltro', $data);
    }


    public function estatuspersonalreporte(){


    	if($_POST['idEmpresa'] != 'Todas'){
				$this->db->where('Evaluados.idEmpresa', $_POST['idEmpresa']);
		}

        if($_POST['idEstatus'] != 'Todas'){
                $this->db->where('Evaluados.Estatus', $_POST['idEstatus']);
        }

    	$this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
		$this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);

    	$this->db->select('Evaluados.*, EstatusEvaluado.Descripcion as descEstatus, EmpresaSecundaria.Nombre as nombreEmpresa');	
    	$this->db->join('EmpresaSecundaria', 'EmpresaSecundaria.id = Evaluados.idEmpresa', 'inner');
    	$this->db->join('EstatusEvaluado', 'EstatusEvaluado.id = Evaluados.Estatus', 'left');
    	$resultado = $this->db->get('Evaluados')->result_array();
    	//die($this->db->last_query());

    	$data['resultados'] = $resultado;


    	$TotalCancelado = 0;
    	$TotalR1 = 0;
    	$TotalR2 = 0;
    	$TotalR3 = 0;
    	$TotalEnproceso = 0;
    	$TotalsinResultado = 0;


    	//echo "<pre>";
    	//print_r($data['resultados']);
    	//die;
    	$cont = 0;
    	foreach ($resultado as $key => $value) {
    		
    				

    				$contadorTotalEmpresa = 0;
    				foreach ($resultado as $key2 => $value2) {


    					if($value2['idEmpresa'] == $value['idEmpresa']){
    						$contadorTotalEmpresa++;
    					}

    								  
    				}

                 if(!in_array($value['nombreEmpresa'], $Empresa)){

                    $Empresa[$cont] = ($value['nombreEmpresa']); 
                    $totalEmpresa[$cont] = $contadorTotalEmpresa; 
                    $cont ++;

                 }   
                    



    			if($value['Estatus'] == 1){
    				$TotalsinResultado++;
    			}

    			if($value['Estatus'] == 2){
    				$TotalR1++;
    			}

    			if($value['Estatus'] == 3){
    				$TotalR3++;
    			}

    			if($value['Estatus'] == 4){
    				$TotalR2++;
    			}

    			if($value['Estatus'] == 7){
    				$TotalEnproceso++;
    			}


    			if($value['Estatus'] == 8){
    				$TotalCancelado++;
    			}


    			

    	}


    	//print_r($Empresa);
    	//print_r($totalEmpresa);

    	$data['TotalCancelado'] = $TotalCancelado;
    	$data['TotalR1'] = $TotalR1;
    	$data['TotalR2'] = $TotalR2;
    	$data['TotalR3'] = $TotalR3;
    	$data['TotalEnproceso'] = $TotalEnproceso;
    	$data['TotalsinResultado'] = $TotalsinResultado;


    	$data['empresas'] = implode(',', $Empresa);
    	$data['Totalempresas'] = implode(',', $totalEmpresa);

    	

    	$this->parser->parse('Reportes/estatusPersonalReporte', $data);

    }



    public function evaluadosFacturadosreporte(){


        if($_POST['idEmpresa'] != 'Todas'){
                $this->db->where('Evaluados.idEmpresa', $_POST['idEmpresa']);
        }


        $this->db->where('DocumentoEncabezado.Fecha >=', $_POST['FechaInicio']);
        $this->db->where('DocumentoEncabezado.Fecha <=', $_POST['FechaFin']);
        $this->db->group_by("DocumentoEncabezado.idCliente"); 
        $resultado = $this->db->get('DocumentoEncabezado')->result_array();


        
        foreach ($resultado as $key => $value) {


                $SihayFactura = 0;
                $NohayFactura = 0;
                $totalEvaluados = 0;

                 $SqlFactura = "SELECT Evaluados.*, FacturaEvaluado.idEvaluado AS verFacturado  FROM Evaluados LEFT JOIN FacturaEvaluado ON FacturaEvaluado.idEvaluado = Evaluados.id WHERE Evaluados.idEmpresa = '".$value['idCliente']."' ";
                 $facturados = $this->db->query( $SqlFactura )->result_array(); 

                        foreach ($facturados as $key2 => $value2) {
                               
                                $totalEvaluados ++;

                                if($value2['verFacturado'] != ''){
                                    $SihayFactura++;
                                }else{
                                    $NohayFactura++;
                                }


                        }

                $resultado[$key]['NumeroFacturados']   = $SihayFactura;
                $resultado[$key]['NumeroNoFacturados'] = $NohayFactura;
                $resultado[$key]['totalEvaluados'] = $totalEvaluados;
         }   
        



        $data['resultados'] = $resultado;

        $this->parser->parse('Reportes/personalFacturado', $data);

    }

    public function Pagos(){

    		$data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();
    	 	$this->parser->parse('Reportes/pagosFiltros', $data);
    }


    public function pagosreporte(){

            $this->db->where('DocumentoEncabezado.Fecha >=', $_POST['FechaInicio']);
            $this->db->where('DocumentoEncabezado.Fecha <=', $_POST['FechaFin']);
            $this->db->where('DocumentoEncabezado.Cancelada', NULL);

            if($_POST['idEmpresa'] != 'Todas'){
                 $this->db->where('DocumentoEncabezado.idCliente', $_POST['idEmpresa'] );
            }


            if($_POST['tipoDocumento'] != 'Todos'){
                 $this->db->where('DocumentoEncabezado.Estatus', $_POST['tipoDocumento'] );
            }else{
                $whereData = array('2');
                $this->db->where_in('DocumentoEncabezado.Estatus', $whereData );
            }


            if($_POST['estatusPago'] != 'Todas'){

                if($_POST['estatusPago'] == 'pendientes'){
                    $this->db->where('DocumentoEncabezado.PorPagar >=',1);
                }else{
                    $this->db->where('DocumentoEncabezado.PorPagar <=',1);
                }

            }

            $this->db->join('EmpresaSecundaria', 'EmpresaSecundaria.id = DocumentoEncabezado.idCliente', 'inner');

            $this->db->select('DocumentoEncabezado.*, EmpresaSecundaria.Nombre as nombreEmpresa');
            $resultado = $this->db->get('DocumentoEncabezado')->result_array();
            //die($this->db->last_query());  
          

            $contadorRecibo = 0;
            $contadorFactura = 0;

            $contadoporpagar = 0;
            $contadorpagado = 0;
            $contadorsinpagar = 0;

  
            foreach ($resultado as $key => $value) {
                         $this->db->where('PagosCobranza.idDocumento', $value['id'] );
                         $this->db->where('PagosCobranza.Documento', "Factura" );
                     $pagos = $this->db->get('PagosCobranza')->result_array();

                     $resultado[$key]['PagosDocumento'] = $pagos;


                     if($value['Estatus'] == 2){
                            $contadorFactura++;
                     }


                     if($value['Estatus'] == 3){
                           $contadorRecibo++;
                     }

                     if($value['PorPagar']  == 0){
                         $contadorpagado++;
                     }

                     if($value['PorPagar']  > 0 && $value['PorPagar'] != $value['Total']){
                         $contadoporpagar++;
                     }

                     if($value['PorPagar'] == $value['Total']){
                         $contadorsinpagar++;
                     }




            }

            /******************************/


            $this->db->where('DocumentoEncabezadoRecibo.Fecha >=', $_POST['FechaInicio']);
            $this->db->where('DocumentoEncabezadoRecibo.Fecha <=', $_POST['FechaFin']);

            if($_POST['idEmpresa'] != 'Todas'){
                 $this->db->where('DocumentoEncabezadoRecibo.idCliente', $_POST['idEmpresa'] );
            }


            if($_POST['tipoDocumento'] != 'Todos'){
                 $this->db->where('DocumentoEncabezadoRecibo.Estatus', $_POST['tipoDocumento'] );
            }else{
                $whereData = array('3');
                $this->db->where_in('DocumentoEncabezadoRecibo.Estatus', $whereData );
            }


            if($_POST['estatusPago'] != 'Todas'){

                if($_POST['estatusPago'] == 'pendientes'){
                    $this->db->where('DocumentoEncabezadoRecibo.PorPagar >=',1);
                }else{
                    $this->db->where('DocumentoEncabezadoRecibo.PorPagar <=',1);
                }

            }

            $this->db->join('EmpresaSecundaria', 'EmpresaSecundaria.id = DocumentoEncabezadoRecibo.idCliente', 'inner');

            $this->db->select('DocumentoEncabezadoRecibo.*, EmpresaSecundaria.Nombre as nombreEmpresa');
            $resultado2 = $this->db->get('DocumentoEncabezadoRecibo')->result_array();
            //die($this->db->last_query());  
          

  
            foreach ($resultado2 as $key => $value) {
                         $this->db->where('PagosCobranza.idDocumento', $value['id'] );
                          $this->db->where('PagosCobranza.Documento', "Recibo" );
                     $pagos = $this->db->get('PagosCobranza')->result_array();

                     $resultado2[$key]['PagosDocumento'] = $pagos;


                     if($value['Estatus'] == 2){
                            $contadorFactura++;
                     }


                     if($value['Estatus'] == 3){
                           $contadorRecibo++;
                     }

                     if($value['PorPagar']  == 0){
                         $contadorpagado++;
                     }

                     if($value['PorPagar']  > 0 && $value['PorPagar'] != $value['Total']){
                         $contadoporpagar++;
                     }

                     if($value['PorPagar'] == $value['Total']){
                         $contadorsinpagar++;
                     }




            }

            //echo "<pre>";
            //print_r($resultado);
            //print_r($resultado2);
            /*****************************/



            $data['resultado']      = array_merge($resultado, $resultado2);
            //print_r($data['resultado']);
            //die;


            $data['TotalFacturas']  = $contadorFactura;
            $data['TotalRecibos']   = $contadorRecibo;

            $data['Totalpagar']    = $contadorpagado;
            $data['Totalporpagar'] = $contadoporpagar;
            $data['Totalsinpagar'] = $contadorsinpagar;

            $this->parser->parse('Reportes/pagosReporte', $data);
    }

    public function FacturasRecibos(){

    	 	$this->parser->parse('Reportes/facturasRecibosFiltros', $data);
    }


	public function Freelance(){

            $data['Freelance']   = $this->catalogos_model->getAllFreelance();
            $this->parser->parse('Reportes/freelanceFiltros', $data);
    }


    public function pendientesEvaluados(){

            $data['Empresas']   = $this->catalogos_model->getAllEmpresaSecundaria();
 
            $this->parser->parse('Reportes/pendientesEvaluadosFiltros', $data);
            
    }


    public function Psicologa(){

            $data['Psicologa']   = $this->catalogos_model->getAllPsicologa();
            $data['Sucursal']   = $this->catalogos_model->getAllSucursal();
            $this->parser->parse('Reportes/psicologaFiltros', $data);
    }
	
    public function freelancereporte(){

            $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
            $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);

            if($_POST['idFreelance'] != 'Todos'){
                 $this->db->where('Evaluados.idFreelance', $_POST['idFreelance']);
            }

            $this->db->join('Freelance', 'Freelance.id = Evaluados.idFreelance', 'inner');

            $this->db->select('Evaluados.*, count(Evaluados.idFreelance) as totalEvaluados, Freelance.Nombre as nombreFreelance, Freelance.Costo');

            $this->db->group_by("Evaluados.idFreelance"); 

            $resultado = $this->db->get('Evaluados')->result_array();

            $data['resultadoTotal'] = $resultado;

            //echo "<pre>";
            //print_r($data['resultadoTotal']);
            //die;

            $this->parser->parse('Reportes/freelanceReporte', $data);

    }


    public function psicologareporte(){

            $this->db->where('Evaluados.FechaRegistro >=', $_POST['FechaInicio']);
            $this->db->where('Evaluados.FechaRegistro <=', $_POST['FechaFin']);

            if($_POST['idPsicologa'] != 'Todos'){
                 $this->db->where('Evaluados.idPsicologa', $_POST['idPsicologa']);
            }


            if($_POST['idSucursal'] != 'Todos'){
                 $this->db->where('Evaluados.Sucursal', $_POST['idSucursal']);
            }

            $this->db->join('Psicologa', 'Psicologa.id = Evaluados.idPsicologa', 'inner');

            $this->db->select('Evaluados.*, GROUP_CONCAT(Evaluados.id) as idEvaluacion, count(Evaluados.idPsicologa) as totalEvaluados, Psicologa.NombrePsicologa as NombrePsicologa, Psicologa.CostoPsicologa');

            $this->db->group_by("Evaluados.idPsicologa"); 
            $resultado = $this->db->get('Evaluados')->result_array();




            foreach ($resultado as $key => $value) {
                     $this->db->select('Certificaciones.Certificaciones,  count(Evaluados.idPsicologa) as totalEvaluados');       
                     $this->db->join('Certificaciones', 'Certificaciones.id = Evaluados.idCertificado', 'inner');  

                     $cadenaEvaluados = explode(',', $value['idEvaluacion']);
                     $this->db->where_in('Evaluados.id', $cadenaEvaluados);
                     $this->db->group_by("Evaluados.idCertificado");
                      $agruparCertificacion = $this->db->get('Evaluados')->result_array();  

                      //die($this->db->last_query()); 
                      
                      $contador = 0;
                      foreach ($agruparCertificacion as $key2 => $value2) {
                            $resultado[$key]['DetalleCertidicacion'][$contador]['CertifiNombre'] = $value2['Certificaciones'];
                            $resultado[$key]['DetalleCertidicacion'][$contador]['totalCerti'] = $value2['totalEvaluados'];
                     

                             $contador++; 
                      }


            }




            $data['resultadoTotal'] = $resultado;

            //echo "<pre>";
            //print_r($data['resultadoTotal']);
            //die;





            $this->parser->parse('Reportes/psicologaReporte', $data);

    }

    public function pendientePago(){

        
    }
	
}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
