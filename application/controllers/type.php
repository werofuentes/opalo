<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Type extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->model('type_model');
		$this->load->model('user_model');
	}
	
		
	public function index()
	{

		 $data = array();
		 $pages=10; //Número de registros mostrados por páginas
        $this->load->library('pagination'); //Cargamos la librería de paginación
        $config['base_url'] = base_url().'type/index'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
        $config['total_rows'] = $this->type_model->getAllContadorTypes();//calcula el número de filas  
        $config['per_page'] = $pages; //Número de registros mostrados por páginas
        $config['num_links'] = 20; //Número de links mostrados en la paginación
        $config['first_link'] = 'Primera';//primer link
        $config['last_link'] = 'Última';//último link
        $config["uri_segment"] = 3;//el segmento de la paginación
        $config['next_link'] = 'Siguiente';//siguiente link
        $config['prev_link'] = 'Anterior';//anterior link
        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
        $this->pagination->initialize($config); //inicializamos la paginación 
		 
		 
		 $types = $this->type_model->getAllTypes(null,null,null,null,$config['per_page'],$this->uri->segment(3));
		 $data['types'] = $types;
         $data['NumPaginas'] = $this->pagination->create_links();
		 $this->parser->parse('configuracion/type', $data);
	}
	
	public function formAddUType()
	{

		$data = array();
		$data['usuarios'] = 'hola';
		$this->parser->parse('type_form_add', $data);
	}
	
	
	public function formEditType($id)
	{
         
		$where['typ_id'] = $id;
		$type = $this->type_model->getOneType($where);
		$data['type'] = $type;
		$wherePermisos['per_typ_id'] = $id;
		$typePermisos = $this->type_model->getOneTypePermisos($wherePermisos);
		$data['permisos'] = $typePermisos;
		
		
		
		$this->parser->parse('configuracion/type_form_edit', $data);
	}
	
	public function editType()
	{
		$typeAdd = array();

		$data['typ_name'] = $_POST['name'];
		$data['typ_reference'] = $_POST['reference'];
		$data['typ_active'] = $_POST['activo'];
		

			
		$dataPermisos = array();
		
		if(isset($_POST['per_empresa_primaria']))
		{
         
		    $dataPermisos['per_empresa_primaria'] = 1;
        
        }else{
		
			$dataPermisos['per_empresa_primaria'] = 0;
        
		}



		if(isset($_POST['agregar_empresa_primaria']))
		{
         
		    $dataPermisos['agregar_empresa_primaria'] = 1;
        
        }else{
		
			$dataPermisos['agregar_empresa_primaria'] = 0;
        
		}




		if(isset($_POST['editar_empresa_primaria']))
		{
         
		    $dataPermisos['editar_empresa_primaria'] = 1;
        
        }else{
		
			$dataPermisos['editar_empresa_primaria'] = 0;
        
		}



		if(isset($_POST['sololectura_empresa_primaria']))
		{
         
		    $dataPermisos['sololectura_empresa_primaria'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_empresa_primaria'] = 0;
        
		}




		if(isset($_POST['per_empresa_secundaria']))
		{
         
		    $dataPermisos['per_empresa_secundaria'] = 1;
        
        }else{
		
			$dataPermisos['per_empresa_secundaria'] = 0;
        
		}



		if(isset($_POST['agregar_empresa_secundaria']))
		{
         
		    $dataPermisos['agregar_empresa_secundaria'] = 1;
        
        }else{
		
			$dataPermisos['agregar_empresa_secundaria'] = 0;
        
		}




		if(isset($_POST['editar_empresa_secundaria']))
		{
         
		    $dataPermisos['editar_empresa_secundaria'] = 1;
        
        }else{
		
			$dataPermisos['editar_empresa_secundaria'] = 0;
        
		}



		if(isset($_POST['sololectura_empresa_secundaria']))
		{
         
		    $dataPermisos['sololectura_empresa_secundaria'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_empresa_secundaria'] = 0;
        
		}



		if(isset($_POST['per_certificaciones']))
		{
         
		    $dataPermisos['per_certificaciones'] = 1;
        
        }else{
		
			$dataPermisos['per_certificaciones'] = 0;
        
		}



		if(isset($_POST['agregar_certificaciones']))
		{
         
		    $dataPermisos['agregar_certificaciones'] = 1;
        
        }else{
		
			$dataPermisos['agregar_certificaciones'] = 0;
        
		}



		if(isset($_POST['editar_certificaciones']))
		{
         
		    $dataPermisos['editar_certificaciones'] = 1;
        
        }else{
		
			$dataPermisos['editar_certificaciones'] = 0;
        
		}


		if(isset($_POST['sololectura_certificaciones']))
		{
         
		    $dataPermisos['sololectura_certificaciones'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_certificaciones'] = 0;
        
		}


		if(isset($_POST['per_examenes']))
		{
         
		    $dataPermisos['per_examenes'] = 1;
        
        }else{
		
			$dataPermisos['per_examenes'] = 0;
        
		}


		if(isset($_POST['agregar_examenes']))
		{
         
		    $dataPermisos['agregar_examenes'] = 1;
        
        }else{
		
			$dataPermisos['agregar_examenes'] = 0;
        
		}



		if(isset($_POST['editar_examenes']))
		{
         
		    $dataPermisos['editar_examenes'] = 1;
        
        }else{
		
			$dataPermisos['editar_examenes'] = 0;
        
		}



		if(isset($_POST['sololectura_examenes']))
		{
         
		    $dataPermisos['sololectura_examenes'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_examenes'] = 0;
        
		}



		if(isset($_POST['per_vendedores']))
		{
         
		    $dataPermisos['per_vendedores'] = 1;
        
        }else{
		
			$dataPermisos['per_vendedores'] = 0;
        
		}



		if(isset($_POST['agregar_vendedores']))
		{
         
		    $dataPermisos['agregar_vendedores'] = 1;
        
        }else{
		
			$dataPermisos['agregar_vendedores'] = 0;
        
		}



		if(isset($_POST['editar_vendedores']))
		{
         
		    $dataPermisos['editar_vendedores'] = 1;
        
        }else{
		
			$dataPermisos['editar_vendedores'] = 0;
        
		}



		if(isset($_POST['sololectura_vendedores']))
		{
         
		    $dataPermisos['sololectura_vendedores'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_vendedores'] = 0;
        
		}



		if(isset($_POST['per_freelance']))
		{
         
		    $dataPermisos['per_freelance'] = 1;
        
        }else{
		
			$dataPermisos['per_freelance'] = 0;
        
		}


		if(isset($_POST['agregar_freelance']))
		{
         
		    $dataPermisos['agregar_freelance'] = 1;
        
        }else{
		
			$dataPermisos['agregar_freelance'] = 0;
        
		}


		if(isset($_POST['editar_freelance']))
		{
         
		    $dataPermisos['editar_freelance'] = 1;
        
        }else{
		
			$dataPermisos['editar_freelance'] = 0;
        
		}

		if(isset($_POST['sololectura_freelance']))
		{
         
		    $dataPermisos['sololectura_freelance'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_freelance'] = 0;
        
		}
		/********/

		if(isset($_POST['per_psicologa']))
		{
         
		    $dataPermisos['per_psicologa'] = 1;
        
        }else{
		
			$dataPermisos['per_psicologa'] = 0;
        
		}


		if(isset($_POST['agregar_psicologa']))
		{
         
		    $dataPermisos['agregar_psicologa'] = 1;
        
        }else{
		
			$dataPermisos['agregar_psicologa'] = 0;
        
		}


		if(isset($_POST['editar_psicologa']))
		{
         
		    $dataPermisos['editar_psicologa'] = 1;
        
        }else{
		
			$dataPermisos['editar_psicologa'] = 0;
        
		}

		if(isset($_POST['sololectura_psicologa']))
		{
         
		    $dataPermisos['sololectura_psicologa'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_psicologa'] = 0;
        
		}
		/*******/


		if(isset($_POST['per_sucursales']))
		{
         
		    $dataPermisos['per_sucursales'] = 1;
        
        }else{
		
			$dataPermisos['per_sucursales'] = 0;
        
		}


		if(isset($_POST['agregar_sucursales']))
		{
         
		    $dataPermisos['agregar_sucursales'] = 1;
        
        }else{
		
			$dataPermisos['agregar_sucursales'] = 0;
        
		}



		if(isset($_POST['editar_sucursales']))
		{
         
		    $dataPermisos['editar_sucursales'] = 1;
        
        }else{
		
			$dataPermisos['editar_sucursales'] = 0;
        
		}




		if(isset($_POST['sololectura_sucursales']))
		{
         
		    $dataPermisos['sololectura_sucursales'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_sucursales'] = 0;
        
		}


		if(isset($_POST['per_cotizacion']))
		{
         
		    $dataPermisos['per_cotizacion'] = 1;
        
        }else{
		
			$dataPermisos['per_cotizacion'] = 0;
        
		}



		if(isset($_POST['agregar_cotizacion']))
		{
         
		    $dataPermisos['agregar_cotizacion'] = 1;
        
        }else{
		
			$dataPermisos['agregar_cotizacion'] = 0;
        
		}


		if(isset($_POST['editar_cotizacion']))
		{
         
		    $dataPermisos['editar_cotizacion'] = 1;
        
        }else{
		
			$dataPermisos['editar_cotizacion'] = 0;
        
		}


		if(isset($_POST['sololectura_cotizacion']))
		{
         
		    $dataPermisos['sololectura_cotizacion'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_cotizacion'] = 0;
        
		}


		if(isset($_POST['per_evaluacion']))
		{
         
		    $dataPermisos['per_evaluacion'] = 1;
        
        }else{
		
			$dataPermisos['per_evaluacion'] = 0;
        
		}


		if(isset($_POST['agregar_evaluacion']))
		{
         
		    $dataPermisos['agregar_evaluacion'] = 1;
        
        }else{
		
			$dataPermisos['agregar_evaluacion'] = 0;
        
		}


		if(isset($_POST['sololectura_evaluacion']))
		{
         
		    $dataPermisos['sololectura_evaluacion'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_evaluacion'] = 0;
        
		}


		if(isset($_POST['editar_evaluacion']))
		{
         
		    $dataPermisos['editar_evaluacion'] = 1;
        
        }else{
		
			$dataPermisos['editar_evaluacion'] = 0;
        
		}


		if(isset($_POST['per_factrec']))
		{
         
		    $dataPermisos['per_factrec'] = 1;
        
        }else{
		
			$dataPermisos['per_factrec'] = 0;
        
		}



		if(isset($_POST['agregar_factrec']))
		{
         
		    $dataPermisos['agregar_factrec'] = 1;
        
        }else{
		
			$dataPermisos['agregar_factrec'] = 0;
        
		}


		if(isset($_POST['editar_factrec']))
		{
         
		    $dataPermisos['editar_factrec'] = 1;
        
        }else{
		
			$dataPermisos['editar_factrec'] = 0;
        
		}


		if(isset($_POST['sololectura_factrec']))
		{
         
		    $dataPermisos['sololectura_factrec'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_factrec'] = 0;
        
		}



		if(isset($_POST['per_cobranza']))
		{
         
		    $dataPermisos['per_cobranza'] = 1;
        
        }else{
		
			$dataPermisos['per_cobranza'] = 0;
        
		}


		if(isset($_POST['agregar_cobranza']))
		{
         
		    $dataPermisos['agregar_cobranza'] = 1;
        
        }else{
		
			$dataPermisos['agregar_cobranza'] = 0;
        
		}


		if(isset($_POST['editar_cobranza']))
		{
         
		    $dataPermisos['editar_cobranza'] = 1;
        
        }else{
		
			$dataPermisos['editar_cobranza'] = 0;
        
		}



		if(isset($_POST['sololectura_cobranza']))
		{
         
		    $dataPermisos['sololectura_cobranza'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_cobranza'] = 0;
        
		}




		if(isset($_POST['per_generarCredencial']))
		{
         
		    $dataPermisos['per_generarCredencial'] = 1;
        
        }else{
		
			$dataPermisos['per_generarCredencial'] = 0;
        
		}



		if(isset($_POST['agregar_generarCredencial']))
		{
         
		    $dataPermisos['agregar_generarCredencial'] = 1;
        
        }else{
		
			$dataPermisos['agregar_generarCredencial'] = 0;
        
		}


		if(isset($_POST['editar_generarCredencial']))
		{
         
		    $dataPermisos['editar_generarCredencial'] = 1;
        
        }else{
		
			$dataPermisos['editar_generarCredencial'] = 0;
        
		}


		if(isset($_POST['sololectura_generarCredencial']))
		{
         
		    $dataPermisos['sololectura_generarCredencial'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_generarCredencial'] = 0;
        
		}



		if(isset($_POST['per_registroMovimientos']))
		{
         
		    $dataPermisos['per_registroMovimientos'] = 1;
        
        }else{
		
			$dataPermisos['per_registroMovimientos'] = 0;
        
		}



		if(isset($_POST['agregar_registroMovimientos']))
		{
         
		    $dataPermisos['agregar_registroMovimientos'] = 1;
        
        }else{
		
			$dataPermisos['agregar_registroMovimientos'] = 0;
        
		}


		if(isset($_POST['editar_registroMovimientos']))
		{
         
		    $dataPermisos['editar_registroMovimientos'] = 1;
        
        }else{
		
			$dataPermisos['editar_registroMovimientos'] = 0;
        
		}


		if(isset($_POST['sololectura_registroMovimientos']))
		{
         
		    $dataPermisos['sololectura_registroMovimientos'] = 1;
        
        }else{
		
			$dataPermisos['sololectura_registroMovimientos'] = 0;
        
		}


		if(isset($_POST['per_reporte_mov_personal']))
		{
         
		    $dataPermisos['per_reporte_mov_personal'] = 1;
        
        }else{
		
			$dataPermisos['per_reporte_mov_personal'] = 0;
        
		}


		if(isset($_POST['per_reporte_estatus_personal']))
		{
         
		    $dataPermisos['per_reporte_estatus_personal'] = 1;
        
        }else{
		
			$dataPermisos['per_reporte_estatus_personal'] = 0;
        
		}



		if(isset($_POST['per_reporte_pagos']))
		{
         
		    $dataPermisos['per_reporte_pagos'] = 1;
        
        }else{
		
			$dataPermisos['per_reporte_pagos'] = 0;
        
		}




		if(isset($_POST['per_reporte_pendientes_facturar']))
		{
         
		    $dataPermisos['per_reporte_pendientes_facturar'] = 1;
        
        }else{
		
			$dataPermisos['per_reporte_pendientes_facturar'] = 0;
        
		}


		if(isset($_POST['per_reporte_pendientes_cobrar']))
		{
         
		    $dataPermisos['per_reporte_pendientes_cobrar'] = 1;
        
        }else{
		
			$dataPermisos['per_reporte_pendientes_cobrar'] = 0;
        
		}


		if(isset($_POST['per_usuarios_sistema']))
		{
         
		    $dataPermisos['per_usuarios_sistema'] = 1;
        
        }else{
		
			$dataPermisos['per_usuarios_sistema'] = 0;
        
		}


		if(isset($_POST['per_perfil_usuario']))
		{
         
		    $dataPermisos['per_perfil_usuario'] = 1;
        
        }else{
		
			$dataPermisos['per_perfil_usuario'] = 0;
        
		}
		
	
		
	
		$wherePermisos['per_typ_id'] = $_POST['id'];
		$permisosTypes = $this->type_model->updTypePermisos($dataPermisos,$wherePermisos);
	  	
	   
	   
	  	$where['typ_id'] = $_POST['id'];
		$type = $this->type_model->updType($data,$where); 
		
	
		
		
		
		 $data = array();
		 $types = $this->type_model->getAllTypes();
		 $data['types'] = $types;

		 $this->parser->parse('configuracion/type', $data);
		
	  		 
	
		
		
	}
	
	public function addType()
	{
			
			
		$typeAdd = array();
		//$data['typ_id'] = NULL;
		$data['typ_name'] = $_POST['name'];
		$data['typ_reference'] = $_POST['reference'];
		$data['typ_active'] = $_POST['activo'];
		
	  if($_POST['name'] == '' && $_POST['reference'] != ''){
	  	 $data['mensajes'] = '1';
		 $data['reference'] = $_POST['reference'] ;
	  	 $this->parser->parse('type_form_add', $data);
	  }	

	  if($_POST['name'] != '' && $_POST['reference'] == ''){
	  	 $data['mensajes'] = '2';
		 $data['name'] = $_POST['name'] ;
	  	 $this->parser->parse('type_form_add', $data);
	  }		 
	 
	 if($_POST['name'] == '' && $_POST['reference'] == ''){
	  	 $data['mensajes'] = '3';
	  	 $this->parser->parse('type_form_add', $data);
	  }		 
	
	 if($_POST['name'] != '' && $_POST['reference'] != ''){
	  	 
		$dataPermisos = array();
		if(isset($_POST['usuarios']))
		{
         
		  $dataPermisos['per_mod_users'] = 1;
        
        }else{
		
			$dataPermisos['per_mod_users'] = 0;
        
		}
		
		
		if(isset($_POST['guias']))
		{
		
		  $dataPermisos['per_mod_guide'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_guide'] = 0;
		}	

        if(isset($_POST['moneda']))
		{
		
		  $dataPermisos['per_mod_coins'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_coins'] = 0;
		}	
		
        if(isset($_POST['tipos']))
		{
		
		  $dataPermisos['per_mod_type'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_type'] = 0;
		}
		
       if(isset($_POST['estimados']))
		{
		
		  $dataPermisos['per_mod_estimates'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_estimates'] = 0;
		}
		
        if(isset($_POST['categorias']))
		{
		
		  $dataPermisos['per_mod_categories'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_categories'] = 0;
		}	
			
			
        if(isset($_POST['subcategorias']))
		{
		
		  $dataPermisos['per_mod_subcategories'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_subcategories'] = 0;
		}	
		
	    if(isset($_POST['objetos']))
		{
		
		  $dataPermisos['per_mod_objects'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_objects'] = 0;
		}
	    
	    if(isset($_POST['acompanantes']))
		{
		
		  $dataPermisos['per_mod_ads'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_ads'] = 0;
		}


       if(isset($_POST['intereses']))
	   {
		
		  $dataPermisos['per_mod_interest'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_interest'] = 0;
		}	
			
       if(isset($_POST['propuestas']))
	   {
		
		  $dataPermisos['per_mod_proposals'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_proposals'] = 0;
		}	
		
		if(isset($_POST['estados']))
	   {
		
		  $dataPermisos['per_mod_states'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_states'] = 0;
		}
		
		if(isset($_POST['cuentas']))
	   {
		
		  $dataPermisos['per_mod_accounts'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_accounts'] = 0;
		}

		if(isset($_POST['articulo']))
	   {
		
		  $dataPermisos['per_mod_article'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_article'] = 0;
		}	
	
		if(isset($_POST['baner']))
	   {
		
		  $dataPermisos['per_mod_baner'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_baner'] = 0;
		}
		
		if(isset($_POST['galeriaGeneral']))
	   {
		
		  $dataPermisos['per_mod_galeriageneral'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_galeriageneral'] = 0;
		}
		
		if(isset($_POST['contenidoFFline']))
	   {
		
		  $dataPermisos['per_mod_contenido'] = 1;
		
		}else{
			
		  $dataPermisos['per_mod_contenido'] = 0;
		}
		
		
		if(isset($_POST['qhoy']))
	   {
		
		  $dataPermisos['per_modo_qhoy'] = 1;
		
		}else{
			
		  $dataPermisos['per_modo_qhoy'] = 0;
		}
		 
		 
		if($type = $this->type_model->addType($data))
		{
			
			
		$dataPermisos['per_typ_id'] = $type;	
		$permisosTypes = $this->type_model->addTypePermisos($dataPermisos);
		
		$dataTodos = array();
		$types = $this->type_model->getAllTypes();
		$dataTodos['types'] = $types;
		$dataTodos['mensajes'] = '3';
		
		
		}
	  	 $this->parser->parse('type', $dataTodos);
	  }		 
	
		 
	}
	
	
	public function deleteType($id)
	{
         
		$where['use_typ_id'] = $id;
		$usuariosTypo = $this->user_model->getOneUser($where);

       if($usuariosTypo != 0){
       	   $data = array();
		   $types = $this->type_model->getAllTypes();
		   $data['mensajes'] = '1';
		   $data['types'] = $types;
		   $this->parser->parse('type', $data);
       }else{
		
       	$whereEliminar['typ_id'] = $id;
		$type = $this->type_model->dropType($whereEliminar);
		$data = array();
		$types = $this->type_model->getAllTypes();
		$data['types'] = $types;
		$data['mensajes'] = '2';
        $this->parser->parse('type', $data);
	   }
		
	}
	
	
	
	
}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
