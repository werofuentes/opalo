<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * it4pymes.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Credencialqr extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->model('catalogos_model');
		$this->load->model('documentos_model');
	}
	
		
	public function index()
	{	
	}


	/**
	 * Método para registrar y mostrar los datos de un registro a tréves de código QR
	 * @access public 
	 * @param string $codigoCredencial Código de la credencial(Opcional)
	 */
	public function detallesMovimiento($codigoCredencial = '') {

		if(isset($_SESSION['codigoCredencial'])) :
			$data['codigoCredencial'] = $_SESSION['codigoCredencial'];
		else:
			$data['codigoCredencial'] = $codigoCredencial;
		endif;	

		$data['Codigo'] = $data['codigoCredencial'];
		
		if($data['codigoCredencial'] != ''):

			$data['codigoCredencial'] = str_replace('_','-',$data['codigoCredencial']);

			unset($_SESSION['codigoCredencial']);

			
			$query = "SELECT Credencial.*, EmpresaSecundaria.RazonSocial, Evaluados.Nombre,Evaluados.Foto FROM Credencial INNER JOIN EmpresaSecundaria ON EmpresaSecundaria.id = Credencial.idEmprea INNER JOIN Evaluados ON Evaluados.id = Credencial.idEvaluado WHERE CodigoEmpleado = '".$data['codigoCredencial']."'";
			$credencial = $this->db->query( $query )->result_array();

			$data2 = [
				'idEmpleado'	=>	$credencial[0]['idEvaluado'],
				'Fecha'			=>	date('Y:m:d h:m:s'),
				'Empresa'		=>	$credencial[0]['RazonSocial']
			];	


			$this->db->insert('Movimientos', $data2);
			$idRegistro = $this->db->insert_id();

			$data['idEvaluado']	= $credencial[0]['idEvaluado'];
			$data['Empresa']	= $credencial[0]['RazonSocial'];
			$data['FechaExpira'] = $credencial[0]['FechaExpira'];
			$data['Codigo']		= $data['codigoCredencial'];
			$data['idRegistro']	= $idRegistro;

			$data['foto']		= "<img src='".base_url().$credencial[0]['Foto']."' alt='Credencial' style='width:100px;height:120px;' >";

			$this->parser->parse('Credencial/detallesMovimiento', $data);
		else:
			redirect(base_url() . 'login');
		endif;


	}



	public function registromovimiento($codigoCredencial=''){



	
					
		
					if(isset($_SESSION['codigoCredencial'])){
						$data['codigoCredencial'] =$_SESSION['codigoCredencial'];
					}else{
						$data['codigoCredencial'] =$codigoCredencial;
					}
					
					

					$this->parser->parse('loginQR', $data);
		




			
	}


	public function registromovimiento2(){


			$codigoCredencial = $_SESSION['codigoCredencial'];	

			unset($_SESSION['codigoCredencial']);


			if($codigoCredencial !=''){

					$query = "SELECT Credencial.*, EmpresaSecundaria.RazonSocial, Evaluados.Nombre,Evaluados.Foto FROM Credencial INNER JOIN EmpresaSecundaria ON EmpresaSecundaria.id = Credencial.idEmprea INNER JOIN Evaluados ON Evaluados.id = Credencial.idEvaluado WHERE CodigoEmpleado = '".$codigoCredencial."'";
		  				 $credencial = $this->db->query( $query )->result_array(); 

		  				 //print_r($credencial);




		  				 	$data2 = array(
					               'idEmpleado' => $credencial[0]['idEvaluado'],
					               'Fecha' 		=> date('Y-m-d h:m:s'),
					               'Empresa' 	=> $credencial[0]['RazonSocial']
					            );


							$this->db->insert('Movimientos', $data2);
							$idRegistro = $this->db->insert_id();

					$data['idEvaluado'] = $credencial[0]['idEvaluado'];
					$data['Empresa'] = $credencial[0]['RazonSocial'];
					$data['FechaExpira'] = $credencial[0]['FechaExpira'];
					$data['Codigo'] = $codigoCredencial;
					$data['idRegistro'] = $idRegistro;

					$data['foto'] =  "<img src='".base_url().$credencial[0]['Foto']."' alt='Credencial' style='width:100px;height:120px;'>";

					$this->parser->parse('Credencial/detallesMovimiento', $data);
			}




			
	}

	public function guardarMovimiento() {

		if($_POST){
			
			if(isset($_POST['comentario'])):
				$comentario = $_POST['comentario'];
			else:
				$comentario = 'Sin problemas';
			endif;

			$data = array(
				'Comentarios' => $comentario
			);

			$this->db->where('id', $_POST['idRegistro']);
			$this->db->update('Movimientos', $data);
		}

		redirect('credencialqr/mensajeGuardardo');

	}

	public function GuardarMovimientoExterno(){

		if($_POST){
			$data = array(
				'Comentarios' => $_POST['comentario']
			);

			$this->db->where('id', $_POST['idRegistro']);
			$this->db->update('Movimientos', $data);
		}
							

		redirect('credencialqr/mensajeGuardardo');
	}

	public function mensajeGuardardo(){
		die('Verificación de Código QR completado de manera exitosa');
	}


}
// END Searchs controller

/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
