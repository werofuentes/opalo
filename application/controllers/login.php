<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Login extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->model('login_model');
		$this->load->model('type_model');
		$this->load->model('user_model');
		$this->load->library('email');
		
	}	

	public function index($mensaje = NULL)
	{
		$data = array();
		$data['mensaje'] = $mensaje;
		$this->parser->parse('login', $data);
	}
	
	
	public function valid()
	{

			$login = $this->login_model->loginUser($_POST['usuario'], $_POST['password']);
			
	
			if(isset($login['error']) && "" != $login['error'])
			{
				$this->index("error");
				//redirect( base_url() . "login" );
			}
			else
			{
				
				
			if($_POST['usuario'] != null && $_POST['password'] != null){
				
				 // Agrega usuario a SESSION
				$_SESSION['SEUS'] = $login;
			
				//permisos
		        $idType = $_SESSION['SEUS']['use_typ_id'];
				$wherePermisos['per_typ_id'] = $idType;
				$typePermisos = $this->type_model->getOneTypePermisos($wherePermisos);

				//print_r($typePermisos);
				//die;
				$_SESSION['PERMISOS'] = $typePermisos;
				
				
                redirect( base_url() . "home" );
				
			}else{
				
				redirect(base_url() . "login");
				
			}	
								

			}
		
		
	}

	public function validqr()
	{

			$login = $this->login_model->loginUser($_POST['usuario'], $_POST['password']);
			
			

			if(isset($login['error']) && "" != $login['error'])
			{
				$this->index("error");
				$_SESSION['codigoCredencial'] = $_POST['codigoCredencial'];
				redirect(base_url() . "credencialqr/registromovimiento");
			}
			else
			{
				
				
			if($_POST['usuario'] != null && $_POST['password'] != null){
				

				$_SESSION['codigoCredencial'] = $_POST['codigoCredencial'];


				
				
                redirect( base_url() . "credencialqr/registromovimiento2" );
				
			}else{	

				$_SESSION['codigoCredencial'] = $_POST['codigoCredencial'];
				
				redirect(base_url() . "credencialqr/registromovimiento");
				
			}	
								

			}
		
		
	}

	// destroy session
	public function logout()
	{
		// Destruimos la sesión
		$this->login_model->destroyLogin(SEUS);
		redirect(base_url() . "login");
	}
	
	
	 public function recuperar()
	{
		$email = $this->input->post('email', TRUE);
		
		$where['use_email']      = $email;
		$usuarios             = $this->user_model->getOneUser($where);	

		
		
		if(count($usuarios) > 0 && $email != '')
		{
				
			if($usuarios['use_active'] == 1){
					
					
					$cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
					$longitudCadena=strlen($cadena);
					$longitudPass=10;
					
					for($i=1 ; $i<=$longitudPass ; $i++){
			          $pos=rand(0,$longitudCadena-1);
			          $pass .= substr($cadena,$pos,1);
			         }
							
			  $datausers['use_key']         = hash('sha256',$pass);	
		      $datausers_where['use_email'] = $email;	

		     
		      $usersG                       = $this->user_model->updUser($datausers, $datausers_where);

				
				$asunto = 'Recuperacion de contraseña SISTEMA OPALO';
				$mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/logo.jpg" width="178" height="145" /><td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) usuario tus accesos son los siguientes</td>
				  </tr>
				  <tr>
				     <td >Usuario:</td>
				     <td align="center">'.$usuarios['use_login'].'</td>
				  </tr>
				  <tr>
				     <td >Password:</td>
				     <td align="center">'.$pass.'</td>
				  </tr>
				</tablet>
				';
				
				//$to = 'jessep.barba@gmail.com';
				
				//mailSend("GRUPO APRO", "contacto@grupoapro.com", $to, $asunto, $mensaje, null, null);
				
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				
				$this->email->initialize($config);
				$email = $usuarios['use_email'];
				
				$this->email->from('no-replay@opalosoft.com', 'SISTEMA OPALO');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
				
				echo "si";
			}else{
				
				echo "no";
			}	
		}else{
			
			echo "no";
		}

		
		
	}

}
