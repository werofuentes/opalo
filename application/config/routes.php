<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|   example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|   http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|   $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|   $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
//$route['404_override'] = '';

$route['default_controller']                        = "home";

// $route['home/(:any)']                               = "home/index/$1";
// $route['detail/(:any)']                             = "detail/index/$1";
// $route['results/(:any)']                            = "results/index/$1";
// 
// $route['directorio-servicios']                      = "directorio_servicios/index";
// $route['directorio-servicios/search']               = "directorio_servicios/search";
// $route['directorio-servicios/(:any)']               = "directorio_servicios/servicio/$1";
// 
// $route['article/noticias']                          = "article/index/noticias";
// $route['article/hogar']                             = "article/index/hogar";
// $route['article/inmobiliarias']                     = "article/index/inmobiliarias";
// $route['article/noticias/listado']                  = "article/list_articles/noticias";
// $route['article/hogar/listado']                     = "article/list_articles/hogar";
// $route['article/inmobiliarias/listado']             = "article/list_articles/inmobiliarias";
// $route['article/(:any)']                            = "article/detalle/$1";
// $route['articulo/(:any)']                           = "article/$1";
// 
// $route['vivienda-nueva/fraccionamiento/(:any)']	    = "vivienda_nueva/fraccionamiento/index/$1";
// $route['vivienda-nueva/(:any)']        			    = "vivienda_nueva/home/$1";
// 
// $route['comparador-propiedades']                    = "comparador/index/$1";
// 
// $route['centrocompras/datos-personales/(:num)']		= "centrocompras/datos_personales/index/$1";
// $route['centrocompras/datos-personales/paso_dos/']	= "centrocompras/datos_personales/index/$1";
// 
// $route['calculadora-hipotecaria']                   = "calculator/index";
// $route['calculadora-hipotecaria/(:num)']            = "calculator/index/$1";


$route['404_override']                              = 'error_404';

// $route['(:any)/detalle-(:any)\.html']  		        = "old_urls/togo/detalle/$1/$2";
// $route['(:any)/articulo/(:any)\.html'] 		        = "old_urls/togo/articulo/$1/$2";
// $route['(:any)/(:any)/(todo|renta|
       // compra|traspasar|compra,renta,
       // traspasar|compra,renta|renta,
       // traspasar|compra,traspasar)/
       // (:any)/(:any)/(:any)\.html']                 = "old_urls/togo/resultado/$1/$2_$3_$4_$5_$6";
// 
// $route['(:any)/inicio\.html']                       = "old_urls/togo/home/$1/$2";

$route['(.+)-(.+)']                                 = "$1_$2";

/* End of file routes.php */
/* Location: ./application/config/routes.php */