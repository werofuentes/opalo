<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

// $hook['post_controller_constructor'][] = array(
                                // 'class'    => 'Geolocation',
                                // 'function' => 'location',
                                // 'filename' => 'geolocation.php',
                                // 'filepath' => 'hooks'
                                // );
// 
// $hook['post_controller_constructor'][] = array(
                                // 'class'    => 'Language',
                                // 'function' => 'lang',
                                // 'filename' => 'language.php',
                                // 'filepath' => 'hooks'
                                // );
// 			
			
$hook['post_controller_constructor'][] = array(
                                'class'    => 'Hlogin',
                                'function' => 'check_login',
                                'filename' => 'hlogin.php',
                                'filepath' => 'hooks'
                                );
								
$hook['post_controller_constructor'][] = array(
                                'class'    => 'Hlogin',
                                'function' => 'permisos_login',
                                'filename' => 'hlogin.php',
                                'filepath' => 'hooks'
                                );

/* End of file hooks.php */
/* Location: ./application/config/hooks.php */