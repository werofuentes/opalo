<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Cat Serv Sections
| -------------------------------------------------------------------------
| This file lets you determine whether or not various sections of Profiler
| data are displayed when the Profiler is enabled.
| Please see the user guide for info:
|
|   http://codeigniter.com/user_guide/general/profiling.html
|
*/

$config['servicios']    = array(
                                    'avaluos'           => 'Avalúos',
                                    'construccion'      => 'Construcción',
                                    'decoracion'        => 'Decoración',
                                    'financiamiento'    => 'Financiamiento',
                                    'mantenimiento'     => 'mantenimiento',
                                    'notarias'          => 'Notarías'
                               );

/* End of file catserv.php */
/* Location: ./application/config/catserv.php */