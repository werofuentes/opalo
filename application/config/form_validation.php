<?php
$config = array(
	'searchengine/search' => array(
				array(
						'field'   => 'type_prop',
						'label'   => 'Tipo de propiedad',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Debe seleccionar un tipo!'
				),
				array(
						'field'   => 'locations',
						'label'   => 'locations',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Debe escribir una ubicación!'
				)
		),
	'centrocompras/datos_personales' => array(
				array(
						'field'   => 'nombre',
						'label'   => 'Nombre',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta Nombre!'
				),
				array(
						'field'   => 'apellidos',
						'label'   => 'Apellidos',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Faltan apellidos!'
				),
				array(
						'field'   => 'email',
						'label'   => 'Email',
						'rules'   => 'trim|required|xss_clean|valid_email|is_unique[usuarios.usu_username]',
						'message' => '¡Email invalido o ya existe!'
				),
				array(
						'field'   => 'estado',
						'label'   => 'Estado',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Debe especificar un estado!'
				)
		),	
	'centrocompras/datos_personales-factura' => array(
				array(
						'field'   => 'municipio',
						'label'   => 'Municipio',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta Municipio!'
				),
				array(
						'field'   => 'colonia',
						'label'   => 'Colonia',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta Colonia!'
				),
				array(
						'field'   => 'rfc',
						'label'   => 'RFC',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta RFC!'
				),
				array(
						'field'   => 'calle',
						'label'   => 'Calle',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta Calle!'
				),
				array(
						'field'   => 'num',
						'label'   => 'Num',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta Num!'
				),
				array(
						'field'   => 'cp',
						'label'   => 'CP',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta CP!'
				),
				array(
						'field'   => 'razon',
						'label'   => 'Razon',
						'rules'   => 'trim|required|xss_clean',
						'message' => '¡Falta razón social!'
				)
		),		
	'registro/save'     => array(
                array(
                        'field'   => 'name',
                        'label'   => 'Nombre',
                        'rules'   => 'trim|required|xss_clean',
                        'message' => '¡Indica tu nombre!'
                ),
                array(
                        'field'   => 'email',
                        'label'   => 'Email',
                        'rules'   => 'trim|required|xss_clean|valid_email',
                        'message' => '¡Indica un email válido!'
                ),
                array(
                        'field'   => 'telephone',
                        'label'   => 'Teléfono',
                        'rules'   => 'trim|required|xss_clean',
                        'message' => '¡Indica un teléfono!'
                ),
                array(
                        'field'   => 'passwd',
                        'label'   => 'Contraseña',
                        'rules'   => 'trim|required|xss_clean',
                        'message' => '¡Ingresa contraseña!'
                ),
                array(
                        'field'   => 'repasswd',
                        'label'   => 'Repita contraseña',
                        'rules'   => 'trim|required|xss_clean|matches[passwd]',
                        'message' => '¡Confirma contraseña!'
                )
    
       )
);