<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * mbsoft-cloud.com.mx - Sysmaster
 * Controller: Newsletter
 * 
 * @Src: /application/controllers/
 * @Copyright: Copyright 2014 - MBSOFT CLOUD mbsoft-cloud.com.mx
 * @Developer: Jessep Barba (jessep.barba@gmail.com)
 * @Create: 01-Marzo-204
 * 
*/

class Solicitudes extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
		$this->load->library('encrypt');
		$this->load->library('form_validation');
		$this->load->helper('move_files_helper');
		$this->load->model('cuestionarios_model');
		$this->load->model('estatus_ese_model');
		$this->load->model('estadocivil_model');
		$this->load->model('parentesco_model');
		$this->load->model('solicitudes_model');
		$this->load->model('tipo_candidato_model');
		$this->load->model('tipo_ese_model');
		$this->load->model('empresa_model');
		$this->load->model('employees_model');
		$this->load->model('puesto_model');
		$this->load->model('medio_vacantes_model');
		$this->load->model('cuestionarios_model');
		$this->load->model('referencia_escolares_model');
		$this->load->model('refcandidato_model');
		$this->load->model('refeconomicas_candidato_model');
		$this->load->model('documentos_model');
		$this->load->model('opciones_model');
		$this->load->model('escolaridad_model');
		$this->load->model('conceptos_eco_model');
		$this->load->model('evaluacion_laboral_model');
		$this->load->model('reflabcandidato_model');
		$this->load->model('refimss_model');
		$this->load->model('refvivienda_model');
		$this->load->model('estados_model');
		$this->load->model('municipalities_model');
		$this->load->model('logger_model');
		$this->load->library('email');
		$this->load->library('MY_Dompdf');
		
		
	}
	 public function fcrearExcel()
	 {
	 	$this->load->library('Excel');
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("SOEA")
							 ->setLastModifiedBy("SOEA")
							 ->setTitle("Socios")
							 ->setSubject("Socios")
							 ->setDescription("Listado de Socios")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");
		// Add some data
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
		$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->getFont()->setBold(true);
		
		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LISTADO ESE')
				->setCellValue('A2', 'FOLIO APRO')
				->setCellValue('B2', 'NOMBRE')
				->setCellValue('C2', 'FOLIO EMPRESA')
				->setCellValue('D2', 'EMPRESA')
				->setCellValue('E2', 'FECHA INGRESO')
				->setCellValue('F2', 'FECHA APLICACION')
				->setCellValue('G2', 'FECHA TERMINACION')
				->setCellValue('H2', 'ESTATUS');
				
		$data             = array();

		
		if($_SESSION['SEUS']['use_typ_id'] == 1 || $_SESSION['SEUS']['use_typ_id'] == 2  || $_SESSION['SEUS']['use_typ_id'] == 7 ){
		
		
			
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );
			
			$whereEmpresa           = null;
		}
		
		if($_SESSION['SEUS']['use_typ_id'] == 3 || $_SESSION['SEUS']['use_typ_id'] == 4){
			
			
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Usuario_has_Empresa',
											  'cond'  => 'Usuario_has_Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );
			

			
			$whereEmpresa['Usuario_has_Empresa.idUsuario'] = $_SESSION['SEUS']['use_id'];
			
		}	
		
		if( $_SESSION['SEUS']['use_typ_id'] == 5){
				
				
				
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );	
				
			$id_empleado = $_SESSION['SEUS']['id_empleado'];
			$whereEmpresa['ESE.idEmpleado'] = $id_empleado;
		
		}
					
				
		$usuarios         = $this->solicitudes_model->getAllSolicitudesESE($whereEmpresa,null,$join,null,null);		
					
		
		 $y = 3;
			for($x=0; $x < count($usuarios); $x++){
				$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
				$objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$y, $usuarios[$x]['idESE'])
				->setCellValue('B'.$y, $usuarios[$x]['Nombres'].' '.$usuarios[$x]['ApellidoPaterno'].' '.$usuarios[$x]['ApellidoMaterno'])
				->setCellValue('C'.$y, $usuarios[$x]['Folio'])
				->setCellValue('D'.$y, $usuarios[$x]['Nombre'])
				->setCellValue('E'.$y, $usuarios[$x]['FechaSolicitud'])
				->setCellValue('F'.$y, $usuarios[$x]['FechaTentativaVisita'])
				->setCellValue('G'.$y, $usuarios[$x]['FechaVisita'])
				->setCellValue('H'.$y, $usuarios[$x]['Descripcion']);
				$y++;
			}


		 // Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Listado ESE');


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		
		
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="listado_ESE.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		

		
		//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
				
				
		
	 }	
		
	public function index($offset = 0)
	{

		$data             = array();
		//print_r($_SESSION);
		$data['dia'] = date('Y-m-d');

		$where['idEstatusESE']      = '6';
		$estatus                    = $this->estatus_ese_model->getOneEstatusESE($where);
		$data['estatus']            = $estatus;
        $data['Estadocivil']        = $this->estadocivil_model->getAllEstadoCivil();
		$data['Parentesco']         = $this->parentesco_model->getAllParentesco();
		$data['TipoCandidato']      = $this->tipo_candidato_model->getAllTipoCandidato();
		
		$whereTipo           = array (
			                          'idCuestionarioTipo >' => '1'
			                         );
		$data['TipoEse']            = $this->tipo_ese_model->getAllTipoESE($whereTipo);

		$data['Puestos']            = $this->puesto_model->getAllPuestos();
		$data['MedioVacantes']      = $this->medio_vacantes_model->getAllMedioVacantes();
		
		
		$data['Estados']                 = $this->estados_model->getAllEstados();
		

		if($_SESSION['SEUS']['use_typ_id'] == 1 || $_SESSION['SEUS']['use_typ_id'] == 3 ){
			
	
			$data['Empresas']           = $this->empresa_model->getAllEmpresa();
		}
		
		
		if($_SESSION['SEUS']['use_typ_id'] == 2){
			
					$whereEMpresaAPro           = array (
			                          'Empresa.idEmpresa' => '5'
			                         );
			$data['Empresas']           = $this->empresa_model->getAllEmpresa($whereEMpresaAPro);
		}
		
		
		$data['baseURL']            = base_url();
		
		$this->parser->parse('solicitudes/ingresarSolicitud', $data);
      }

	
	
	public function mostrarObservaciones($id='')
	{
			$wheres['idESE'] = $id;
			//$ESE= $this->logger_model->getAlllogger($wheres);
			//$data['datos_mensaje'] = $ESE;
			$ESE= $this->solicitudes_model->getOneSolicitudesESE($wheres);
			$data['datos_mensaje'] = $ESE['ObservacionesESE'];
			//print_r($data['datos_mensaje']);
			$this->parser->parse('solicitudes/mensaje', $data);
			
			
	}
	
	
	public function addSolicitudes()
	{
			
			$dataref['idPuesto']                = $this->input->post('idPuesto', TRUE);
			$dataref['idSexo']                = $this->input->post('tipo_sexo', TRUE);
			$dataref['idMediosVacante']         = $this->input->post('idMediosVacante', TRUE);
			
			if($this->input->post('idParentesco', TRUE) != ''){
			  $dataref['idParentesco']            = $this->input->post('idParentesco', TRUE);	
			}else{
				 $dataref['idParentesco'] = 5;
			}
			
			//h:i:s $data['dia'] = date('Y-m-d');
			
		    $dataref['Nombres']                 = strtoupper($this->input->post('Nombres', TRUE));
			$dataref['ApellidoPaterno']         = strtoupper($this->input->post('ApellidoPaterno', TRUE));
			$dataref['ApellidoMaterno']         = strtoupper($this->input->post('ApellidoMaterno', TRUE));
			$FechaNac                           = $this->input->post('FechaNac', TRUE);
			$fechaNacimiento                    = explode('/', $FechaNac);
			$dataref['FechaNac']                = $fechaNacimiento[0].'-'.$fechaNacimiento[1].'-'.$fechaNacimiento[2];
			$dataref['LugarNac']                = strtoupper($this->input->post('LugarNac', TRUE));
			$dataref['Edad']                    = strtoupper($this->input->post('Edad', TRUE));
			$dataref['Mail']                    = strtoupper($this->input->post('Mail', TRUE));
			$dataref['Domicilio1']              = strtoupper($this->input->post('Domicilio1', TRUE));
			$dataref['Colonia']                 = strtoupper($this->input->post('Colonia', TRUE));
			$dataref['Ciudad']                  = strtoupper($this->input->post('Ciudad', TRUE));
			
			//$dataref['Municipio']               = strtoupper($this->input->post('Municipio', TRUE));
			//$dataref['Estado']                  = strtoupper($this->input->post('Estado', TRUE));
			$dataref['idMunicipio']               = $this->input->post('Municipio', TRUE);
			$dataref['idEstado']                  = strtoupper($this->input->post('Estado', TRUE));
			
			$dataref['CodigoPost']              = strtoupper($this->input->post('CodigoPost', TRUE));
			$dataref['TelCasa']                 = strtoupper($this->input->post('TelCasa', TRUE));
			$dataref['Cel']                     = strtoupper($this->input->post('Cel', TRUE));
			$dataref['OtroTel']                 = strtoupper($this->input->post('OtroTel', TRUE));
			$dataref['idEdoCivil']              = $this->input->post('idEdoCivil', TRUE);
			
			$dataref['RFC']                     = strtoupper($this->input->post('RFC', TRUE));
			$dataref['CURP']                    = strtoupper($this->input->post('CURP', TRUE));
			$dataref['NSS']                     = strtoupper($this->input->post('NSS', TRUE));
			$dataref['IFE']                     = strtoupper($this->input->post('IFE', TRUE));
			
			$dataref['Competencia']             = strtoupper($this->input->post('Competencia', TRUE));
			$dataref['Distribuidor']            = strtoupper($this->input->post('Distribuidor', TRUE));
			//$dataref['Familiares']              = strtoupper($this->input->post('Familiares', TRUE));
			$dataref['Area']                    = strtoupper($this->input->post('Area', TRUE));
			$dataref['Fotografia']              = $this->input->post('fotoCandidatoInput', TRUE);

            
		    $solicitudes                        = $this->solicitudes_model->addSolicitudes($dataref);
		

			 //$data['FechaSolicitud']            = date('Y-m-d');
			 $data['Folio']                     = $this->input->post('Folio', TRUE);
			 //$data['idTipoCandidato']           = $this->input->post('idTipoCandidato', TRUE);
			 $data['idEstatusESE']              = 6;
			 $data['idEmpresa']                 = $this->input->post('idEmpresa', TRUE);
			 $data['idCandidato']               = $solicitudes;
			 $data['idCuestionarioTipo']        = $this->input->post('idTipoESE', TRUE);
			 $data['FechaSolicitud']            = date('Y-m-d h:i:s');	
			 
			 ///////////////////////
			 ///////////////////// URL
					 $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
					 $longitudCadena=strlen($cadena);
					 $longitudPass=10;
						 
					 for($i=1 ; $i<=$longitudPass ; $i++){
				          $pos=rand(0,$longitudCadena-1);
				          $pass .= substr($cadena,$pos,1);
				     }
					 $data['TokenCandidato']          = $pass;//hash('sha256',$pass);
					 $urlCodifo = $data['TokenCandidato'];
					 
					 
					 $mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/iusacell.png" width="178" height="145" /><td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) Candidato continue su proceso de Estudio Socio Economico</td>
				  </tr>
				 
				  <tr>
				    <td colspan="2"><a href="http://grupoapro.adeexs.com/url/PreguntasANP/'.$urlCodifo.'" > >>> INGRESA <<< </a></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br> </td>
				  </tr>
				   <tr>
				    <td colspan="2"><img src="'.base_url().'inc/aviso.png" width="519" height="607" /></td>
				  </tr>
				</tablet>
				';

		 		$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);
				$email = $this->input->post('Mail', TRUE);
				
				$this->email->from('no-replay@grupoapro.com', 'PREGUNTAS REFERENCIAS LABORALES Y ANTECEDENTES NO PENALES');
		        $this->email->to($email);
				$asunto = 'INGRESO REFERENCIAS LABORALES  Y ANTECEDENTES NO PENALES';
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
			 ///////////////////////
			 
			 $ESE                               = $this->solicitudes_model->addESE($data);
			
			
			
			$dataEseCuest['idESE']                  = $ESE;
			$dataEseCuest['idCuestionario']         = $this->input->post('idTipoESE', TRUE);
			$CueEse                                 = $this->solicitudes_model->addESECuestionario($dataEseCuest);
			 
			 
			// $dataCandCues['idTipoCandidato']       = $this->input->post('idTipoCandidato', TRUE);
			// $dataCandCues['idCuestionarioTipo']    = $this->input->post('idTipoESE', TRUE);
			// $dataCandCues['Prioridad']    = 1;
            // $CandCues                              = $this->solicitudes_model->addCuestionarioTipo($dataCandCues);
			
			
			redirect('solicitudes/solicitudesESE');
	}

	public function RenviarCuestionario($id='')
	{

					 $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
					 $longitudCadena=strlen($cadena);
					 $longitudPass=10;
						 
					 for($i=1 ; $i<=$longitudPass ; $i++){
				          $pos=rand(0,$longitudCadena-1);
				          $pass .= substr($cadena,$pos,1);
				     }
					 $data['TokenCandidato']          = $pass;//hash('sha256',$pass);
					 
					 $where['idESE']       = $id;

		
			        $urlCodifo = $data['TokenCandidato'];
					$Ese  = $this->solicitudes_model->updSolicitudesESE($data,$where);
					
					$wheres['ESE.idESE']         = $id;
		
		
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  )
		                                 );
		
		
			$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheres,$join);			
			
										 
					 
			$mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/iusacell.png" width="178" height="145" /><td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) Candidato continue su proceso de Estudio Socio Economico</td>
				  </tr>
				  <tr>
				    <td colspan="2"><a href="http://grupoapro.adeexs.com/url/PreguntasANP/'.$urlCodifo.'" > >>> INGRESA <<< </a></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br> </td>
				  </tr>
				   <tr>
				    <td colspan="2"><img src="'.base_url().'inc/aviso.png" width="519" height="607" /></td>
				  </tr>
				</tablet>
				';
					 
		
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				$this->email->initialize($config);
				$email = $this->input->post('Mail', TRUE);
				
				$this->email->from('no-replay@grupoapro.com', 'PREGUNTAS REFERENCIAS LABORALES Y ANTECEDENTES NO PENALES');
		        $this->email->to($ESE['Mail']);
				$asunto = 'INGRESO REFERENCIAS LABORALES  Y ANTECEDENTES NO PENALES';
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send(); 
					 
		redirect('solicitudes/solicitudesESE');
	}

    public function solicitudesESE()
    {
    	
		$data             = array();

		
		if($_SESSION['SEUS']['use_typ_id'] == 1  || $_SESSION['SEUS']['use_typ_id'] == 7 ){
		
		
			
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );
			
			$whereEmpresa           = null;
		}
		
		if($_SESSION['SEUS']['use_typ_id'] == 3 || $_SESSION['SEUS']['use_typ_id'] == 4){
			
			
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Usuario_has_Empresa',
											  'cond'  => 'Usuario_has_Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );
			

			
			$whereEmpresa['Usuario_has_Empresa.idUsuario'] = $_SESSION['SEUS']['use_id'];
			
		}	
		
		if( $_SESSION['SEUS']['use_typ_id'] == 5){
				
				
				
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );	
				
			$id_empleado = $_SESSION['SEUS']['id_empleado'];
			$whereEmpresa['ESE.idEmpleado'] = $id_empleado;
		
		}
		
		if( $_SESSION['SEUS']['use_typ_id'] == 2){
				
				
				
			$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ) ,  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Empresa',
											  'cond'  => 'Empresa.idEmpresa = ESE.idEmpresa',
											  'type'  => 'inner' 
											  )
											   
		                                 );	
				
			$id_empresa = 5;
			$whereEmpresa['ESE.idEmpresa'] = $id_empresa;
		
		}
			

			
			
        $this->load->library('pagination'); //Cargamos la librería de paginación
        $config['base_url'] = base_url().'solicitudes/solicitudesESE'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
        $config['total_rows'] = $this->solicitudes_model->getAllSolicitudesESE($whereEmpresa,null,$join);//calcula el número de filas  
        $config['per_page'] = $pages; //Número de registros mostrados por páginas
        $config['num_links'] = 20; //Número de links mostrados en la paginación
        $config['first_link'] = 'Primera';//primer link
        $config['last_link'] = 'Última';//último link
        $config["uri_segment"] = 3;//el segmento de la paginación
        $config['next_link'] = 'Siguiente';//siguiente link
        $config['prev_link'] = 'Anterior';//anterior link
        $config['full_tag_open'] = '<div class="span6"><div class="pagination pull-right no-margin">';//el div que debemos maquetar
        $config['full_tag_close'] = '</div></div>';//el cierre del div de la paginación
        $this->pagination->initialize($config); //inicializamos la paginación        
        
        
     
		
		$usuarios         = $this->solicitudes_model->getAllSolicitudesESE($whereEmpresa,null,$join,null,null,$config['per_page'],$this->uri->segment(3));
		//die($this->db->last_query());
		
		//echo "<pre>";
		//print_r($usuarios);
		//echo "</pre>";
		
		$data['ESE'] = $usuarios;
		$data['NumPaginas'] = $this->pagination->create_links();
        
	
	
	   $this->parser->parse('solicitudes/solicitudesEse_form', $data);
		
    }



    public function deleteSolicitud($id = null)
	{
       
       	$whereEliminar['idESE']      = $id;
		$users                        = $this->solicitudes_model->dropSolicitudesESE($whereEliminar);
		redirect('solicitudes/solicitudesESE');
	}
	
	
	public function apoyoSolicitud($id = null)
	{
		
		$data             = array();
		$where['idESE']      =     $id;
		
		 $join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ),  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  )
		                                 );
		
		$Ese               = $this->solicitudes_model->getOneSolicitudesESE($where,$join);
	    $data['ESE'] =  $Ese;
		
		$fechaVisita                 = explode(' ', $Ese['FechaTentativaVisita']);
		
		$data['FechaVisita'] = $fechaVisita[0];
		$data['horaVisita'] = $fechaVisita[1];
		
		//$where2['tipo_usuario !=']    =     2;
		$where2['use_typ_id']   =     5;
		$where2['idEstado']     =     $Ese['idEstado'];
		$where2['use_active']   =     1;
		
		 $join2                = array(
			                             array(
											  'table' => 'users',
											  'cond'  => 'users.id_empleado = Empleado.idEmpleado',
											  'type'  => 'inner' 
											  )
		                                 );
		$Empleados         = $this->employees_model->getAllUsers($where2,null,$join2);
		$data['empleados'] = $Empleados;
		//die($this->db->last_query());	
		//echo $this->db->last_query();
		
		///getOneSolicitudesESE
		
		
		$this->parser->parse('solicitudes/solicitudesApoyo', $data);
		
	}
	
	public function apoyoSolicitudAceptar($id = null){
		
		$data             = array();
		$where['idESE']      =     $id;
		
		 $join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ),  
									     array(
											  'table' => 'EstatusESE',
											  'cond'  => 'EstatusESE.idEstatusESE = ESE.idEstatusESE',
											  'type'  => 'inner' 
											  )
		                                 );
		
		$Ese               = $this->solicitudes_model->getOneSolicitudesESE($where,$join);

	   
		
		
	    $data['ESE'] =  $Ese;
		
		$Empleados         = $this->employees_model->getAllUsers();
		$data['empleados'] = $Empleados;
		
		
		$this->parser->parse('solicitudes/solicitudesApoyo2', $data);
		
	}
	
	public function AceptarSolcitudApoyo(){
		
		$where['idESE']       = $this->input->post('idESE', TRUE);
		$data['idEstatusESE'] = '7';
		
		///////////////////// URL
		 //$cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		 //$longitudCadena=strlen($cadena);
		 //$longitudPass=10;
			 
		 //for($i=1 ; $i<=$longitudPass ; $i++){
	          //$pos=rand(0,$longitudCadena-1);
	          //$pass .= substr($cadena,$pos,1);
	     //}
		 //$data['TokenCandidato']          = $pass;//hash('sha256',$pass);
		
        $urlCodifo = 		 $data['TokenCandidato'];
		$Ese                  = $this->solicitudes_model->updSolicitudesESE($data,$where);
		
		
	   $mensaje = '
				<tablet>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/iusacell.png" width="178" height="145" /><td>
				  </tr>
				  <tr>
				    <td colspan="2"><br><br><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) Candidato continue su proceso de Estudio Socio Economico</td>
				  </tr>
				  <tr>
				    <td colspan="2">CODIGO: '.$urlCodifo.'</td>
				  </tr>
				  <tr>
				    <td colspan="2"><a href="http://grupoapro.adeexs.com/url/" > >>> INGRESA <<< </a></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br> </td>
				  </tr>
				   <tr>
				    <td colspan="2"><img src="'.base_url().'inc/aviso.png" width="519" height="607" /></td>
				  </tr>
				</tablet>
				';

		 		//$config['wordwrap'] = TRUE;
				//$config['mailtype'] = "html";
				//$this->email->initialize($config);
				//$email = $this->input->post('email', TRUE);
				
				//$this->email->from('no-replay@grupoapro.com', 'PREGUNTAS REFERENCIAS LABORALES');
		        //$this->email->to($email);
				//$asunto = 'INGRESO REFERENCIAS LABORALES';
		        //$this->email->subject($asunto);
		        //$this->email->message($mensaje);
		        //$this->email->send();
		

		redirect('solicitudes/solicitudesESE');
	}
	
	
	public function CancelarSolcitudApoyo(){
		
		$where['idESE']       = $this->input->post('idESE', TRUE);
		$data['idEstatusESE'] = '6';
		$data['idEmpleado'] = '';
		
		$Ese1                  = $this->solicitudes_model->updSolicitudesESE($data,$where);
		
		$wheres['ESE.idESE']         = $this->input->post('idESE', TRUE);
		
		
		 $join                          = array(
			                               array(
											   'table' => 'Candidato',
											   'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											   'type'  => 'inner' 
											   ),
										   array(
											   'table' => 'ESE_has_CuestionarioESE',
											   'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											   'type'  => 'inner' 
											   ),
										   array(
											   'table' => 'Empleado',
											   'cond'  => 'Empleado.idEmpleado = ESE.idEmpleado',
											   'type'  => 'inner' 
											   )
											   
		                                  );
 		
 		
		$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheres,$join);
		
		
		$asunto = 'Solicitud ESE no aceptada por el investigador';
		
		 $mensaje = '
				<table>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/iusacell.png" width="178" height="145" /><td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) Coordinador(a), el Investigador '.$ESE['Nombres'].' '.$ESE['ApellidoPaterno'].' '.$ESE['ApellidoMaterno'].' rechazo la solicitud  </td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td ><B>FOLIO APRO:</B></td>
				     <td align="center">'.$this->input->post('idESE', TRUE).'</td>
				  </tr>
				</table>
				';
	
		        $config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				
				$this->email->initialize($config);				
				$email = 'coordina.ese@grupoapro.com.mx';
				
				$this->email->from('no-replay@grupoapro.com', 'SISTEMA ESE');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
		

		redirect('solicitudes/solicitudesESE');
	}
		
	public function enviarSolicitudApoyo(){
		$dataref['idESE']                 = $this->input->post('idESE', TRUE);
		$dataref['folio']                 = $this->input->post('folio', TRUE);
		$dataref['nombre']                = $this->input->post('nombre', TRUE);
		$dataref['contacto']              = $this->input->post('contacto', TRUE);
		$dataref['direccion']              = $this->input->post('direccion', TRUE);
		
		
		$dataref['empleado']              = $this->input->post('empleado', TRUE);
		$dataref['FechaVisita']           = $this->input->post('FechaVisita', TRUE);
		//$dataref['hora_de']               = $this->input->post('hora_de', TRUE);
		$dataref['hora_de']               = $this->input->post('hora_de', TRUE);
		//$dataref['hora_de1']              = $this->input->post('hora_de1', TRUE);
		$dataref['viaticos']              = $this->input->post('viaticos', TRUE);
		
		
		
		
		
		 $asunto = 'Solicitud ESE asignada Grupo Apro';
		
		 $mensaje = '
				<table>
				  <tr>
				    <td colspan="2"><img src="'.base_url().'inc/iusacell.png" width="178" height="145" /><td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td colspan="2"> Estimado(a) se te asigno la siguiente solicitud ESE, ingresa al sistema para aceptarla o rechazarla</td>
				  </tr>
				  <tr>
				     <td colspan="2" align="center"><a href="'.base_url().'" >'.base_url().'</a></td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td ><B>FOLIO:</B></td>
				     <td align="center">'.$dataref['folio'].'</td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td ><B>NOMBRE CANDIDATO:</B></td>
				     <td align="center">'.$dataref['nombre'].'</td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td ><B>CONTACTO:</B></td>
				     <td align="center">'.$dataref['contacto'].'</td>
				  </tr>
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				   <tr>
				     <td ><B>DIRECCION:</B></td>
				     <td align="center">'.$dataref['direccion'].'</td>
				  </tr>
				  <br>
				  <tr>
				     <td ><B>FECHA DE VISITA:</B></td>
				     <td align="center">'.$dataref['FechaVisita'].'</td>
				  </tr>
			
				  
				   <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  <tr>
				     <td ><B>HORARIO:</B></td>
				     <td align="center">'.$dataref['hora_de'].'</td>
				  </tr>
				  <tr>
				    <td colspan="2"><br></td>
				  </tr>
				  
				  <tr>
				     <td ><B>VIATICOS:</B></td>
				     <td align="center">'.$dataref['viaticos'].'</td>
				  </tr>
				   <tr>
				    <td colspan="2">
				    <br>
				  <br>
				  <br>
				  <br>
				  <tr>
				  </td>
				  </tr>
				  
				     <td colspan="2" style="color:red"><B>Importante</B></td>
				  </tr>
				  
				  </tr>
				  
				     <td colspan="2"><B>Si el estudio no cumple con las características acordadas, se te regresara para que lo modifiques y envíes con las correcciones adecuadas.</B></td>
				  </tr>
				  </tr>
				  
				     <td colspan="2"><B>Comunícate con el candidato para formalizar la visita, para acordar una hora exacta de vista y para que te explique como llegar a su domicilio</B></td>
				  </tr>
				</table>
				';
	
		        $config['wordwrap'] = TRUE;
				$config['mailtype'] = "html";
				
				$this->email->initialize($config);
				
				$where['idEmpleado']    = $this->input->post('empleado', TRUE);
				$empleados             = $this->employees_model->getOneEmpleados($where);
				
				
				
				$email = $empleados['Mail'];
				
				$this->email->from('no-replay@grupoapro.com', 'SISTEMA ESE');
		        $this->email->to($email);
		        $this->email->subject($asunto);
		        $this->email->message($mensaje);
		        $this->email->send();
				
				$wheres['idESE']       = $this->input->post('idESE', TRUE);
				//$data['idEmpleado']  = 1;//$this->input->post('empleado', TRUE);
				
				
				$data['idEstatusESE']  = 6;	
				$FechaNac                        = $this->input->post('FechaVisita', TRUE);
				$fechaNacimiento                 = explode('/', $FechaNac);
				$data['FechaTentativaVisita']    = $fechaNacimiento[0].'-'.$fechaNacimiento[1].'-'.$fechaNacimiento[2].' '.$this->input->post('hora_de', TRUE).':00';
                $data['idEmpleado']              = $this->input->post('empleado', TRUE);
                //$data['horario_inicio']          = $this->input->post('hora_de', TRUE);
				//$data['horario_final']           = $this->input->post('hora_de1', TRUE);
				$data['ViaticoAutorizado']       = $this->input->post('viaticos', TRUE);
				$data['idEstatusESE']            = 5;
				

            	$usuarios              = $this->solicitudes_model->updSolicitudesESE($data,$wheres);	
				//die($this->db->last_query());	
                redirect('solicitudes/solicitudesESE');
	}
	
	public function editarANP(){
		
		//// ELIMINA PARA VOLVER A REGISTRAR
		$whereesCuestiona['idCandidato']   = $this->input->post('idCandidato', TRUE);	
		$whereesCuestiona['idCuestionario']   = $this->input->post('idTipoEse', TRUE);	
		$ResCuestionaDel = $this->cuestionarios_model->dropRespCandidato($whereesCuestiona);	
		
		
		$NumeroPreguntas                    = $this->input->post('NumeroPreguntas', TRUE);
		
		for ($i=1; $i <= $NumeroPreguntas ; $i++) { 

			 if(isset($_POST['Respuesta'.$i])){
				if(!is_null($_POST['Respuesta'.$i])){
					
					 $RespuestaVal = $_POST['Respuesta'.$i];
				     $dataRespuestasCuestionario['Respuesta'.$i] = $RespuestaVal;
				
				}else{
					
					 $RespuestaVal = '-';
					$dataRespuestasCuestionario['Respuesta'.$i] = $RespuestaVal;
				}
				
			}
		}
		$dataRespuestasCuestionario['idCuestionario'] =$this->input->post('idTipoEse', TRUE);
		$dataRespuestasCuestionario['idCandidato']    =$this->input->post('idCandidato', TRUE);
		
		$ResCuestiona       = $this->cuestionarios_model->addResCuestionario($dataRespuestasCuestionario);
		
		redirect('solicitudes/solicitudesESE');
	}	
	
	
	public function editSolicitud(){
	    
		
		$logger = array();
		$logger['idUsuario'] = $_SESSION['SEUS']['use_id'];
		$logger['idESE'] = $this->input->post('idESE', TRUE);
		
		if($this->input->post('observacionesEstatus', TRUE)){
			$MensajeEstatus = $this->input->post('observacionesEstatus', TRUE);
		}else{
			$MensajeEstatus = 'Sin ninguna Obervacion';
		}
		
		switch ($this->input->post('idEstatusESE', TRUE)) {
			case '1':
				$varEstatus = 'Sin Resultado';
				break;
			case '2':
				$varEstatus = 'Viable';
				break;
			case '3':
				$varEstatus = 'No viable';
				break;
			case '4':
				$varEstatus = 'Con reservas';
				break;
			case '5':
				$varEstatus = 'Asignado';
				break;
			case '6':
				$varEstatus = 'Sin Asignar';
				break;
			case '7':
				$varEstatus = 'En proceso';
				break;
			case '8':
				$varEstatus = 'Cancelado';
				break;
			case '9':
				$varEstatus = 'Suspendido';
				break;
			
			default:
				break;
		}
		
		
		////////////////////////////
		$whereEseObervaciones['idESE'] = $logger['idESE'];
		$EseObservaciones = $this->solicitudes_model->getOneSolicitudesESE($whereEseObervaciones);
		////////////////////////////
		$loggers2 = array(
               'ObservacionesESE' => $EseObservaciones['ObservacionesESE'].'<br> Usuario: '.$_SESSION['SEUS']['use_session'].' - Estatus: '.$varEstatus.' - Obervacion: '.$MensajeEstatus
            );
		//$logger['ObservacionesESE'] = 'Usuario: '.$_SESSION['SEUS']['use_session'].' - Estatus: '.$varEstatus.' - Obervacion: '.$MensajeEstatus;
		

		$this->db->where('idESE', $logger['idESE']);
		$this->db->update('ESE', $loggers2); 
		
		//$this->db->insert('logger', $logger); 
		
		///////////////////////////////////////////////////////////////////////////////////////
		$comprarEstatus = $this->input->post('idEstatusESE', TRUE);
		
		if(($_SESSION['PERMISOS']['per_typ_id'] == 1 || $_SESSION['PERMISOS']['per_typ_id'] == 3 || $_SESSION['PERMISOS']['per_typ_id'] == 4 || $_SESSION['PERMISOS']['per_typ_id'] == 7) && ($comprarEstatus != 2 && $comprarEstatus != 3 && $comprarEstatus != 8) ){
		  
			$whereEseCandidatoEditar['idCandidato'] = $this->input->post('idCandidato', TRUE);
			
			$dataref['idMediosVacante']         = $this->input->post('idMediosVacante', TRUE);
			
			if($this->input->post('idParentesco', TRUE) != ''){
			  $dataref['idParentesco']            = $this->input->post('idParentesco', TRUE);	
			}else{
				 $dataref['idParentesco'] = 5;
			}
			
			//h:i:s $data['dia'] = date('Y-m-d');
			
		    $dataref['Nombres']                 = strtoupper($this->input->post('Nombres', TRUE));
			$dataref['ApellidoPaterno']         = strtoupper($this->input->post('ApellidoPaterno', TRUE));
			$dataref['ApellidoMaterno']         = strtoupper($this->input->post('ApellidoMaterno', TRUE));
			$FechaNac                           = $this->input->post('FechaNac', TRUE);
			$fechaNacimiento                    = explode('/', $FechaNac);
			$dataref['FechaNac']                = $fechaNacimiento[0].'-'.$fechaNacimiento[1].'-'.$fechaNacimiento[2];
			$dataref['LugarNac']                = strtoupper($this->input->post('LugarNac', TRUE));
			$dataref['Edad']                    = strtoupper($this->input->post('Edad', TRUE));
			$dataref['Mail']                    = strtoupper($this->input->post('Mail', TRUE));
			$dataref['Domicilio1']              = strtoupper($this->input->post('Domicilio1', TRUE));
			$dataref['Colonia']                 = strtoupper($this->input->post('Colonia', TRUE));
			$dataref['Ciudad']                  = strtoupper($this->input->post('Ciudad', TRUE));
			
			//$dataref['Municipio']               = strtoupper($this->input->post('Municipio', TRUE));
			//$dataref['Estado']                  = strtoupper($this->input->post('Estado', TRUE));
			$dataref['idMunicipio']               = $this->input->post('Municipio', TRUE);
			$dataref['idEstado']                  = strtoupper($this->input->post('Estado', TRUE));
			
			$dataref['CodigoPost']              = strtoupper($this->input->post('CodigoPost', TRUE));
			$dataref['TelCasa']                 = strtoupper($this->input->post('TelCasa', TRUE));
			$dataref['Cel']                     = strtoupper($this->input->post('Cel', TRUE));
			$dataref['OtroTel']                 = strtoupper($this->input->post('OtroTel', TRUE));
			$dataref['idEdoCivil']              = $this->input->post('idEdoCivil', TRUE);
			
			$dataref['RFC']                     = strtoupper($this->input->post('RFC', TRUE));
			$dataref['CURP']                    = strtoupper($this->input->post('CURP', TRUE));
			$dataref['NSS']                     = strtoupper($this->input->post('NSS', TRUE));
			$dataref['IFE']                     = strtoupper($this->input->post('IFE', TRUE));
			
			$dataref['Competencia']             = strtoupper($this->input->post('Competencia', TRUE));
			$dataref['Distribuidor']            = strtoupper($this->input->post('Distribuidor', TRUE));
			//$dataref['Familiares']              = strtoupper($this->input->post('Familiares', TRUE));
			$dataref['Area']                    = strtoupper($this->input->post('Area', TRUE));
			$dataref['Fotografia']              = $this->input->post('fotoCandidatoInput', TRUE);

            
		    $solicitudesUPDATE                  = $this->solicitudes_model->updSolicitudes($dataref,$whereEseCandidatoEditar);
		
		}
		
		
		//////////////////////////////////////////////////////////////////////
		
		
		
		
		//// ELIMINA PARA VOLVER A REGISTRAR
		$whereesCuestiona['idCandidato']   = $this->input->post('idCandidato', TRUE);	
		$whereesCuestiona['idCuestionario']   = $this->input->post('idTipoEse', TRUE);
		$ResCuestionaDel = $this->cuestionarios_model->dropRespCandidato($whereesCuestiona);	
			
		$whereesreferenciaDel['idCandidato']   = $this->input->post('idCandidato', TRUE);		
		$referenciaDel = $this->referencia_escolares_model->dropReferenciaEscolar($whereesreferenciaDel);
		
		$whereesreferenciaCandidatoDel['idCandidato']   = $this->input->post('idCandidato', TRUE);		
		$referenciaCandidatoDel = $this->refcandidato_model->dropReferenciasCandidato($whereesreferenciaCandidatoDel);
		
		$whereesreferenciaLabCandidatoDel['idCandidato']   = $this->input->post('idCandidato', TRUE);		
		$referenciaLabCandidatoDel = $this->reflabcandidato_model->dropReferenciasLabCandidato($whereesreferenciaLabCandidatoDel);
				
		$whereesreferenciasImssDel['idCandidato']   = $this->input->post('idCandidato', TRUE);		
		$referenciasImssDel = $this->refimss_model->dropReferenciasImssCandidato($whereesreferenciasImssDel);


		$whereesreferenciasViviendaDel['idCandidato']   = $this->input->post('idCandidato', TRUE);		
		$referenciasViviendaDel = $this->refvivienda_model->dropReferenciasViviendaCandidato($whereesreferenciasViviendaDel);
		
		
		$whereesreferenciasEconomicasCandDel['idCandidato']   = $this->input->post('idCandidato', TRUE);		
		$referenciasEconomicasCandDel = $this->refeconomicas_candidato_model->dropReferenciasEconomicasCandidato($whereesreferenciasEconomicasCandDel);
		///////
		
		if(isset($_POST['idEstatusESE'])){
	    	$where['idCandidato']    = $this->input->post('idCandidato', TRUE);
			$data['idEstatusESE']    = $this->input->post('idEstatusESE', TRUE);
			$data['FechaVisita']  = date('Y-m-d H:i:m');/// LA FECHA DE SOLICITUD ES CUANDO SE CERRO
			$EseEstausMod            = $this->solicitudes_model->updSolicitudesESE($data,$where);
		}
		
		if(isset($_POST['fotoCandidatoInput']) && $_POST['fotoCandidatoInput'] != null){
			$where3Imagen['idCandidato']  = $this->input->post('idCandidato', TRUE);
			$data3Imagen['Fotografia']    = $this->input->post('fotoCandidatoInput', TRUE);
			$EseEstausMod                 = $this->solicitudes_model->updSolicitudes($data3Imagen,$where3Imagen);
			
		}	
		
		$NumeroPreguntas                    = $this->input->post('NumeroPreguntas', TRUE);
	
		for ($i=1; $i <= $NumeroPreguntas ; $i++) { 

			 if(isset($_POST['Respuesta'.$i])){
				if(!is_null($_POST['Respuesta'.$i])){
					
					 $RespuestaVal = $_POST['Respuesta'.$i];
				     $dataRespuestasCuestionario['Respuesta'.$i] = strtoupper($RespuestaVal);
				
				}else{
					
					 $RespuestaVal = '-';
					$dataRespuestasCuestionario['Respuesta'.$i] = $RespuestaVal;
				}
				
			}
		}
		
		
		$dataRespuestasCuestionario['idCuestionario'] =$this->input->post('idTipoEse', TRUE);
		$dataRespuestasCuestionario['idCandidato']    =$this->input->post('idCandidato', TRUE);

		$ResCuestiona       = $this->cuestionarios_model->addResCuestionario($dataRespuestasCuestionario);
		

		//ReferenciaEscolar
		if(isset($_POST['nivel_escolar9']) && $_POST['idDocumentoEscolar9'] != ''){

			$dataReferenciaEscolar9['idCandidato']         =$this->input->post('idCandidato', TRUE);
			
			
			if($this->input->post('idDocumentoEscolar9', TRUE)){
				$dataReferenciaEscolar9['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar9', TRUE);
			}else{
			    $dataReferenciaEscolar9['idDocumentoEscolar']  =1;	
			}
			
			
			$dataReferenciaEscolar9['idEstatusEscolar']    =1;
			
			$dataReferenciaEscolar9['idEscolaridad']       =$this->input->post('nivel_escolar9', TRUE);

		
			
			$dataReferenciaEscolar9['PeriodoInicio']       =$this->input->post('inicio_escolar9', TRUE);
			$dataReferenciaEscolar9['PeriodoFin']          =$this->input->post('fin_escolar9', TRUE);
			$dataReferenciaEscolar9['NombreInst']          =strtoupper($this->input->post('nombre_institucion9', TRUE));
			$dataReferenciaEscolar9['Folio']               =strtoupper($this->input->post('folio9', TRUE));
			
			
			$referencia9       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar9);
		}

		if(isset($_POST['nivel_escolar8']) && $_POST['idDocumentoEscolar8'] != ''){

			$dataReferenciaEscolar8['idCandidato']         =$this->input->post('idCandidato', TRUE);
			//$dataReferenciaEscolar8['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar8', TRUE);
			
			if($this->input->post('idDocumentoEscolar8', TRUE)){
				$dataReferenciaEscolar8['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar8', TRUE);
			}else{
			    $dataReferenciaEscolar8['idDocumentoEscolar']  =1;	
			}
			
			$dataReferenciaEscolar8['idEstatusEscolar']    =1;
			$dataReferenciaEscolar8['idEscolaridad']       =$this->input->post('nivel_escolar8', TRUE);
			$dataReferenciaEscolar8['PeriodoInicio']       =$this->input->post('inicio_escolar8', TRUE);
			$dataReferenciaEscolar8['PeriodoFin']          =$this->input->post('fin_escolar8', TRUE);
			$dataReferenciaEscolar8['NombreInst']          =strtoupper($this->input->post('nombre_institucion8', TRUE));
			$dataReferenciaEscolar8['Folio']               =strtoupper($this->input->post('folio8', TRUE));
			
			$referencia8       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar8);
		}


		if(isset($_POST['nivel_escolar1']) && $_POST['idDocumentoEscolar1'] != ''){

			$dataReferenciaEscolar1['idCandidato']         =$this->input->post('idCandidato', TRUE);
			//$dataReferenciaEscolar1['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar1', TRUE);
			if($this->input->post('idDocumentoEscolar1', TRUE)){
				$dataReferenciaEscolar1['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar1', TRUE);
			}else{
			    $dataReferenciaEscolar1['idDocumentoEscolar']  =1;	
			}
			
			
			$dataReferenciaEscolar1['idEstatusEscolar']    =1;
			$dataReferenciaEscolar1['idEscolaridad']       =$this->input->post('nivel_escolar1', TRUE);

			
			
			$dataReferenciaEscolar1['PeriodoInicio']       =$this->input->post('inicio_escolar1', TRUE);
			$dataReferenciaEscolar1['PeriodoFin']          =$this->input->post('fin_escolar1', TRUE);
			$dataReferenciaEscolar1['NombreInst']          =strtoupper($this->input->post('nombre_institucion1', TRUE));
			$dataReferenciaEscolar1['Folio']               =strtoupper($this->input->post('folio1', TRUE));
			
			$referencia1       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar1);
			
		}

		if(isset($_POST['nivel_escolar2']) && $_POST['idDocumentoEscolar2'] != ''){

			$dataReferenciaEscolar2['idCandidato']         =$this->input->post('idCandidato', TRUE);
			//$dataReferenciaEscolar2['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar2', TRUE);
			if($this->input->post('idDocumentoEscolar2', TRUE)){
				$dataReferenciaEscolar2['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar2', TRUE);
			}else{
			    $dataReferenciaEscolar2['idDocumentoEscolar']  =1;	
			}
			
			
			$dataReferenciaEscolar2['idEstatusEscolar']    =1;
			$dataReferenciaEscolar2['idEscolaridad']       =$this->input->post('nivel_escolar2', TRUE);

			
			$dataReferenciaEscolar2['PeriodoInicio']       =$this->input->post('inicio_escolar2', TRUE);
			$dataReferenciaEscolar2['PeriodoFin']          =$this->input->post('fin_escolar2', TRUE);
			$dataReferenciaEscolar2['NombreInst']          =strtoupper($this->input->post('nombre_institucion2', TRUE));
			$dataReferenciaEscolar2['Folio']               =strtoupper($this->input->post('folio2', TRUE));
			
			$referencia2       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar2);
			
		}

        if(isset($_POST['nivel_escolar6']) && $_POST['idDocumentoEscolar6'] != ''){

			$dataReferenciaEscolar6['idCandidato']         =$this->input->post('idCandidato', TRUE);
			$dataReferenciaEscolar6['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar6', TRUE);

			$dataReferenciaEscolar6['idEstatusEscolar']    =1;
			$dataReferenciaEscolar6['idEscolaridad']       =$this->input->post('nivel_escolar6', TRUE);

			
			$dataReferenciaEscolar6['PeriodoInicio']       =$this->input->post('inicio_escolar6', TRUE);
			$dataReferenciaEscolar6['PeriodoFin']          =$this->input->post('fin_escolar6', TRUE);
			$dataReferenciaEscolar6['NombreInst']          =strtoupper($this->input->post('nombre_institucion6', TRUE));
			$dataReferenciaEscolar6['Folio']               =strtoupper($this->input->post('folio6', TRUE));
			
			$referencia6       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar6);			
		}
		
		if(isset($_POST['nivel_escolar7']) && $_POST['idDocumentoEscolar7'] != ''){

			$dataReferenciaEscolar7['idCandidato']         =$this->input->post('idCandidato', TRUE);
			$dataReferenciaEscolar7['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar7', TRUE);
	
			
			$dataReferenciaEscolar7['idEstatusEscolar']    =1;
			$dataReferenciaEscolar7['idEscolaridad']       =$this->input->post('nivel_escolar7', TRUE);

			
			
			$dataReferenciaEscolar7['PeriodoInicio']       =$this->input->post('inicio_escolar7', TRUE);
			$dataReferenciaEscolar7['PeriodoFin']          =$this->input->post('fin_escolar7', TRUE);
			$dataReferenciaEscolar7['NombreInst']          =strtoupper($this->input->post('nombre_institucion7', TRUE));
			$dataReferenciaEscolar7['Folio']               =strtoupper($this->input->post('folio7', TRUE));
			
			$referencia7       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar7);			
			
		}
		
		
		if(isset($_POST['nivel_escolar4']) && $_POST['idDocumentoEscolar4'] != ''){

			$dataReferenciaEscolar4['idCandidato']         =$this->input->post('idCandidato', TRUE);
			//$dataReferenciaEscolar4['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar4', TRUE);
			if($this->input->post('idDocumentoEscolar4', TRUE)){
				$dataReferenciaEscolar4['idDocumentoEscolar']  =$this->input->post('idDocumentoEscolar4', TRUE);
			}else{
			    $dataReferenciaEscolar4['idDocumentoEscolar']  =1;	
			}
			
			$dataReferenciaEscolar4['idEstatusEscolar']    =1;
			$dataReferenciaEscolar4['idEscolaridad']       =$this->input->post('nivel_escolar4', TRUE);

			
			
			$dataReferenciaEscolar4['PeriodoInicio']       =$this->input->post('inicio_escolar4', TRUE);
			$dataReferenciaEscolar4['PeriodoFin']          =$this->input->post('fin_escolar4', TRUE);
			$dataReferenciaEscolar4['NombreInst']          =strtoupper($this->input->post('nombre_institucion4', TRUE));
			$dataReferenciaEscolar4['Folio']               =strtoupper($this->input->post('folio4', TRUE));
			
			$referencia4       = $this->referencia_escolares_model->addReferenciaEscolar($dataReferenciaEscolar4);			
			
		}
		
		
		if(isset($_POST['nombre_familiar1']) &&  $_POST['nombre_familiar1'] != ''){
			$dataRefPersonalesCandidato1['idEscolaridad']         =$this->input->post('escolaridad_familiar1', TRUE);
			$dataRefPersonalesCandidato1['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato1['idParentesco']          =$this->input->post('parentesco_familiar1', TRUE);
			$dataRefPersonalesCandidato1['Nombres']               =strtoupper($this->input->post('nombre_familiar1', TRUE));
			$dataRefPersonalesCandidato1['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar1', TRUE));
			$dataRefPersonalesCandidato1['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar1', TRUE));
			$dataRefPersonalesCandidato1['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar1', TRUE));
		
			if($_POST['mismaVivienda1']){
			 $dataRefPersonalesCandidato1['MismaVivienda']       =$this->input->post('mismaVivienda1', TRUE);
			}
			
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato1);
			
		}
		
		
		if(isset($_POST['nombre_familiar2']) &&  $_POST['nombre_familiar2'] != ''){
			$dataRefPersonalesCandidato2['idEscolaridad']         =$this->input->post('escolaridad_familiar2', TRUE);
			$dataRefPersonalesCandidato2['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato2['idParentesco']          =$this->input->post('parentesco_familiar2', TRUE);
			$dataRefPersonalesCandidato2['Nombres']               =strtoupper($this->input->post('nombre_familiar2', TRUE));
			$dataRefPersonalesCandidato2['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar2', TRUE));
			$dataRefPersonalesCandidato2['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar2', TRUE));
			$dataRefPersonalesCandidato2['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar2', TRUE));
		
			if($_POST['mismaVivienda2']){
			 $dataRefPersonalesCandidato2['MismaVivienda']       =$this->input->post('mismaVivienda2', TRUE);
			}
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato2);
			
		}
		
		
		if(isset($_POST['nombre_familiar3']) &&  $_POST['nombre_familiar3'] != ''){
			$dataRefPersonalesCandidato3['idEscolaridad']         =$this->input->post('escolaridad_familiar3', TRUE);
			$dataRefPersonalesCandidato3['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato3['idParentesco']          =$this->input->post('parentesco_familiar3', TRUE);
			$dataRefPersonalesCandidato3['Nombres']               =strtoupper($this->input->post('nombre_familiar3', TRUE));
			$dataRefPersonalesCandidato3['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar3', TRUE));
			$dataRefPersonalesCandidato3['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar3', TRUE));
			$dataRefPersonalesCandidato3['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar3', TRUE));
		
			if($_POST['mismaVivienda3']){
			 $dataRefPersonalesCandidato3['MismaVivienda']       =$this->input->post('mismaVivienda3', TRUE);
			}
			
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato3);
		}
		
		
		if(isset($_POST['nombre_familiar4']) &&  $_POST['nombre_familiar4'] != ''){
			$dataRefPersonalesCandidato4['idEscolaridad']         =$this->input->post('escolaridad_familiar4', TRUE);
			$dataRefPersonalesCandidato4['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato4['idParentesco']          =$this->input->post('parentesco_familiar4', TRUE);
			$dataRefPersonalesCandidato4['Nombres']               =strtoupper($this->input->post('nombre_familiar4', TRUE));
			$dataRefPersonalesCandidato4['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar4', TRUE));
			$dataRefPersonalesCandidato4['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar4', TRUE));
			$dataRefPersonalesCandidato4['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar4', TRUE));
		
			if($_POST['mismaVivienda4']){
			 $dataRefPersonalesCandidato4['MismaVivienda']       =$this->input->post('mismaVivienda4', TRUE);
			}
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato4);
			
		}
		
		if(isset($_POST['nombre_familiar5']) &&  $_POST['nombre_familiar5'] != ''){
			$dataRefPersonalesCandidato5['idEscolaridad']         =$this->input->post('escolaridad_familiar5', TRUE);
			$dataRefPersonalesCandidato5['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato5['idParentesco']          =$this->input->post('parentesco_familiar5', TRUE);
			$dataRefPersonalesCandidato5['Nombres']               =strtoupper($this->input->post('nombre_familiar5', TRUE));
			$dataRefPersonalesCandidato5['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar5', TRUE));
			$dataRefPersonalesCandidato5['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar5', TRUE));
			$dataRefPersonalesCandidato5['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar5', TRUE));
		
			if($_POST['mismaVivienda5']){
			 $dataRefPersonalesCandidato5['MismaVivienda']       =$this->input->post('mismaVivienda5', TRUE);
			}
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato5);
			
		}

		if(isset($_POST['nombre_familiar6']) &&  $_POST['nombre_familiar6'] != ''){
			$dataRefPersonalesCandidato6['idEscolaridad']         =$this->input->post('escolaridad_familiar6', TRUE);
			$dataRefPersonalesCandidato6['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato6['idParentesco']          =$this->input->post('parentesco_familiar6', TRUE);
			$dataRefPersonalesCandidato6['Nombres']               =strtoupper($this->input->post('nombre_familiar6', TRUE));
			$dataRefPersonalesCandidato6['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar6', TRUE));
			$dataRefPersonalesCandidato6['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar6', TRUE));
			$dataRefPersonalesCandidato6['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar6', TRUE));
		
			if($_POST['mismaVivienda6']){
			 $dataRefPersonalesCandidato6['MismaVivienda']       =$this->input->post('mismaVivienda6', TRUE);
			}
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato6);
		}
		
		if(isset($_POST['nombre_familiar7']) &&  $_POST['nombre_familiar7'] != ''){
			$dataRefPersonalesCandidato7['idEscolaridad']         =$this->input->post('escolaridad_familiar7', TRUE);
			$dataRefPersonalesCandidato7['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato7['idParentesco']          =$this->input->post('parentesco_familiar7', TRUE);
			$dataRefPersonalesCandidato7['Nombres']               =strtoupper($this->input->post('nombre_familiar7', TRUE));
			$dataRefPersonalesCandidato7['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar7', TRUE));
			$dataRefPersonalesCandidato7['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar7', TRUE));
			$dataRefPersonalesCandidato7['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar7', TRUE));
		
			if($_POST['mismaVivienda7']){
			 $dataRefPersonalesCandidato7['MismaVivienda']       =$this->input->post('mismaVivienda7', TRUE);
			}
			
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato7);
			
		}

        if(isset($_POST['nombre_familiar8']) &&  $_POST['nombre_familiar8'] != ''){
			$dataRefPersonalesCandidato8['idEscolaridad']         =$this->input->post('escolaridad_familiar8', TRUE);
			$dataRefPersonalesCandidato8['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato8['idParentesco']          =$this->input->post('parentesco_familiar8', TRUE);
			$dataRefPersonalesCandidato8['Nombres']               =strtoupper($this->input->post('nombre_familiar8', TRUE));
			$dataRefPersonalesCandidato8['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar8', TRUE));
			$dataRefPersonalesCandidato8['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar8', TRUE));
			$dataRefPersonalesCandidato8['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar8', TRUE));
		
			if($_POST['mismaVivienda8']){
			 $dataRefPersonalesCandidato8['MismaVivienda']       =$this->input->post('mismaVivienda8', TRUE);
			}
			
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato8);
		}
		
		
		if(isset($_POST['nombre_familiar9']) &&  $_POST['nombre_familiar9'] != ''){
			$dataRefPersonalesCandidato9['idEscolaridad']         =$this->input->post('escolaridad_familiar9', TRUE);
			$dataRefPersonalesCandidato9['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato9['idParentesco']          =$this->input->post('parentesco_familiar9', TRUE);
			$dataRefPersonalesCandidato9['Nombres']               =strtoupper($this->input->post('nombre_familiar9', TRUE));
			$dataRefPersonalesCandidato9['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar9', TRUE));
			$dataRefPersonalesCandidato9['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar9', TRUE));
			$dataRefPersonalesCandidato9['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar9', TRUE));
		
			if($_POST['mismaVivienda9']){
			 $dataRefPersonalesCandidato9['MismaVivienda']       =$this->input->post('mismaVivienda9', TRUE);
			}
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato9);
		}
		
		
		if(isset($_POST['nombre_familiar10']) &&  $_POST['nombre_familiar10'] != ''){
			$dataRefPersonalesCandidato10['idEscolaridad']         =$this->input->post('escolaridad_familiar10', TRUE);
			$dataRefPersonalesCandidato10['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefPersonalesCandidato10['idParentesco']          =$this->input->post('parentesco_familiar10', TRUE);
			$dataRefPersonalesCandidato10['Nombres']               =strtoupper($this->input->post('nombre_familiar10', TRUE));
			$dataRefPersonalesCandidato10['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_familiar10', TRUE));
			$dataRefPersonalesCandidato10['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_familiar10', TRUE));
			$dataRefPersonalesCandidato10['EmpresaOcupacion']      =strtoupper($this->input->post('empresa_familiar10', TRUE));
		
			if($_POST['mismaVivienda10']){
			 $dataRefPersonalesCandidato10['MismaVivienda']       =$this->input->post('mismaVivienda10', TRUE);
			}
			
			$referenciaCandidato = $this->refcandidato_model->addReferenciasCandidato($dataRefPersonalesCandidato10);
			
		}



        if(isset($_POST['nombre_referenciEconFamiliar1']) && $_POST['nombre_referenciEconFamiliar1'] != ''){
			$dataRefEconomicas['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas['idConceptosEco']        =$this->input->post('conceptosEco1', TRUE);
			$dataRefEconomicas['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar1', TRUE));
			$dataRefEconomicas['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar1', TRUE));
			$dataRefEconomicas['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar1', TRUE));
			$dataRefEconomicas['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar1', TRUE));
			$dataRefEconomicas['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar1', TRUE));
			$dataRefEconomicas['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar1', TRUE));
		
			$referenciaEcoCandidato = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas);
			
		}


        if(isset($_POST['nombre_referenciEconFamiliar2']) && $_POST['nombre_referenciEconFamiliar2'] != ''){
			$dataRefEconomicas2['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas2['idConceptosEco']        =$this->input->post('conceptosEco2', TRUE);
			$dataRefEconomicas2['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas2['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar2', TRUE));
			$dataRefEconomicas2['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar2', TRUE));
			$dataRefEconomicas2['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar2', TRUE));
			$dataRefEconomicas2['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar2', TRUE));
			$dataRefEconomicas2['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar2', TRUE));
			$dataRefEconomicas2['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar2', TRUE));
		
			$referenciaEcoCandidato2 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas2);
			
		}


        if(isset($_POST['nombre_referenciEconFamiliar3']) && $_POST['nombre_referenciEconFamiliar3'] != ''){
			$dataRefEconomicas3['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas3['idConceptosEco']        =$this->input->post('conceptosEco3', TRUE);
			$dataRefEconomicas3['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas3['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar3', TRUE));
			$dataRefEconomicas3['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar3', TRUE));
			$dataRefEconomicas3['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar3', TRUE));
			$dataRefEconomicas3['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar3', TRUE));
			$dataRefEconomicas3['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar3', TRUE));
			$dataRefEconomicas3['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar3', TRUE));
		
			$referenciaEcoCandidato3 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas3);
			
		}


       if(isset($_POST['nombre_referenciEconFamiliar4']) && $_POST['nombre_referenciEconFamiliar4'] != ''){
			$dataRefEconomicas4['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas4['idConceptosEco']        =$this->input->post('conceptosEco4', TRUE);
			$dataRefEconomicas4['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas4['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar4', TRUE));
			$dataRefEconomicas4['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar4', TRUE));
			$dataRefEconomicas4['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar4', TRUE));
			$dataRefEconomicas4['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar4', TRUE));
			$dataRefEconomicas4['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar4', TRUE));
			$dataRefEconomicas4['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar4', TRUE));
		
			$referenciaEcoCandidato4 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas4);
			
		}


        if(isset($_POST['nombre_referenciEconFamiliar5']) && $_POST['nombre_referenciEconFamiliar5'] != ''){
			$dataRefEconomicas5['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas5['idConceptosEco']        =$this->input->post('conceptosEco5', TRUE);
			$dataRefEconomicas5['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas5['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar5', TRUE));
			$dataRefEconomicas5['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar5', TRUE));
			$dataRefEconomicas5['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar5', TRUE));
			$dataRefEconomicas5['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar5', TRUE));
			$dataRefEconomicas5['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar5', TRUE));
			$dataRefEconomicas5['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar5', TRUE));
		
			$referenciaEcoCandidato5 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas5);
			
		}

        
		if(isset($_POST['nombre_referenciEconFamiliar6']) && $_POST['nombre_referenciEconFamiliar6'] != ''){
			$dataRefEconomicas6['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas6['idConceptosEco']        =$this->input->post('conceptosEco6', TRUE);
			$dataRefEconomicas6['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas6['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar6', TRUE));
			$dataRefEconomicas6['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar6', TRUE));
			$dataRefEconomicas6['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar6', TRUE));
			$dataRefEconomicas6['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar6', TRUE));
			$dataRefEconomicas6['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar6', TRUE));
			$dataRefEconomicas6['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar6', TRUE));
		
			$referenciaEcoCandidato6 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas6);
			
		}  


        if(isset($_POST['nombre_referenciEconFamiliar7']) && $_POST['nombre_referenciEconFamiliar7'] != ''){
			$dataRefEconomicas7['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas7['idConceptosEco']        =$this->input->post('conceptosEco7', TRUE);
			$dataRefEconomicas7['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas7['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar7', TRUE));
			$dataRefEconomicas7['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar7', TRUE));
			$dataRefEconomicas7['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar7', TRUE));
			$dataRefEconomicas7['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar7', TRUE));
			$dataRefEconomicas7['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar7', TRUE));
			$dataRefEconomicas7['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar7', TRUE));
		
			$referenciaEcoCandidato7 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas7);
			
		}   

        if(isset($_POST['nombre_referenciEconFamiliar8']) && $_POST['nombre_referenciEconFamiliar8'] != ''){
			$dataRefEconomicas8['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas8['idConceptosEco']        =$this->input->post('conceptosEco8', TRUE);
			$dataRefEconomicas8['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas8['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar8', TRUE));
			$dataRefEconomicas8['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar8', TRUE));
			$dataRefEconomicas8['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar8', TRUE));
			$dataRefEconomicas8['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar8', TRUE));
			$dataRefEconomicas8['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar8', TRUE));
			$dataRefEconomicas8['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar8', TRUE));
		
			$referenciaEcoCandidato8 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas8);
			
		}

         if(isset($_POST['nombre_referenciEconFamiliar9']) && $_POST['nombre_referenciEconFamiliar9'] != ''){
			$dataRefEconomicas9['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas9['idConceptosEco']        =$this->input->post('conceptosEco9', TRUE);
			$dataRefEconomicas9['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas9['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar9', TRUE));
			$dataRefEconomicas9['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar9', TRUE));
			$dataRefEconomicas9['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar9', TRUE));
			$dataRefEconomicas9['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar9', TRUE));
			$dataRefEconomicas9['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar9', TRUE));
			$dataRefEconomicas9['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar9', TRUE));
		
			$referenciaEcoCandidato9 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas9);
			
		}


        if(isset($_POST['nombre_referenciEconFamiliar10']) && $_POST['nombre_referenciEconFamiliar10'] != ''){
			$dataRefEconomicas10['idCuestionario']        =$this->input->post('idTipoEse', TRUE);
			$dataRefEconomicas10['idConceptosEco']        =$this->input->post('conceptosEco10', TRUE);
			$dataRefEconomicas10['idCandidato']           =$this->input->post('idCandidato', TRUE);
			$dataRefEconomicas10['Nombres']               =strtoupper($this->input->post('nombre_referenciEconFamiliar10', TRUE));
			$dataRefEconomicas10['ApellidoPaterno']       =strtoupper($this->input->post('apellidop_referenciEconFamiliar10', TRUE));
			$dataRefEconomicas10['ApellidoMaterno']       =strtoupper($this->input->post('apellidom_referenciEconFamiliar10', TRUE));
			$dataRefEconomicas10['Ingreso']               =strtoupper($this->input->post('cuantogana_referenciEconFamiliar10', TRUE));
			$dataRefEconomicas10['Aportacion']            =strtoupper($this->input->post('cuantoaporta_referenciEconFamiliar10', TRUE));
			$dataRefEconomicas10['Egresos']               =strtoupper($this->input->post('egresos_referenciEconFamiliar10', TRUE));
		
			$referenciaEcoCandidato10 = $this->refeconomicas_candidato_model->addReferenciasEconomicasCandidato($dataRefEconomicas10);
			
		}

         
		
		if(isset($_POST['ref1_nombreEmpresa']) && $_POST['ref1_nombreEmpresa'] != ''){
			$dataRefLaborales['idCuestionario']     =$this->input->post('idTipoEse', TRUE);
			$dataRefLaborales['idCandidato']        =$this->input->post('idCandidato', TRUE);
			$dataRefLaborales['NombreEmpresa']      =strtoupper($this->input->post('ref1_nombreEmpresa', TRUE));
			$dataRefLaborales['Giro']               =strtoupper($this->input->post('ref1_giro', TRUE));
			$dataRefLaborales['numEmpleado']        =strtoupper($this->input->post('ref1_num_empleado', TRUE));
			$dataRefLaborales['Domicilio']          =strtoupper($this->input->post('ref1_domicilio', TRUE));
			$dataRefLaborales['Telefono']           =strtoupper($this->input->post('ref1_telefono', TRUE));
			$dataRefLaborales['FechaIngreso']       =strtoupper($this->input->post('ref1_fecha_ingreso', TRUE));
			$dataRefLaborales['FechaSalida']        =strtoupper($this->input->post('ref1_fecha_salida', TRUE));
			$dataRefLaborales['PrimerPuesto']       =strtoupper($this->input->post('ref1_primer_puesto', TRUE));
			$dataRefLaborales['UltimoPuesto']       =strtoupper($this->input->post('ref1_ultimo_puesto', TRUE));
			$dataRefLaborales['SueldoIni']          =strtoupper($this->input->post('ref1_sueldo_inicial', TRUE));
			$dataRefLaborales['SueldoFin']          =strtoupper($this->input->post('ref1_sueldo_final', TRUE));
			$dataRefLaborales['JefeInmediato']      =strtoupper($this->input->post('ref1_jefeInmediato', TRUE));
			$dataRefLaborales['PuestoJefe']         =strtoupper($this->input->post('ref1_puesto', TRUE));
			$dataRefLaborales['MotivoSalida']       =$this->input->post('ref1_motivoSalida', TRUE);
			$dataRefLaborales['Eficiencia']         =$this->input->post('ref1_eficencia', TRUE);
			$dataRefLaborales['Puntualidad']        =$this->input->post('ref1_puntualidad', TRUE);
			$dataRefLaborales['Responsabilidad']    =$this->input->post('ref1_responsibilidad', TRUE);
			$dataRefLaborales['Dinamismo']          =$this->input->post('ref1_dinamismo', TRUE);
			$dataRefLaborales['Cooperacion']        =$this->input->post('ref1_cooperacion', TRUE);
			$dataRefLaborales['Honradez']           =$this->input->post('ref1_honradez', TRUE);
			$dataRefLaborales['Disciplina']         =$this->input->post('ref1_disciplina', TRUE);
			$dataRefLaborales['ActitudJefe']        =$this->input->post('ref1_actitud_jefe', TRUE);
			$dataRefLaborales['ActitudComp']        =$this->input->post('ref1_actitud_compa', TRUE);
			$dataRefLaborales['ActitudSub']        =$this->input->post('ref1_subordinados', TRUE);
			$dataRefLaborales['InteresTrabajo']        =$this->input->post('ref1_interes_trabajo', TRUE);
			$dataRefLaborales['Iniciativa']        =$this->input->post('ref1_iniciativa', TRUE);
			$dataRefLaborales['ActitudesSindicales']        =$this->input->post('ref1_actitudes_sindicales', TRUE);
			$dataRefLaborales['AccidentesEnf']        =$this->input->post('ref1_accidentes_enfermedades', TRUE);
			$dataRefLaborales['FuenteInfo']        =$this->input->post('ref1_fecha_informacion', TRUE);
			$dataRefLaborales['Puesto']        =$this->input->post('ref1_puesto', TRUE);
			$dataRefLaborales['FechaEnt']        =$this->input->post('ref1_fecha_ref', TRUE);
			$dataRefLaborales['TelefonoEnt']        =$this->input->post('ref1_telefono_ref', TRUE);
			$dataRefLaborales['Observaciones']        =strtoupper($this->input->post('ref1_observaciones_ref', TRUE));
			
			
			$referenciaLabCandidato1 = $this->reflabcandidato_model->addReferenciasLabCandidato($dataRefLaborales);
		 
		} 


         if(isset($_POST['ref2_nombreEmpresa']) && $_POST['ref2_nombreEmpresa'] != ''){
			$dataRefLaborales2['idCuestionario']     =$this->input->post('idTipoEse', TRUE);
			$dataRefLaborales2['idCandidato']        =$this->input->post('idCandidato', TRUE);
			$dataRefLaborales2['NombreEmpresa']      =strtoupper($this->input->post('ref2_nombreEmpresa', TRUE));
			$dataRefLaborales2['Giro']               =strtoupper($this->input->post('ref2_giro', TRUE));
			$dataRefLaborales2['numEmpleado']        =strtoupper($this->input->post('ref2_num_empleado', TRUE));
			$dataRefLaborales2['Domicilio']          =strtoupper($this->input->post('ref2_domicilio', TRUE));
			$dataRefLaborales2['Telefono']           =strtoupper($this->input->post('ref2_telefono', TRUE));
			$dataRefLaborales2['FechaIngreso']       =strtoupper($this->input->post('ref2_fecha_ingreso', TRUE));
			$dataRefLaborales2['FechaSalida']        =strtoupper($this->input->post('ref2_fecha_salida', TRUE));
			$dataRefLaborales2['PrimerPuesto']       =strtoupper($this->input->post('ref2_primer_puesto', TRUE));
			$dataRefLaborales2['UltimoPuesto']       =strtoupper($this->input->post('ref2_ultimo_puesto', TRUE));
			$dataRefLaborales2['SueldoIni']          =strtoupper($this->input->post('ref2_sueldo_inicial', TRUE));
			$dataRefLaborales2['SueldoFin']          =strtoupper($this->input->post('ref2_sueldo_final', TRUE));
			$dataRefLaborales2['JefeInmediato']      =strtoupper($this->input->post('ref2_jefeInmediato', TRUE));
			$dataRefLaborales2['PuestoJefe']         =strtoupper($this->input->post('ref2_puesto', TRUE));
			$dataRefLaborales2['MotivoSalida']       =strtoupper($this->input->post('ref2_motivoSalida', TRUE));
			$dataRefLaborales2['Eficiencia']         =$this->input->post('ref2_eficencia', TRUE);
			$dataRefLaborales2['Puntualidad']        =$this->input->post('ref2_puntualidad', TRUE);
			$dataRefLaborales2['Responsabilidad']    =$this->input->post('ref2_responsibilidad', TRUE);
			$dataRefLaborales2['Dinamismo']          =$this->input->post('ref2_dinamismo', TRUE);
			$dataRefLaborales2['Cooperacion']        =$this->input->post('ref2_cooperacion', TRUE);
			$dataRefLaborales2['Honradez']           =$this->input->post('ref2_honradez', TRUE);
			$dataRefLaborales2['Disciplina']         =$this->input->post('ref2_disciplina', TRUE);
			$dataRefLaborales2['ActitudJefe']        =$this->input->post('ref2_actitud_jefe', TRUE);
			$dataRefLaborales2['ActitudComp']        =$this->input->post('ref2_actitud_compa', TRUE);
			$dataRefLaborales2['ActitudSub']        =$this->input->post('ref2_subordinados', TRUE);
			$dataRefLaborales2['InteresTrabajo']        =$this->input->post('ref2_interes_trabajo', TRUE);
			$dataRefLaborales2['Iniciativa']        =$this->input->post('ref2_iniciativa', TRUE);
			$dataRefLaborales2['ActitudesSindicales']        =$this->input->post('ref2_actitudes_sindicales', TRUE);
			$dataRefLaborales2['AccidentesEnf']        =$this->input->post('ref2_accidentes_enfermedades', TRUE);
			$dataRefLaborales2['FuenteInfo']        =$this->input->post('ref2_fecha_informacion', TRUE);
			$dataRefLaborales2['Puesto']        =$this->input->post('ref2_puesto', TRUE);
			$dataRefLaborales2['FechaEnt']        =$this->input->post('ref2_fecha_ref', TRUE);
			$dataRefLaborales2['TelefonoEnt']        =$this->input->post('ref2_telefono_ref', TRUE);
			$dataRefLaborales2['Observaciones']        =strtoupper($this->input->post('ref2_observaciones_ref', TRUE));
			
			$referenciaLabCandidato2 = $this->reflabcandidato_model->addReferenciasLabCandidato($dataRefLaborales2);
		
		} 

        
         if(isset($_POST['ref3_nombreEmpresa'])  && $_POST['ref3_nombreEmpresa'] != ''){
			$dataRefLaborales3['idCuestionario']     =$this->input->post('idTipoEse', TRUE);
			$dataRefLaborales3['idCandidato']        =$this->input->post('idCandidato', TRUE);
			$dataRefLaborales3['NombreEmpresa']      =strtoupper($this->input->post('ref3_nombreEmpresa', TRUE));
			$dataRefLaborales3['Giro']               =strtoupper($this->input->post('ref3_giro', TRUE));
			$dataRefLaborales3['numEmpleado']        =strtoupper($this->input->post('ref3_num_empleado', TRUE));
			$dataRefLaborales3['Domicilio']          =strtoupper($this->input->post('ref3_domicilio', TRUE));
			$dataRefLaborales3['Telefono']           =strtoupper($this->input->post('ref3_telefono', TRUE));
			$dataRefLaborales3['FechaIngreso']       =strtoupper($this->input->post('ref3_fecha_ingreso', TRUE));
			$dataRefLaborales3['FechaSalida']        =strtoupper($this->input->post('ref3_fecha_salida', TRUE));
			$dataRefLaborales3['PrimerPuesto']       =strtoupper($this->input->post('ref3_primer_puesto', TRUE));
			$dataRefLaborales3['UltimoPuesto']       =strtoupper($this->input->post('ref3_ultimo_puesto', TRUE));
			$dataRefLaborales3['SueldoIni']          =strtoupper($this->input->post('ref3_sueldo_inicial', TRUE));
			$dataRefLaborales3['SueldoFin']          =strtoupper($this->input->post('ref3_sueldo_final', TRUE));
			$dataRefLaborales3['JefeInmediato']      =strtoupper($this->input->post('ref3_jefeInmediato', TRUE));
			$dataRefLaborales3['PuestoJefe']         =strtoupper($this->input->post('ref3_puesto', TRUE));
			$dataRefLaborales3['MotivoSalida']       =$this->input->post('ref3_motivoSalida', TRUE);
			$dataRefLaborales3['Eficiencia']         =$this->input->post('ref3_eficencia', TRUE);
			$dataRefLaborales3['Puntualidad']        =$this->input->post('ref3_puntualidad', TRUE);
			$dataRefLaborales3['Responsabilidad']    =$this->input->post('ref3_responsibilidad', TRUE);
			$dataRefLaborales3['Dinamismo']          =$this->input->post('ref3_dinamismo', TRUE);
			$dataRefLaborales3['Cooperacion']        =$this->input->post('ref3_cooperacion', TRUE);
			$dataRefLaborales3['Honradez']           =$this->input->post('ref3_honradez', TRUE);
			$dataRefLaborales3['Disciplina']         =$this->input->post('ref3_disciplina', TRUE);
			$dataRefLaborales3['ActitudJefe']        =$this->input->post('ref3_actitud_jefe', TRUE);
			$dataRefLaborales3['ActitudComp']        =$this->input->post('ref3_actitud_compa', TRUE);
			$dataRefLaborales3['ActitudSub']        =$this->input->post('ref3_subordinados', TRUE);
			$dataRefLaborales3['InteresTrabajo']        =$this->input->post('ref3_interes_trabajo', TRUE);
			$dataRefLaborales3['Iniciativa']        =$this->input->post('ref3_iniciativa', TRUE);
			$dataRefLaborales3['ActitudesSindicales']        =$this->input->post('ref3_actitudes_sindicales', TRUE);
			$dataRefLaborales3['AccidentesEnf']        =$this->input->post('ref3_accidentes_enfermedades', TRUE);
			$dataRefLaborales3['FuenteInfo']        =$this->input->post('ref3_fecha_informacion', TRUE);
			$dataRefLaborales3['Puesto']        =$this->input->post('ref3_puesto', TRUE);
			$dataRefLaborales3['FechaEnt']        =$this->input->post('ref3_fecha_ref', TRUE);
			$dataRefLaborales3['TelefonoEnt']        =$this->input->post('ref3_telefono_ref', TRUE);
			$dataRefLaborales3['Observaciones']        =strtoupper($this->input->post('ref3_observaciones_ref', TRUE));
			
			$referenciaLabCandidato3 = $this->reflabcandidato_model->addReferenciasLabCandidato($dataRefLaborales3);
		
		} 
         
		if(isset($_POST['imss1_empresa']) && $_POST['imss1_empresa'] != ''){    
			$dataRefImss['idCuestionario'] =$this->input->post('idTipoEse', TRUE);
			$dataRefImss['idCandidato'] =$this->input->post('idCandidato', TRUE);
		    
			if($this->input->post('imss1_empresa', TRUE)){
				
			    $imss1_empresa = $this->input->post('imss1_empresa', TRUE);
				
			}else{
				$imss1_empresa = '-';
			}
			
			if($this->input->post('imss2_empresa', TRUE)){
				
			    $imss2_empresa = $this->input->post('imss2_empresa', TRUE);
				
			}else{
				$imss2_empresa = '-';
			}

            if($this->input->post('imss3_empresa', TRUE)){
				
			    $imss3_empresa = $this->input->post('imss3_empresa', TRUE);
				
			}else{
				$imss3_empresa = '-';
			}

			if($this->input->post('imss4_empresa', TRUE)){
				
			    $imss4_empresa = $this->input->post('imss4_empresa', TRUE);
				
			}else{
				$imss4_empresa = '-';
			}
			
			if($this->input->post('imss5_empresa', TRUE)){
				
			    $imss5_empresa = $this->input->post('imss5_empresa', TRUE);
				
			}else{
				$imss5_empresa = '-';
			}
			
			if($this->input->post('imss6_empresa', TRUE)){
				
			    $imss6_empresa = $this->input->post('imss6_empresa', TRUE);
				
			}else{
				$imss6_empresa = '-';
			}

			if($this->input->post('imss7_empresa', TRUE)){
				
			    $imss7_empresa = $this->input->post('imss7_empresa', TRUE);
				
			}else{
				$imss7_empresa = '-';
			}

			if($this->input->post('imss8_empresa', TRUE)){
				
			    $imss8_empresa = $this->input->post('imss8_empresa', TRUE);
				
			}else{
				$imss8_empresa = '-';
			}
			
			if($this->input->post('imss10_empresa', TRUE)){
				
			    $imss10_empresa = $this->input->post('imss10_empresa', TRUE);
				
			}else{
				$imss10_empresa = '-';
			}
			
			if($this->input->post('imss9_empresa', TRUE)){
				
			    $imss9_empresa = $this->input->post('imss9_empresa', TRUE);
				
			}else{
				$imss9_empresa = '-';
			}
			
			
			if($this->input->post('imss1_periodo', TRUE)){
				
			    $imss1_periodo = $this->input->post('imss1_periodo', TRUE);
				
			}else{
				$imss1_periodo = '-';
			}


           if($this->input->post('imss2_periodo', TRUE)){
				
			    $imss2_periodo = $this->input->post('imss2_periodo', TRUE);
				
			}else{
				$imss2_periodo = '-';
			}

           if($this->input->post('imss3_periodo', TRUE)){
				
			    $imss3_periodo = $this->input->post('imss3_periodo', TRUE);
				
			}else{
				$imss3_periodo = '-';
			}
			
			if($this->input->post('imss4_periodo', TRUE)){
				
			    $imss4_periodo = $this->input->post('imss4_periodo', TRUE);
				
			}else{
				$imss4_periodo = '-';
			}
			
			if($this->input->post('imss5_periodo', TRUE)){
				
			    $imss5_periodo = $this->input->post('imss5_periodo', TRUE);
				
			}else{
				$imss5_periodo = '-';
			}
			
			if($this->input->post('imss6_periodo', TRUE)){
				
			    $imss6_periodo = $this->input->post('imss6_periodo', TRUE);
				
			}else{
				$imss6_periodo = '-';
			}

            if($this->input->post('imss7_periodo', TRUE)){
				
			    $imss7_periodo = $this->input->post('imss7_periodo', TRUE);
				
			}else{
				$imss7_periodo = '-';
			}

           if($this->input->post('imss8_periodo', TRUE)){
				
			    $imss8_periodo = $this->input->post('imss8_periodo', TRUE);
				
			}else{
				$imss8_periodo = '-';
			}

           if($this->input->post('imss9_periodo', TRUE)){
				
			    $imss9_periodo = $this->input->post('imss9_periodo', TRUE);
				
			}else{
				$imss9_periodo = '-';
			}
			
			if($this->input->post('imss10_periodo', TRUE)){
				
			    $imss10_periodo = $this->input->post('imss10_periodo', TRUE);
				
			}else{
				$imss10_periodo = '-';
			}
			
			
		    
		    $imssEmpresa = $imss1_empresa.'|'.$imss2_empresa.'|'.$imss3_empresa.'|'.$imss4_empresa.'|'.$imss5_empresa.'|'.$imss6_empresa.'|'.$imss7_empresa.'|'.$imss8_empresa.'|'.$imss9_empresa.'|'.$imss10_empresa;
            $imssPeriodo = $imss1_periodo.'|'.$imss2_periodo.'|'.$imss3_periodo.'|'.$imss4_periodo.'|'.$imss5_periodo.'|'.$imss6_periodo.'|'.$imss7_periodo.'|'.$imss8_periodo.'|'.$imss9_periodo.'|'.$imss10_periodo;
			$dataRefImss['Empresa']        =$imssEmpresa;
			$dataRefImss['Periodo']        =$imssPeriodo;
			$dataRefImss['ReportadaCandidato']   =$this->input->post('imss_reportadas_ca', TRUE);
			$dataRefImss['ReportadaImss']        =$this->input->post('imss_reportadas_imms', TRUE);
			
            $referenciasImss = $this->refimss_model->addReferenciasImssCandidato($dataRefImss);
          }
		    
			
			
		  $dataRefVivienda['idCuestionario'] =$this->input->post('idTipoEse', TRUE);
		  $dataRefVivienda['idCandidato'] =$this->input->post('idCandidato', TRUE);	
		  $dataRefVivienda['FotoVivienda'] =$this->input->post('fotoViviendaInput1', TRUE);
		  $dataRefVivienda['FotoInterior'] =$this->input->post('fotoViviendaInput2', TRUE);
		  $dataRefVivienda['FotoCadidato'] =$this->input->post('fotoViviendaInput3', TRUE);
		  $dataRefVivienda['CoordX'] =$this->input->post('latitud', TRUE);
		  $dataRefVivienda['CoordY'] =$this->input->post('longitud', TRUE);	
		  $referenciasVivienda = $this->refvivienda_model->addReferenciasViviendaCandidato($dataRefVivienda);	
			
			
		redirect('solicitudes/solicitudesESE');
	}
	
	public function pdf_crearANP()
	{
		$wheres2['ESE.idESE']         = $_POST['idESE'];
		
		
		 $join                          = array(
			                               array(
											   'table' => 'Candidato',
											   'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											   'type'  => 'inner' 
											   ),
										   array(
											   'table' => 'ESE_has_CuestionarioESE',
											   'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											   'type'  => 'inner' 
											   )
		                                  );
 		
 		
		$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheres2,$join);
	    $data['datos'] =$ESE;
		
		
		
		$wheresEs['ESE.idESE']         =  $_POST['idESE'];
		
		$joinEs                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  )
		                                 );
		
		
		$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheresEs,$joinEs);

		$data['idESE']              = $id;
		$data['tipoEse1']            = 1;
		$data['idCandidato']            = $ESE['idCandidato'];

		
		
		$join                          = array(
			                             array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'inner' 
											  )
                                            );
		$wheres['Pregunta.idCuestionario']   = 1;									
					
											
	    $ANP = $this->cuestionarios_model->getAllANP($wheres,NULL,$join);
		
		
		
		$wheresRes['idCandidato']         = $ESE['idCandidato'];
		$wheresRes['idCuestionario']      = 1;

		$ResANP = $this->cuestionarios_model->getOneResCuestionarioANP($wheresRes);
		
		 
		$htmlANP = " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = 1;
		  for ($i=0; $i < count($ANP); $i++) {
		  	   $formulario = '';
			   $respuesta = 'Respuesta'.$contador;
			  if($ANP[$i]["tipo"] == 'Numerica'){
				
				$formulario = $ResANP[$respuesta];
			  }
			  
			  if($ANP[$i]["tipo"] == 'Texto'){
						
						$formulario = $ResANP[$respuesta];
			  
			  }
			  
			  if($ANP[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones['idPregunta'] = $ANP[$i]["idPregunta"];
			  	    $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);
			
				   foreach ($Opciones as $key => $value) {

						
			           if($ResANP[$respuesta] == $value['idOpcionesRespuesta']){
			           	   $formulario .=  $value['Respuesta'];
			           }
				        
			
                    
				                     
				   }	
					
			
			  }

			  
			  if($ANP[$i]["tipo"] == 'Opcion Multiple Multiple'){
			  	  
				  
				  $whereOpciones['idPregunta'] = $ANP[$i]["idPregunta"];
			  	  $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);  		
				  
				  foreach ($Opciones as $key => $value) {
				  	
					if($ResANP[$respuesta] == $value['Respuesta']){
			    
					      $formulario .= "<br>".$value['Respuesta'];	
					 	
			           }
					
					 
			       }	
			    	
			  }
			  

			  
			  $htmlANP .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($ANP[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.htmlentities($formulario).'</td>
			   </tr>
			  ';
			  
			  $htmlANP .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlANP .= " 
		 </table>
		 ";
 		
        $data['contadorP']              = $contador;
		$data['PreguntasANP']            = $htmlANP;
		
		
		
		require_once('./application/third_party/dompdf/dompdf_config.inc.php');

if($_SESSION['SEUS']['use_typ_id'] == 1 || $_SESSION['SEUS']['use_typ_id'] == 2  || $_SESSION['SEUS']['use_typ_id'] == 3  || $_SESSION['SEUS']['use_typ_id'] == 4 ){
       $codigo = $this->parser->parse('solicitudes/preguntas_ANP_reporte', $data);
	   $nombre = 'estudio_ANP_reporte.pdf';
}else{
	   $codigo = $this->parser->parse('solicitudes/preguntas_ANP', $data);
	   $nombre = 'estudio_ANP.pdf';
	
}

		$codigo = utf8_encode($codigo);
					$dompdf = new DOMPDF();
					$dompdf->load_html($codigo);
					ini_set('memory_limit', '-1');
					$dompdf->render();
					$dompdf->stream($nombre);	
		
	}
	
	public function cuestionarioANP($id = null){
        
		
		
		$wheresEs['ESE.idESE']         = $id;
		
		$joinEs                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  )
		                                 );
		
		
		$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheresEs,$joinEs);

		$data['idESE']              = $id;
		$data['tipoEse1']            = 1;
		$data['idCandidato']            = $ESE['idCandidato'];

		
		
		$join                          = array(
			                             array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'inner' 
											  )
                                            );
		$wheres['Pregunta.idCuestionario']   = 1;									
					
											
	    $ANP = $this->cuestionarios_model->getAllANP($wheres,NULL,$join);
		
		
		
		$wheresRes['idCandidato']         = $ESE['idCandidato'];
		$wheresRes['idCuestionario']      = 1;

		$ResANP = $this->cuestionarios_model->getOneResCuestionarioANP($wheresRes);
		
		 
		$htmlANP = " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = 1;
		  for ($i=0; $i < count($ANP); $i++) {
		  	   $formulario = '';
			   $respuesta = 'Respuesta'.$contador;
			  if($ANP[$i]["tipo"] == 'Numerica'){
			  	$data = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $ResANP[$respuesta],
				              'class'       => 'span7',
				            );
				
				$formulario = form_input($data);
			  }
			  
			  if($ANP[$i]["tipo"] == 'Texto'){
					  $data = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $ResANP[$respuesta],
						              'class'       => 'span7',
						            );
						
						$formulario = form_textarea($data);
			  
			  }
			  
			  if($ANP[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones['idPregunta'] = $ANP[$i]["idPregunta"];
			  	    $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);
	
					$formulario .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario .="<option value=''>Elige una Opcion</option>";
			
				   foreach ($Opciones as $key => $value) {

						
			           if($ResANP[$respuesta] == $value['idOpcionesRespuesta']){
			           	   $formulario .=  "<option value='".$value['idOpcionesRespuesta']."'   selected>".$value['Respuesta']."</option>";
			           }else{
			           	      $formulario .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
			           }
				        
			
                    
				                     
				   }	
					$formulario .= "</select>";
			
			  }

			  
			  if($ANP[$i]["tipo"] == 'Opcion Multiple Multiple'){
			  	  
				  
				  $whereOpciones['idPregunta'] = $ANP[$i]["idPregunta"];
			  	  $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);  		
				  
				  foreach ($Opciones as $key => $value) {
				  	
					if($ResANP[$respuesta] == $value['Respuesta']){
			    
					      $formulario .= "<br>
				     	  <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				      	  <span class='lbl'>".$value['Respuesta']."</span> ";	
					 	
			           }else{
			           	 $formulario .= "<br>
				     	  <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				      	  <span class='lbl'>".$value['Respuesta']."</span> ";	
			           }
					
					 
			       }	
			    	
			  }
			  

			  
			  $htmlANP .= '
			   <tr>
			     <td colspan="1" width="40%">'.$ANP[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario.'</td>
			   </tr>
			  ';
			  
			  $htmlANP .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlANP .= " 
		 </table>
		 ";
 		
         $data['contadorP']              = $contador;
		$data['PreguntasANP']            = $htmlANP;
		 
		 
		 $this->parser->parse('solicitudes/investigadorSolicitudANP', $data);
		 
		 
		 //die($this->db->last_query());										
		
	} 
	
	
	public function formEditSolicitud($id = null)
	{
		
		
		$data             = array();
		$wheres['ESE.idESE']         = $id;
		
		
		$join                          = array(
			                             array(
											  'table' => 'Candidato',
											  'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  )// ),
										  // array(
											  // 'table' => 'Cuestionario',
											  // 'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  // 'type'  => 'inner' 
											  // )
		                                 );
		
		
		$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheres,$join);
		// $wheresRefeEs['idCandidato']         = $ESE['idCandidato'];
		// $RefeEs      = $this->referencia_escolares_model->getAllReferenciaEscolar($wheresRefeEs);
		//die($this->db->last_query());	

		$data['Estados']                 = $this->estados_model->getAllEstados();
		$wheresEsta['idEstado']               = $ESE['idEstado'];
		$data['Municipio']               = $this->municipalities_model->getAllMunicipalities($wheresEsta);
		
		$data['tipoEse1'] = $ESE['idCuestionarioTipo'];
		
		$data['datos']   = $ESE;
	 
		
		$data['baseURL']            = base_url();
		$data['TipoCandidato']      = $this->tipo_candidato_model->getAllTipoCandidato();
        $data['Estadocivil']        = $this->estadocivil_model->getAllEstadoCivil();
		$data['Parentesco']         = $this->parentesco_model->getAllParentesco();
		$data['Escolaridad']        = $this->escolaridad_model->getAllEscolaridad();
		
		
		$whereTipo                  = array (
			                          'idCuestionarioTipo >' => '1'
			                          );
		$data['TipoEse']            = $this->tipo_ese_model->getAllTipoESE($whereTipo);
		
		

		$data['Puestos']            = $this->puesto_model->getAllPuestos();
		$data['Estatus']            = $this->estatus_ese_model->getAllEstatusESE();
		$data['MedioVacantes']      = $this->medio_vacantes_model->getAllMedioVacantes();
		$data['Empresas']           = $this->empresa_model->getAllEmpresa();
		$data['conceptosEco']           = $this->conceptos_eco_model->getAllConceptosEco();
		
		$data['DocumentosEscolar']  = $this->documentos_model->getAllDocumentoEscolar();
		
		
		$data['evaluacionLaboral']  = $this->evaluacion_laboral_model->getAllEvaluacionLaboral();
		

		
		if($ESE['Municipio'] != '' && $ESE['Ciudad'] != '' && $ESE['Estado'] != ''){
			$data['mapa']  = $ESE['Municipio'].', '.$ESE['Ciudad'].', '.$ESE['Estado'];
		}else{
		    $data['mapa']	  = 'jalisco';	
		}
		
        
        
        $data['jsE']             = '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>';
		
		
		//// ACADAMECIAS 
		$wheresESE['ESE.idESE']                        = $id;
		$wheresESE['Pregunta.idGrupoPregunta']         = 2;
		$joinESE                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );
		
		
		$ReferenciasAcademicas = $this->cuestionarios_model->getAllESE($wheresESE,NULL,$joinESE);
		
		//print_r($ReferenciasAcademicas);
		//die($this->db->last_query());	
		
		
		////////7
		$wheresRes['idCandidato']         = $ESE['idCandidato'];
		$wheresRes['idCuestionario']      = $ESE['idCuestionarioTipo'];

		$Resespuestas = $this->cuestionarios_model->getOneResCuestionarioESE($wheresRes);
		///
	    ///////REFERENCIAS ACADEMICAS
	    $wheresRefAcademicas['idCandidato']         = $ESE['idCandidato'];
	    $RespuestRefAcademicas    = $this->referencia_escolares_model->getAllReferenciaEscolar($wheresRefAcademicas);
	     
		 
		 for ($i=0; $i <= count($RespuestRefAcademicas); $i++) {
		 	 	
				
		 	 $numNivel = $RespuestRefAcademicas[$i]['idEscolaridad'];
			 $data['val_ref_acade_PeriodoInicio'.$numNivel] = $RespuestRefAcademicas[$i]['PeriodoInicio'];
			 $data['val_ref_acade_PeriodoFin'.$numNivel] = $RespuestRefAcademicas[$i]['PeriodoFin'];
			 $data['val_ref_acade_NombreInst'.$numNivel] = $RespuestRefAcademicas[$i]['NombreInst'];
			 $data['val_ref_acade_idDocumentoEscolar'.$numNivel] = $RespuestRefAcademicas[$i]['idDocumentoEscolar'];
			 $data['val_ref_acade_Folio'.$numNivel] = $RespuestRefAcademicas[$i]['Folio'];
			
		 }	
		///// REFERENCIAS PERSONALES
		 $wheresRefPersonalesCandidato['idCandidato'] = $ESE['idCandidato'];
		 $RespuestRefPersonalesCandidato = $this->refcandidato_model->getAllReferenciasCandidato($wheresRefPersonalesCandidato);
		 $contadors = 1;
		 for ($i=0; $i <= count($RespuestRefPersonalesCandidato); $i++) {
		 	
		 	$data['nombre_familiar'.$contadors]      = $RespuestRefPersonalesCandidato[$i]['Nombres'];
		 	$data['apellidop_familiar'.$contadors]   = $RespuestRefPersonalesCandidato[$i]['ApellidoPaterno'];
		 	$data['apellidom_familiar'.$contadors]   = $RespuestRefPersonalesCandidato[$i]['ApellidoMaterno'];
			$data['parentesco_familiar'.$contadors] = $RespuestRefPersonalesCandidato[$i]['idParentesco'];
			$data['escolaridad_familiar'.$contadors] = $RespuestRefPersonalesCandidato[$i]['idEscolaridad'];
			$data['empresa_familiar'.$contadors]     = $RespuestRefPersonalesCandidato[$i]['EmpresaOcupacion'];
			$data['mismaVivienda'.$contadors]        = $RespuestRefPersonalesCandidato[$i]['MismaVivienda'];
			$contadors ++;
		 }	
		 
		 //// REFERENCIAS ECONOMICAS
		 $wheresRefEconomicas['idCandidato'] = $ESE['idCandidato'];
		 $RespuestRefEconomicas = $this->refeconomicas_candidato_model->getAllReferenciasEconomicasCandidato($wheresRefEconomicas);
		 $contadors1 = 1;
		 for ($i=0; $i <= count($RespuestRefEconomicas); $i++) {
		   
		   $data['nombre_referenciEconFamiliar'.$contadors1]        = $RespuestRefEconomicas[$i]['Nombres'];
		   $data['apellidop_referenciEconFamiliar'.$contadors1]     = $RespuestRefEconomicas[$i]['ApellidoPaterno'];
		   $data['apellidom_referenciEconFamiliar'.$contadors1]     = $RespuestRefEconomicas[$i]['ApellidoMaterno'];
		   $data['cuantogana_referenciEconFamiliar'.$contadors1]    = $RespuestRefEconomicas[$i]['Ingreso'];
		   $data['cuantoaporta_referenciEconFamiliar'.$contadors1]  = $RespuestRefEconomicas[$i]['Aportacion'];
		   $data['conceptosEco'.$contadors1]                        = $RespuestRefEconomicas[$i]['idConceptosEco'];
		   $data['egresos_referenciEconFamiliar'.$contadors1]       = $RespuestRefEconomicas[$i]['Egresos'];
		   $contadors1 ++;
		 }
		 
		 //// Referencias Laborales
		 
		 $wheresRefLaborales['idCandidato'] = $ESE['idCandidato'];
		 $RespuestRefLaborales = $this->reflabcandidato_model->getAllReferenciasLabCandidato($wheresRefLaborales);
		 $contadors2 = 1;
		 for ($i=0; $i <  count($RespuestRefLaborales); $i++) {
		 	
			 $data['ref'.$contadors2.'_nombreEmpresa']     = $RespuestRefLaborales[$i]['NombreEmpresa'];
			 $data['ref'.$contadors2.'_giro']              = $RespuestRefLaborales[$i]['Giro'];
			 $data['ref'.$contadors2.'_domicilio']         = $RespuestRefLaborales[$i]['Domicilio'];
			 $data['ref'.$contadors2.'_num_empleado']      = $RespuestRefLaborales[$i]['numEmpleado'];
			 $data['ref'.$contadors2.'_telefono']          = $RespuestRefLaborales[$i]['Telefono'];
			 $data['ref'.$contadors2.'_fecha_ingreso']     = $RespuestRefLaborales[$i]['FechaIngreso'];
			 $data['ref'.$contadors2.'_fecha_salida']      = $RespuestRefLaborales[$i]['FechaSalida'];
			 $data['ref'.$contadors2.'_primer_puesto']     = $RespuestRefLaborales[$i]['PrimerPuesto'];
			 $data['ref'.$contadors2.'_sueldo_inicial']    = $RespuestRefLaborales[$i]['SueldoIni'];
			 $data['ref'.$contadors2.'_ultimo_puesto']     = $RespuestRefLaborales[$i]['UltimoPuesto'];
			 $data['ref'.$contadors2.'_sueldo_final']      = $RespuestRefLaborales[$i]['SueldoFin'];
			 $data['ref'.$contadors2.'_jefeInmediato']     = $RespuestRefLaborales[$i]['JefeInmediato'];
			 $data['ref'.$contadors2.'_puesto']            = $RespuestRefLaborales[$i]['PuestoJefe'];
			 $data['ref'.$contadors2.'_motivoSalida']      = $RespuestRefLaborales[$i]['MotivoSalida'];
			 $data['ref'.$contadors2.'_eficencia']         = $RespuestRefLaborales[$i]['Eficiencia'];
			 $data['ref'.$contadors2.'_puntualidad']       = $RespuestRefLaborales[$i]['Puntualidad'];
			 $data['ref'.$contadors2.'_responsibilidad']   = $RespuestRefLaborales[$i]['Responsabilidad'];
			 $data['ref'.$contadors2.'_dinamismo']         = $RespuestRefLaborales[$i]['Dinamismo'];
			 $data['ref'.$contadors2.'_cooperacion']       = $RespuestRefLaborales[$i]['Cooperacion'];
			 $data['ref'.$contadors2.'_honradez']          = $RespuestRefLaborales[$i]['Honradez'];
			 $data['ref'.$contadors2.'_disciplina']        = $RespuestRefLaborales[$i]['Disciplina'];
			 $data['ref'.$contadors2.'_actitud_jefe']      = $RespuestRefLaborales[$i]['ActitudJefe'];
			 $data['ref'.$contadors2.'_actitud_compa']     = $RespuestRefLaborales[$i]['ActitudComp'];
			 $data['ref'.$contadors2.'_subordinados']      = $RespuestRefLaborales[$i]['ActitudSub'];
			 $data['ref'.$contadors2.'_interes_trabajo']   = $RespuestRefLaborales[$i]['InteresTrabajo'];
			 $data['ref'.$contadors2.'_iniciativa']        = $RespuestRefLaborales[$i]['Iniciativa'];
			 $data['ref'.$contadors2.'_actitudes_sindicales']      = $RespuestRefLaborales[$i]['ActitudesSindicales'];
			 $data['ref'.$contadors2.'_accidentes_enfermedades']   = $RespuestRefLaborales[$i]['AccidentesEnf'];
			 $data['ref'.$contadors2.'_puesto']                    = $RespuestRefLaborales[$i]['Puesto'];
			 $data['ref'.$contadors2.'_fecha_informacion']         = $RespuestRefLaborales[$i]['FuenteInfo'];
			 $data['ref'.$contadors2.'_fecha_ref']                 = $RespuestRefLaborales[$i]['FechaEnt'];
			 $data['ref'.$contadors2.'_telefono_ref']              = $RespuestRefLaborales[$i]['TelefonoEnt'];
			 $data['ref'.$contadors2.'_observaciones_ref']         = $RespuestRefLaborales[$i]['Observaciones'];
	 	
			
			$contadors2 ++;
		 }	
		 
		 /// IMSS
		 $wheresImssRef['idCandidato'] = $ESE['idCandidato'];
		 $RespuestImss = $this->refimss_model->getOneReferenciasImssCandidato($wheresImssRef);
		 
		 $EmpresImss = explode('|',$RespuestImss['Empresa']);
		 $contadors3 = 1;
		 for ($i=0; $i < count($EmpresImss); $i++) { 
			   $data['imss'.$contadors3.'_empresa'] = $EmpresImss[$i];
			   
			   $contadors3++;
		 }
		 
		 
		 $PeriodoImss = explode('|',$RespuestImss['Periodo']);
		 $contadors4 = 1;
		 for ($i=0; $i < count($PeriodoImss); $i++) { 
			   $data['imss'.$contadors4.'_periodo'] = $PeriodoImss[$i];
			   
			   $contadors4++;
		 }
		 $data['imss_reportadas_ca'] = $RespuestImss['ReportadaCandidato'];
		 $data['imss_reportadas_imms'] = $RespuestImss['ReportadaImss'];
		 
		 /// VIVIENDA
		 $wheresViviendaRef['idCandidato'] = $ESE['idCandidato'];
		 $RespuestViviend = $this->refvivienda_model->getOneReferenciasViviendaCandidato($wheresViviendaRef);
		 $data['latitud']        = $RespuestViviend['CoordX'];
		 $data['longitud']       = $RespuestViviend['CoordY'];
		 $data['fotoVivienda1']  = $RespuestViviend['FotoVivienda'];
		 $data['fotoVivienda2']  = $RespuestViviend['FotoInterior'];
		 $data['fotoVivienda3']  = $RespuestViviend['FotoCadidato'];
		 if($RespuestViviend['CoordX'] != null && $RespuestViviend['CoordY'] != null){
		 	$data['mapasUb'] = 2; 
		 }else{
		 	$data['mapasUb'] = 1; 
		 }
		
		 
		$htmlReferenciasAcademicas = " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = 1;
		  for ($i=0; $i < count($ReferenciasAcademicas); $i++) {
		  	$formulario = '';
			   $respuesta = 'Respuesta'.$contador;
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Numerica'){
			  	$data = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario = form_input($data);
			  }
			  
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Texto'){
					  $data = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario = form_textarea($data);
			  
			  }
			  
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones['idPregunta'] = $ReferenciasAcademicas[$i]["idPregunta"];
			  	    $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);
	
					$formulario .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones as $key => $value) {
						
					   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
					   	 $formulario .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						
					   }else{
	                       $formulario .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
					   	
					   }
			
									                 
				   }	
					$formulario .= "</select>";
			
			  }

			  
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Opcion Multiple Multiple'){
			  	  
				  
				  $whereOpciones['idPregunta'] = $ReferenciasAcademicas[$i]["idPregunta"];
			  	  $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);  		
				  
				  foreach ($Opciones as $key => $value) {
					  	
					if($Resespuestas[$respuesta] == $value['Respuesta']){
						$formulario .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				       <span class='lbl'>".$value['Respuesta']."</span> ";	
					}else{
					   $formulario .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";	
					}	

				  }	
			    	
			  }
			  

			  
			  $htmlReferenciasAcademicas .= '
			   <tr>
			     <td colspan="1" width="40%">'.$ReferenciasAcademicas[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario.'</td>
			   </tr>
			  ';
			  
			  $htmlReferenciasAcademicas .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlReferenciasAcademicas .= " 
		 </table>
		 ";
 		

		$data['PreguntasAcademicas']            = $htmlReferenciasAcademicas;
		



         
		$wheresESE4['ESE.idESE']                        = $id;
		$wheresESE4['Pregunta.idGrupoPregunta']         = 4;
		$joinESE4                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$descInmueble = $this->cuestionarios_model->getAllESE($wheresESE4,NULL,$joinESE4);
		
		
	
		
	
		$htmldescInmueble = " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($descInmueble); $i++) {
		  	$formulario4 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($descInmueble[$i]["tipo"] == 'Numerica'){

			  	$data4 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario4 = form_input($data4);
			  }
			  
			  if($descInmueble[$i]["tipo"] == 'Texto'){
			  
					  $data4 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario4 = form_textarea($data4);
			  
			  }
			  
			  if($descInmueble[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones4['idPregunta'] = $descInmueble[$i]["idPregunta"];
			  	    $Opciones4 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones4);
					
					$formulario4 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario4 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones4 as $key => $value) {
				
			         if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			           $formulario4 .=  "<option value='".$value['idOpcionesRespuesta']."'selected>".$value['Respuesta']."</option>";
					   
					}else{
                       $formulario4 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
					   						
					}
              
				   }	
					$formulario4 .= "</select>";
			
			  }

			  
			  if($descInmueble[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones4['idPregunta'] = $descInmueble[$i]["idPregunta"];
			  	  $Opciones4 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones4);  		
				   $formulario4 .= "";
				  foreach ($Opciones4 as $key => $value) {
					   if($Resespuestas[$respuesta] == $value['Respuesta']){
					   		 $formulario4 .= "<br>
					       <input type='checkbox' value=".$value['Respuesta']." id='".$respuesta."' name='".$respuesta."' checked/>
					       <span class='lbl'>".$value['Respuesta']."</span> ";
					   	
					   }else{
					   	 $formulario4 .= "<br>
					       <input type='checkbox' value=".$value['Respuesta']." id='".$respuesta."' name='".$respuesta."' />
					       <span class='lbl'>".$value['Respuesta']."</span> ";
					   }	
					    
						$contador++; 
						$respuesta = 'Respuesta'.$contador;
			    
			    
			    
			       }	
			    	
			  }
			  
             if($descInmueble[$i]["tipo"] == 'Flotante'){

			  	$data4 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario4 = form_input($data4);
			  }
			  
			  $htmldescInmueble .= '
			   <tr>
			     <td colspan="1" width="40%">'.$descInmueble[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario4.'</td>
			   </tr>
			  ';
			  
			  $htmldescInmueble .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmldescInmueble .= " 
		 </table>
		 ";
		 
	$data['PreguntasdescInmueble']            = $htmldescInmueble;
		 
		 $wheresESE5['ESE.idESE']                       = $id;
		$wheresESE5['Pregunta.idGrupoPregunta']         = 5;
		$joinESE5                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$aprecVivienda = $this->cuestionarios_model->getAllESE($wheresESE5,NULL,$joinESE5);
		
		
	
		
	
		$htmlaprecVivienda= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  
		  for ($i=0; $i < count($aprecVivienda); $i++) {
		    	$formulario5 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($aprecVivienda[$i]["tipo"] == 'Numerica'){
       
			  	$data5 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				               'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario5 = form_input($data5);
			  }
			  
			  if($aprecVivienda[$i]["tipo"] == 'Texto'){
		
					  $data5 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario5 = form_textarea($data5);
			  
			  }
			  
			  if($aprecVivienda[$i]["tipo"] == 'Opcion Multiple Unica'){
			
			  	    $whereOpciones5['idPregunta'] = $aprecVivienda[$i]["idPregunta"];
			  	    $Opciones5 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones5);
	
					$formulario5 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario5 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones5 as $key => $value) {
                       	
			          if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			          	$formulario5 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						
					  }else{
                       $formulario5 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
					 					  	
					  }	
				        
						
						             
				   }	
					$formulario5 .= "</select>";
			
			  }

			  
			  if($aprecVivienda[$i]["tipo"] == 'Opcion Multiple Multiple'){
		
				  
				  $whereOpciones5['idPregunta'] = $descInmueble[$i]["idPregunta"];
			  	  $Opciones5 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones5);  		
				  
				  foreach ($Opciones5 as $key => $value) {
					  			
					 if($Resespuestas[$respuesta] == $value['Respuesta']){ 		
					  $formulario5 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				       <span class='lbl'>".$value['Respuesta']."</span> ";
				     }else{
				       $formulario5 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";	
				     } 	
					  
					   $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($aprecVivienda[$i]["tipo"] == 'Flotante'){

			  	$data5 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario5 = form_input($data5);
			  }
			  
			  $htmlaprecVivienda .= '
			   <tr>
			     <td colspan="1" width="40%">'.$aprecVivienda[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario5.'</td>
			   </tr>
			  ';
			  
			  $htmlaprecVivienda .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlaprecVivienda .= " 
		 </table>
		 ";


		$data['aprecVivienda']            = $htmlaprecVivienda;
		///////////////////////////////////////////////////////
		
		
		$wheresESE6['ESE.idESE']                       = $id;
		$wheresESE6['Pregunta.idGrupoPregunta']         = 6;
		$joinESE6                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$percepcionesSocialPersonales = $this->cuestionarios_model->getAllESE($wheresESE6,NULL,$joinESE6);
		
		
	
		
	
		$htmlpercepcionesSocialPersonales= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($percepcionesSocialPersonales); $i++) {
		  	   $formulario6 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Numerica'){

			  	$data6 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario6 = form_input($data6);
			  }
			  
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Texto'){
			  
					  $data6 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario6 = form_textarea($data6);
			  
			  }
			  
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones6['idPregunta'] = $percepcionesSocialPersonales[$i]["idPregunta"];
			  	    $Opciones6 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones6);
	
					$formulario6 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario6 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones6 as $key => $value) {
                      	
			           if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			           	$formulario6 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						
			           }else{
                        $formulario6 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
			           	
			           }
				      
					                 
				   }	
					$formulario6 .= "</select>";
			
			  }

			  
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones6['idPregunta'] = $percepcionesSocialPersonales[$i]["idPregunta"];
			  	  $Opciones6 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones6);  		
				  
				  foreach ($Opciones6 as $key => $value) {
						
					  if($Resespuestas[$respuesta] == $value['Respuesta']){
					  	$formulario6 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					  }else{
					   $formulario6 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";	
					  }	
					  
			            $contador++; 
						$respuesta = 'Respuesta'.$contador;
				  }	
			    	
			  }
			  
             if($percepcionesSocialPersonales[$i]["tipo"] == 'Flotante'){

			  	$data6 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario6 = form_input($data6);
			  }
			  
			  $htmlpercepcionesSocialPersonales .= '
			   <tr>
			     <td colspan="1" width="40%">'.$percepcionesSocialPersonales[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario6.'</td>
			   </tr>
			  ';
			  
			  $htmlpercepcionesSocialPersonales .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlpercepcionesSocialPersonales .= " 
		 </table>
		 ";
 		

		$data['percepcionesSocialPersonales']            = $htmlpercepcionesSocialPersonales;
		
		
		
		
		
		
		$wheresESE7['ESE.idESE']                       = $id;
		$wheresESE7['Pregunta.idGrupoPregunta']         = 7;
		$joinESE7                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$social = $this->cuestionarios_model->getAllESE($wheresESE7,NULL,$joinESE7);
		
		
	
		
	
		$htmlsocial= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($social); $i++) {
		  	   $formulario7 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($social[$i]["tipo"] == 'Numerica'){

			  	$data7 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario7 = form_input($data7);
			  }
			  
			  if($social[$i]["tipo"] == 'Texto'){
			  
					  $data7 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario7 = form_textarea($data7);
			  
			  }
			  
			  if($social[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones7['idPregunta'] = $social[$i]["idPregunta"];
			  	    $Opciones7 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones7);
	
					$formulario7 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario7 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones7 as $key => $value) {
                       	
			         if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			         $formulario7 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
					 	
			         }else{
                       $formulario7 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";			         	
			         }  
				       
					                 
				   }	
					$formulario7 .= "</select>";
			
			  }

			  
			  if($social[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones7['idPregunta'] = $social[$i]["idPregunta"];
			  	  $Opciones7 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones7);  		
				  
				  foreach ($Opciones7 as $key => $value) {
					  	
					if($Resespuestas[$respuesta] == $value['Respuesta']){
						$formulario7 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked />
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					}else{
						$formulario7 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					}	
					    $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($social[$i]["tipo"] == 'Flotante'){

			  	$data7 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario7 = form_input($data7);
			  }
			  
			  $htmlsocial .= '
			   <tr>
			     <td colspan="1" width="40%">'.$social[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario7.'</td>
			   </tr>
			  ';
			  
			  $htmlsocial .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlsocial .= " 
		 </table>
		 ";
 		

		$data['social']            = $htmlsocial;
		
		
		
		$wheresESE8['ESE.idESE']                       = $id;
		$wheresESE8['Pregunta.idGrupoPregunta']         = 8;
		$joinESE8                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$familiar28 = $this->cuestionarios_model->getAllESE($wheresESE8,NULL,$joinESE8);
		
		
	
		
	
		$htmlfamiliar28= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($familiar28); $i++) {
		  	   $formulario8 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($familiar28[$i]["tipo"] == 'Numerica'){

			  	$data8 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario8 = form_input($data8);
			  }
			  
			  if($familiar28[$i]["tipo"] == 'Texto'){
			  
					  $data8 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario8 = form_textarea($data8);
			  
			  }
			  
			  if($familiar28[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones8['idPregunta'] = $familiar28[$i]["idPregunta"];
			  	    $Opciones8 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones8);
	
					$formulario8 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario8 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones8 as $key => $value) {
			           
					   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
                       $formulario8 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
					   	
					   }else{
                       $formulario8 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
					   	
					   }
				                     
				   }	
					$formulario8 .= "</select>";
			
			  }

			  
			  if($familiar28[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones8['idPregunta'] = $familiar28[$i]["idPregunta"];
			  	  $Opciones8 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones8);  		
				  
				  foreach ($Opciones8 as $key => $value) {
					
					  	
					if($Resespuestas[$respuesta] == $value['Respuesta']){
						 $formulario8 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					}else{
					 $formulario8 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";	
					}	
					    $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       
				  }	
			    	
			  }
			  
             if($familiar28[$i]["tipo"] == 'Flotante'){

			  	$data8 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario8 = form_input($data8);
			  }
			  
			  $htmlfamiliar28 .= '
			   <tr>
			     <td colspan="1" width="40%">'.$familiar28[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario8.'</td>
			   </tr>
			  ';
			  
			  $htmlfamiliar28 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlfamiliar28 .= " 
		 </table>
		 ";
 		

		$data['familiar28']            = $htmlfamiliar28;
		
		
		
		$wheresESE9['ESE.idESE']                       = $id;
		$wheresESE9['Pregunta.idGrupoPregunta']         = 9;
		$joinESE9                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$clinica9 = $this->cuestionarios_model->getAllESE($wheresESE9,NULL,$joinESE9);
		
		
	
		
	
		$htmlclinica9= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($clinica9); $i++) {
		  	    $formulario9 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($clinica9[$i]["tipo"] == 'Numerica'){

			  	$data9 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario9 = form_input($data9);
			  }
			  
			  if($clinica9[$i]["tipo"] == 'Texto'){
			  
					  $data9 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario9 = form_textarea($data9);
			  
			  }
			  
			  if($clinica9[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones9['idPregunta'] = $clinica9[$i]["idPregunta"];
			  	    $Opciones9 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones9);
	
					$formulario9 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario9 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones9 as $key => $value) {
			           
					   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
                       $formulario9 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
					   	
					   }else{
                       $formulario9 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
					   	
					   }
				                     
				   }	
					$formulario9 .= "</select>";
			
			  }

			  
			  if($clinica9[$i]["tipo"] == 'Opcion Multiple Multiple'){
			     
				  
				  $whereOpciones9['idPregunta'] = $clinica9[$i]["idPregunta"];
			  	  $Opciones9 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones9);  		
				  
				  foreach ($Opciones9 as $key => $value) {
					  	
					if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						
						$formulario9 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					}else{
						$formulario9 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					}		
						
					  $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($clinica9[$i]["tipo"] == 'Flotante'){

			  	$data9 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario9 = form_input($data9);
			  }
			  
			  $htmlclinica9 .= '
			   <tr>
			     <td colspan="1" width="40%">'.$clinica9[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario9.'</td>
			   </tr>
			  ';
			  
			  $htmlclinica9 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlclinica9 .= " 
		 </table>
		 ";
 		

		$data['clinica']            = $htmlclinica9;
		
		
		
		//------------
		
		$wheresESE11['ESE.idESE']                       = $id;
		$wheresESE11['Pregunta.idGrupoPregunta']         = 11;
		$joinESE11                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$adeudos11 = $this->cuestionarios_model->getAllESE($wheresESE11,NULL,$joinESE11);
		
		
	
		
	
		$htmladeudos11= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($adeudos11); $i++) {
		  	   $formulario11 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($adeudos11[$i]["tipo"] == 'Numerica'){

			  	$data11 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario11 = form_input($data11);
			  }
			  
			  if($adeudos11[$i]["tipo"] == 'Texto'){
			  
					  $data11 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario11 = form_textarea($data11);
			  
			  }
			  
			  if($adeudos11[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones11['idPregunta'] = $adeudos11[$i]["idPregunta"];
			  	    $Opciones11 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones11);
	
					$formulario11 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario11 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones11 as $key => $value) {
			           	
						if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						  $formulario11 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						  	
						}else{
						 $formulario11 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
							
						}
			           
				                     
				   }	
					$formulario11 .= "</select>";
			
			  }

			  
			  if($adeudos11[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones11['idPregunta'] = $adeudos11[$i]["idPregunta"];
			  	  $Opciones11 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones11);  		
				  
				  foreach ($Opciones11 as $key => $value) {
					
						if($Resespuestas[$respuesta] == $value['Respuesta']){
							 $formulario11 .= "<br>
					       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked />
					       <span class='lbl'>".$value['Respuesta']."</span> ";
						}else{
							 $formulario11 .= "<br>
					       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
					       <span class='lbl'>".$value['Respuesta']."</span> ";
						}  	
						$contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($adeudos11[$i]["tipo"] == 'Flotante'){

			  	$data11 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario11 = form_input($data11);
			  }
			  
			  $htmladeudos11 .= '
			   <tr>
			     <td colspan="1" width="40%">'.$adeudos11[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario11.'</td>
			   </tr>
			  ';
			  
			  $htmladeudos11 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmladeudos11 .= " 
		 </table>
		 ";
 		

		$data['adeudos']            = $htmladeudos11;
		
		
		
		//------------
		
		$wheresESE12['ESE.idESE']                       = $id;
		$wheresESE12['Pregunta.idGrupoPregunta']         = 12;
		$joinESE12                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$documentos12 = $this->cuestionarios_model->getAllESE($wheresESE12,NULL,$joinESE12);
		
		
	
		
	
		$htmldocumentos12= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($documentos12); $i++) {
		  	   $formulario12 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($documentos12[$i]["tipo"] == 'Numerica'){

			  	$data12 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario12 = form_input($data12);
			  }
			  
			  if($documentos12[$i]["tipo"] == 'Texto'){
			  
					  $data12 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario12 = form_textarea($data12);
			  
			  }
			  
			  if($documentos12[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones12['idPregunta'] = $documentos12[$i]["idPregunta"];
			  	    $Opciones12 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones12);
	
					$formulario12 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario12 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones12 as $key => $value) {
			            
					if($Resespuestas[$respuesta] == $value['Respuesta']){
                       $formulario12 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						
					}else{
                       $formulario12 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
						
					}	
				                     
				   }	
					$formulario12 .= "</select>";
			
			  }

			  
			  if($documentos12[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones12['idPregunta'] = $documentos12[$i]["idPregunta"];
			  	  $Opciones12 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones12);  		
				  
				  foreach ($Opciones12 as $key => $value) {
					  $formulario11 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";
				       
				       $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($documentos12[$i]["tipo"] == 'Flotante'){

			  	$data12 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario12 = form_input($data12);
			  }
			  
			  $htmldocumentos12 .= '
			   <tr>
			     <td colspan="1" width="40%">'.$documentos12[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario12.'</td>
			   </tr>
			  ';
			  
			  $htmldocumentos12 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmldocumentos12 .= " 
		 </table>
		 ";
 		

		$data['documentos']            = $htmldocumentos12;
		
		
		//------------
		
		$wheresESE13['ESE.idESE']                       = $id;
		$wheresESE13['Pregunta.idGrupoPregunta']         = 13;
		$joinESE13                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$refVecinales13 = $this->cuestionarios_model->getAllESE($wheresESE13,NULL,$joinESE13);
		
		
	
		
	
		$htmlrefVecinales13= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($refVecinales13); $i++) {
		  	    $formulario13 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($refVecinales13[$i]["tipo"] == 'Numerica'){

			  	$data13= array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario13 = form_input($data13);
			  }
			  
			  if($refVecinales13[$i]["tipo"] == 'Texto'){
			  
					  $data13 = array(
						              'name'        => $respuesta,
						              'id'          => $respuesta,
						              'value'       => $Resespuestas[$respuesta],
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
						            );
						
						$formulario13 = form_textarea($data13);
			  
			  }
			  
			  if($refVecinales13[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones13['idPregunta'] = $refVecinales13[$i]["idPregunta"];
			  	    $Opciones13 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones13);
	
					$formulario13 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
					$formulario13 .="<option value=''>Elige una Opcion</option>";
				   foreach ($Opciones13 as $key => $value) {
			           
					  if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
                       $formulario13 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
					  	
					  }else{
                       $formulario13 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
					  	
					  } 
				                     
				   }	
					$formulario13 .= "</select>";
			
			  }

			  
			  if($refVecinales13[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones13['idPregunta'] = $refVecinales13[$i]["idPregunta"];
			  	  $Opciones13 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones13);  		
				  
				  foreach ($Opciones13 as $key => $value) {
					  	
					 if($Resespuestas[$respuesta] == $value['Respuesta']){
					 	$formulario13 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
				       <span class='lbl'>".$value['Respuesta']."</span> ";
					 }else{
					 $formulario13 .= "<br>
				       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
				       <span class='lbl'>".$value['Respuesta']."</span> ";	
					 }	
					  $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($refVecinales13[$i]["tipo"] == 'Flotante'){

			  	$data13 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				              'style'       => 'text-transform:uppercase;'
				            );
				
				$formulario13 = form_input($data13);
			  }
			  
			  $htmlrefVecinales13 .= '
			   <tr>
			     <td colspan="1" width="40%">'.$refVecinales13[$i]["Oracion"].'</td>
			     <td colspan="2" width="60%">'.$formulario13.'</td>
			   </tr>
			  ';
			  
			  $htmlrefVecinales13 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlrefVecinales13 .= " 
		 </table>
		 ";
 		

		$data['refVecinales']            = $htmlrefVecinales13;
		
		
		
		//------------
		
		$wheresESE14['ESE.idESE']                       = $id;
		$wheresESE14['Pregunta.idGrupoPregunta']         = 14;
		$joinESE14                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$refdomicilio14 = $this->cuestionarios_model->getAllESE($wheresESE14,NULL,$joinESE14);
		
	
		

 	
			$htmlrefdomicilio14= " 
			<table width='100%' frame='box' border='0'>
			";
			  $contador = $contador;
			  for ($i=0; $i < count($refdomicilio14); $i++) {
	               $formulario14 = '';
				   $respuesta = 'Respuesta'.$contador;
				   
				  if($refdomicilio14[$i]["tipo"] == 'Numerica'){
	
				  	$data14= array(
					              'name'        => $respuesta,
					              'id'          => $respuesta,
					              'value'       => $Resespuestas[$respuesta],
					              'class'       => 'span3',
					              'style'       => 'text-transform:uppercase;'
					            );
					
					$formulario14 = form_input($data14);
				  }
				  
				  if($refdomicilio14[$i]["tipo"] == 'Texto'){
				  
						  $data14 = array(
							              'name'        => $respuesta,
							              'id'          => $respuesta,
							              'value'       => $Resespuestas[$respuesta],
							              'class'       => 'span3',
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
							            );
							
							$formulario14 = form_textarea($data14);
				  
				  }
				  
				  if($refdomicilio14[$i]["tipo"] == 'Opcion Multiple Unica'){
				 
				  	    $whereOpciones14['idPregunta'] = $refdomicilio14[$i]["idPregunta"];
				  	    $Opciones14 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones14);
		
						$formulario14 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
						$formulario14 .="<option value=''>Elige una Opcion</option>";
					   foreach ($Opciones14 as $key => $value) {
				           
						   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						   	$formulario14 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						  
						   }else{
	                       $formulario14 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
						   	
						   }
						   
					                     
					   }	
						$formulario14 .= "</select>";
				
				  }
	
				  
				  if($refdomicilio14[$i]["tipo"] == 'Opcion Multiple Multiple'){
				 
					  
					  $whereOpciones14['idPregunta'] = $refdomicilio14[$i]["idPregunta"];
				  	  $Opciones14 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones14);  		
					  
					  foreach ($Opciones14 as $key => $value) {
						  			
						  if($Resespuestas[$respuesta] == $value['Respuesta']){
						  	 $formulario14 .= "<br>
					       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
					       <span class='lbl'>".$value['Respuesta']."</span> ";
						  }else{
						    $formulario14 .= "<br>
					       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
					       <span class='lbl'>".$value['Respuesta']."</span> ";	
						  }		
						  	
						  $contador++; 
						$respuesta = 'Respuesta'.$contador;
				       }	
				    	
				  }
				  
	             if($refdomicilio14[$i]["tipo"] == 'Flotante'){
	
				  	$data14 = array(
					              'name'        => $respuesta,
					              'id'          => $respuesta,
					              'value'       => $Resespuestas[$respuesta],
					              'class'       => 'span3',
					              'style'       => 'text-transform:uppercase;'
					            );
					
					$formulario14 = form_input($data14);
				  }
				  
				  $htmlrefdomicilio14 .= '
				   <tr>
				     <td colspan="1" width="40%">'.$refdomicilio14[$i]["Oracion"].'</td>
				     <td colspan="2" width="60%">'.$formulario14.'</td>
				   </tr>
				  ';
				  
				  $htmlrefdomicilio14 .= '
				   <tr>
				     <td colspan="3"><br<br></td>
	
				   </tr>
				  ';
				  
				  $contador++;
			  }
	 		
		  $htmlrefdomicilio14 .= " 
			 </table>
			 ";
		 

		$data['refdomicilio']            = $htmlrefdomicilio14;
		
		
		
		///////////////////////
		
		
		//------------
		
		$wheresESE16['ESE.idESE']                       = $id;
		$wheresESE16['Pregunta.idGrupoPregunta']         = 16;
		$joinESE16                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )//,
										  // array(
											  // 'table' => 'OpcionesRespuesta',
											  // 'cond'  => 'OpcionesRespuesta.idPregunta = Pregunta.idPregunta',
											  // 'type'  => 'left' 
											  //)//,
										  // array(
											  // 'table' => 'RespuestasCuestionario',
											  // 'cond'  => 'RespuestasCuestionario.idCandidato = ESE.idCandidato',
											  // 'type'  => 'left' 
											  // )
		                                 );


	
		$cartasRecomenacion16 = $this->cuestionarios_model->getAllESE($wheresESE16,NULL,$joinESE16);
		

		

 	
			$htmlcartasRecomendacion16= " 
			<table width='100%' frame='box' border='0'>
			";
			  $contador = $contador;
			  for ($i=0; $i < count($cartasRecomenacion16); $i++) {
	               $formulario16 = '';
				   $respuesta = 'Respuesta'.$contador;
				   
				  if($cartasRecomenacion16[$i]["tipo"] == 'Numerica'){
	
				  	$data16= array(
					              'name'        => $respuesta,
					              'id'          => $respuesta,
					              'value'       => $Resespuestas[$respuesta],
					              'class'       => 'span3',
					              'style'       => 'text-transform:uppercase;'
					            );
					
					$formulario16 = form_input($data16);
				  }
				  
				  if($cartasRecomenacion16[$i]["tipo"] == 'Texto'){
				  
						  $data16 = array(
							              'name'        => $respuesta,
							              'id'          => $respuesta,
							              'value'       => $Resespuestas[$respuesta],
							              'class'       => 'span3',
						              'rows'            => 2,
						              'cols'            => 40,
						              'style'       => 'text-transform:uppercase;'
							            );
							
							$formulario16 = form_textarea($data16);
				  
				  }
				  
				  if($cartasRecomenacion16[$i]["tipo"] == 'Opcion Multiple Unica'){
				 
				  	    $whereOpciones16['idPregunta'] = $cartasRecomenacion16[$i]["idPregunta"];
				  	    $Opciones16 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones14);
		
						$formulario16 .= "<br><select class='form-field-select-1' id='".$respuesta."' name='".$respuesta."'>";
						$formulario16 .="<option value=''>Elige una Opcion</option>";
					   foreach ($Opciones16 as $key => $value) {
				           
						   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						   	$formulario16 .=  "<option value='".$value['idOpcionesRespuesta']."' selected>".$value['Respuesta']."</option>";
						  
						   }else{
	                       $formulario16 .=  "<option value='".$value['idOpcionesRespuesta']."'>".$value['Respuesta']."</option>";
						   	
						   }
						   
					                     
					   }	
						$formulario16 .= "</select>";
				
				  }
	
				  
				  if($cartasRecomenacion16[$i]["tipo"] == 'Opcion Multiple Multiple'){
				 
					  
					  $whereOpciones16['idPregunta'] = $cartasRecomenacion16[$i]["idPregunta"];
				  	  $Opciones16 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones14);  		
					  
					  foreach ($Opciones16 as $key => $value) {
						  			
						  if($Resespuestas[$respuesta] == $value['Respuesta']){
						  	 $formulario16 .= "<br>
					       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' checked/>
					       <span class='lbl'>".$value['Respuesta']."</span> ";
						  }else{
						    $formulario16 .= "<br>
					       <input type='checkbox' id='".$respuesta."' name='".$respuesta."' />
					       <span class='lbl'>".$value['Respuesta']."</span> ";	
						  }		
						  	$contador++; 
						$respuesta = 'Respuesta'.$contador;
						  
				       }	
				    	
				  }
				  
	             if($cartasRecomenacion16[$i]["tipo"] == 'Flotante'){
	
				  	$data16 = array(
					              'name'        => $respuesta,
					              'id'          => $respuesta,
					              'value'       => $Resespuestas[$respuesta],
					              'class'       => 'span3',
					              'style'       => 'text-transform:uppercase;'
					            );
					
					$formulario16 = form_input($data16);
				  }
				  
				  $htmlcartasRecomendacion16 .= '
				   <tr>
				     <td colspan="1" width="40%">'.$cartasRecomenacion16[$i]["Oracion"].'</td>
				     <td colspan="2" width="60%">'.$formulario16.'</td>
				   </tr>
				  ';
				  
				  $htmlcartasRecomendacion16 .= '
				   <tr>
				     <td colspan="3"><br<br></td>
	
				   </tr>
				  ';
				  
				  $contador++;
			  }
	 		
		  $htmlcartasRecomendacion16 .= " 
			 </table>
			 ";
		 

		$data['cartRecomendacion']            = $htmlcartasRecomendacion16;
		//////////////////////
		
		
		
		
		
		
		
		
	
		$data['contadorP']              = $contador;
		
		
		
		$this->parser->parse('solicitudes/investigadorSolicitud', $data);
	}
    
	public function pdf_crear(){
		$codigo='';

		require_once('./application/third_party/dompdf/dompdf_config.inc.php');
		//$_POST['idCandidato'];
		
	     $longitud = $_POST['longitud1'];
		 $latitud = $_POST['latitud1'];
		$data['mapaImagen'] = '<img id="mapa_ubicacion" src="http://maps.googleapis.com/maps/api/staticmap?center='.$latitud.','.$longitud.'&zoom=15&markers=color:red|'.$latitud.','.$longitud.'&size=409x228&sensor=false" width="409" height="228" alt="Maps">';
        sleep(5);

		 $wheres['ESE.idESE']         = $_POST['idESE'];
		
		
		 $join                          = array(
			                               array(
											   'table' => 'Candidato',
											   'cond'  => 'Candidato.idCandidato = ESE.idCandidato',
											   'type'  => 'inner' 
											   ),
										   array(
											   'table' => 'ESE_has_CuestionarioESE',
											   'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											   'type'  => 'inner' 
											   ),
										   array(
											   'table' => 'Empleado',
											   'cond'  => 'Empleado.idEmpleado = ESE.idEmpleado',
											   'type'  => 'inner' 
											   )
											   
		                                  );
 		
 		
		$ESE         = $this->solicitudes_model->getOneSolicitudesESE($wheres,$join);
		//print_r($ESE);
	    $data['datos'] =$ESE;
	    
		
		
		$data['longitud'] =  $_POST['latitud1'];
		$data['latitud']  =  $_POST['longitud1'];
 		
		$data['DocumentosEscolar']  = $this->documentos_model->getAllDocumentoEscolar();
		$data['baseURL']            = base_url();
		$data['TipoCandidato']      = $this->tipo_candidato_model->getAllTipoCandidato();
        $data['Estadocivil']        = $this->estadocivil_model->getAllEstadoCivil();
		$data['Parentesco']         = $this->parentesco_model->getAllParentesco();
		$data['Escolaridad']        = $this->escolaridad_model->getAllEscolaridad();
		$data['conceptosEco']           = $this->conceptos_eco_model->getAllConceptosEco();
		$data['evaluacionLaboral']  = $this->evaluacion_laboral_model->getAllEvaluacionLaboral();
	 
	
if($_SESSION['SEUS']['use_typ_id'] == 1 || $_SESSION['SEUS']['use_typ_id'] == 2  || $_SESSION['SEUS']['use_typ_id'] == 3  || $_SESSION['SEUS']['use_typ_id'] == 4   || $_SESSION['SEUS']['use_typ_id'] == 7 ){
			
		$id = $_POST['idESE'];
		//// ACADAMECIAS 
		$wheresESE['ESE.idESE']                        = $id;
		$wheresESE['Pregunta.idGrupoPregunta']         = 2;
		$joinESE                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );
		
		
		$ReferenciasAcademicas = $this->cuestionarios_model->getAllESE($wheresESE,NULL,$joinESE);
	
		
		
		////////7
		$wheresRes['idCandidato']         = $ESE['idCandidato'];
		$wheresRes['idCuestionario']      = $ESE['idCuestionarioTipo'];

		$Resespuestas = $this->cuestionarios_model->getOneResCuestionarioESE($wheresRes);
		///
	    ///////REFERENCIAS ACADEMICAS
	    $wheresRefAcademicas['idCandidato']         = $ESE['idCandidato'];
	    $RespuestRefAcademicas    = $this->referencia_escolares_model->getAllReferenciaEscolar($wheresRefAcademicas);
	    
		 
		 for ($i=0; $i <= count($RespuestRefAcademicas); $i++) {
		 	 	
				
		 	 $numNivel = $RespuestRefAcademicas[$i]['idEscolaridad'];
			 $data['val_ref_acade_PeriodoInicio'.$numNivel] = htmlentities($RespuestRefAcademicas[$i]['PeriodoInicio']);
			 $data['val_ref_acade_PeriodoFin'.$numNivel] = htmlentities($RespuestRefAcademicas[$i]['PeriodoFin']);
			 $data['val_ref_acade_NombreInst'.$numNivel] = htmlentities($RespuestRefAcademicas[$i]['NombreInst']);
			 $data['val_ref_acade_idDocumentoEscolar'.$numNivel] = htmlentities($RespuestRefAcademicas[$i]['idDocumentoEscolar']);
			 $data['val_ref_acade_Folio'.$numNivel] = htmlentities($RespuestRefAcademicas[$i]['Folio']);
			
		 }	
		
		///// REFERENCIAS PERSONALES
		 $wheresRefPersonalesCandidato['idCandidato'] = $ESE['idCandidato'];
		 $RespuestRefPersonalesCandidato = $this->refcandidato_model->getAllReferenciasCandidato($wheresRefPersonalesCandidato);
		 $contadors = 1;
		 for ($i=0; $i <= count($RespuestRefPersonalesCandidato); $i++) {
		 	
		 	$data['nombre_familiar'.$contadors]      = htmlentities($RespuestRefPersonalesCandidato[$i]['Nombres']);
		 	$data['apellidop_familiar'.$contadors]   = htmlentities($RespuestRefPersonalesCandidato[$i]['ApellidoPaterno']);
		 	$data['apellidom_familiar'.$contadors]   = htmlentities($RespuestRefPersonalesCandidato[$i]['ApellidoMaterno']);
			$data['parentesco_familiar'.$contadors] = htmlentities($RespuestRefPersonalesCandidato[$i]['idParentesco']);
			$data['escolaridad_familiar'.$contadors] = htmlentities($RespuestRefPersonalesCandidato[$i]['idEscolaridad']);
			$data['empresa_familiar'.$contadors]     = htmlentities($RespuestRefPersonalesCandidato[$i]['EmpresaOcupacion']);
			$data['mismaVivienda'.$contadors]        = htmlentities($RespuestRefPersonalesCandidato[$i]['MismaVivienda']);
			$contadors ++;
		 }	
		
		 
		 
		 //// REFERENCIAS ECONOMICAS
		 $wheresRefEconomicas['idCandidato'] = $ESE['idCandidato'];
		 $RespuestRefEconomicas = $this->refeconomicas_candidato_model->getAllReferenciasEconomicasCandidato($wheresRefEconomicas);
		 $contadors1 = 1;
		 for ($i=0; $i <= count($RespuestRefEconomicas); $i++) {
		   
		   $data['nombre_referenciEconFamiliar'.$contadors1]        = htmlentities($RespuestRefEconomicas[$i]['Nombres']);
		   $data['apellidop_referenciEconFamiliar'.$contadors1]     = htmlentities($RespuestRefEconomicas[$i]['ApellidoPaterno']);
		   $data['apellidom_referenciEconFamiliar'.$contadors1]     = htmlentities($RespuestRefEconomicas[$i]['ApellidoMaterno']);
		   $data['cuantogana_referenciEconFamiliar'.$contadors1]    = htmlentities($RespuestRefEconomicas[$i]['Ingreso']);
		   $data['cuantoaporta_referenciEconFamiliar'.$contadors1]  = htmlentities($RespuestRefEconomicas[$i]['Aportacion']);
		   $data['conceptosEco'.$contadors1]                        = htmlentities($RespuestRefEconomicas[$i]['idConceptosEco']);
		   $data['egresos_referenciEconFamiliar'.$contadors1]       = htmlentities($RespuestRefEconomicas[$i]['Egresos']);
		   $contadors1 ++;
		 }
		 
		 //// Referencias Laborales
		 
		 $wheresRefLaborales['idCandidato'] = $ESE['idCandidato'];
		 $RespuestRefLaborales = $this->reflabcandidato_model->getAllReferenciasLabCandidato($wheresRefLaborales);
		 $contadors2 = 1;
		 for ($i=0; $i <  count($RespuestRefLaborales); $i++) {
		 	
			 $data['ref'.$contadors2.'_nombreEmpresa']     = $RespuestRefLaborales[$i]['NombreEmpresa'];
			 $data['ref'.$contadors2.'_giro']              = $RespuestRefLaborales[$i]['Giro'];
			 $data['ref'.$contadors2.'_domicilio']         = $RespuestRefLaborales[$i]['Domicilio'];
			 $data['ref'.$contadors2.'_num_empleado']      = $RespuestRefLaborales[$i]['numEmpleado'];
			 $data['ref'.$contadors2.'_telefono']          = $RespuestRefLaborales[$i]['Telefono'];
			 $data['ref'.$contadors2.'_fecha_ingreso']     = $RespuestRefLaborales[$i]['FechaIngreso'];
			 $data['ref'.$contadors2.'_fecha_salida']      = $RespuestRefLaborales[$i]['FechaSalida'];
			 $data['ref'.$contadors2.'_primer_puesto']     = $RespuestRefLaborales[$i]['PrimerPuesto'];
			 $data['ref'.$contadors2.'_sueldo_inicial']    = $RespuestRefLaborales[$i]['SueldoIni'];
			 $data['ref'.$contadors2.'_ultimo_puesto']     = $RespuestRefLaborales[$i]['UltimoPuesto'];
			 $data['ref'.$contadors2.'_sueldo_final']      = $RespuestRefLaborales[$i]['SueldoFin'];
			 $data['ref'.$contadors2.'_jefeInmediato']     = $RespuestRefLaborales[$i]['JefeInmediato'];
			 $data['ref'.$contadors2.'_puesto']            = $RespuestRefLaborales[$i]['PuestoJefe'];
			 $data['ref'.$contadors2.'_motivoSalida']      = $RespuestRefLaborales[$i]['MotivoSalida'];
			 $data['ref'.$contadors2.'_eficencia']         = $RespuestRefLaborales[$i]['Eficiencia'];
			 $data['ref'.$contadors2.'_puntualidad']       = $RespuestRefLaborales[$i]['Puntualidad'];
			 $data['ref'.$contadors2.'_responsibilidad']   = $RespuestRefLaborales[$i]['Responsabilidad'];
			 $data['ref'.$contadors2.'_dinamismo']         = $RespuestRefLaborales[$i]['Dinamismo'];
			 $data['ref'.$contadors2.'_cooperacion']       = $RespuestRefLaborales[$i]['Cooperacion'];
			 $data['ref'.$contadors2.'_honradez']          = $RespuestRefLaborales[$i]['Honradez'];
			 $data['ref'.$contadors2.'_disciplina']        = $RespuestRefLaborales[$i]['Disciplina'];
			 $data['ref'.$contadors2.'_actitud_jefe']      = $RespuestRefLaborales[$i]['ActitudJefe'];
			 $data['ref'.$contadors2.'_actitud_compa']     = $RespuestRefLaborales[$i]['ActitudComp'];
			 $data['ref'.$contadors2.'_subordinados']      = $RespuestRefLaborales[$i]['ActitudSub'];
			 $data['ref'.$contadors2.'_interes_trabajo']   = $RespuestRefLaborales[$i]['InteresTrabajo'];
			 $data['ref'.$contadors2.'_iniciativa']        = $RespuestRefLaborales[$i]['Iniciativa'];
			 $data['ref'.$contadors2.'_actitudes_sindicales']      = $RespuestRefLaborales[$i]['ActitudesSindicales'];
			 $data['ref'.$contadors2.'_accidentes_enfermedades']   = $RespuestRefLaborales[$i]['AccidentesEnf'];
			 $data['ref'.$contadors2.'_puesto']                    = $RespuestRefLaborales[$i]['Puesto'];
			 $data['ref'.$contadors2.'_fecha_informacion']         = $RespuestRefLaborales[$i]['FuenteInfo'];
			 $data['ref'.$contadors2.'_fecha_ref']                 = $RespuestRefLaborales[$i]['FechaEnt'];
			 $data['ref'.$contadors2.'_telefono_ref']              = $RespuestRefLaborales[$i]['TelefonoEnt'];
			 $data['ref'.$contadors2.'_observaciones_ref']         = $RespuestRefLaborales[$i]['Observaciones'];
	 	
			
			$contadors2 ++;
		 }	
		 
		 /// IMSS
		 $wheresImssRef['idCandidato'] = $ESE['idCandidato'];
		 $RespuestImss = $this->refimss_model->getOneReferenciasImssCandidato($wheresImssRef);
		 
		 $EmpresImss = explode('|',$RespuestImss['Empresa']);
		 $contadors3 = 1;
		 for ($i=0; $i < count($EmpresImss); $i++) { 
			   $data['imss'.$contadors3.'_empresa'] = htmlentities($EmpresImss[$i]);
			   
			   $contadors3++;
		 }
		 
		 
		 $PeriodoImss = explode('|',$RespuestImss['Periodo']);
		 $contadors4 = 1;
		 for ($i=0; $i < count($PeriodoImss); $i++) { 
			   $data['imss'.$contadors4.'_periodo'] = $PeriodoImss[$i];
			   
			   $contadors4++;
		 }
		 $data['imss_reportadas_ca'] = htmlentities($RespuestImss['ReportadaCandidato']);
		 $data['imss_reportadas_imms'] = htmlentities($RespuestImss['ReportadaImss']);
		 
		 /// VIVIENDA
		 $wheresViviendaRef['idCandidato'] = $ESE['idCandidato'];
		 $RespuestViviend = $this->refvivienda_model->getOneReferenciasViviendaCandidato($wheresViviendaRef);
		 $data['latitud']        = $RespuestViviend['CoordX'];
		 $data['longitud']       = $RespuestViviend['CoordY'];
		 $data['fotoVivienda1']  = $RespuestViviend['FotoVivienda'];
		 $data['fotoVivienda2']  = $RespuestViviend['FotoInterior'];
		 $data['fotoVivienda3']  = $RespuestViviend['FotoCadidato'];
		 if($RespuestViviend['CoordX'] != null && $RespuestViviend['CoordY'] != null){
		 	$data['mapasUb'] = 2; 
		 }else{
		 	$data['mapasUb'] = 1; 
		 }
		
		 
		$htmlReferenciasAcademicas = " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = 1;
		  for ($i=0; $i < count($ReferenciasAcademicas); $i++) {
		  	$formulario = '';
			   $respuesta = 'Respuesta'.$contador;
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Numerica'){
					$formulario = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Texto'){

						$formulario = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones['idPregunta'] = $ReferenciasAcademicas[$i]["idPregunta"];
			  	    $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);

				   foreach ($Opciones as $key => $value) {
						
					   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
					   	
					   	 $formulario .=  htmlentities($value['Respuesta']);
						
					   }
									                 
				   }	
				
			
			  }

			  
			  if($ReferenciasAcademicas[$i]["tipo"] == 'Opcion Multiple Multiple'){
			  	  
				  
				  $whereOpciones['idPregunta'] = $ReferenciasAcademicas[$i]["idPregunta"];
			  	  $Opciones = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones);  		
				  
				  foreach ($Opciones as $key => $value) {
					  	
					if($Resespuestas[$respuesta] == $value['Respuesta']){
						$formulario .= "<br><span class='lbl'>".htmlentities($value['Respuesta'])."</span>";	
					}

				  }	
			    	
			  }
			  

	
			  $htmlReferenciasAcademicas .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($ReferenciasAcademicas[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario.'</td>
			   </tr>
			  ';
			  
			  $htmlReferenciasAcademicas .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlReferenciasAcademicas .= " 
		 </table>
		 ";
 		

		$data['PreguntasAcademicas']            = $htmlReferenciasAcademicas;
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
         $wheresESE4['ESE.idESE']                        = $id;
		$wheresESE4['Pregunta.idGrupoPregunta']         = 4;
		$joinESE4                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$descInmueble = $this->cuestionarios_model->getAllESE($wheresESE4,NULL,$joinESE4);
		
		
	
		
	
		$htmldescInmueble = " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($descInmueble); $i++) {
		  	$formulario4 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($descInmueble[$i]["tipo"] == 'Numerica'){
				
				$formulario4 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($descInmueble[$i]["tipo"] == 'Texto'){
			  
						
						$formulario4 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($descInmueble[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones4['idPregunta'] = $descInmueble[$i]["idPregunta"];
			  	    $Opciones4 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones4);
	
					
				   foreach ($Opciones4 as $key => $value) {
				
				         if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
				           $formulario4 .=  htmlentities($value['Respuesta']);
						   
						}
              
				   }	

			
			  }

			  
			  if($descInmueble[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones4['idPregunta'] = $descInmueble[$i]["idPregunta"];
			  	  $Opciones4 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones4);  		
				  
				  foreach ($Opciones4 as $key => $value) {
				
					   if($Resespuestas[$respuesta] == $value['Respuesta']){
					   
					   		 $formulario4 .= "<br>".htmlentities($value['Respuesta']);
					   	
					   }
						$contador++;
			    		$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($descInmueble[$i]["tipo"] == 'Flotante'){

			  	$data4 = array(
				              'name'        => $respuesta,
				              'id'          => $respuesta,
				              'value'       => $Resespuestas[$respuesta],
				              'class'       => 'span3',
				            );
				
				$formulario4 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmldescInmueble .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($descInmueble[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario4.'</td>
			   </tr>
			  ';
			  
			  $htmldescInmueble .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmldescInmueble .= " 
		 </table>
		 ";
		 
	$data['PreguntasdescInmueble']            = $htmldescInmueble;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////        	
			
$wheresESE5['ESE.idESE']                       = $id;
		$wheresESE5['Pregunta.idGrupoPregunta']         = 5;
		$joinESE5                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$aprecVivienda = $this->cuestionarios_model->getAllESE($wheresESE5,NULL,$joinESE5);
		
		
	
		
	
		$htmlaprecVivienda= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  
		  for ($i=0; $i < count($aprecVivienda); $i++) {
		    	$formulario5 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($aprecVivienda[$i]["tipo"] == 'Numerica'){
				
				$formulario5 = $Resespuestas[$respuesta];
			  }
			  
			  if($aprecVivienda[$i]["tipo"] == 'Texto'){
						
						$formulario5 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($aprecVivienda[$i]["tipo"] == 'Opcion Multiple Unica'){
			
			  	    $whereOpciones5['idPregunta'] = $aprecVivienda[$i]["idPregunta"];
			  	    $Opciones5 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones5);
	
					$formulario5 .= "<br>";
					
				   foreach ($Opciones5 as $key => $value) {
                       	
			          if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			          	$formulario5 .=  htmlentities($value['Respuesta']);
						
					  }
           
				   }	
					
			
			  }

			  
			  if($aprecVivienda[$i]["tipo"] == 'Opcion Multiple Multiple'){
		
				  
				  $whereOpciones5['idPregunta'] = $descInmueble[$i]["idPregunta"];
			  	  $Opciones5 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones5);  		
				  
				  foreach ($Opciones5 as $key => $value) {
					  			
					 if($Resespuestas[$respuesta] == $value['Respuesta']){ 		
					   $formulario5 .= "<br>".htmlentities($value['Respuesta']);
				     } 	
					  
					    $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($aprecVivienda[$i]["tipo"] == 'Flotante'){
		
				$formulario5 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmlaprecVivienda .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($aprecVivienda[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario5.'</td>
			   </tr>
			  ';
			  
			  $htmlaprecVivienda .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlaprecVivienda .= " 
		 </table>
		 ";


		$data['aprecVivienda']            = $htmlaprecVivienda;
			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		$wheresESE6['ESE.idESE']                       = $id;
		$wheresESE6['Pregunta.idGrupoPregunta']         = 6;
		$joinESE6                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$percepcionesSocialPersonales = $this->cuestionarios_model->getAllESE($wheresESE6,NULL,$joinESE6);
		
		$htmlpercepcionesSocialPersonales= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($percepcionesSocialPersonales); $i++) {
		  	   $formulario6 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Numerica'){
				
				$formulario6 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Texto'){
						
						$formulario6 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones6['idPregunta'] = $percepcionesSocialPersonales[$i]["idPregunta"];
			  	    $Opciones6 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones6);
	
					$formulario6 .= "<br>";
				   foreach ($Opciones6 as $key => $value) {
                      	
			           if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			           	$formulario6 .=  htmlentities($value['Respuesta']);
						
			           }
				      
					                 
				   }	
			
			  }

			  
			  if($percepcionesSocialPersonales[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones6['idPregunta'] = $percepcionesSocialPersonales[$i]["idPregunta"];
			  	  $Opciones6 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones6);  		
				  
				  foreach ($Opciones6 as $key => $value) {
						
					  if($Resespuestas[$respuesta] == $value['Respuesta']){
					  	$formulario6 .= "<br>".htmlentities($value['Respuesta']);
					  }
					  
			            $contador++; 
						$respuesta = 'Respuesta'.$contador;
				  }	
			    	
			  }
			  
             if($percepcionesSocialPersonales[$i]["tipo"] == 'Flotante'){

				
				$formulario6 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmlpercepcionesSocialPersonales .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($percepcionesSocialPersonales[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario6.'</td>
			   </tr>
			  ';
			  
			  $htmlpercepcionesSocialPersonales .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlpercepcionesSocialPersonales .= " 
		 </table>
		 ";
 		

		$data['percepcionesSocialPersonales']            = $htmlpercepcionesSocialPersonales;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $wheresESE7['ESE.idESE']                       = $id;
		$wheresESE7['Pregunta.idGrupoPregunta']         = 7;
		$joinESE7                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$social = $this->cuestionarios_model->getAllESE($wheresESE7,NULL,$joinESE7);
		
		
	
		
	
		$htmlsocial= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($social); $i++) {
		  	   $formulario7 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($social[$i]["tipo"] == 'Numerica'){
				
				$formulario7 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($social[$i]["tipo"] == 'Texto'){

						
						$formulario7 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($social[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones7['idPregunta'] = $social[$i]["idPregunta"];
			  	    $Opciones7 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones7);
	
					$formulario7 .= "<br>";
					
				   foreach ($Opciones7 as $key => $value) {
                       	
			         if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
			         $formulario7 .=  htmlentities($value['Respuesta']);
					 	
			         }
         
				   }	

			
			  }

			  
			  if($social[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones7['idPregunta'] = $social[$i]["idPregunta"];
			  	  $Opciones7 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones7);  		
				  
				  foreach ($Opciones7 as $key => $value) {
					  	
					if($Resespuestas[$respuesta] == $value['Respuesta']){
						$formulario7 .= "<br>".htmlentities($value['Respuesta']);
					}	
					    $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($social[$i]["tipo"] == 'Flotante'){

				
				$formulario7 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmlsocial .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($social[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario7.'</td>
			   </tr>
			  ';
			  
			  $htmlsocial .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlsocial .= " 
		 </table>
		 ";
 		

		$data['social']            = $htmlsocial;


/////////////////////////////////////////////////////////////////////////////////////////////////////////////	
$wheresESE8['ESE.idESE']                       = $id;
		$wheresESE8['Pregunta.idGrupoPregunta']         = 8;
		$joinESE8                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$familiar28 = $this->cuestionarios_model->getAllESE($wheresESE8,NULL,$joinESE8);
		
		
	
		
	
		$htmlfamiliar28= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($familiar28); $i++) {
		  	   $formulario8 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($familiar28[$i]["tipo"] == 'Numerica'){

				$formulario8 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($familiar28[$i]["tipo"] == 'Texto'){
			  

						$formulario8 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($familiar28[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones8['idPregunta'] = $familiar28[$i]["idPregunta"];
			  	    $Opciones8 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones8);
	

				   foreach ($Opciones8 as $key => $value) {
			           
					   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
                       $formulario8 .=  htmlentities($value['Respuesta']);
					   }
				                     
				   }	
			
			  }

			  
			  if($familiar28[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones8['idPregunta'] = $familiar28[$i]["idPregunta"];
			  	  $Opciones8 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones8);  		
				  
				  foreach ($Opciones8 as $key => $value) {
					
					  	
					if($Resespuestas[$respuesta] == $value['Respuesta']){
						 $formulario8 .= "<br>".htmlentities($value['Respuesta']);
					}
					    $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       
				  }	
			    	
			  }
			  
             if($familiar28[$i]["tipo"] == 'Flotante'){
				
				$formulario8 = $Resespuestas[$respuesta];
			  }
			  
			  $htmlfamiliar28 .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($familiar28[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario8.'</td>
			   </tr>
			  ';
			  
			  $htmlfamiliar28 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlfamiliar28 .= " 
		 </table>
		 ";
 		

		$data['familiar28']            = $htmlfamiliar28;

////////////////////////////////////////////////////////////////////////////////////////////////////////////	
$wheresESE9['ESE.idESE']                       = $id;
		$wheresESE9['Pregunta.idGrupoPregunta']         = 9;
		$joinESE9                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$clinica9 = $this->cuestionarios_model->getAllESE($wheresESE9,NULL,$joinESE9);
		
		
	
		
	
		$htmlclinica9= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($clinica9); $i++) {
		  	    $formulario9 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($clinica9[$i]["tipo"] == 'Numerica'){

				
				$formulario9 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($clinica9[$i]["tipo"] == 'Texto'){
			  
						
						$formulario9 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($clinica9[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones9['idPregunta'] = $clinica9[$i]["idPregunta"];
			  	    $Opciones9 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones9);
	
					
				   foreach ($Opciones9 as $key => $value) {
			           
					   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
                       $formulario9 .=  htmlentities($value['Respuesta']);
					   	
					   }
				                     
				   }	
			
			  }

			  
			  if($clinica9[$i]["tipo"] == 'Opcion Multiple Multiple'){
			     
				  
				  $whereOpciones9['idPregunta'] = $clinica9[$i]["idPregunta"];
			  	  $Opciones9 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones9);  		
				  
				  foreach ($Opciones9 as $key => $value) {
					  	
					if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						
						$formulario9 .= "<br>".htmlentities($value['Respuesta']);
					}	
						
					  $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($clinica9[$i]["tipo"] == 'Flotante'){

				
				$formulario9 = $Resespuestas[$respuesta];
			  }
			  
			  $htmlclinica9 .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($clinica9[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario9.'</td>
			   </tr>
			  ';
			  
			  $htmlclinica9 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlclinica9 .= " 
		 </table>
		 ";
 		

		$data['clinica']            = $htmlclinica9;


///////////////////////////////////////////////////////////////////////////////////////////////////////////	

$wheresESE11['ESE.idESE']                       = $id;
		$wheresESE11['Pregunta.idGrupoPregunta']         = 11;
		$joinESE11                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$adeudos11 = $this->cuestionarios_model->getAllESE($wheresESE11,NULL,$joinESE11);
		
		
	
		
	
		$htmladeudos11= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($adeudos11); $i++) {
		  	   $formulario11 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($adeudos11[$i]["tipo"] == 'Numerica'){
				
				$formulario11 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($adeudos11[$i]["tipo"] == 'Texto'){
			  
						$formulario11 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($adeudos11[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones11['idPregunta'] = $adeudos11[$i]["idPregunta"];
			  	    $Opciones11 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones11);
	
					
					foreach ($Opciones11 as $key => $value) {
			           	
						if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						  $formulario11 .=  htmlentities($value['Respuesta']);
						  	
						}          
				   }	

			
			  }

			  
			  if($adeudos11[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones11['idPregunta'] = $adeudos11[$i]["idPregunta"];
			  	  $Opciones11 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones11);  		
				  
				  foreach ($Opciones11 as $key => $value) {
					
						if($Resespuestas[$respuesta] == $value['Respuesta']){
							 $formulario11 .= "<br>".htmlentities($value['Respuesta']);
						}
						$contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($adeudos11[$i]["tipo"] == 'Flotante'){
				
				$formulario11 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmladeudos11 .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($adeudos11[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario11.'</td>
			   </tr>
			  ';
			  
			  $htmladeudos11 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmladeudos11 .= " 
		 </table>
		 ";
 		

		$data['adeudos']            = $htmladeudos11;



///////////////////////////////////////////////////////////////////////////////////////////////////////////

		$wheresESE12['ESE.idESE']                       = $id;
		$wheresESE12['Pregunta.idGrupoPregunta']         = 12;
		$joinESE12                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$documentos12 = $this->cuestionarios_model->getAllESE($wheresESE12,NULL,$joinESE12);
		
		
	
		
	
		$htmldocumentos12= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($documentos12); $i++) {
		  	   $formulario12 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($documentos12[$i]["tipo"] == 'Numerica'){

				$formulario12 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($documentos12[$i]["tipo"] == 'Texto'){
						
						$formulario12 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($documentos12[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones12['idPregunta'] = $documentos12[$i]["idPregunta"];
			  	    $Opciones12 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones12);
	
			
				   foreach ($Opciones12 as $key => $value) {
			            
					if($Resespuestas[$respuesta] == $value['Respuesta']){
                       $formulario12 .=  htmlentities($value['Respuesta']);
						
					}
				                     
				   }	
			
			  }

			  
			  if($documentos12[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones12['idPregunta'] = $documentos12[$i]["idPregunta"];
			  	  $Opciones12 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones12);  		
				  
				  foreach ($Opciones12 as $key => $value) {
					  $formulario11 .= "<br>".htmlentities($value['Respuesta']);
				       
				       $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($documentos12[$i]["tipo"] == 'Flotante'){
				
				$formulario12 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmldocumentos12 .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($documentos12[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario12.'</td>
			   </tr>
			  ';
			  
			  $htmldocumentos12 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmldocumentos12 .= " 
		 </table>
		 ";
 		

		$data['documentos']            = $htmldocumentos12;



/////////////////////////////////////////////////////////////////////////////////////////////////////////

$wheresESE13['ESE.idESE']                       = $id;
		$wheresESE13['Pregunta.idGrupoPregunta']         = 13;
		$joinESE13                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$refVecinales13 = $this->cuestionarios_model->getAllESE($wheresESE13,NULL,$joinESE13);
		
		
	
		
	
		$htmlrefVecinales13= " 
		<table width='100%' frame='box' border='0'>
		";
		 $contador = $contador;
		  for ($i=0; $i < count($refVecinales13); $i++) {
		  	    $formulario13 = '';
			   $respuesta = 'Respuesta'.$contador;
			   
			  if($refVecinales13[$i]["tipo"] == 'Numerica'){
				
				$formulario13 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  if($refVecinales13[$i]["tipo"] == 'Texto'){
			  
						$formulario13 = htmlentities($Resespuestas[$respuesta]);
			  
			  }
			  
			  if($refVecinales13[$i]["tipo"] == 'Opcion Multiple Unica'){
			 
			  	    $whereOpciones13['idPregunta'] = $refVecinales13[$i]["idPregunta"];
			  	    $Opciones13 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones13);
	
					
				   foreach ($Opciones13 as $key => $value) {
			           
					  if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
                       $formulario13 .=  htmlentities($value['Respuesta']);
					  	
					  }
				                     
				   }	
			
			  }

			  
			  if($refVecinales13[$i]["tipo"] == 'Opcion Multiple Multiple'){
			 
				  
				  $whereOpciones13['idPregunta'] = $refVecinales13[$i]["idPregunta"];
			  	  $Opciones13 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones13);  		
				  
				  foreach ($Opciones13 as $key => $value) {
					  	
					 if($Resespuestas[$respuesta] == $value['Respuesta']){
					 	$formulario13 .= "<br>".htmlentities($value['Respuesta']);
					 }
					  $contador++; 
						$respuesta = 'Respuesta'.$contador;
			       }	
			    	
			  }
			  
             if($refVecinales13[$i]["tipo"] == 'Flotante'){


				$formulario13 = htmlentities($Resespuestas[$respuesta]);
			  }
			  
			  $htmlrefVecinales13 .= '
			   <tr>
			     <td colspan="1" width="40%">'.htmlentities($refVecinales13[$i]["Oracion"]).'</td>
			     <td colspan="2" width="60%">'.$formulario13.'</td>
			   </tr>
			  ';
			  
			  $htmlrefVecinales13 .= '
			   <tr>
			     <td colspan="3"><br<br></td>

			   </tr>
			  ';
			  
			  $contador++;
		  }
 		
	  $htmlrefVecinales13 .= " 
		 </table>
		 ";
 		

		$data['refVecinales']            = $htmlrefVecinales13;








///////////////////////////////////////////////////////////////////////////////////////////////////////


$wheresESE14['ESE.idESE']                       = $id;
		$wheresESE14['Pregunta.idGrupoPregunta']         = 14;
		$joinESE14                  = array(
			                             array(
											  'table' => 'ESE_has_CuestionarioESE',
											  'cond'  => 'ESE_has_CuestionarioESE.idESE = ESE.idESE',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Cuestionario',
											  'cond'  => 'Cuestionario.idCuestionario = ESE_has_CuestionarioESE.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'Pregunta',
											  'cond'  => 'Pregunta.idCuestionario = Cuestionario.idCuestionario',
											  'type'  => 'inner' 
											  ),
										  array(
											  'table' => 'TipoPregunta',
											  'cond'  => 'TipoPregunta.idTipoPregunta = Pregunta.idTipoPregunta',
											  'type'  => 'left' 
											  )
		                                 );


	
		$refdomicilio14 = $this->cuestionarios_model->getAllESE($wheresESE14,NULL,$joinESE14);
		
	
		

 	
			$htmlrefdomicilio14= " 
			<table width='100%' frame='box' border='0'>
			";
			  $contador = $contador;
			  for ($i=0; $i < count($refdomicilio14); $i++) {
	               $formulario14 = '';
				   $respuesta = 'Respuesta'.$contador;
				   
				  if($refdomicilio14[$i]["tipo"] == 'Numerica'){
	
					
					$formulario14 = htmlentities($Resespuestas[$respuesta]);
				  }
				  
				  if($refdomicilio14[$i]["tipo"] == 'Texto'){
				  
							$formulario14 = htmlentities($Resespuestas[$respuesta]);
				  
				  }
				  
				  if($refdomicilio14[$i]["tipo"] == 'Opcion Multiple Unica'){
				 
				  	    $whereOpciones14['idPregunta'] = $refdomicilio14[$i]["idPregunta"];
				  	    $Opciones14 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones14);
		
					   foreach ($Opciones14 as $key => $value) {
				           
						   if($Resespuestas[$respuesta] == $value['idOpcionesRespuesta']){
						   	$formulario14 .=  htmlentities($value['Respuesta']);
						  
						   }
						   
					                     
					   }	
						$formulario14 .= "</select>";
				
				  }
	
				  
				  if($refdomicilio14[$i]["tipo"] == 'Opcion Multiple Multiple'){
				 
					  
					  $whereOpciones14['idPregunta'] = $refdomicilio14[$i]["idPregunta"];
				  	  $Opciones14 = $this->opciones_model->getAllOpcionesRespuesta($whereOpciones14);  		
					  
					  foreach ($Opciones14 as $key => $value) {
						  			
						  if($Resespuestas[$respuesta] == $value['Respuesta']){
						  	 $formulario14 .= "<br>".htmlentities($value['Respuesta']);
						  }	
						  	
						  $contador++; 
						$respuesta = 'Respuesta'.$contador;
				       }	
				    	
				  }
				  
	             if($refdomicilio14[$i]["tipo"] == 'Flotante'){
	
					
					$formulario14 = htmlentities($Resespuestas[$respuesta]);
				  }
				  
				  $htmlrefdomicilio14 .= '
				   <tr>
				     <td colspan="1" width="40%">'.htmlentities($refdomicilio14[$i]["Oracion"]).'</td>
				     <td colspan="2" width="60%">'.$formulario14.'</td>
				   </tr>
				  ';
				  
				  $htmlrefdomicilio14 .= '
				   <tr>
				     <td colspan="3"><br<br></td>
	
				   </tr>
				  ';
				  
				  $contador++;
			  }
	 		
		  $htmlrefdomicilio14 .= " 
			 </table>
			 ";
		 

		$data['refdomicilio']            = $htmlrefdomicilio14;




////////////////////////////////////////////////////////////////////////////////////////////////////////


					
		if($_POST['idTipoEse'] == 2){
			
			$codigo = $this->parser->parse('solicitudes/preguntas_generales_reporte', $data);  
	
			 $nombre = 'estudio_socio_economico_general_reporte.pdf';
			 //die;
		}		
		
		
		if($_POST['idTipoEse'] == 3){		
				
				$codigo = $this->parser->parse('solicitudes/preguntas_directivo_reporte', $data);
				$nombre = 'estudio_socio_economico_directivo_reporte.pdf';
		}		
		
		
		if($_POST['idTipoEse'] == 4){		
				
				$codigo = $this->parser->parse('solicitudes/preguntas_remotas_reporte', $data);
			    $nombre = 'estudio_socio_economico_remoto_reporte.pdf';
		}	
		
		
}else{
	
	if($_POST['idTipoEse'] == 2){		
				
				$codigo = $this->parser->parse('solicitudes/preguntas_generales', $data);
			    $nombre = 'estudio_socio_economico_general_reporte.pdf';
		}		
		
		
		if($_POST['idTipoEse'] == 3){		
				
				$codigo = $this->parser->parse('solicitudes/preguntas_directivo', $data);
				$nombre = 'estudio_socio_economico_directivo_reporte.pdf';
		}		
		
		
		if($_POST['idTipoEse'] == 4){		
				
				$codigo = $this->parser->parse('solicitudes/preguntas_remotas', $data);
			    $nombre = 'estudio_socio_economico_remoto_reporte.pdf';
		}
	
}	

			$codigo = utf8_encode($codigo);
			$dompdf = new DOMPDF();
			$dompdf->load_html($codigo);
			ini_set('memory_limit', '-1');
			$dompdf->render();
			$dompdf->stream($nombre);	
			
		
		
	}

	
	
	
}
// END Searchs controller
/* End of file searchs.php */
/* Location: ./application/controllers/searchs.php */
