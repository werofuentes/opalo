<?php
$config = array(
	'results' => array(
		'text_next' 	=> '&gt;',
		'text_prev' 	=> '&lt;',
		'text_begin' 	=> '&lt;&lt; primera',
		'text_end' 		=> '&uacute;ltima &gt;&gt;',
		'active_class'	=> 'inactivo',
		'page_limit' 	=> 5
	)
);
