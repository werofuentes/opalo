{extends file="structure/main.tpl"}

{block name=content}

      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-inbox"></i>
							<a href="#">Usuarios del Sistema</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>


					</ul><!--.breadcrumb-->

				</div>
				
				<div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}users/formAddUSers">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
				
		
 				</div>
 				
 				
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Usuarios
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>ID USUARIO</th>
											<th>NOMBRE</th>
											<th class="hidden-480">ACTIVO</th>

											<th class="hidden-phone">
												<i class="icon-time bigger-110 hidden-phone"></i>
												ULTIMO ACCESO
											</th>


											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$usuarios key=key item=item}
										 {if $item.use_id neq ''}
											<tr>
												<td>{$item.use_id}</td>
												<td>{$item.use_login}</td>
												<td>{$item.use_active}</td>
												<td>{$item.use_lastaccess}</td>
												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
	
														<a class="green" href="{base_url()}users/formEditUSers/{$item.use_id}">
															<i class="icon-pencil bigger-130"></i>
														</a>
	
														<a class="red" href="{base_url()}users/deleteUsers/{$item.use_id}">
															<i class="icon-trash bigger-130"></i>
														</a>
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																<a href="{base_url()}users/formEditUSers/{$item.use_id}" class="tooltip-success" data-rel="tooltip" title="Edit">
																	<span class="green">
																		<i class="icon-edit bigger-120"></i>
																	</span>
																</a>
															</li>

															<li>
																<a href="{base_url()}users/deleteUsers/{$item.use_id}" class="tooltip-error" data-rel="tooltip" title="Delete">
																	<span class="red">
																		<i class="icon-trash bigger-120"></i>
																	</span>
																</a>
															</li>
														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>

							{$NumPaginas}

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#configuaracion').attr('class','active open');
		$('#configuaracion_usuarios').attr('class','active');
		


</script>
    


{/block}
