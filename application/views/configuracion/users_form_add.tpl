{extends file="structure/main.tpl"}

{block name=content}

<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-inbox"></i>
							<a href="#">Usuarios del Sistema</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						
						<li>
							<a href="#">Nuevo Usuario</a>
						</li>


					</ul><!--.breadcrumb-->

	</div>

	<div class="span7">
	  <div class="row-fluid">
		  <div class="widget-box">
			<div class="widget-header">
				<h4>Nuevo Usuario</h4>
			</div>
			<div class="widget-body">
			   <div class="widget-main">
				  <form class="form-horizontal" id="formUser" name="formUser" action="{base_url()}users/addUsers" method="post"/>
				   <input class="text-input small-input" type="hidden" id="id" name="id" value="{$usuarios.use_id}" /> 

					
						  <div id="error_login" class="control-group">
							<label class="control-label" for="form-field-1">USUARIO</label>
							<div  class="controls">
								<input class="form-field-1" type="text" id="login" name="login"  value="" placeholder="Usuario" />
							    <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>						
						   </div>
						   
						   <div id="error_keys" class="control-group">
							<label class="control-label" for="form-field-1">CONTRASEÑA</label>
							<div class="controls">
								<input class="form-field-1" type="text" id="keys" name="keys" value="" placeholder="Contraseña" />
							    <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>
						   </div>
						   
						    <div id="error_email" class="control-group">
							<label class="control-label" for="form-field-1">CORREO</label>
							<div class="controls">
								<input class="form-field-1" type="text" id="email" name="email" value="" placeholder="Correo">
							    <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>
						   </div>
						   			
						   
						    <div id="error_session" class="control-group">
							<label class="control-label" for="form-field-1">NOMBRE</label>
							<div class="controls">
								<input class="form-field-1" id="session" name="session" value="" type="text" placeholder="Nombre"/>
							    <span id="error_5" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>
						   </div>
						   
						   <div id="error_tipo_usuario" class="control-group">
						      <label class="control-label"  for="form-field-1">TIPO DE USUARIO</label>
						      	<div class="controls">
						      	<select class="form-field-select-1" id="tipo_usuario" name="tipo_usuario" onchange="mostrarEmpresa();mostrarPsicologa();mostrarFreelance();">
						          <option value="" > Elige </option>
								   {foreach from=$types key=key item=item}
                                       	<option value="{$item.typ_id}">{$item.typ_name}</option>
                                   {/foreach}
							    </select>
							    <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
						       </div>
						   </div>
						   
						   <div id="mostrarEmpresa" style="display: none">
						   <div id="error_empresa" class="control-group">
						      <label class="control-label"  for="form-field-1">EMPRESA</label>
						      	<div class="controls">
						      	<select class="form-field-select-1" id="empresa" name="empresa" >
						          <option value="" > Elige </option>
								   {foreach from=$empresa key=key item=item}
                                       	<option value="{$item.idEmpresa}">{$item.Nombre}</option>
                                   {/foreach}
							    </select>
							    <span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 
						       </div>
						   </div>
						   </div>

						    <div id="mostrarPsicologa" style="display: none">
						   <div id="error_psicologa" class="control-group">
						      <label class="control-label"  for="form-field-1">PSICOLOGA</label>
						      	<div class="controls">
						      	<select class="form-field-select-1" id="psicologa" name="psicologa" >
						          <option value="" > Elige </option>
								   {foreach from=$psicologa key=key item=item}
                                       	<option value="{$item.id}">{$item.NombrePsicologa}</option>
                                   {/foreach}
							    </select>
							    <span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span> 
						       </div>
						   </div>
						   </div>


						    <div id="mostrarFreelance" style="display: none">
						   <div id="error_freelance" class="control-group">
						      <label class="control-label"  for="form-field-1">TRABAJADORA SOCIAL</label>
						      	<div class="controls">
						      	<select class="form-field-select-1" id="freelance" name="freelance" >
						          <option value="" > Elige </option>
								   {foreach from=$freelance key=key item=item}
                                       	<option value="{$item.id}">{$item.Nombre}</option>
                                   {/foreach}
							    </select>
							    <span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span> 
						       </div>
						   </div>
						   </div>
						   
						   <div id="error_activo" class="control-group">
						      <label class="control-label"  for="form-field-1">ACTIVO</label>
						      	<div class="controls">
						      	<select class="form-field-select-1" id="activo" name="activo">
						          <option value="" > Elige </option>
						          <option value="1"> SI </option>
						          <option value="0" > NO </option>
							    </select>
							    <span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 
						       </div>
						   </div>
					</form>	
					<div style="margin-left:458px;margin-top:-33px;">
						<a  href="{base_url()}users">
						   <button class="btn btn-app btn-success btn-mini">
						    <i class="icon-remove bigger-160"></i>
						    Cancelar
						   </button>
						</a>									 
					</div>
					
					<div style="margin-left:530px;margin-top:-65px;;">
							    <button class="btn btn-app btn-success btn-mini" onclick="guardarUser();">
								   <i class="icon-save bigger-160"></i>
								    Guardar
								 </button>
					</div>						
				   </div>
			</div>
		</div> 					
	</div>
</div>    
<script type="text/javascript">

function mostrarEmpresa(){
  
  var val = $('#tipo_usuario').val();
  if(val == 7){

  	document.getElementById('mostrarEmpresa').style.display='block';
  }else{

    document.getElementById('mostrarEmpresa').style.display='none';
  }
}


function mostrarPsicologa(){
  
  var val = $('#tipo_usuario').val();
  if(val == 8){

  	document.getElementById('mostrarPsicologa').style.display='block';
  }else{

    document.getElementById('mostrarPsicologa').style.display='none';
  }
}

function mostrarFreelance(){
 var val = $('#tipo_usuario').val();
  if(val == 9){

  	document.getElementById('mostrarFreelance').style.display='block';
  }else{

    document.getElementById('mostrarFreelance').style.display='none';
  }

}

function guardarUser(){
 

  var login = $('#login').val();
  var keys = $('#keys').val();
  var email = $('#email').val();
  var token = $('#token').val();
  var tipo_usuario = $('#tipo_usuario').val();
  var activo = $('#activo').val();
  var session = $('#session').val();



  if(login == ''){
  	$('#error_login').addClass("control-group error");
  	document.getElementById('error_1').style.display='block';
  }else{
   	$('#error_login').removeClass("control-group error");
    $('#error_login').addClass("control-group");
    document.getElementById('error_1').style.display='none';
  }
  
  
  if(keys == ''){
  	$('#error_keys').addClass("control-group error");
  	document.getElementById('error_2').style.display='block';
  }else{
   	$('#error_keys').removeClass("control-group error");
    $('#error_keys').addClass("control-group");
    document.getElementById('error_2').style.display='none';
  }
  
  
  if(email == ''){
  	$('#error_email').addClass("control-group error");
  	document.getElementById('error_3').style.display='block';
  }else{
   	$('#error_email').removeClass("control-group error");
    $('#error_email').addClass("control-group");
    document.getElementById('error_3').style.display='none';
  }
  
 
  
  if(tipo_usuario == ''){
  	$('#error_tipo_usuario').addClass("control-group error");
  	document.getElementById('error_5').style.display='block';
  }else{
   	$('#error_tipo_usuario').removeClass("control-group error");
    $('#error_tipo_usuario').addClass("control-group");
    document.getElementById('error_2').style.display='none';
  }
  
  
  if(activo == ''){
  	$('#error_activo').addClass("control-group error");
  	document.getElementById('error_6').style.display='block';
  }else{
   	$('#error_activo').removeClass("control-group error");
    $('#error_activo').addClass("control-group");
    document.getElementById('error_6').style.display='none';
  }
  
  if(session == ''){
  	$('#error_session').addClass("control-group error");
  	document.getElementById('error_7').style.display='block';
  }else{
   	$('#error_session').removeClass("control-group error");
    $('#error_session').addClass("control-group");
    document.getElementById('error_7').style.display='none';
  }
  
  
  
   if (login != '' && keys != '' && email != ''  && tipo_usuario != '' && activo != '' && session != '' ){
 
        $( "#formUser" ).submit();
   }



}
		$('#configuaracion').attr('class','active open');
		$('#configuaracion_usuarios').attr('class','active');

</script>  
{/block}
