{extends file="structure/main.tpl"}

{block name=content}



      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-inbox"></i>
							<a href="#">Tipos de usuarios</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>


					</ul><!--.breadcrumb-->

				</div>
				
 				
 	
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Tipos de usuarios
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>ID TIPO</th>
											<th>NOMBRE</th>
											<th>DESCRIPCION</th>
                                            <th>ACTIVO</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$types key=key item=item}
										 {if $item.typ_id neq ''}
											<tr>
												<td>{$item.typ_id}</td>
												<td>{$item.typ_name}</td>
												<td>{$item.typ_reference}</td>
												<td>{$item.typ_active}</td>
												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
	
														<a class="green" href="{base_url()}type/formEditType/{$item.typ_id}">
															<i class="icon-pencil bigger-130"></i>
														</a>
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																<a href="{base_url()}type/formEditType/{$item.typ_id}" class="tooltip-success" data-rel="tooltip" title="Edit">
																	<span class="green">
																		<i class="icon-edit bigger-120"></i>
																	</span>
																</a>
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#configuaracion').attr('class','active open');
		$('#configuaracion_tipos').attr('class','active');

</script>
    









{/block}
