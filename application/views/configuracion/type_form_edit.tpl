{extends file="structure/main.tpl"}

{block name=content}
    

<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-inbox"></i>
							<a href="#">Tipo de usuarios</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						
						<li>
							<a href="#">Permisos</a>
						</li>


					</ul><!--.breadcrumb-->

	</div>

	<div class="span7">
	  <div class="row-fluid">
		  <div class="widget-box">
			<div class="widget-header">
				<h4>Permisos</h4>
			</div>
			<div class="widget-body">
			   <div class="widget-main">
				  <form class="form-horizontal" id="formType" name="formType" action="{base_url()}type/editType" method="post"/>
				   <input class="text-input small-input" type="hidden" id="id" name="id" value="{$type.typ_id}" /> 

					
						  <div id="error_login" class="control-group">
							<label class="control-label" for="form-field-1">NOMBRE</label>
							<div  class="controls">
								<input class="form-field-1" type="text" id="name" name="name"  value="{$type.typ_name}" placeholder="Nombre" />
							    <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>						
						   </div>
						   
						   <div id="error_keys" class="control-group">
							<label class="control-label" for="form-field-1">REFERENCIA</label>
							<div class="controls">
								<input class="form-field-1" type="text" id="reference" name="reference" value="{$type.typ_reference}" placeholder="Referencia" />
							    <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>
						   </div>
						   
						   <div id="error_activo" class="control-group">
						      <label class="control-label"  for="form-field-1">ACTIVO</label>
						      	<div class="controls">
						      	<select class="form-field-select-1" id="activo" name="activo">
						          <option value="" > Elige </option>
						          <option value="1" {if $type.typ_active eq 1} selected="selected" {/if}> SI </option>
						          <option value="0"  {if $type.typ_active eq 0} selected="selected" {/if}> NO </option>
							    </select>
							    <span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 
						       </div>
						   </div>
						   <div class="control-group">
						   <label class="control-label">PERMISOS</label>
							   <div class="controls">
								   
								   	 	
							   		
							   		Modulo Catalogos:
								    <label>
										<input type="checkbox" name="per_empresa_primaria"  {if $permisos.per_empresa_primaria eq 1} checked="checked" {/if}/> 
										<span class="lbl">Empresas Primarias</span>
									</label>
									
									<div align="center">
											<input type="checkbox" name="agregar_empresa_primaria"  {if $permisos.agregar_empresa_primaria eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_empresa_primaria"  {if $permisos.editar_empresa_primaria eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_empresa_primaria"  {if $permisos.sololectura_empresa_primaria eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<label>
										<input type="checkbox" name="per_empresa_secundaria" {if $permisos.per_empresa_secundaria eq 1} checked="checked" {/if}/>  
										<span class="lbl">Empresas Secundarias</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_empresa_secundaria"  {if $permisos.agregar_empresa_secundaria eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_empresa_secundaria"  {if $permisos.editar_empresa_secundaria eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_empresa_secundaria"  {if $permisos.sololectura_empresa_secundaria eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<label>
										<input type="checkbox" name="per_certificaciones" {if $permisos.per_certificaciones eq 1} checked="checked" {/if}/>  
										<span class="lbl">Certificaciones</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_certificaciones"  {if $permisos.agregar_certificaciones eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_certificaciones"  {if $permisos.editar_certificaciones eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_certificaciones"  {if $permisos.sololectura_certificaciones eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<!--
									<label>
										<input type="checkbox" name="per_examenes" {if $permisos.per_examenes eq 1} checked="checked" {/if}/>  
										<span class="lbl">Examenes</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_examenes"  {if $permisos.agregar_examenes eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_examenes"  {if $permisos.editar_examenes eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_examenes"  {if $permisos.sololectura_examenes eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>-->
									<label>
										<input type="checkbox" name="per_vendedores" {if $permisos.per_vendedores eq 1} checked="checked" {/if}/>  
										<span class="lbl">Vendedores</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_vendedores"  {if $permisos.agregar_vendedores eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_vendedores"  {if $permisos.editar_vendedores eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_vendedores"  {if $permisos.sololectura_vendedores eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>

									<label>
										<input type="checkbox" name="per_freelance" {if $permisos.per_freelance eq 1} checked="checked" {/if}/>  
										<span class="lbl">Freelance</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_freelance"  {if $permisos.agregar_freelance eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_freelance"  {if $permisos.editar_freelance eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_freelance"  {if $permisos.sololectura_freelance eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>

									<label>
										<input type="checkbox" name="per_psicologa" {if $permisos.per_psicologa eq 1} checked="checked" {/if}/>  
										<span class="lbl">Psicologa</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_psicologa"  {if $permisos.agregar_psicologa eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_psicologa"  {if $permisos.editar_psicologa eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_psicologa"  {if $permisos.sololectura_psicologa eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<label>
										<input type="checkbox" name="per_sucursales" {if $permisos.per_sucursales eq 1} checked="checked" {/if}/>  
										<span class="lbl">Sucursales</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_sucursales"  {if $permisos.agregar_sucursales eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_sucursales"  {if $permisos.editar_sucursales eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_sucursales"  {if $permisos.sololectura_sucursales eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>

									<hr>
									Modulo Administracion:
									<label>
										<input type="checkbox" name="per_cotizacion" {if $permisos.per_cotizacion eq 1} checked="checked" {/if}/>  
										<span class="lbl">Cotización</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_cotizacion"  {if $permisos.agregar_cotizacion eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_cotizacion"  {if $permisos.editar_cotizacion eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_cotizacion"  {if $permisos.sololectura_cotizacion eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<label>
										<input type="checkbox" name="per_evaluacion" {if $permisos.per_evaluacion eq 1} checked="checked" {/if}/>  
										<span class="lbl">Evaluación</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_evaluacion"  {if $permisos.agregar_evaluacion eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_evaluacion"  {if $permisos.editar_evaluacion eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_evaluacion"  {if $permisos.sololectura_evaluacion eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<label>
										<input type="checkbox" name="per_factrec" {if $permisos.per_factrec eq 1} checked="checked" {/if}/>  
										<span class="lbl">Facturación/Recibos</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_factrec"  {if $permisos.agregar_factrec eq 1} checked="checked" {/if}/>
											<span class="lbl">Generar Factura/Recibo</span>

											<!--
											<input type="checkbox" name="editar_factrec"  {if $permisos.editar_factrec eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_factrec"  {if $permisos.sololectura_factrec eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
											-->
									</div>
									<label>
										<input type="checkbox" name="per_cobranza" {if $permisos.per_cobranza eq 1} checked="checked" {/if}/>  
										<span class="lbl">Cobranza</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_cobranza"  {if $permisos.agregar_cobranza eq 1} checked="checked" {/if}/>
											<span class="lbl"> Registrar pagos</span>

											<!--
											<input type="checkbox" name="editar_cobranza"  {if $permisos.editar_cobranza eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_cobranza"  {if $permisos.sololectura_cobranza eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
											-->
									</div>
									<hr>
									Modulo Credencialización:
									<label>
										<input type="checkbox" name="per_generarCredencial" {if $permisos.per_generarCredencial eq 1} checked="checked" {/if}/>  
										<span class="lbl">Generar Credencial</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_generarCredencial"  {if $permisos.agregar_generarCredencial eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

										<!--
											<input type="checkbox" name="editar_generarCredencial"  {if $permisos.editar_generarCredencial eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_generarCredencial"  {if $permisos.sololectura_generarCredencial eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span
										-->
									</div>
									<label>
										<input type="checkbox" name="per_registroMovimientos" {if $permisos.per_registroMovimientos eq 1} checked="checked" {/if}/>  
										<span class="lbl">Registros de movimientos</span>
									</label>
									<div align="center">
											<input type="checkbox" name="agregar_registroMovimientos"  {if $permisos.agregar_registroMovimientos eq 1} checked="checked" {/if}/>
											<span class="lbl">Agregar</span>

											<input type="checkbox" name="editar_registroMovimientos"  {if $permisos.editar_registroMovimientos eq 1} checked="checked" {/if}/>
											<span class="lbl">Editar</span>

											<input type="checkbox" name="sololectura_registroMovimientos"  {if $permisos.sololectura_registroMovimientos eq 1} checked="checked" {/if}/>
											<span class="lbl">Solo Lectura</span>
									</div>
									<hr>
									Modulo Reportes:
									<label>
										<input type="checkbox" name="per_reporte_mov_personal" {if $permisos.per_reporte_mov_personal eq 1} checked="checked" {/if}/>  
										<span class="lbl">Movimientos personal</span>
									</label>
									<label>
										<input type="checkbox" name="per_reporte_estatus_personal" {if $permisos.per_reporte_estatus_personal eq 1} checked="checked" {/if}/>  
										<span class="lbl">Estatus de personal</span>
									</label>
									<label>
										<input type="checkbox" name="per_reporte_pagos" {if $permisos.per_reporte_pagos eq 1} checked="checked" {/if}/>  
										<span class="lbl">Pagos</span>
									</label>
					
									<hr>
									Modulo Configuracion:
									<label>
										<input type="checkbox" name="per_usuarios_sistema" {if $permisos.per_usuarios_sistema eq 1} checked="checked" {/if}/>  
										<span class="lbl">Usuarios del Sistema</span>
									</label>
									<label>
										<input type="checkbox" name="per_perfil_usuario" {if $permisos.per_usuarios_sistema eq 1} checked="checked" {/if}/>  
										<span class="lbl">Perfil de Usuario</span>
									</label>


							   </div>
						   </div>
					</form>	
					
					<div style="margin-left:530px;margin-top:15px;">
							
							    <button class="btn btn-app btn-success btn-mini" onclick="guardarType();">
								   <i class="icon-save bigger-160"></i>
								    Guardar
								 </button>
					</div>						
				   </div>
			</div>
		</div> 					
	</div>
</div>  
<script type="text/javascript">
		$('#configuaracion').attr('class','active open');
		$('#configuaracion_tipos').attr('class','active');
		
		function guardarType(){
		    $( "#formType" ).submit();
		}

</script>
{/block}
