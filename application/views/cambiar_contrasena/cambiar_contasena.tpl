{extends file="structure/main.tpl"}

{block name=content}

<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-cog"></i>
							<a href="#">Personalizar contraseña</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
					</ul><!--.breadcrumb-->
	</div>

	<div class="span7">
	  <div class="row-fluid">
		  <div class="widget-box">
			<div class="widget-header">
				<h4>Personalizar contraseña</h4>
			</div>
			<div class="widget-body">
			   <div class="widget-main">
				   <form class="form-horizontal" id="formUser" name="formUser" action="{base_url()}users/cambiaContrasena" method="post"/>
				   <input class="text-input small-input" type="hidden" id="id" name="id" value="{$usuarios.use_id}" /> 
						   <div id="error_keys" class="control-group">
							<label class="control-label" for="form-field-1">CONTRASEÑA</label>
							<div class="controls">
								<input class="form-field-1" type="text" id="keys" name="keys" value="" placeholder="Contraseña" />
							    <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span>
							</div>
						   </div>
					</form>		
					<br><br>
					<div style="margin-left:458px;margin-top:-33px;">
						<a  href="{base_url()}">
						   <button class="btn btn-app btn-success btn-mini">
						    <i class="icon-remove bigger-160"></i>
						    Cancelar
						   </button>
						</a>									 
					</div>
					<div style="margin-left:530px;margin-top:-64px;">
						<button class="btn btn-app btn-success btn-mini" onclick="guardarUser();">
						   <i class="icon-save bigger-160"></i>
						    Cambiar
						</button>
					</div>			
				   </div>
			</div>
		</div> 					
	</div>
</div>    
<script type="text/javascript">
function guardarUser(){
 


        $( "#formUser" ).submit();


}
</script>  
{/block}
