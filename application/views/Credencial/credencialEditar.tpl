{extends file="structure/main.tpl"}

{block name=content}

{literal}
<style type="text/css">
	
.card {
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    border-radius: 5px; /* 5px rounded corners */
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

/* Add some padding inside the card container */
.container2 {
    padding: 2px 16px;
}



/* Add rounded corners to the top left and the top right corner of the image */
img {
    border-radius: 5px 5px 0 0;
}

</style>
{/literal}

<div class="main-content">
<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
				<i class="icon-credit-card"></i>
					<a href="#">Credencialización</a>

					<span class="divider">
						<i class="icon-angle-right arrow-icon"></i>
					</span>
					<a href="#">Generar Credencial</a>
			</li>
		</ul><!--.breadcrumb-->

</div>
<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
								<table width="100%" border="0">
													<tr>
												      <td colspan="3" align="center">
												      	 <div >
																           <label class="control-label" for="form-field-1">Empresa</label>
																          
																           <input type="text" id="Empresa" name="Empresa" value="{$EmpresasSec.RazonSocial}" readonly>

																          

																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  

												      </td>
												      <td colspan="3" align="center">
												      	 <div >
																           <label class="control-label" for="form-field-1">Evaluado</label>
																           
																           <input type="text" id="Empresa" name="Empresa" value="{$evaluados.Nombre}" readonly>

																           <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  

												      </td>
												    </tr>
												    
												    <tr>
												    	<td colspan="2" align="center">
												    	
												    	</td>
												   

												 
												    	<td colspan="2" align="center">
												    		<table width="30%" border="0">
												    		 <tr>
												    		 	<td >
													    		<div id="CredenciaDiv">
													    		{$htmlCredencial}
													    		</div>
													    		</td>
													    	 </tr>
												    		</table>
												    	</td>
												

												  
												    	<td colspan="2" align="center">
												    	
												    	</td>
												    </tr>
												</table>


							<div class="space"></div> <!-- aqui no-->
							

							  
							
					

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
	
</div> 


<script type="text/javascript">
		$('#credencializacion').attr('class','active open');
		$('#generar').attr('class','active');

		AddFilaCotizacion(1);

		function GuardarCredencial(){
				$( "#formCotizar" ).submit();
		}

		function GenerarCredencial(){
			var idEmpresaSec = $('#idEmpresaSec').val();
			var idEvaluado = $('#idEvaluado').val();



			$.ajax ({
		                url     : base_url + 'credencial/CregarCredencial',
		                type    : 'POST',
		                data    : 'idEmpresaSec='+idEmpresaSec+'&idEvaluado='+idEvaluado,
		                async   : false,
		                cache   : false,
		                success : function(data){
		                   if( 1 < data.length ){
		                        // Mostramos la modal
		                        $('#CredenciaDiv').html(data);



		                   } 
		                }
		            });
		}
	
</script>	

{/block}
