{extends file="structure/main.tpl"}

{block name=content}



      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-credit-card"></i>
							<a href="#">Credencialización</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Generar Credencial</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
				
 				<div id="modal_cancelar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  				<br>
						<div align="center">
						<form name="formCcancelarEval" id="formCcancelarEval" action="{base_url()}credencial/deleteCredencial" method="post" enctype="multipart/form-data"/>
						 <B>¿Quieres eliminar la credencial? </B>
						 <input type="hidden" name="eliminarID" id="eliminarID" value="">

						<p>
						  <button type="button" class="btn btn-primary btn-lg" onclick="cancelEvaluados();">Si</button>
						  <button type="button" data-dismiss="modal" class="btn btn-default btn-lg">No</button>
						</p>

						</form>
						</div>

						<br>
		        </div>


		        <div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}credencial/NuevaCredencial">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
 				</div>

 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Generar credencial
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th># Empleado</th>
											<th>Nombre empleado</th>
											<th>Empresa</th>
											

											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$Credencial key=key item=item}
										 {if $item.idCredencial neq ''}
											<tr>
												<td><font face="Arial">{$item.CodigoEmpleado}</font></td>
												<td><font face="Arial">{$item.Nombre}</font></td>
												<td><font face="Arial">{$item.RazonSocial}</font></td>
								
										
															<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">

														<a class="green" href="{base_url()}credencial/verCredencial/{$item.idCredencial}">
															<i class="icon-caret-down bigger-130"></i>
														</a>

														<a class="green" target="_blank" href="{base_url()}credencial/generarImagenCredencial/{$item.idCredencial}">
															<IMG SRC="{base_url()}/inc/Icono-credencial.png" width="30px" ALT="Factura/Recibo">
														</a>
														<a class="red" href="#" onclick="eliminarCliente({$item.idCredencial});">
															<i class="icon-trash bigger-130"></i>
														</a>
														
	
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																<a class="green" style="cursor:pointer;">
																	<span class="green">
																		<i class="icon-edit bigger-120" onclick="AutorizarCotizacion({$item.id});"></i>
																	</span>
																</a>

														
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
      


<script type="text/javascript">
		$('#credencializacion').attr('class','active open');
		$('#generar').attr('class','active');


			function eliminarCliente(eliminarID){

			 $('#modal_cancelar').modal('show');
			 $('#eliminarID').val(eliminarID);
		}

		function cancelEvaluados(){

					$( "#formCcancelarEval" ).submit();

		}


</script>
    









{/block}
