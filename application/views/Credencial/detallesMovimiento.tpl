<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		  
		
		
		<!-- Jquery base_url -->
  		<script type="text/javascript">var base_url = '{base_url()}';</script>
  		
		<!--basic styles-->

		<link href="{base_url()}inc/css_apro/bootstrap.min.css" rel="stylesheet" />
		<link href="{base_url()}inc/css_apro/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/font-awesome.min.css" />
		

        <link rel="stylesheet" href="{base_url()}inc/css_apro/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/chosen.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/datepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/daterangepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/colorpicker.css" /> 
		

		<!--[if IE 7]>
		  <link rel="stylesheet" href="{base_url()}inc/css_apro/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace-responsive.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace-skins.min.css" />
		
		
		<!-- BOTON CSS -->
		<link rel="stylesheet" href="{base_url()}inc/css/css/invalid.css" />
		

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="{base_url()}inc/css_apro/ace-ie.min.css" />
		<![endif]-->
		

		<!-- Load other css -->
		{$css|default:''}
		
		
		
		<!--                       Javascripts                       -->
  	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  	    
  	    <script type="text/javascript">
			window.jQuery || document.write("<script src='{base_url()}inc/js_apro/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>
  		
  		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='{base_url()}inc/js_apro/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		
		<script src="{base_url()}inc/js_apro/bootstrap.min.js"></script>
	  	<script src="{base_url()}inc/js_apro/ace-elements.min.js"></script>
		<script src="{base_url()}inc/js_apro/ace.min.js"></script>
		
		<script src="{base_url()}inc/js_apro/jquery.dataTables.min.js"></script>
		<script src="{base_url()}inc/js_apro/jquery.dataTables.bootstrap.js"></script>
			

		<script src="{base_url()}inc/js_apro/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="{base_url()}inc/js_apro/jquery.maskedinput.min.js"></script>
		
		<link rel="stylesheet" href="{base_url()}inc/jquery/development-bundle/themes/base/jquery.ui.all.css">
		<script src="{base_url()}inc/jquery/development-bundle/ui/jquery.ui.core.js"></script>
		<script src="{base_url()}inc/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
		<script src="{base_url()}inc/jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
      	

	    <script src="{base_url()}inc/js/general.js"></script>


	      <link rel="stylesheet" href="{base_url()}inc/alerti/alertify.core.css" />
		<link rel="stylesheet" href="{base_url()}inc/alerti/alertify.default.css" id="toggleCSS" />
		<script src="{base_url()}inc/alerti/alertify.min.js"></script>
	    

	
	    {$js|default:''}	
	    {$jsE|default:''}	
   </head>

{block name=content}

<div>
<form id="formMovimientos" name="formMovimientos" action="{base_url()}credencialqr/guardarMovimiento" method="post">	
	<input class="span12" id="idEmpleado"  name="idEmpleado" value="{$idEvaluado}" type="hidden"/>
 	<input class="span12" id="idRegistro"  name="idRegistro" value="{$idRegistro}" type="hidden">

				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >
										<ul class="nav nav-tabs" id="myTab">
											<li class="active" id="empleado-tab">
												<a data-toggle="tab" href="#investigador">
													Registro movimiento
												</a>
											</li>

									
											
											
										</ul>

										<div class="tab-content">
											<div id="investigador" class="tab-pane in active">
												
												<table width="100%" border="0">
												  <tr>
												      <td colspan="1">
												          <div id="error_codigo" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Codigo empleado</label>
																           <input class="span8" id="CodigoEmpleado"  name="CodigoEmpleado" value="{$Codigo}" type="text" placeholder="Codigo" style='text-transform:uppercase;' readonly>
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>

												      <td colspan="1">
												          <div id="error_codigo" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Vigencia credencial</label>
																           <input class="span3" id="vigencia"  name="vigencia" value="{$FechaExpira}" type="text" placeholder="Codigo" style='text-transform:uppercase;' readonly>
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>

												      <td colspan="1">
												          <div id="error_codigo" class="control-group">
																  <div >
																           
																        	{$foto}   
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Nombre</label>
																           <input class="span12" id="Nombre" value="{$Empleado}"  name="Nombre" type="text" placeholder="Nombre" style='text-transform:uppercase;' readonly>
																           <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 

												  <tr>
												      <td colspan="3">
												          <div id="error_empresa" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa</label>
																           <input class="span12" id="Empresa" value="{$Empresa}"  name="Empresa" type="text" placeholder="Empresa" style='text-transform:uppercase;' readonly>
																           <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												  	<td colspan="3">
												  		
												  		 <div id="error_comentarios" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios</label>
																          <textarea id="comentario" name="comentario" class="span12" placeholder="" ></textarea>
																           
																    </div>  
														   </div> 
												  	</td>
												  </tr> 
												  
												  
												   
												  
										
												</table>
	                                                   
											</div>

							
											
							
	
										</div>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
							

							
					

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
</form>


	
	<div style="margin-left:157px;margin-top:-65px;">
	   <button class="btn btn-app btn-success btn-mini" onclick="guardarMovimiento();">
	    <i class="icon-save bigger-160"></i>
	    Guardar
	   </button>									 
	</div>	
     
</div> 



<script type="text/javascript">

$("#CodigoEmpleado").on('keyup', function (e) {
    if (e.keyCode == 13) {
       	
       	var CodigoEmpleado = $('#CodigoEmpleado').val();

    	$.ajax ({
		                url     : base_url + 'credencial/buscarEmpleado',
		                type    : 'POST',
		                data    : 'CodigoEmpleado='+CodigoEmpleado,
		                async   : false,
		                cache   : false,
		                dataType	: 'json',
		                success : function(data){
		              
		                        // Mostramos la modal
		                 		
		                 		console.log(data);
		                 		$('#Nombre').val(data['Nombre']);
		                 		$('#idEmpleado').val(data['idEmpleado']);
		                 		$('#fecha').val(data['Hora']);


		                 
		                }
		            });




    }
});

function guardarMovimiento(){
   	

        $( "#formMovimientos" ).submit();

}


		$('#credencializacion').attr('class','active open');
		$('#movimientos').attr('class','active');

</script> 

{/block}