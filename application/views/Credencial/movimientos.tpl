{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
       <div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-credit-card"></i>
							<a href="#">Credencialización</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Generar Credencial</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
	
					     
	 <span id="error_general" style="display: none;color:red;margin-left:20px;" class="help-inline"></span> 	   

 <form id="formMovimientos" name="formMovimientos" action="{base_url()}credencial/GuardarMovimiento" method="post"/>				 <input class="span12" id="idEmpleado"  name="idEmpleado" value="" type="hidden">

				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >
										<ul class="nav nav-tabs" id="myTab">
											<li class="active" id="empleado-tab">
												<a data-toggle="tab" href="#investigador">
													Registro movimiento
												</a>
											</li>

									
											
											
										</ul>

										<div class="tab-content">
											<div id="investigador" class="tab-pane in active">
												
												<table width="100%" border="0">
												  <tr>
												      <td colspan="3">
												          <div id="error_codigo" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Codigo empleado</label>
																           <input class="span3" id="CodigoEmpleado"  name="CodigoEmpleado" value="" type="text" placeholder="Codigo" style='text-transform:uppercase;'>
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Nombre</label>
																           <input class="span12" id="Nombre" value=""  name="Nombre" type="text" placeholder="Nombre" style='text-transform:uppercase;' readonly>
																           <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 

												  <tr>
												      <td colspan="3">
												          <div id="error_empresa" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa</label>
																           <input class="span12" id="Empresa" value=""  name="Empresa" type="text" placeholder="Empresa" style='text-transform:uppercase;'>
																           <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												  	<td colspan="3">
												  		
												  		 <div id="error_comentarios" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios</label>
																          <textarea id="comentario" name="comentario" class="span12" placeholder="" ></textarea>
																           
																    </div>  
														   </div> 
												  	</td>
												  </tr> 
												  
												  
												   
												  
										
												</table>
	                                                   
											</div>

							
											
							
	
										</div>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
							

							
					

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
</form>

	<div style="margin-left:87px;margin-top:-65px;">
		<a  href="{base_url()}credencial/movimientos">
		   <button class="btn btn-app btn-success btn-mini">
		    <i class="icon-remove bigger-160"></i>
		    Cancelar
		   </button>
		</a>									 
	</div>	
	
	<div style="margin-left:157px;margin-top:-65px;">
	   <button class="btn btn-app btn-success btn-mini" onclick="guardarMovimiento();">
	    <i class="icon-save bigger-160"></i>
	    Guardar
	   </button>									 
	</div>	
     
</div> 


<script type="text/javascript">

$("#CodigoEmpleado").on('keyup', function (e) {
    if (e.keyCode == 13) {
       	
       	var CodigoEmpleado = $('#CodigoEmpleado').val();

    	$.ajax ({
		                url     : base_url + 'credencial/buscarEmpleado',
		                type    : 'POST',
		                data    : 'CodigoEmpleado='+CodigoEmpleado,
		                async   : false,
		                cache   : false,
		                dataType	: 'json',
		                success : function(data){
		              
		                        // Mostramos la modal
		                 		
		                 		console.log(data);
		                 		$('#Nombre').val(data['Nombre']);
		                 		$('#idEmpleado').val(data['idEmpleado']);
		                 		$('#fecha').val(data['Hora']);


		                 
		                }
		            });




    }
});

function guardarMovimiento(){
   	

        $( "#formMovimientos" ).submit();

}


		$('#credencializacion').attr('class','active open');
		$('#movimientos').attr('class','active');

</script> 
{/block}
