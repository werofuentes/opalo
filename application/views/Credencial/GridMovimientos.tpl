{extends file="structure/main.tpl"}

{block name=content}



      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-credit-card"></i>
							<a href="#">Credencialización</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Generar Credencial</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
				
 	


				{if $smarty.session.PERMISOS.agregar_registroMovimientos eq 1}
		        <div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}credencial/registrarMovimientos">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
 				</div>
 				{/if}

 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Movimientos
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Empresa</th>
											<th>Empleado</th>
											<th>Fecha</th>
											

											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$movimientos key=key item=item}
										 {if $item.id neq ''}
											<tr>
												<td><font face="Arial">{$item.Empresa}</font></td>
												<td><font face="Arial">{$item.Nombre}</font></td>
												<td><font face="Arial">{$item.Fecha}</font></td>
								
										
															<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">

														<a class="green" href="{base_url()}credencial/verMovimiento/{$item.id}">
															<i class="icon-caret-down bigger-130"></i>
														</a>
														
	
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																<a class="green" style="cursor:pointer;">
																	<span class="green">
																		<i class="icon-edit bigger-120" onclick="AutorizarCotizacion({$item.id});"></i>
																	</span>
																</a>

														
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
      


<script type="text/javascript">
		$('#credencializacion').attr('class','active open');
		$('#movimientos').attr('class','active');





</script>
    









{/block}
