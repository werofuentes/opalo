<div class="sidebar" id="sidebar">
				<ul class="nav nav-list">


					{if $smarty.session.PERMISOS.per_empresa_primaria eq 1 || $smarty.session.PERMISOS.per_empresa_secundaria eq 1 || $smarty.session.PERMISOS.per_certificaciones eq 1 || $smarty.session.PERMISOS.per_examenes eq 1 || $smarty.session.PERMISOS.per_vendedores eq 1 || $smarty.session.PERMISOS.per_sucursales eq 1}
					<li id="catalogos">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-book"></i>
							<span class="menu-text"> Catalogos </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">

						  <!--
							<li id="empresasPrimarias">
								<a  href="{base_url()}catalogos/primarias">
									<i class="icon-double-angle-right"></i>
									Empresas Primarias
								</a>
							</li>
							
							<li id="empresasSecundarias">
								<a  href="{base_url()}catalogos/secundaria">
									<i class="icon-double-angle-right"></i>
									Empresas Secundarias
								</a>
							</li>
							-->

							{if $smarty.session.PERMISOS.per_empresa_primaria eq 1}
							<li id="empresasSecundarias">
								<a  href="{base_url()}catalogos/secundaria">
									<i class="icon-double-angle-right"></i>
									Empresas Primarias
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_empresa_secundaria eq 1}
							<li id="empresasPrimarias">
								<a  href="{base_url()}catalogos/primarias">
									<i class="icon-double-angle-right"></i>
									Empresas Secundarias
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_certificaciones eq 1}
							<li id="certificacion">
								<a  href="{base_url()}catalogos/certificaciones">
									<i class="icon-double-angle-right"></i>
									Certificaciones
								</a>
							</li>
							{/if}



							{if $smarty.session.PERMISOS.per_examenes eq 1}
							<!--<li id="servicio">
								<a  href="{base_url()}catalogos/servicios">
									<i class="icon-double-angle-right"></i>
									Examenes
								</a>
							</li>-->
							{/if}

							{if $smarty.session.PERMISOS.per_vendedores eq 1}
							<li id="vendedores">
								<a  href="{base_url()}catalogos/vendedores">
									<i class="icon-double-angle-right"></i>
									Vendedores
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_freelance eq 1}
							<li id="freelance">
								<a  href="{base_url()}catalogos/freelance">
									<i class="icon-double-angle-right"></i>
									Trabajadora social
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_psicologa eq 1}
							<li id="psicologa">
								<a  href="{base_url()}catalogos/psicologas">
									<i class="icon-double-angle-right"></i>
									Psicologas(os)
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_sucursales eq 1}
							<li id="sucursales">
								<a  href="{base_url()}catalogos/sucursales">
									<i class="icon-double-angle-right"></i>
									Sucursales
								</a>
							</li>
							{/if}
						</ul>
					</li>
					{/if}

					{if $smarty.session.PERMISOS.per_cotizacion eq 1 ||  $smarty.session.PERMISOS.per_evaluacion eq 1 || $smarty.session.PERMISOS.per_factrec eq 1 || $smarty.session.PERMISOS.per_cobranza eq 1 }
					<li id="administracion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-briefcase"></i>
							<span class="menu-text"> Administracion </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">


							{if $smarty.session.PERMISOS.per_cotizacion eq 1}
							<li id="cotizacion">
								<a  href="{base_url()}documentos/cotizacion">
									<i class="icon-double-angle-right"></i>
									Cotización
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_evaluacion eq 1}
							<li id="evaluaciones">
								<a  href="{base_url()}documentos/evaluacion">
									<i class="icon-double-angle-right"></i>
									Evaluación
								</a>
							</li>
							{/if}
							

							{if $smarty.session.PERMISOS.per_factrec eq 1}
							<li id="recibos">
								<a  href="{base_url()}documentos/Recibos">
									<i class="icon-double-angle-right"></i>
									Recibos
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_factrec eq 1}
							<li id="facturacion">
								<a  href="{base_url()}documentos/facturacionRecibos">
									<i class="icon-double-angle-right"></i>
									Facturación
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_cobranza eq 1}
							<li id="cobranza">
								<a  href="{base_url()}documentos/cobranza">
									<i class="icon-double-angle-right"></i>
									Cobranza
								</a>
							</li>
							{/if}
						</ul>
					</li>
					{/if}



					<li id="credencializacion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-credit-card"></i>
							<span class="menu-text"> Credencialización </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">

							<li id="generar">
								<a  href="{base_url()}credencial/generar">
									<i class="icon-double-angle-right"></i>
									Generar Credencial
								</a>
							</li>
								
							<li id="movimientos">
								<a  href="{base_url()}credencial/movimientos">
									<i class="icon-double-angle-right"></i>
									Registros de movimientos
								</a>
							</li>
						</ul>
					</li>

	
					
                {if $smarty.session.PERMISOS.per_reporte_mov_personal eq 1 ||  $smarty.session.PERMISOS.per_reporte_estatus_personal eq 1 ||  $smarty.session.PERMISOS.per_reporte_pagos eq 1 ||  $smarty.session.PERMISOS.per_reporte_pendientes_facturar eq 1 || $smarty.session.PERMISOS.per_reporte_pendientes_cobrar eq 1}

					<li id="reportes">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-bar-chart"></i>
							<span class="menu-text"> Reportes </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">
							
							{if $smarty.session.PERMISOS.per_reporte_estatus_personal eq 1}
							<li id="reportes_estatus_personal">
								<a  href="{base_url()}reporte/EstatusPersonal">
									<i class="icon-double-angle-right"></i>
									Estatus de personal
								</a>
							</li>
							{/if}
							{if $smarty.session.PERMISOS.per_reporte_pagos eq 1}
							<li id="reportes_pagos">
								<a  href="{base_url()}reporte/Pagos">
									<i class="icon-double-angle-right"></i>
									Cobranza
								</a>
							</li>
							{/if}

							<li id="reportes_freelance">
								<a  href="{base_url()}reporte/Freelance">
									<i class="icon-double-angle-right"></i>
									<!--Trabajadora social-->
									ESE realizados
								</a>
							</li>

							<li id="reportes_psicologa">
								<a  href="{base_url()}reporte/Psicologa">
									<i class="icon-double-angle-right"></i>
									<!--Psicologa-->
									Evaluaciones realizadas
								</a>
							</li>


							<li id="reportes_pendientes_evaluados">
								<a  href="{base_url()}reporte/pendientesEvaluados">
									<i class="icon-double-angle-right"></i>
									Evaluados pendientes de facturar
								</a>
							</li>

							<li id="reportes_evaluaciones_realizadas">
								<a  href="{base_url()}reporte/evaluacionesRealizadas">
									<i class="icon-double-angle-right"></i>
									Resumen de Evaluaciones 
								</a>
							</li>

							{if $smarty.session.PERMISOS.per_reporte_mov_personal eq 1}
							<li id="reportes_movimientos_personal">
								<a  href="{base_url()}reporte/Movimientos">
									<i class="icon-double-angle-right"></i>
									Movimientos personal
								</a>
							</li>
							{/if}

							<!--
							{if $smarty.session.PERMISOS.per_reporte_pendientes_facturar eq 1}
							<li id="reportes_pendiente_facturar">
								<a  href="{base_url()}reporte/FacturasRecibos">
									<i class="icon-double-angle-right"></i>
									Facturas/Recibos
								</a>
							</li>
							{/if}
							-->
							
						</ul>
					</li>
    					{/if}
                   
                  	 {if $smarty.session.PERMISOS.per_usuarios_sistema eq 1 ||  $smarty.session.PERMISOS.per_perfil_usuario eq 1}

					<li id="configuaracion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-inbox"></i>
							<span class="menu-text"> Configuracion </span>

							<b class="arrow icon-angle-down"></b>
						</a>
                        
						<ul class="submenu">

							{if $smarty.session.PERMISOS.per_usuarios_sistema eq 1}
							<li id="configuaracion_usuarios">
								<a  href="{base_url()}users">
									<i class="icon-double-angle-right"></i>
									Usuarios del Sistema
								</a>
							</li>
							{/if}

							{if $smarty.session.PERMISOS.per_perfil_usuario eq 1}

								<li id="configuaracion_tipos">
									<a  href="{base_url()}type">
										<i class="icon-double-angle-right"></i>
										Perfil de Usuario
									</a>
								</li>
							{/if}

						</ul>
					</li>

					{/if}

				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>
