		<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
			
			<h1 id="sidebar-title"><a href="#">Simpla Admin</a></h1>
		  
			<!-- Logo (221px wide) -->
			<a href="#"><img id="logo" src="{base_url()}inc/images/Huesped.png" alt="Casas y Terrenos Huesped" /></a>
		  
			<!-- Sidebar Profile links -->
			<div id="profile-links">
				Hola,  <a href="#" title="Edita tu perfil">{$smarty.session.SEUS.use_token}<!--{$smarty.session.users_huesped.token|substr:0}--></a><!--, you have <a href="#messages" rel="modal" title="3 Messages">3 Messages</a>--><br />
				<br />
				<a href="http://www.casasyterrenos.com" target="_blank" title="Ir a casasyterrenos.com">Ir al sitio</a> | <a href="{base_url()}login/logout" title="Cerrar Sesión">Cerrar Sesión</a><!--<a href="" title="Cerrar Sesión">Cerrar Sesión</a>-->
			</div>        
			
			<ul id="main-nav">  <!-- Accordion Menu -->
				
				<li>
				    <!---nav-top-item current--->
					<a href="#" class="nav-top-item" id="Config"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						Configuracion
					</a>
					<ul id="configList">
					<!--class="current"-->
					     {if $smarty.session.PERMISOS.per_mod_users eq 1}<li><a  id="1"  href="{base_url()}users">Usuarios</a></li>{/if}
					     {if $smarty.session.PERMISOS.per_mod_guide eq 1}<li><a id="6" href="{base_url()}guides">Guías</a></li>{/if}
					     {if $smarty.session.PERMISOS.per_mod_coins eq 1}<li ><a id="2" href="{base_url()}coins">Monedas</a></li>{/if}
					     
					{if $smarty.session.PERMISOS.per_typ_id eq 1}
					     {if $smarty.session.PERMISOS.per_mod_type eq 1}
					     	<li><a   id="3" href="{base_url()}type">Tipos</a></li>
					     {/if}
					 {/if}    
					     
					     
					     
					</ul>
				    <a href="#" class="nav-top-item" id="Categ"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						Categorias
					</a>
				    <ul id="catList">
				        {if $smarty.session.PERMISOS.per_mod_categories eq 1}<li><a id="4" href="{base_url()}categories">Categorias</a></li>{/if}
				        {if $smarty.session.PERMISOS.per_mod_subcategories eq 1}<li><a id="5" href="{base_url()}subcategories">Subcategorías</a></li>{/if}
				    </ul>
					<a href="#" class="nav-top-item " id="gui"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						Guia
					</a>
	                <ul id="guiaList">
	                    <!-- {if $smarty.session.PERMISOS.per_mod_accounts eq 1}<li><a id="13" href="{base_url()}accounts">Cuentas</a></li>{/if} -->
	                    <!-- {if $smarty.session.PERMISOS.per_mod_guide eq 1}<li><a id="6" href="{base_url()}guides">Guías</a></li>{/if} -->
	                    {if $smarty.session.PERMISOS.per_mod_galeriageneral eq 1}<li><a id="16" href="{base_url()}galeria_general">Galeria General</a></li>{/if}
	                    <!-- {if $smarty.session.PERMISOS.per_mod_article eq 1}<li><a id="14" href="{base_url()}article">Articulo</a></li>{/if} -->
	                    {if $smarty.session.PERMISOS.per_mod_objects eq 1}<li><a id="7" href="{base_url()}objects">Objetos</a></li>{/if}
	                    {if $smarty.session.PERMISOS.per_mod_ads eq 1}<li><a id="8" href="{base_url()}ads">Tipo de Viaje</a></li>{/if}
	                    {if $smarty.session.PERMISOS.per_mod_estimates eq 1}<li><a id="9" href="{base_url()}estimates">Estimacion</a></li>{/if}
	                    <!-- {if $smarty.session.PERMISOS.per_mod_interest eq 1}<li><a id="10" href="{base_url()}interest">Intereses</a></li>{/if} -->
	                    {if $smarty.session.PERMISOS.per_mod_proposals eq 1}<li><a id="11" href="{base_url()}proposals">Propuestas</a></li>{/if}
	                    {if $smarty.session.PERMISOS.per_mod_states eq 1}<li><a id="12" href="{base_url()}estados">Estados</a></li>{/if}
	                    {if $smarty.session.PERMISOS.per_mod_baner eq 1}<li><a id="15" href="{base_url()}baner">Baners</a></li>{/if}
	                    {if $smarty.session.PERMISOS.per_modo_qhoy eq 1}<li><a id="17" href="{base_url()}que_hay_hoy">Que hay hoy</a></li>{/if}
	                
	                
	                
	                
	                </ul>
	                
	                {if $smarty.session.PERMISOS.per_typ_id eq 1}     
					<a href="#" class="nav-top-item" id="Oflines"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						Offline Contenido
					</a>
					<ul id="Ofline">
						{if $smarty.session.PERMISOS.per_mod_contenido eq 1}<li><li><a id="14" href="{base_url()}article">Contenido Offline</a></li>{/if}
					</ul>
					 {/if} 
					
				</li>
				
				      
				
			</ul> <!-- End #main-nav -->
			
			<div id="messages" style="display: none"> <!-- Messages are shown when a link with these attributes are clicked: href="#messages" rel="modal"  -->
				
				<h3>3 Messages</h3>
			 
				<p>
					<strong>17th May 2009</strong> by Admin<br />
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
					<small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
				</p>
			 
				<p>
					<strong>2nd May 2009</strong> by Jane Doe<br />
					Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.
					<small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
				</p>
			 
				<p>
					<strong>25th April 2009</strong> by Admin<br />
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue.
					<small><a href="#" class="remove-link" title="Remove message">Remove</a></small>
				</p>
				
				<form action="" method="post">
					
					<h4>New Message</h4>
					
					<fieldset>
						<textarea class="textarea" name="textfield" cols="79" rows="5"></textarea>
					</fieldset>
					
					<fieldset>
					
						<select name="dropdown" class="small-input">
							<option value="option1">Send to...</option>
							<option value="option2">Everyone</option>
							<option value="option3">Admin</option>
							<option value="option4">Jane Doe</option>
						</select>
						
						<input class="button" type="submit" value="Send" />
						
					</fieldset>
					
				</form>
				
			</div> <!-- End #messages -->
			
		</div></div> <!-- End #sidebar -->