<div class="sidebar" id="sidebar">
				<ul class="nav nav-list">
				  {if $smarty.session.PERMISOS.per_mod_ingresar_solicitudes eq 1  || $smarty.session.PERMISOS.per_typ_id eq 2    || $smarty.session.PERMISOS.per_typ_id eq 6 }
					<li id="solicitudesIngresar" >
						<a href="{base_url()}solicitudes">
							<i class="icon-edit"></i>
							<span class="menu-text"> Ingresar Solcitud ESE </span>
						</a>
					</li>
                   {/if}
                   {if $smarty.session.PERMISOS.per_mod_solicitudes eq 1  || $smarty.session.PERMISOS.per_typ_id eq 2   || $smarty.session.PERMISOS.per_typ_id eq 3    || $smarty.session.PERMISOS.per_typ_id eq 4    || $smarty.session.PERMISOS.per_typ_id eq 5    || $smarty.session.PERMISOS.per_typ_id eq 6}
					<li id="solicitudesEse">
					  <a href="{base_url()}solicitudes/solicitudesESE">
							<i class="icon-book"></i>
							<span class="menu-text"> Solicitudes ESE </span>
						</a>
					</li>
					{/if}
					
                   {if $smarty.session.PERMISOS.per_mod_solicitudes eq 1  || $smarty.session.PERMISOS.per_typ_id eq 2   || $smarty.session.PERMISOS.per_typ_id eq 3   || $smarty.session.PERMISOS.per_typ_id eq 6}

					<li id="reportes">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-bar-chart"></i>
							<span class="menu-text"> Reportes </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						<ul class="submenu">
							<li id="reportes_investigador">
								<a  href="{base_url()}reporte">
									<i class="icon-double-angle-right"></i>
									Reportes ESE
								</a>
							</li>
							<li id="reportes_investigador">
								<a  href="{base_url()}reporte">
									<i class="icon-double-angle-right"></i>
									Control General
								</a>
							</li>
							<li id="reportes_investigador">
								<a  href="{base_url()}reporte">
									<i class="icon-double-angle-right"></i>
									Control Particular
								</a>
							</li>
							<li id="reportes_investigador">
								<a  href="{base_url()}reporte">
									<i class="icon-double-angle-right"></i>
									Control Semanal
								</a>
							</li>
							<!--<li id="configuaracion_usuarios2">
								<a  href="#">
									<i class="icon-double-angle-right"></i>
									Control Facturacion ESE
								</a>
							</li>-->
							
						</ul>
					</li>
                    {/if}
					
				  {if $smarty.session.PERMISOS.per_mod_asignar eq 1  || $smarty.session.PERMISOS.per_typ_id eq 2   || $smarty.session.PERMISOS.per_typ_id eq 3}
					<li id="resource">
						<a href="{base_url()}resource">
							<i class="icon-download-alt"></i>
							<span class="menu-text"> Asignar Recurso </span>
						</a>
					</li>
                   {/if}
                   {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2 || $smarty.session.PERMISOS.per_typ_id eq 3}
					<li id="empleados">
						<a href="{base_url()}employees">
							<i class="icon-group"></i>
							<span class="menu-text"> Empleados </span>
						</a>
					</li>
                    {/if}
                   
                   
                   
                   <li id="ayuda">
						<a href="{base_url()}ayuda">
							<i class="icon-comment-alt"></i>
							<span class="menu-text"> Ayuda </span>
						</a>
					</li> 
                    {if $smarty.session.PERMISOS.per_typ_id eq 1}
					<li id="configuaracion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-inbox"></i>
							<span class="menu-text"> Configuracion </span>

							<b class="arrow icon-angle-down"></b>
						</a>
                        
						<ul class="submenu">
							{if $smarty.session.PERMISOS.per_mod_users eq 1}
							<li id="configuaracion_usuarios">
								<a  href="{base_url()}users">
									<i class="icon-double-angle-right"></i>
									Usuarios del Sistema
								</a>
							</li>
							{/if}
							{if $smarty.session.PERMISOS.per_typ_id eq 1}
							  {if $smarty.session.PERMISOS.per_mod_type eq 1}
								<li id="configuaracion_tipos">
									<a  href="{base_url()}type">
										<i class="icon-double-angle-right"></i>
										Perfil de Usuario
									</a>
								</li>
								{/if}
							{/if}
						</ul>
					</li>
                     {/if}
				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>
