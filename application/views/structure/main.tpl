<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<head>		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		  
		
		
		<!-- Jquery base_url -->
  		<script type="text/javascript">var base_url = '{base_url()}';</script>
  		
		<!--basic styles-->

		<link href="{base_url()}inc/css_apro/bootstrap.min.css" rel="stylesheet" />
		<link href="{base_url()}inc/css_apro/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/font-awesome.min.css" />
		

        <link rel="stylesheet" href="{base_url()}inc/css_apro/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/chosen.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/datepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/daterangepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/colorpicker.css" /> 
		

		<!--[if IE 7]>
		  <link rel="stylesheet" href="{base_url()}inc/css_apro/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace-responsive.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace-skins.min.css" />
		
		
		<!-- BOTON CSS -->
		<link rel="stylesheet" href="{base_url()}inc/css/css/invalid.css" />
		

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="{base_url()}inc/css_apro/ace-ie.min.css" />
		<![endif]-->
		

		<!-- Load other css -->
		{$css|default:''}
		

		
		
		<!--                       Javascripts                       -->
  	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
  	    
  	    <script type="text/javascript">
			window.jQuery || document.write("<script src='{base_url()}inc/js_apro/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>
  		
  		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='{base_url()}inc/js_apro/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		
		<script src="{base_url()}inc/js_apro/bootstrap.min.js"></script>
	  	<script src="{base_url()}inc/js_apro/ace-elements.min.js"></script>
		<script src="{base_url()}inc/js_apro/ace.min.js"></script>
		
		<script src="{base_url()}inc/js_apro/jquery.dataTables.min.js"></script>
		<script src="{base_url()}inc/js_apro/jquery.dataTables.bootstrap.js"></script>
			

		<script src="{base_url()}inc/js_apro/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="{base_url()}inc/js_apro/jquery.maskedinput.min.js"></script>
		
		<link rel="stylesheet" href="{base_url()}inc/jquery/development-bundle/themes/base/jquery.ui.all.css">
		<script src="{base_url()}inc/jquery/development-bundle/ui/jquery.ui.core.js"></script>
		<script src="{base_url()}inc/jquery/development-bundle/ui/jquery.ui.widget.js"></script>
		<script src="{base_url()}inc/jquery/development-bundle/ui/jquery.ui.datepicker.js"></script>
      	

		<script type="text/javascript" src="{base_url()}inc/js/jquery_domsearch.js"></script>
	    <script src="{base_url()}inc/js/general.js"></script>


	      <link rel="stylesheet" href="{base_url()}inc/alerti/alertify.core.css" />
		<link rel="stylesheet" href="{base_url()}inc/alerti/alertify.default.css" id="toggleCSS" />
		<script src="{base_url()}inc/alerti/alertify.min.js"></script>
	    

	
	    {$js|default:''}	
	    {$jsE|default:''}	
   </head>
	<body>
	
	
	<div class="navbar">
			<div class="navbar-inner">
				<div class="container-fluid">
					<a href="#" class="brand">
						<small>
							
						    <IMG SRC="{base_url()}/inc/logo.png" WIDTH=266 HEIGHT=180 ALT="DIMAC">
						</small>
					</a><!--/.brand-->

					<ul class="nav ace-nav pull-right">

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<span class="user-info">
									<small>Bienvenido,</small>
									{{$smarty.session.SEUS.use_session}|substr:0}
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
								<li class="divider"></li>

								<li>
									<a href="{base_url()}login/logout" title="Cerrar Sesión">
										<i class="icon-off"></i>
										Cerrar Sesion
									</a>
								</li>
								<li>
									<a href="{base_url()}users/contrasenaV" title="Personalizar Contraseña">
										<i class="icon-cog"></i>
										Personalizar Contraseña
									</a>
								</li>
							</ul>
						</li>
					</ul><!--/.ace-nav-->
				</div><!--/.container-fluid-->
			</div><!--/.navbar-inner-->
		</div>
	    <div class="main-container container-fluid">
	        <a class="menu-toggler" id="menu-toggler" href="#">
				<span class="menu-text"></span>
			</a>
	        {include file="structure/menu.tpl"}
	        
	        {block name=content}{/block}
	        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	        
	        <!-- footer -->
			{include file="structure/footer.tpl"}
	    </div>
	     
	</body> 
</html>



