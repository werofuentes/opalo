{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
			<i class="icon-briefcase"></i>
			<a href="#">Administracion</a>

			<span class="divider">
					<i class="icon-angle-right arrow-icon"></i>
			</span>
			<a href="#">Evaluaciones</a>
			</li>
		</ul><!--.breadcrumb-->

	</div>
	<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable">
										<form name="formAlta" id="formAlta" method="post" action="{base_url()}documentos/editarAltaFormulario" enctype="multipart/form-data">

										
										<input id="id" name="id" class="span7" value="{$Evaluados.id}"  type="hidden" >
										{if $Evaluados.FechaRegistro eq ''}
										<input id="tieneFecha" name="tieneFecha" class="span7" value="No"  type="hidden" >
										{else}
										<input id="tieneFecha" name="tieneFecha" class="span7" value="Si"  type="hidden" >
										{/if}


										<ul class="nav nav-tabs" id="myTab">
											
											<li class="active">
												<a data-toggle="tab" href="#datosCandidato">
													Datos Personales
												</a>
											</li>
											
											<li >
												<a data-toggle="tab" href="#referenciasLaborales">
													Referencia Laboral 1
												</a>
											</li>

											<li >
												<a data-toggle="tab" href="#referenciasLaborales2">
													Referencia Laboral 2
												</a>
											</li>

											<li >
												<a data-toggle="tab" href="#referenciasLaborales3">
													Referencia Laboral 3
												</a>
											</li>

											<li >
												<a data-toggle="tab" href="#referenciasLaborales4">
													Referencia Laboral 4
												</a>
											</li>


											<li >
												<a data-toggle="tab" href="#referenciasLaborales5">
													Referencia Laboral 5
												</a>
											</li>

											<li >
												<a data-toggle="tab" href="#socioEconomico">
													Socioeconomico
												</a>
											</li>
                                         
										</ul>

										<div class="tab-content">
										
										
										
										<div id="datosCandidato" class="tab-pane in active">
											<table width="100%" border="0">
								                   <tr>
								                     <td align="center">
								                        FOTO <br>
								                      
								                      	 {if $Evaluados.Foto eq ''}
								                         <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="{base_url()}inc/images_apro/facebook350.jpg" />
								                        
								                          {else}	
								                          <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="{base_url()}{$Evaluados.Foto}" />
								                          {/if}
								                         <br />
								                         <input name="fotoCandidatoInput" id="fotoCandidatoInput" type="hidden" value="" /> 
								                        <input name="file-input" id="file-input" type="file" /> 


	
											         </td>
											         
											         <td>
											         <table width="100%" border="0">
											         	<tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Folio:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="">
											                	 <input id="folAsignar" name="folAsignar" value="{'%05d'|sprintf:$idFolio}"  class="span12" value="" type="text" placeholder="Folio" readonly>

											                
														 		
														 	 </div>
											                </td>
											              </tr>
											              
											         	<tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Tipo evaluacion:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_certificacion">
											                	<select class="form-field-select-1" id="certificacionId" name="certificacionId">
																		 <option value="" > Elige </option>
																		 {foreach from=$Certificaciones key=key item=item}
																		 <option value="{$item.id}" {if $Evaluados.idCertificado eq $item.id} selected="selected" {/if}>{$item.Certificaciones}</option>
																		 {/foreach}
																 </select>
														 		 <span id="error_1A" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>
											              <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Sucursal:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_sucursal">
											                	<select class="form-field-select-1" id="sucursal" name="sucursal">
																		 <option value="" > Elige </option>
																		 {foreach from=$Sucursal key=key item=item}
																		 <option value="{$item.idSucursal}" {if $Evaluados.Sucursal eq $item.idSucursal} selected="selected" {/if}>{$item.Sucursal}</option>
																		 {/foreach}
																 </select>
														 		 <span id="error_1sucursal" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>



											              <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Empresa:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_idEmpresa">
											                	<select class="form-field-select-1" id="idEmpresa" name="idEmpresa">
															 		<option value="" > Elige </option>
															  		{foreach from=$Empresas key=key item=item}
													                <option value="{$item.id}" {if $Evaluados.idEmpresa eq $item.id} selected="selected" {/if}>{$item.Nombre}</option>
														      		{/foreach}
														 		 </select>
														 		 <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>

											              <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Psicologa:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_idPsicologa">
											                	<select class="form-field-select-1" id="idPsicologa" name="idPsicologa">
															 		<option value="" > Elige </option>
															  		{foreach from=$Psicologa key=key item=item}
													                <option value="{$item.id}" {if $Evaluados.idPsicologa eq $item.id} selected="selected" {/if}>{$item.NombrePsicologa}</option>
														      		{/foreach}
														 		 </select>
														 		 <span id="error_1_idPsicologa" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>
											              <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Trabajadora social:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_Freelance">
											                	<select class="form-field-select-1" id="Freelance" name="Freelance" >
															 		<option value="" > Elige </option>
															  		{foreach from=$Freelance key=key item=item}
													                <option value="{$item.id}" {if $Evaluados.idFreelance eq $item.id} selected="selected" {/if}>{$item.Nombre}</option>
														      		{/foreach}
														 		 </select>
														 		 <span id="error_1_freelance" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>

											              <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Puesto:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_Puesto">
											                	 <input id="Puesto" name="Puesto"  class="span12" value="{$Evaluados.Puesto}" type="text" placeholder="Puesto">
														 		 <span id="error_1B" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>
											               <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Sexo:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_tipo_sexo">
											                		<div class="controls">
																	      	<select class="form-field-select-1" id="tipo_sexo" name="tipo_sexo">
																	               <option value="" > Elige </option>
																	               <option value="1" {if $Evaluados.Sexo eq 1} selected="selected" {/if}>MASCULINO</option>
																	               <option value="2" {if $Evaluados.Sexo eq 2} selected="selected" {/if}>FEMENINO</option>
																	                                 
																	                               
																			    </select>
																	  <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
											                       </div>
											                 </div>
											                </td>
											              </tr>
											               <tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Estatus evaluado:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="">
											                		<div class="controls">
																	      	<select class="form-field-select-1" id="EstatusEvaluado" name="EstatusEvaluado">
																	              
																	               <option value="1"  {if $Evaluados.EstatusEvaluado eq 1} selected="selected" {/if}>Activo</option>
																	               <option value="0"  {if $Evaluados.EstatusEvaluado eq 0} selected="selected" {/if}>Baja</option>
																	                                 
																	                               
																			    </select>
																	 
											                       </div>
											                 </div>
											                </td>
											              </tr>
											          </table>
											         </td>
											       </tr>
											    </table>
											    <br>
											     <table width="100%" frame="box" border="0">
											    <tr>
											      <td colspan="3">
											         <div id="error_Nombres">
															 <label class="control-label" for="form-field-1">Nombre: 
															 <br>
															 <input id="Nombres" name="Nombres"  class="span12" value="{$Evaluados.Nombre}" type="text" placeholder="Nombres">
															 </label>											           
															 <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
													  </div> 
										          </td>
										          
										          
										        </tr>
										         <tr>
											      <td colspan="3">
											         <div id="error_Curp">
															 <label class="control-label" for="form-field-1">Curp: 
															 <br>
															 <input id="Curp" name="Curp"  class="span12" value="{$Evaluados.Curp}" type="text" placeholder="Curp">
															 </label>											           
															 <span id="error_3Curp" style="display: none" class="help-inline">Campo Obligatorio </span> 
													  </div> 
										          </td>
										          
										          
										        </tr>

										         <tr>
										          <td>
										          <div id="error_Edad">
														 <label class="control-label" for="form-field-1">Edad:<br>
														 <input id="Edad" name="Edad" onkeypress="return justNumbers(event);" class="span10" type="text" value="{$Evaluados.Edad}" placeholder="Edad" >
														 </label>
														 <span id="error_4Edad" style="display: none" class="help-inline">Campo Obligatorio </span> 
																									           
												  </div>
										          </td>
										          <td>
										          <div id="error_Peso">
														 <label class="control-label" for="form-field-1">Peso:<br>
														 <input id="Peso" onkeypress="return justNumbers(event);"  name="Peso" class="span12" value="{$Evaluados.Peso}" type="text" placeholder="Peso" >
														 </label>											           
														 <span id="error_5Peso" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div> 
										          </td>
										          <td>
										           <div id="error_Estatura">
														 <label class="control-label" for="form-field-1">Estatura: <br>
														 <input id="Estatura" onkeypress="return justNumbers(event);" name="Estatura"  class="span12" type="text" value="{$Evaluados.Estatura}" placeholder="Estatura" >
														 </label>											           
														 <span id="error_6Estatura" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div>
										          </td>
										        </tr>

										        <tr>
										          <td>
										          <div id="error_Mail">
														 <label class="control-label" for="form-field-1">Correo:<br>
														 <input id="Mail" name="Mail"  class="span10" type="text" value="{$Evaluados.Correo}" placeholder="Correo" >
														 </label>
														 <span id="error_4" style="display: none" class="help-inline">Campo Obligatorio </span> 
																									           
												  </div>
										          </td>
										          <td>
										          <div id="error_Domicilio1">
														 <label class="control-label" for="form-field-1">Direccion:<br>
														 <input id="Domicilio1"  name="Domicilio12" class="span12" value="{$Evaluados.Domicilio}" type="text" placeholder="Direccion" >
														 </label>											           
														 <span id="error_5" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div> 
										          </td>
										          <td>
										           <div id="error_Colonia">
														 <label class="control-label" for="form-field-1">Colonia: <br>
														 <input id="Colonia" name="Colonia"  class="span12" type="text" value="{$Evaluados.Colonia}" placeholder="Colonia" >
														 </label>											           
														 <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div>
										          </td>
										        </tr>
										        <tr>
										          <td>
										          <div id="error_Ciudad">
														 <label class="control-label" for="form-field-1">Ciudad:
														 <br>
														 <input id="Ciudad" name="Ciudad"  class="span10" value="{$Evaluados.Ciudad}" type="text" placeholder="Ciudad" >
														 </label>											           
														 <span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 
									
												  </div>
										          </td>
										          <td>
										          <div id="error_Estado">
		
														 <label class="control-label" for="form-field-1">Estado<br>
															 <select class="form-field-select-1" id="Estado" name="Estado" onchange="TipoMunicipio(this.value);" >
															   <option value="" > Elige </option>
																{foreach from=$Estados key=key item=item}
																        	<option value="{$item.idEstado}"  {if $Evaluados.Estado eq $item.idEstado} selected="selected" {/if}>{$item.Nombre}</option>
																{/foreach}
															</select>
														  </label>
														 <span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div> 
										          </td>
										          <td>
										          <div id="error_Municipio">
														
														  <label class="control-label" for="form-field-1">Municipio <br>
														    <select class="form-field-select-1" id="Municipio" name="Municipio">
																	 <option value="" > Elige </option>
																	   {foreach from=$Municipio key=key item=item}
													                      <option value="{$item.idMunicipio}" {if $Evaluados.Municipio eq $item.idMunicipio} selected="selected" {/if}>{$item.Nombre}</option>
													                  {/foreach}
														    </select>
														 </label>
														 <span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span>
												  </div> 
										          </td>
										          
										        </tr>
										        <tr>
										          <td>
										          <div id="error_CodigoPost">
														 <label class="control-label" for="form-field-1">CP:<br>
														 <input id="CodigoPost" name="CodigoPost"  class="span7" value="{$Evaluados.Cp}" type="text" placeholder="CP" >
														 </label>
														 <span id="error_10" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div> 
										          </td>
										          <td>
										          <div id="error_TelCasa" >
														 <label class="control-label" for="form-field-1">Telefono: <br>
														 <input id="TelCasa" name="TelCasa" class="span7" value="{$Evaluados.Telefono}"  type="text" placeholder="Telefono" >
														 </label>
														 <span id="error_11" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div>
										          </td>
										          <td>
										          <div id="error_Cel">
														 <label class="control-label" for="form-field-1">Celular:<br> 
														 <input id="Cel" name="Cel"  value="{$Evaluados.Celular}" class="span7" type="text" placeholder="Celular" >
														 </label>
														 <span id="error_12" style="display: none" class="help-inline">Campo Obligatorio </span> 
												  </div>
										          </td>
										        </tr>


										        
										      </table>
												
										</div>
								
										<div id="referenciasLaborales" class="tab-pane">
											<table width="100%" frame="box" border="0">
											  <tr>
											    <td colspan="1" width="20%" >NOMBRE DE LA EMPRESA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="NombreEmpresa" class="span8" type="text" placeholder="Nombre empresa" value="{$Referencias.NombreEmpresa}" name="NombreEmpresa">
											
												</div>
											    
											    </td>
											  </tr>

											  <tr>
											    <td width="20%">DOMICILIO</td>
											    <td width="30%"><input id="Domicilio" class="span12" type="text"  placeholder="Domicilio" value="{$Referencias.Domicilio}" name="Domicilio"></td>
											    <td width="25%">TELEFONO</td>
											    <td width="25%"><input id="Telefono" class="span8" type="text"  placeholder="Telefono" value="{$Referencias.Telefono}" name="Telefono"></td>
											  </tr>

											  <tr>
											    <td width="25%">FECHA INGRESO</td>
											    <td width="25%"><input id="FechaIngreso" class="span12 date-picker" type="text"  placeholder="Fecha Ingreso" value="{$Referencias.FechaIngreso}" name="FechaIngreso"></td>
											    <td width="25%">FECHA SALIDA</td>
											    <td width="25%"><input id="FechaSalida" class="span12 date-picker" type="text"  placeholder="Fecha Salida" value="{$Referencias.FechaSalida}" name="FechaSalida"></td>
											  </tr>
											  
											  <tr>
											    <td width="25%">JEFE INMEDIATO</td>
											    <td width="25%"><input id="JefeInmediato" class="span12" type="text"  placeholder="Jefe Inmediato" value="{$Referencias.JefeInmediato}" name="JefeInmediato"></td>
											    <td width="25%">PUESTO</td>
											    <td width="25%"><input id="PuestoJefe" class="span12" type="text"  placeholder="Puesto" value="{$Referencias.PuestoJefe}" name="PuestoJefe"></td>
											  </tr>
											  <tr>
											    <td colspan="1" width="20%" >MOTIVO DE SALIDA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="MotivoSalida" class="span12" type="text" placeholder="Motivo de Salida" value="{$Referencias.MotivoSalida}" name="MotivoSalida">
											
												</div>
											    
											    </td>
											  </tr>
											</table>
									
												
										</div>

										<div id="referenciasLaborales2" class="tab-pane">
											<table width="100%" frame="box" border="0">
											  <tr>
											    <td colspan="1" width="20%" >NOMBRE DE LA EMPRESA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="NombreEmpresa2" class="span8" type="text" placeholder="Nombre empresa" value="{$Referencias.NombreEmpresa2}" name="NombreEmpresa2">
											
												</div>
											    
											    </td>
											  </tr>

											  <tr>
											    <td width="20%">DOMICILIO</td>
											    <td width="30%"><input id="Domicilio2" class="span12" type="text"  placeholder="Domicilio" value="{$Referencias.Domicilio2}" name="Domicilio2"></td>
											    <td width="25%">TELEFONO</td>
											    <td width="25%"><input id="Telefono2" class="span8" type="text"  placeholder="Telefono" value="{$Referencias.Telefono2}" name="Telefono2"></td>
											  </tr>

											  <tr>
											    <td width="25%">FECHA INGRESO</td>
											    <td width="25%"><input id="FechaIngreso2" class="span12 date-picker" type="text"  placeholder="Fecha Ingreso" value="{$Referencias.FechaIngreso2}" name="FechaIngreso2"></td>
											    <td width="25%">FECHA SALIDA</td>
											    <td width="25%"><input id="FechaSalida2" class="span12 date-picker" type="text"  placeholder="Fecha Salida" value="{$Referencias.FechaSalida2}" name="FechaSalida2"></td>
											  </tr>
											  
											  <tr>
											    <td width="25%">JEFE INMEDIATO</td>
											    <td width="25%"><input id="JefeInmediato2" class="span12" type="text"  placeholder="Jefe Inmediato" value="{$Referencias.JefeInmediato2}" name="JefeInmediato2"></td>
											    <td width="25%">PUESTO</td>
											    <td width="25%"><input id="PuestoJefe2" class="span12" type="text"  placeholder="Puesto" value="{$Referencias.PuestoJefe2}" name="PuestoJefe2"></td>
											  </tr>
											  <tr>
											    <td colspan="1" width="20%" >MOTIVO DE SALIDA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="MotivoSalida2" class="span12" type="text" placeholder="Motivo de Salida" value="{$Referencias.MotivoSalida2}" name="MotivoSalida2">
											
												</div>
											    
											    </td>
											  </tr>
											</table>
									
												
										</div>



										<div id="referenciasLaborales3" class="tab-pane">
											<table width="100%" frame="box" border="0">
											  <tr>
											    <td colspan="1" width="20%" >NOMBRE DE LA EMPRESA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="NombreEmpresa3" class="span8" type="text" placeholder="Nombre empresa" value="{$Referencias.NombreEmpresa3}" name="NombreEmpresa3">
											
												</div>
											    
											    </td>
											  </tr>

											  <tr>
											    <td width="20%">DOMICILIO</td>
											    <td width="30%"><input id="Domicilio3" class="span12" type="text"  placeholder="Domicilio" value="{$Referencias.Domicilio3}" name="Domicilio3"></td>
											    <td width="25%">TELEFONO</td>
											    <td width="25%"><input id="Telefono3" class="span8" type="text"  placeholder="Telefono" value="{$Referencias.Telefono3}" name="Telefono3"></td>
											  </tr>

											  <tr>
											    <td width="25%">FECHA INGRESO</td>
											    <td width="25%"><input id="FechaIngreso3" class="span12 date-picker" type="text"  placeholder="Fecha Ingreso" value="{$Referencias.FechaIngreso3}" name="FechaIngreso3"></td>
											    <td width="25%">FECHA SALIDA</td>
											    <td width="25%"><input id="FechaSalida3" class="span12 date-picker" type="text"  placeholder="Fecha Salida" value="{$Referencias.FechaSalida3}" name="FechaSalida3"></td>
											  </tr>
											  
											  <tr>
											    <td width="25%">JEFE INMEDIATO</td>
											    <td width="25%"><input id="JefeInmediato3" class="span12" type="text"  placeholder="Jefe Inmediato" value="{$Referencias.JefeInmediato3}" name="JefeInmediato3"></td>
											    <td width="25%">PUESTO</td>
											    <td width="25%"><input id="PuestoJefe3" class="span12" type="text"  placeholder="Puesto" value="{$Referencias.PuestoJefe3}" name="PuestoJefe3"></td>
											  </tr>
											  <tr>
											    <td colspan="1" width="20%" >MOTIVO DE SALIDA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="MotivoSalida3" class="span12" type="text" placeholder="Motivo de Salida" value="{$Referencias.MotivoSalida3}" name="MotivoSalida3">
											
												</div>
											    
											    </td>
											  </tr>
											</table>
									
												
										</div>

										
									<div id="referenciasLaborales4" class="tab-pane">
											<table width="100%" frame="box" border="0">
											  <tr>
											    <td colspan="1" width="20%" >NOMBRE DE LA EMPRESA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="NombreEmpresa4" class="span8" type="text" placeholder="Nombre empresa" value="{$Referencias.NombreEmpresa4}" name="NombreEmpresa4">
											
												</div>
											    
											    </td>
											  </tr>

											  <tr>
											    <td width="20%">DOMICILIO</td>
											    <td width="30%"><input id="Domicilio4" class="span12" type="text"  placeholder="Domicilio" value="{$Referencias.Domicilio4}" name="Domicilio4"></td>
											    <td width="25%">TELEFONO</td>
											    <td width="25%"><input id="Telefono4" class="span8" type="text"  placeholder="Telefono" value="{$Referencias.Telefono4}" name="Telefono4"></td>
											  </tr>

											  <tr>
											    <td width="25%">FECHA INGRESO</td>
											    <td width="25%"><input id="FechaIngreso4" class="span12 date-picker" type="text"  placeholder="Fecha Ingreso" value="{$Referencias.FechaIngreso4}" name="FechaIngreso4"></td>
											    <td width="25%">FECHA SALIDA</td>
											    <td width="25%"><input id="FechaSalida4" class="span12 date-picker" type="text"  placeholder="Fecha Salida" value="{$Referencias.FechaSalida4}" name="FechaSalida4"></td>
											  </tr>
											  
											  <tr>
											    <td width="25%">JEFE INMEDIATO</td>
											    <td width="25%"><input id="JefeInmediato4" class="span12" type="text"  placeholder="Jefe Inmediato" value="{$Referencias.JefeInmediato4}" name="JefeInmediato4"></td>
											    <td width="25%">PUESTO</td>
											    <td width="25%"><input id="PuestoJefe4" class="span12" type="text"  placeholder="Puesto" value="{$Referencias.PuestoJefe4}" name="PuestoJefe4"></td>
											  </tr>
											  <tr>
											    <td colspan="1" width="20%" >MOTIVO DE SALIDA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="MotivoSalida4" class="span12" type="text" placeholder="Motivo de Salida" value="{$Referencias.MotivoSalida4}" name="MotivoSalida4">
											
												</div>
											    
											    </td>
											  </tr>
											</table>
									
												
										</div>
									
										<div id="referenciasLaborales5" class="tab-pane">
											<table width="100%" frame="box" border="0">
											  <tr>
											    <td colspan="1" width="20%" >NOMBRE DE LA EMPRESA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="NombreEmpresa5" class="span8" type="text" placeholder="Nombre empresa" value="{$Referencias.NombreEmpresa5}" name="NombreEmpresa5">
											
												</div>
											    
											    </td>
											  </tr>

											  <tr>
											    <td width="20%">DOMICILIO</td>
											    <td width="30%"><input id="Domicilio5" class="span12" type="text"  placeholder="Domicilio" value="{$Referencias.Domicilio5}" name="Domicilio5"></td>
											    <td width="25%">TELEFONO</td>
											    <td width="25%"><input id="Telefono5" class="span8" type="text"  placeholder="Telefono" value="{$Referencias.Telefono5}" name="Telefono5"></td>
											  </tr>

											  <tr>
											    <td width="25%">FECHA INGRESO</td>
											    <td width="25%"><input id="FechaIngreso5" class="span12 date-picker" type="text"  placeholder="Fecha Ingreso" value="{$Referencias.FechaIngreso5}" name="FechaIngreso5"></td>
											    <td width="25%">FECHA SALIDA</td>
											    <td width="25%"><input id="FechaSalida5" class="span12 date-picker" type="text"  placeholder="Fecha Salida" value="{$Referencias.FechaSalida5}" name="FechaSalida5"></td>
											  </tr>
											  
											  <tr>
											    <td width="25%">JEFE INMEDIATO</td>
											    <td width="25%"><input id="JefeInmediato5" class="span12" type="text"  placeholder="Jefe Inmediato" value="{$Referencias.JefeInmediato5}" name="JefeInmediato5"></td>
											    <td width="25%">PUESTO</td>
											    <td width="25%"><input id="PuestoJefe5" class="span12" type="text"  placeholder="Puesto" value="{$Referencias.PuestoJefe5}" name="PuestoJefe5"></td>
											  </tr>
											  <tr>
											    <td colspan="1" width="20%" >MOTIVO DE SALIDA</td>
											    <td colspan="3" width="80%">
											    <div>
									                <br>
													<input id="MotivoSalida5" class="span12" type="text" placeholder="Motivo de Salida" value="{$Referencias.MotivoSalida5}" name="MotivoSalida5">
											
												</div>
											    
											    </td>
											  </tr>
											</table>
									
												
										</div>

										<div id="socioEconomico" class="tab-pane">
											<div class="row-fluid">
												<div class="span12">
													<div class="center">
														<div id="" class="control-group">	
															<div class="span2"><div align="right">Archivo:</div></div>
															<div class="span3">
																
																{if $Evaluados.socioEconomicoDoc eq ''}
																<input name="file-input-socioEconomico" id="file-input-socioEconomico" type="file" />

																{else}

																<a href="{base_url()}{$Evaluados.socioEconomicoDoc}" data-rel="colorbox">

																 <img id="fotoPsicolog" name="fotoPsicolog" width="50px" height="50px" src="{base_url()}inc/icono_descarga.png" />

																 </a>

																{/if}


																<span id="error_4psi" style="display: none" class="help-inline">Campo Obligatorio </span> 
															</div>
														


														</div>
														<div id="" class="control-group">

														</div>
														<div id="" class="control-group">		

															<div class="span3"><div align="right"> Entrega de <br>socioeconomico:</div></div>
															<div class="span3">


															 <select class="form-field-select-1" id="Entregasocioeconomico" name="Entregasocioeconomico">
																	 <option value="" > Elige </option>
																	 <option value="OK" {if $Evaluados.estatusSocioDoc eq 'OK'} selected="selected" {/if}> OK </option>

																	 <option value="Debe" {if $Evaluados.estatusSocioDoc eq 'Debe'} selected="selected" {/if}> Debe </option>
																	  
														    </select>

															
															</div>
														</div>	
													</div><!--/center-->	
												</div><!--/span12-->	
											</div><!--/row-fluid-->		
									
									
										</div>
											
											
											
										</div>
										</form>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
						
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

					  <div style="margin-left:87px;margin-top:-65px;">
								<a  href="{base_url()}documentos/evaluacion">
								   <button class="btn btn-app btn-success btn-mini">
								    <i class="icon-remove bigger-160"></i>
								    Cancelar
								   </button>
								</a>									 
							</div>	
							
							{if $smarty.session.PERMISOS.sololectura_evaluacion eq 0}
							<div style="margin-left:157px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="guardar();">
							    <i class="icon-save bigger-160"></i>
							    Guardar
							   </button>									 
							</div>	
							{/if}


</div>

{if $smarty.session.PERMISOS.sololectura_evaluacion eq 1}
<script type="text/javascript">
	DisableEnableForm(document.formAlta,true);
</script>
{/if}

<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#evaluaciones').attr('class','active');

		function justNumbers(e)
        {
        	var keynum = window.event ? window.event.keyCode : e.which;
        	if ((keynum == 8) || (keynum == 46))
        	return true;
         
        	return /\d/.test(String.fromCharCode(keynum));
        }


			$(function() {
		$( "#FechaIngreso" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});

		$( "#FechaSalida" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	    
      		}
		});


	});


$(window).load(function(){


 $(function() {
  $('#file-input').change(function(e) {
      addImage(e); 
     });

     function addImage(e){
      var file = e.target.files[0],
      imageType = /image.*/;
    
      if (!file.type.match(imageType))
       return;
  
      var reader = new FileReader();
      reader.onload = fileOnload;
      reader.readAsDataURL(file);
     }
  
     function fileOnload(e) {
      var result=e.target.result;
      $('#fotoCandidato').attr("src",result);
      $('#fotoCandidatoInput').attr("value",result);
     }
    });
  });


  function guardar(){
  		  var idEmpresa = $('#idEmpresa').val();
  		  var idPsicologa = $('#idPsicologa').val();

  		  
  		  var Puesto = $('#Puesto').val();
  		  var tipo_sexo = $('#tipo_sexo').val();
  		  var Nombres = $('#Nombres').val();

  		    var sucursal = $('#sucursal').val();
  		    var Curp = $('#Curp').val();
  		    var Edad = $('#Edad').val();
  		    var Peso = $('#Peso').val();
  		    var Estatura = $('#Estatura').val();

  		  var Mail = $('#Mail').val();
  		  var Domicilio1 = $('#Domicilio1').val();
  		  var Colonia = $('#Colonia').val();
  		  var Ciudad = $('#Ciudad').val();
  		  var Estado = $('#Estado').val();
  		  var Municipio = $('#Municipio').val();
  		  var CodigoPost = $('#CodigoPost').val();
  		  var TelCasa = $('#TelCasa').val();
  		  var Cel = $('#Cel').val();

  		  if(idEmpresa == ''){
		  	$('#error_idEmpresa').addClass("control-group error");
		  	document.getElementById('error_1').style.display='block';
		  }else{
		   	$('#error_idEmpresa').removeClass("control-group error");
		    $('#error_idEmpresa').addClass("control-group");
		    document.getElementById('error_1').style.display='none';
		  
		  }

		  if(idPsicologa == ''){
		  	$('#error_idPsicologa').addClass("control-group error");
		  	document.getElementById('error_1_idPsicologa').style.display='block';
		  }else{
		   	$('#error_idPsicologa').removeClass("control-group error");
		    $('#error_idPsicologa').addClass("control-group");
		    document.getElementById('error_1_idPsicologa').style.display='none';
		  
		  }

		  

		  if(sucursal == ''){
		  	$('#error_sucursal').addClass("control-group error");
		  	document.getElementById('error_1sucursal').style.display='block';
		  }else{
		   	$('#error_sucursal').removeClass("control-group error");
		    $('#error_sucursal').addClass("control-group");
		    document.getElementById('error_1sucursal').style.display='none';
		  
		  }


		  if(Puesto == ''){
		  	$('#error_Puesto').addClass("control-group error");
		  	document.getElementById('error_1B').style.display='block';
		  }else{
		   	$('#error_Puesto').removeClass("control-group error");
		    $('#error_Puesto').addClass("control-group");
		    document.getElementById('error_1B').style.display='none';
		  
		  }


		  if(tipo_sexo == ''){
		  	$('#error_tipo_sexo').addClass("control-group error");
		  	document.getElementById('error_2').style.display='block';
		  }else{
		   	$('#error_tipo_sexo').removeClass("control-group error");
		    $('#error_tipo_sexo').addClass("control-group");
		    document.getElementById('error_2').style.display='none';
		  
		  }

		  if(Nombres == ''){
		  	$('#error_Nombres').addClass("control-group error");
		  	document.getElementById('error_3').style.display='block';
		  }else{
		   	$('#error_Nombres').removeClass("control-group error");
		    $('#error_Nombres').addClass("control-group");
		    document.getElementById('error_3').style.display='none';
		  
		  }


		  if(Curp == ''){
		  	$('#error_Curp').addClass("control-group error");
		  	document.getElementById('error_3Curp').style.display='block';
		  }else{
		   	$('#error_Curp').removeClass("control-group error");
		    $('#error_Curp').addClass("control-group");
		    document.getElementById('error_3Curp').style.display='none';
		  
		  }

		  if(Edad == ''){
		  	$('#error_Edad').addClass("control-group error");
		  	document.getElementById('error_4Edad').style.display='block';
		  }else{
		   	$('#error_Edad').removeClass("control-group error");
		    $('#error_Edad').addClass("control-group");
		    document.getElementById('error_4Edad').style.display='none';
		  
		  }

		  if(Peso == ''){
		  	$('#error_Peso').addClass("control-group error");
		  	document.getElementById('error_5Peso').style.display='block';
		  }else{
		   	$('#error_Peso').removeClass("control-group error");
		    $('#error_Peso').addClass("control-group");
		    document.getElementById('error_5Peso').style.display='none';
		  
		  }

		  if(Estatura == ''){
		  	$('#error_Estatura').addClass("control-group error");
		  	document.getElementById('error_6Estatura').style.display='block';
		  }else{
		   	$('#error_Estatura').removeClass("control-group error");
		    $('#error_Estatura').addClass("control-group");
		    document.getElementById('error_6Estatura').style.display='none';
		  
		  }

		  if(Mail == ''){
		  	$('#error_Mail').addClass("control-group error");
		  	document.getElementById('error_4').style.display='block';
		  }else{
		   	$('#error_Mail').removeClass("control-group error");
		    $('#error_Mail').addClass("control-group");
		    document.getElementById('error_4').style.display='none';
		  
		  }

		  if(Domicilio1 == ''){
		  	$('#error_Domicilio1').addClass("control-group error");
		  	document.getElementById('error_5').style.display='block';
		  }else{
		   	$('#error_Domicilio1').removeClass("control-group error");
		    $('#error_Domicilio1').addClass("control-group");
		    document.getElementById('error_5').style.display='none';
		  
		  }


		  if(Colonia == ''){
		  	$('#error_Colonia').addClass("control-group error");
		  	document.getElementById('error_6').style.display='block';
		  }else{
		   	$('#error_Colonia').removeClass("control-group error");
		    $('#error_Colonia').addClass("control-group");
		    document.getElementById('error_6').style.display='none';
		  
		  }


		  if(Ciudad == ''){
		  	$('#error_Ciudad').addClass("control-group error");
		  	document.getElementById('error_7').style.display='block';
		  }else{
		   	$('#error_Ciudad').removeClass("control-group error");
		    $('#error_Ciudad').addClass("control-group");
		    document.getElementById('error_7').style.display='none';
		  
		  }


		  if(Estado == ''){
		  	$('#error_Estado').addClass("control-group error");
		  	document.getElementById('error_8').style.display='block';
		  }else{
		   	$('#error_Estado').removeClass("control-group error");
		    $('#error_Estado').addClass("control-group");
		    document.getElementById('error_8').style.display='none';
		  
		  }

		  if(Municipio == ''){
		  	$('#error_Municipio').addClass("control-group error");
		  	document.getElementById('error_9').style.display='block';
		  }else{
		   	$('#error_Municipio').removeClass("control-group error");
		    $('#error_Municipio').addClass("control-group");
		    document.getElementById('error_9').style.display='none';
		  
		  }


		  if(CodigoPost == ''){
		  	$('#error_CodigoPost').addClass("control-group error");
		  	document.getElementById('error_10').style.display='block';
		  }else{
		   	$('#error_CodigoPost').removeClass("control-group error");
		    $('#error_CodigoPost').addClass("control-group");
		    document.getElementById('error_10').style.display='none';
		  
		  }

		  if(TelCasa == ''){
		  	$('#error_TelCasa').addClass("control-group error");
		  	document.getElementById('error_11').style.display='block';
		  }else{
		   	$('#error_TelCasa').removeClass("control-group error");
		    $('#error_TelCasa').addClass("control-group");
		    document.getElementById('error_11').style.display='none';
		  
		  }


		  if(Cel == ''){
		  	$('#error_Cel').addClass("control-group error");
		  	document.getElementById('error_12').style.display='block';
		  }else{
		   	$('#error_Cel').removeClass("control-group error");
		    $('#error_Cel').addClass("control-group");
		    document.getElementById('error_12').style.display='none';
		  
		  }




		  if(idPsicologa != '' && idEmpresa != '' && Curp != '' && Edad != '' && Peso != '' && sucursal != '' && Estatura != '' &&  Puesto != '' && tipo_sexo != '' && Nombres != '' && Mail != '' && Domicilio1 != '' && Colonia != '' && Ciudad != '' && Ciudad != '' && Estado != '' && Municipio != '' && CodigoPost != '' && TelCasa != '' && Cel != ''){
		    	
		    	//$( "#formAlta" ).submit();
		    $('#fotoCandidatoInput').val('');
		  	setTimeout(function(){
  					$( "#formAlta" ).submit();
			  }, 300);
		  
		  }


  }



</script>
{/block}