<html>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link href="{base_url()}inc/css_apro/bootstrap.min.css" rel="stylesheet" />
		<link href="{base_url()}inc/css_apro/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/font-awesome.min.css" />
		

        <link rel="stylesheet" href="{base_url()}inc/css_apro/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/chosen.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/datepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/daterangepicker.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/colorpicker.css" /> 
		

		<!--[if IE 7]>
		  <link rel="stylesheet" href="{base_url()}inc/css_apro/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace-responsive.min.css" />
		<link rel="stylesheet" href="{base_url()}inc/css_apro/ace-skins.min.css" />
<body>
<div class="tab-content">
<table width="100%">
	<tr>
		<td width="50%">
		 	<img src="{$DOCUMENT_ROOT}/opalo_sistem/inc/logo.jpg"/>
 		</td>
		<td width="50%" align="center">
				Folio: {$Documentos.id}

		</td>
	</tr>
</table>



	<div id="informacion" class="tab-pane in active">
		<div class="table-header">
				Información cotización
		</div>
		<table width="100%" border="0">
			<tr>											 			            
			<td width="100%" align="left">
			  <B>Fecha Documento:</B>{$Documentos.Fecha}
			</td>
			</tr>

		</table>

													<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																         <B>Empresa:</B>
																           {$Documentos.NombreCliente}
																  
			
																           
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																          <B>RFC:</B>
																           {$Documentos.Rfc}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Domicilio fiscal:</B>
																           {$Documentos.DireccionCliente}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Telefono:</B>
																           {$Documentos.Telefono}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Vendedor:</B>
																           {$Documentos.VendedoresNombre}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1"><B>Comentarios:</B></label>
																         	{$Documentos.Comentarios}
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle cotización
												</div>

												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>					
															</tr>
														
															{assign var="Subtotal" value="0"}

															{foreach from=$DocumentosDetalle key=key item=item}
															<tr>
															
																<th width="35%">{$item.Servicio}</th>
																<th width="20%">{$item.Cantidad}</th>
																<th width="20%">{$item.Precio}</th>
																<th width="15%">{$item.Importe|number_format:2:".":","}</th>					
															</tr>
																{$Subtotal = $Subtotal + $item.Importe}
															{/foreach}
														</thead>

														
													
													 </table>
													  <table width="100%">
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Subtotal:</B></td>
													       		<td align="center" colspan="2" width="16%">{$Subtotal|number_format:2:".":","}</td>
													       </tr>
													       {$iva = $Subtotal * 0.16}
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Iva:</B></td>
													       		<td align="center" colspan="2" width="16%">{$iva|number_format:2:".":","}</td>
													       </tr>
													       {$total = $Subtotal + $iva}
													       <tr>
													       		<td align="right" colspan="2" width="84%"><B>Total:</B></td>
													       		<td align="center" colspan="2" width="16%">{$total|number_format:2:".":","}</td>
													       </tr>
													 </table>
													
												</div>
											</div>

	</div>
		<div align="center">
			Guadalajara
			Filadelfia #1005 int.4.
			Col. Providencia. Guadalajara, Jalisco.<br>
			Tel.(33) 1594 5644<br>
			www.sistemaopalo.com.mx
	</div>
</div>
</body>
</html>