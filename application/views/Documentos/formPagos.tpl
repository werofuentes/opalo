{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
			<i class="icon-briefcase"></i>
			<a href="#">Administracion</a>

			<span class="divider">
					<i class="icon-angle-right arrow-icon"></i>
			</span>
			<a href="#">Cobranza</a>
			</li>
		</ul><!--.breadcrumb-->

	</div>
	<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable">
										<form name="formAlta" id="formAlta" action="{base_url()}documentos/guardarPago" method="post" enctype="multipart/form-data"/>
										<ul class="nav nav-tabs" id="myTab">
											<input type="hidden" name="id" id="id" value="{$Documentos.id}"  size="15"/>

											<input type="hidden" name="PorPagar" id="PorPagar" value="{$Documentos.PorPagar}"  size="15"/>


											<li class="active">
												<a data-toggle="tab" href="#datosregistrar">
													Registrar cobranza
												</a>
											</li>
											
											<li >
												<a data-toggle="tab" href="#datoshistorial">
													historial cobranza
												</a>
											</li>
                                         
										</ul>

										<div class="tab-content">
										
										
										
								<div id="datosregistrar" class="tab-pane in active">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tr bgcolor="#cedee8">
								     <td>  
									 <table width="100%" border="0" cellspacing="0" cellpadding="0">
								         <tr>
										  <td><B>Folio:</B> &nbsp;{$Documentos.id}</td>
										  <td Colspan="2"><B>Nombre:</B>&nbsp; {$Documentos.NombreCliente}</td>
										  <td colspan="2"><B>Total:</B>&nbsp;${$Documentos.Total}</td>
										  <td><B>Por pagar:</B> &nbsp;${$Documentos.PorPagar}</td>
										 </tr>
								     </table>	 
									 </td>
								  </tr>
								  <tr>
								     <td colspan="6"><br><br></td>
								  </tr>
								  <tr>
								   <td>  
								    <table width="100%" border="0" cellspacing="0" cellpadding="0">
								 	  <tr>
									  <td>Referencia de pago: </td>
									  <td>
									   <div id="error_no_recibo" class="control-group">
									  	<input type="text" name="no_recibo" id="no_recibo"  size="15"/>
									  	 <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
									    </div>
									  </td>
									  <td>Fecha de Recibo: </td>
									  <td>
									  <div id="error_fecha_recibo" class="control-group"><input type="text" name="fecha_recibo" id="fecha_recibo" readonly="readonly" size="20"/>
									  	<span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
									  </td>
									  <td>Monto del pago: </td>
									  <td>
									  	 <div id="error_pago_recibo" class="control-group">
									  $<input type="text" class="right currency" name="pago_recibo" id="pago_recibo"  size="40"/> M.N.
									  <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
									   </div>
									  </td>
									  </tr>
									</table>	
								  </td>
								</table>
											    
								</div>
								
										<div id="datoshistorial" class="tab-pane">
											{if $pagos eq ''}
													<B>No hay pagos</B>
											{else}
												
									<table cellspacing="0" width="100%">
									  <tr bgcolor="#cedee8">
										<th style="text-align: center"><strong>No Recibo</strong></th>
										<th style="text-align: center"><strong>Fecha</strong></th>
										<th style="text-align: center"><strong>Monto</strong></th>
									   </tr>
									   {foreach from=$pagos key=key item=item}	
									   <tr>
										<td style="text-align: center">{$item.NoRecibo}</td>
										<td style="text-align: center">{$item.Fecha}</td>
										<td style="text-align: center">{$item.Monto}</td>
									   </tr>
									   {/foreach}
									   </table>

												
											{/if}
									
										</div>

										
									
									
									
											
											
											
											
										</div>
										</form>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
						
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

					  <div style="margin-left:87px;margin-top:-65px;">
								<a  href="{base_url()}documentos/cobranza">
								   <button class="btn btn-app btn-success btn-mini">
								    <i class="icon-remove bigger-160"></i>
								    Cancelar
								   </button>
								</a>									 
							</div>	
							
							{if  $Documentos.PorPagar neq 0}
							<div style="margin-left:157px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="guardar();">
							    <i class="icon-save bigger-160"></i>
							    Guardar
							   </button>									 
							</div>	
							{/if}

</div>

<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#cobranza').attr('class','active');


			$(function() {
		$( "#fecha_recibo" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});




	});





  function guardar(){
  		 
  			var no_recibo 	 = $('#no_recibo').val();
  			var fecha_recibo = $('#fecha_recibo').val();
  			var pago_recibo  = $('#pago_recibo').val();


			  if(no_recibo == ''){
			  			
			  
				  	$('#error_no_recibo').addClass("control-group error");
				  	document.getElementById('error_1').style.display='block';
			  	
			  }else{
				   	$('#error_no_recibo').removeClass("control-group error");
				    $('#error_no_recibo').addClass("control-group");
				    document.getElementById('error_1').style.display='none';
			  }


			   if(fecha_recibo == ''){
			  			
			  
				  	$('#error_fecha_recibo').addClass("control-group error");
				  	document.getElementById('error_2').style.display='block';
			  	
			  }else{
				   	$('#error_fecha_recibo').removeClass("control-group error");
				    $('#error_fecha_recibo').addClass("control-group");
				    document.getElementById('error_2').style.display='none';
			  }

			  if(pago_recibo == ''){
			  			
			  
				  	$('#error_pago_recibo').addClass("control-group error");
				  	document.getElementById('error_3').style.display='block';
			  	
			  }else{
				   	$('#error_pago_recibo').removeClass("control-group error");
				    $('#error_pago_recibo').addClass("control-group");
				    document.getElementById('error_3').style.display='none';
			  }

			  if(no_recibo != '' && fecha_recibo != '' && pago_recibo != ''){
			  		$( "#formAlta" ).submit();
			  }

  }



</script>
{/block}
