{extends file="structure/main.tpl"}

{block name=content}



      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Facturación/Recibos</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>


				<div id="modal_clientes_autorizar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  
		                  <div class="modal-body" id="contenido_modal_clientes_autorizar">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>
				

 				
 				<div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}documentos/facturaNueva">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nueva
						</button>
					</span>
				</a>
 				</div>
 				<div style="margin-left:111px;margin-top:-65px;">
				<a class="shortcut-button" href="{base_url()}documentos/ExcelFactura">
					<span>
						<button class="btn btn-app btn-info btn-mini radius-4">
						<i class="icon-download  bigger-200"></i>
						Excel
						</button>
					</span>
				</a>
 				</div>


		          {if $FacturaCancelada2 eq 1}
		         	<div class="alert alert-success">
					  <strong>Exito!</strong> La factura se cancelo con exito.
					</div>

		         {/if}
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Facturación/Recibos
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th># Folio</th>
											<th>Fecha</th>
											<th>Cliente</th>
											<th>Total $</th>
											<th>Documento</th>
											<th>Folio fiscal</th>

											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$Documentos key=key item=item}

									   	{if $item.Estatus eq 2}
									   		
									   		{assign var="TotalPagar" value="{$item.Total}"}
									   	{else}
									   		{math assign="TotalPagar" equation="x/y" x=$item.Total y=1.16 format="%.2f"}

									   	{/if}


										 {if $item.id neq ''}
											<tr>
												<td style="cursor: pointer;" onclick="verFactura({$item.id});"><font face="Arial">{$item.id}</font></td>
												<td style="cursor: pointer;" onclick="verFactura({$item.id});"><font face="Arial">{$item.Fecha}</font></td>
												<td style="cursor: pointer;" onclick="verFactura({$item.id});"><font face="Arial">{$item.NombreCliente}</font></td>
												<td style="cursor: pointer;" onclick="verFactura({$item.id});"><font face="Arial">
													{if $item.Estatus eq 2}
													
														{$TotalPagar|string_format:"%.2f"}
													{else}
													
														{$TotalPagar}
													{/if}
												</font>


												</td>
												<td style="cursor: pointer;" onclick="verFactura({$item.id});"><font face="Arial">
													{if $item.Estatus eq 2}
														Factura
													{/if}

													{if $item.Estatus eq 3}
														Recibo
													{/if}

													{if $item.Estatus eq 1}
														Pendiente
													{/if}
												</font>
												</td>
												<td><font face="Arial">
												
												{if $item.Cancelada eq ''}
													{$item.FolioFactura}
												{else}
													Folio cancelado
												{/if}
												</font></td>

												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
														<a class="green" href="{base_url()}documentos/verDocumento/{$item.id}">
															<IMG SRC="{base_url()}/inc/factura.png" width="30px" ALT="Factura/Recibo">
														</a>

														{if $item.Estatus neq 1}
														<a href="{base_url()}documentos/pdfFactura/{$item.id}">
															<IMG SRC="{base_url()}/inc/iconoPdf.png" width="30px" ALT="PDF">
														</a>
														<a href="{base_url()}uploads/facturas/{$item.Rfc}-{$item.id}.xml" download="{$item.Rfc}-{$item.id}.xml">
															<IMG SRC="{base_url()}/inc/xml.ico" width="30px" ALT="PDF">
														</a>
														<br>
														<br>
														<a href="{base_url()}documentos/facturasNotas/{$item.id}">
															<IMG SRC="{base_url()}/inc/iconoNotas.png" width="30px" ALT="NOTAS">
														</a>
														{/if}

													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
	

																<a href="{base_url()}documentos/deleteDocumentos/{$item.id}" class="tooltip-success" data-rel="tooltip" title="Edit">
																	<span class="red">
																		<i class="icon-trash bigger-130"></i>
																	</span>
																</a>
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#facturacion').attr('class','active');





		function AutorizarCotizacionMostrar(){
			var idCotizacion = $('#idCotizacion').val();
			var total = $('#total').val();
			

			 $.ajax ({
                url     	: base_url + 'documentos/ActualizarAutorizacionCoti',
                type   		: 'POST',
                data    	: 'idCotizacion='+idCotizacion+'&total='+total,
                dataType	: 'json',
                async  		: false,
                cache       : false,
                success     : function(data){
                    	
                		window.location.href = base_url + "documentos/cotizacion";

              		
                    	
                }
            });


		}

</script>
    









{/block}
