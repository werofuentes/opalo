{extends file="structure/main.tpl"}

{block name=content}
<style>
#searchform {
	    width: 460px;
	  height:30px;
	    padding: 8px;
	  margin: 0px 150px 11px 91px;
	    background: #ccc;
	    border-radius: 4px;
	    box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 2px 0 rgba(255,255,255,.5);
	}
	 
	#searchform input {
	    width: 190px;
	  height:20px;
	    padding: 5px;
	    float: left;
	    border: 0;
	    background: #eee;
	    border-radius: 3px 0 0 3px;
	  box-shadow: 0 1px 1px rgba(0,0,0,.4) inset;
	}
	 
	#searchform input:focus {
	    outline: 0;
	  height:21px;
	    background: #fff;
	    box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
	}
	 
	#searchform input:-webkit-input-placeholder {
	   color: #999;
	   font-weight: normal;
	   font-style: italic;
	}
	 
	#searchform input:-moz-placeholder {
	    color: #999;
	    font-weight: normal;
	    font-style: italic;
	}
	 
	#searchform input:-ms-input-placeholder {
	    color: #999;
	    font-weight: normal;
	    font-style: italic;
	}   
	 
	#searchform button {
	    position: relative;
	    float: right;
	    border: 0;
	    cursor: pointer;
	    height: 30px;
	    width: 80px;
	    font-size:15px;
	    color: #fff;
	    background: #438cdb;
	    border-radius: 0 3px 3px 0;
	    text-shadow: 0 -1px 0 rgba(0, 0 ,0, .3);
	}  
	 
	#searchform button:hover{
	    background: #2672e0;
	}
	 
	#searchform button:active,
	#searchform button:focus{
	    background: #2672e0;
	}
	 
	#searchform button:before { /* flecha */
	    content: '';
	    position: absolute;
	    border-width: 8px 8px 8px 0;
	    border-style: solid solid solid none;
	    border-color: transparent #438cdb transparent;
	    top: 8px;
	    left: -5px;
	}
	 
	#searchform button:hover:before{
	    border-right-color: #2672e0;
	}
	 
	#searchform button:focus:before{
	    border-right-color: #2672e0;
} 
</style>


      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Cobranza</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
				

 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">


								<form id="searchform" name="searchform" action="{base_url()}documentos/cobranza" method="post">
								    	
								   	<select class="form-field-select-1" id="palabra_clave" name="palabra_clave">
															 		<option value="" > Elige </option>
															  		{foreach from=$Empresas key=key item=item}
													                <option value="{$item.id}">{$item.Nombre}</option>
														      		{/foreach}
									</select>
								    
								     <button type="submit">Buscar</button>
								 </form> 

							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Cobranza
								</div>



								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th># Folio</th>
											<th>Empresa</th>
											<th>Total $</th>
											<th>Por pagar $</th>
											<th>Tipo documento (Facturado/Recibo)</th>

											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$Documentos key=key item=item}
										 {if $item.id neq '' AND $item.Cancelada eq '' AND $item.PorPagar > 0.01}
											<tr>
												<td><font face="Arial">{$item.id}</font></td>
												<td><font face="Arial">{$item.NombreCliente}</font></td>
												<td><font face="Arial">{$item.Total|string_format:"%.2f"}</font></td>
												<td><font face="Arial">{$item.PorPagar|string_format:"%.2f"}</font></td>
												<td><font face="Arial">
													{if $item.Estatus eq 2}
														Facturado
													{/if}

													{if $item.Estatus eq 3}
														Recibo
													{/if}

													{if $item.Estatus eq 1}
														Pendiente
													{/if}
												</font>
												</td>

												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
														

													{if $smarty.session.PERMISOS.agregar_cobranza eq 1}
														<a class="green" href="{base_url()}documentos/verDocumentoCobrar/{$item.id}">
															<IMG SRC="{base_url()}/inc/cobranza.png" width="30px" ALT="Factura/Recibo">
														</a>
													{/if}
															
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
	

																{if $smarty.session.PERMISOS.agregar_cobranza eq 1}
																	<a class="green" href="{base_url()}documentos/verDocumentoCobrar/{$item.id}">
																		<IMG SRC="{base_url()}/inc/cobranza.png" width="30px" ALT="Factura/Recibo">
																	</a>
																{/if}
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#cobranza').attr('class','active');



</script>
    









{/block}
