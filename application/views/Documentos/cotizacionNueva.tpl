{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
	 <ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Cotización</a>
						</li>
		</ul><!--.breadcrumb-->
	</div>

	
	


				<div class="page-content">


				<div id="modal_clientes" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  		
					
									<br>
					<div align="center">
					Buscar : <input id="search" type="text" value="" name="search">
					</div>
		                  <div class="modal-body" id="contenido_modal_clientes">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>

		        <div id="modal_servicio" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  		
					
									<br>
					<div align="center">
					Buscar : <input id="search" type="text" value="" name="search">
					</div>
		                  <div class="modal-body" id="contenido_modal_servicio">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>


					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form name="formCotizar" id="formCotizar" action="{base_url()}documentos/guardarCotizacion" method="post" enctype="multipart/form-data"/>

							<input class="span12" id="IdEmpresaSecundaria"  name="IdEmpresaSecundaria" type="hidden" value="">
							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >


										<div class="tab-content">
											<div id="informacion" class="tab-pane in active">
												<div class="table-header">
													Información cotización
												</div>
												 <br>
										
													<table width="100%" border="0">
														 <tr>
														 
											                  <td width="50%">
											    
											                  <button type="button" class="btn btn-primary" onclick="MostrarCliente();">
											                  	<i class="icon-cloud icon-only bigger-120"></i>
											                    Buscar cliente
											                  </button>
											                  </td>
			            
											                  <td width="25%" align="right"><label class="control-label" for="form-field-1"><B>Fecha:</B></label></td>
											                  <td width="25%">{$fecha}</td>
											              </tr>

													  
														 

													</table>
													<br>
													<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa</label>
																           <input class="span12" id="Nombres"  name="Nombres" type="text" placeholder="Nombres" style='text-transform:uppercase;' >
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC</label>
																           <input class="span12" id="Rfc"  name="Rfc" type="text" placeholder="RFC" style='text-transform:uppercase;' >
																           <span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal</label>
																           <input class="span12" id="DomicilioFiscal"  name="DomicilioFiscal" type="text" placeholder="Domicilio fiscal" style='text-transform:uppercase;' >
																           <span id="error_11" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios</label>
																          <textarea id="comentario" name="comentario" class="span12" placeholder="" ></textarea>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>

												<div class="table-header">
													Detalle cotización
												</div>

												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>
																<th width="10%"> </th>							
															</tr>
														</thead>
													 </table>
													
												</div>
											</div>



											</div>

										</div>	

									</div>
								</div>
							  </div>	



							  </form>	




							  <div style="margin-left:87px;margin-top:-65px;">
								<a  href="{base_url()}documentos/cotizacion">
								   <button class="btn btn-app btn-success btn-mini">
								    <i class="icon-remove bigger-160"></i>
								    Cancelar
								   </button>
								</a>									 
							</div>	
							
							<div style="margin-left:157px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="guardarCotizacion();">
							    <i class="icon-save bigger-160"></i>
							    Guardar
							   </button>									 
							</div>	

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

	

					
	
	
	
     
</div> 


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#cotizacion').attr('class','active');

		AddFilaCotizacion(1);

		function guardarCotizacion(){
				$( "#formCotizar" ).submit();
		}
	
</script>	

{/block}
