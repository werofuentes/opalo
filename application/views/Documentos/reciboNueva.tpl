{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
	 <ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Recibos</a>
						</li>
		</ul><!--.breadcrumb-->
	</div>

	
	


				<div class="page-content">

				<div id="modal_evualuados" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  
		                  <div class="modal-body" id="contenido_modal_evaluados">
		                  </div>
		                 <div class="modal-footer">
		                 	<button type="button"  onclick="AgregarEvaluadosCheck();" class="btn btn-blue">Agregar</button>
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>


				<div id="modal_clientes" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  		
					
									<br>
					<div align="center">
					Buscar : <input id="searchClientes" type="text" value="" name="searchClientes">
					</div>
		                  <div class="modal-body" id="contenido_modal_clientes">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>


		        <div id="modal_factura_enviado" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  		
		                  <div class="modal-body" id="contenido_modal_factura_enviado">
		                  <div align="center">
		                  <IMG SRC="{base_url()}/inc/enviandoFactura.gif"  ALT="ENVIANDO">
		                  <br>
		                  Enviando factura ....
		                  </div>
		                  </div>
		              
		        </div>

		        <div id="modal_servicio" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  		
					
									<br>
					<div align="center">
					Buscar : <input id="search" type="text" value="" name="search">
					</div>
		                  <div class="modal-body" id="contenido_modal_servicio">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>


					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form name="formCotizar" id="formCotizar" action="{base_url()}documentos/guardarRecibos" method="post" enctype="multipart/form-data"/>

							<input class="span12" id="IdEmpresaSecundaria"  name="IdEmpresaSecundaria" type="hidden" value="">
							<input class="span12" id="evaluadosCheck"  name="evaluadosCheck" type="hidden" 
							value="">
							<input class="span12" id="estatusDocumento"  name="estatusDocumento" type="hidden" value="">


							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >


										<div class="tab-content">
											<div id="informacion" class="tab-pane in active">
												<div class="table-header">
													Información Recibos
												</div>
												 <br>
										
													<table width="100%" border="0">
														 <tr>
														 
											                  <td width="50%">
											    
											                  <button type="button" class="btn btn-primary" onclick="MostrarCliente();">
											                  	<i class="icon-cloud icon-only bigger-120"></i>
											                    Buscar cliente
											                  </button>
											                  </td>
			            
											                  <td width="25%" align="right"><label class="control-label" for="form-field-1"><B>Fecha:</B></label></td>
											                  <td width="25%">{$fecha}</td>
											              </tr>

											              <tr>
											                  <td width="100%" align="left">
											    				<br><br>	
											                  	Forma de pago
											                  	<br>
											                  	<select class="form-field-select-1" id="formaPago" name="formaPago">
															               <option value="" > Elige </option>
																	       <option value="01">Efectivo</option>
																	       <option value="02">Cheque normativo</option>
																	       <option value="03">Transferencia electronica de fondos</option>
																	       <option value="04">Tarjeta de Crédito</option>
																	       <option value="28">Tarjeta de Débito</option>
																	       <option value="99">Otros</option>
																	       <option value="98">No aplica</option>
									                                       
									                                 
																    </select>
											                  </td>
											                  <td width="100%" align="left">
											                  	

											                  </td>
											                 
											              </tr>

											              <tr>
											                  <td width="100%" align="left">
											    			
											                  	ultimos 4 digitos cuenta bancaria cliente
											                  	<br>
											                  	
											         
											                  	 
											                  	 <input class="span2" id="CuentaBancariaCliente"  name="CuentaBancariaCliente" type="text" placeholder="4 digitos cuenta" maxlength="4" >

											                  </td>
											                 
											              </tr>

											              <tr>
											                  <td width="100%" align="left">
											    			
											                  	Banco cliente
											                  	<br>
											                  	
											         
											                  	 
											                  	 <input class="span2" id="bancoCliente"  name="bancoCliente" type="text" placeholder="Banco cliente"  >

											                  </td>
											                 
											              </tr>

													  
														 

													</table>
													<br>
													<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa</label>
																           <input class="span12" id="Nombres"  name="Nombres" type="text" placeholder="Nombres" style='text-transform:uppercase;' readonly>
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC</label>
																           <input class="span12" id="Rfc"  name="Rfc" type="text" placeholder="RFC" style='text-transform:uppercase;' readonly>
																           <span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal</label>
																           <input class="span12" id="DomicilioFiscal"  name="DomicilioFiscal" type="text" placeholder="Domicilio fiscal" style='text-transform:uppercase;' readonly>
																           <span id="error_11" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Comentarios</label>
																          <textarea id="comentario" name="comentario" class="span12" placeholder="" ></textarea>
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>

												<div class="table-header">
													Detalle recibo
												</div>

												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>
																<th width="10%"> </th>							
															</tr>
														</thead>
													 </table>
													
												</div>
											</div>



											</div>

											<br>
											<br>
											 <button type="button" class="btn btn-primary" onclick="RelacionarEvaluados();">
											                  	<i class="icon-user icon-only bigger-120"></i>
											                    Relacionar evaluados
											                  </button>
											
											  <br><br>
											  <div class="table-header">
													Evaluados
											  </div>
											  <div id="listadoEvaluados">
													
											  
													<B>No hay evaluados relacionados al documento</B>
											
											  </div>

										</div>	

									</div>
								</div>
							  </div>	



							  </form>	




							  <div style="margin-left:87px;margin-top:-65px;">
								<a  href="{base_url()}documentos/facturacionRecibos">
								   <button class="btn btn-app btn-success btn-mini">
								    <i class="icon-remove bigger-160"></i>
								    Cancelar
								   </button>
								</a>									 
							</div>	
							
							<!--
							<div style="margin-left:157px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="convertirFactura();">
							    <i class="icon-copy bigger-160"></i>
							    Facturar
							   </button>									 
							</div>
							-->
							
							<div style="margin-left:161px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="convertirRecibo();">
							    <i class="icon-save bigger-160"></i>
							    Recibo
							   </button>									 
							</div>	
				


							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

	

					
	
	
	
     
</div> 


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#recibos').attr('class','active');

		AddFilaCotizacion(1);

	



		//AddFilaCotizacion(1);

		function convertirFactura(){
				//$( "#formCotizar" ).submit();
				var evaluadosCheck = 1;//$('#evaluadosCheck').val();
				var formaPago = $('#formaPago').val();
				var validarCliente = $('#IdEmpresaSecundaria').val();

				if(evaluadosCheck != ''){

					
					if(formaPago != ''){

						if(validarCliente != ''){

							$('#estatusDocumento').val(2);
							$('#modal_factura_enviado').modal('show');

						alertify.confirm("¿Seguro desea facturar?", function (e) {
					        if (e) {
					          	 $( "#formCotizar" ).submit();

					        } else {
					 
					        }
					    });

													
						}else{
							alertify.alert('Debe seleccionar un cliente');
						}	




					}else{
							alertify.alert('Debe relacionar una forma de pago');


					}

				


				}else{
					 alertify.alert('Debe relacionar los evaluados con la factura');

					 
				}

		}

		function convertirRecibo(){
				//$( "#formCotizar" ).submit();
				var evaluadosCheck = 1;//$('#evaluadosCheck').val();
				var validarCliente = $('#IdEmpresaSecundaria').val();

				

				if(evaluadosCheck != ''){

					if(validarCliente != ''){

						$('#estatusDocumento').val(3);
						$( "#formCotizar" ).submit();

					}else{
						 alertify.alert('Debe seleccionar un cliente');
					}


				}else{
					 alertify.alert('Debe relacionar los evaluados con el recibo');
				}

		}


		function AgregarEvaluadosCheck(){

				var valCheck = 0;
				var arraryCheck = '';
				var contador = 0;


				$("input:checkbox:checked").each(function(){
					valCheck = 1;
					
					arraryCheck += $(this).val()+','; 

					
					
				});	
				
				if(valCheck == 1){


					 $.ajax ({
		                url     : base_url + 'documentos/ponerEvaluados',
		                type    : 'POST',
		                data    : 'idEvaluados='+arraryCheck,
		                async   : false,
		                cache   : false,
		                success : function(data){
		                   if( 1 < data.length ){
		                        // Mostramos la modal
		                        $('#listadoEvaluados').html(data);
		                        $('#evaluadosCheck').val(arraryCheck+'0');
		                        $('#modal_evualuados').modal('toggle');


		                   } 
		                }
		            });

					

				}else{
						alertify.alert('Debe relacionar un evaluado');


				}
				
		}
	
</script>	

{/block}
