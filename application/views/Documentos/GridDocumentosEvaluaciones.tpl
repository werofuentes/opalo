{extends file="structure/main.tpl"}

{block name=content}

{literal}
<script>
$(document).ready(function() {
$('#search').domsearch('#sample-table-2 tbody',{criteria: ['td.Folio', 'td.NombreEvaluado', 'td.fechaRegistro', 'td.EstatusEvaluado', 'td.Facturado']})
}) 
</script>
{/literal}

      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Evaluaciones</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
				
 				<div id="modal_cancelar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  				<br>
						<div align="center">
						<form name="formCcancelarEval" id="formCcancelarEval" action="{base_url()}documentos/deleteEvaluado" method="post" enctype="multipart/form-data"/>
						 <B>¿Quiere cancelar el folio del evaluado? </B>
						 <input type="hidden" name="eliminarID" id="eliminarID" value="">

						<p>
						  <button type="button" class="btn btn-primary btn-lg" onclick="cancelEvaluados();">Si</button>
						  <button type="button" data-dismiss="modal" class="btn btn-default btn-lg">No</button>
						</p>

						</form>
						</div>

						<br>
		        </div>


				{if $smarty.session.PERMISOS.agregar_evaluacion eq 1}
		        <div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}documentos/InsertarEValuadoPrimeraVez">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
 				</div>
 				{/if}

 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">

								Buscar : <input id="search" type="text" value="" name="search">
								<div class="table-header">
									Evaluaciones
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th># Folio evaluación</th>
											<th>Nombre</th>
											<th>Fecha alta Evaluacion</th>
											<th>Fecha limite</th>
											<th>Empresa</th>
											<th>Psicologa</th>
											<th>Estatus Resultados</th>
											<th>Estatus Evaluado</th>

											<th>Facturado/Recibo</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$Evaluados key=key item=item}
										 {if $item.id neq ''}
											<tr>
												<td class="Folio"><font face="Arial">{"%05d"|sprintf:$item.id}</font></td>
												<td class="NombreEvaluado"><font face="Arial">{$item.Nombre}</font></td>
												<td class="fechaRegistro"><font face="Arial">{$item.FechaRegistro}</font></td>
												<td><font face="Arial">{$item.FechaLimite}</font></td>
												<td><font face="Arial">{$item.nombreEmpresa}</font></td>
												<td><font face="Arial">{$item.nombrePsicologa}</font></td>
												<td><font face="Arial">{$item.Descripcion}</font></td>
												<td class="EstatusEvaluado">
													<font face="Arial">{$item.EstatusEvaluado}</font>
												</td>
												<td class="Facturado">
													<font face="Arial">
														 {if $item.idFactura neq ''}

														 		{if $item.Estatus eq 3}
														 			RECIBO

														 		{/if}


														 		{if $item.Estatus eq 2}
														 			FACTURADO

														 		{/if}
														 {else}

														 	N/A
														 {/if}

													</font>
												</td>
										

												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">


													{if $smarty.session.PERMISOS.editar_evaluacion eq 1}
														<a class="green" href="{base_url()}documentos/editarEvaluado/{$item.id}">
															<i class="icon-pencil bigger-130"></i>
														</a>
													{/if}
														<a class="green" href="{base_url()}documentos/resultadosExamen/{$item.id}">
															<i class="icon-book bigger-130"></i>
														</a>

													{if $smarty.session.PERMISOS.editar_evaluacion eq 1}
														<!--
														<a class="red" href="{base_url()}documentos/deleteEvaluado/{$item.id}">
															<i class="icon-remove bigger-130"></i>
														</a>-->
														<a class="red" href="#" onclick="cancelarRegistro({$item.id});">
															<i class="icon-remove bigger-130"></i>
														</a>-
													{/if}


	
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																{if $smarty.session.PERMISOS.editar_evaluacion eq 1}
																	<a class="green" href="{base_url()}documentos/editarEvaluado/{$item.id}">
																		<i class="icon-pencil bigger-130"></i>
																	</a>
																{/if}
																	<a class="green" href="{base_url()}documentos/resultadosExamen/{$item.id}">
																		<i class="icon-book bigger-130"></i>
																	</a>

																{if $smarty.session.PERMISOS.editar_evaluacion eq 1}
																	<a class="red" href="{base_url()}documentos/deleteEvaluado/{$item.id}">
																		<i class="icon-remove bigger-130"></i>
																	</a>
																{/if}
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
      


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#evaluaciones').attr('class','active');



		function cancelarRegistro(eliminarID){

			 $('#modal_cancelar').modal('show');
			 $('#eliminarID').val(eliminarID);
		}

		function cancelEvaluados(){

					$( "#formCcancelarEval" ).submit();

		}

</script>
    









{/block}
