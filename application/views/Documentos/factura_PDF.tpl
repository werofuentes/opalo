
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link href="{base_url()}inc/css_apro/bootstrap.min.css" rel="stylesheet" />
		

		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

	

<div class="tab-content">
<table width="100%" >
	<tr>
		<td width="60%">
		 	<img src="{base_url()}/inc/logo.jpg"/>
		 	<br>
		 	<div align="center">
		 	
			Sistema Opalo BR s.a. de c.v.
			<br>
			RFC: SOB151104AS6
		 	<br>
		 	Av. Miguel Ángel #14
			Col. Real vallarta, C.P. 45020 Zapopan, Jalisco.<br>
			Tel.(33) 1594 5644<br>
			www.sistemaopalo.com.mx
			

	
			</div>
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white">Folio:</font></div> {$Documentos.id}

		</td>
	</tr>

	{if $Documentos.Estatus eq 2 }
	<tr>
		<td width="60%">
		 	
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white"> Folio fiscal:</font></div> {$Documentos.FolioFactura}
		</td>
	</tr>
		<tr>
		<td width="60%">
		 	
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white"> No. de serie del certificado del CSD:</div></font></div> {$Documentos.NoCertificacionSAT}

		</td>
	</tr>
	<tr>
		<td width="60%">
		 	
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white"> Uso CFDI:</div></font></div> {$Documentos.usoCFDI}

		</td>
	</tr>
	<tr>
		<td width="60%">
		 	
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white"> Método de pago:</div></font></div> {$Documentos.metodoPago}

		</td>
	</tr>
	{/if}
</table>



	<div id="informacion" class="tab-pane in active">
		<div class="table-header">

				Información

				{if $Documentos.Estatus eq 2 }
				 	Facturación
				{else}
					Recibo

				{/if}
		</div>
		<table width="100%" border="0">
			<tr>											 			            
			<td width="100%" align="left">
			  <B>Fecha Documento:</B>{$Documentos.Fecha}
			</td>
			</tr>

		</table>

													<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																         <B>Empresa:</B>
																           {$Documentos.NombreCliente}
																  
			
																           
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																          <B>RFC:</B>
																           {$Documentos.Rfc}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Domicilio fiscal:</B>
																           {$Documentos.DireccionCliente}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Telefono:</B>
																           {$Documentos.Telefono}
																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 

												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Comentarios:</B>
																         	{$Documentos.Comentarios}
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle 
													{if $Documentos.Estatus eq 2 }
													 	Facturación
													{else}
														Recibo

													{/if}
												</div>

												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
															
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>					
															</tr>
														
															{assign var="Subtotal" value="0"}

															{foreach from=$DocumentosDetalle key=key item=item}
															<tr>
															
																<th width="35%">{$item.Servicio}</th>
																<th width="20%">{$item.Cantidad}</th>
																<th width="20%">${$item.Precio|number_format:2:".":","}</th>
																<th width="15%">${$item.Importe|number_format:2:".":","}</th>					
															</tr>
																{$Subtotal = $Subtotal + $item.Importe}
															{/foreach}
													

														
													
													 </table>
													  <table width="100%">
													       <tr>
													       		<td colspan="2" width="52%" align="center">
													       			
													       			Este documento es una representación impresa de un CFDI
													       		</td>

													       		<td align="right" colspan="1" width="32%">
													       		{if $Documentos.Estatus eq 2 }
													       		<B>Subtotal:</B>
													       		{else}
													       		<B>Total:</B>
													       		{/if}
													       		</td>
													       		<td align="center" colspan="1" width="16%">${$Subtotal|number_format:2:".":","}</td>
													       </tr>
													       {$iva = $Subtotal * 0.16}
													       <tr>
													       		<td colspan="2" width="52%" align="center">
													       			{if $Documentos.formaPago neq ''}
													       			Forma de pago: 
																{if $Documentos.formaPago neq '98'}
													       			{$Documentos.formaPago}
																{/if}

													       			{if $Documentos.formaPago eq '01'}
													       				Efectivo
													       			{/if}

													       			{if $Documentos.formaPago eq '02'}
													       				Cheque normativo
													       			{/if}

													       			{if $Documentos.formaPago eq '03'}
													       				Transferencia electronica de fondos
													       			{/if}

													       			{if $Documentos.formaPago eq '04'}
													       				Tarjeta de Crédito
													       			{/if}

													       			{if $Documentos.formaPago eq '28'}
													       				Tarjeta de Débito
													       			{/if}

													       			{if $Documentos.formaPago eq '99'}
													       				Por definir
													       			{/if}

													       			{if $Documentos.formaPago eq '98'}
													       				No aplica
													       			{/if}
													       			
													       			
													       			{/if}
													       			

													       			{if $Documentos.CuentaBancariaCliente neq ''}
													       			cuenta 
													       			{$Documentos.CuentaBancariaCliente} {$Documentos.bancoCliente}
													       			{/if}
													       		</td>
													       		<td align="right" colspan="1" width="32%">
													       		{if $Documentos.Estatus eq 2 }
													       		<B>Iva:</B>
													       		{/if}
													       		</td>
													       		<td align="center" colspan="1" width="16%">
													       		{if $Documentos.Estatus eq 2 }
													       		${$iva|number_format:2:".":","}
													       		{/if}
													       		</td>
													       </tr>
													       {$total = $Subtotal + $iva}
													       <tr>
													       		<td colspan="2" width="52%">
													       			
													       			
													       		</td>
													       		<td align="right" colspan="1" width="32%">
													       		{if $Documentos.Estatus eq 2 }
													       		<B>Total:</B>
													       		{/if}
													       		</td>
													       		<td align="center" colspan="1" width="16%">
													       		{if $Documentos.Estatus eq 2 }
													       		${$total|number_format:2:".":","}
													       		{/if}
													       		</td>
													       </tr>
													 </table>
													
												</div>
												<br><br>
												<div style="background-color: #307ecc"><font color="white">Evaluados</font></div> 
												<table>
													{$informacionHtml}
												</table>



											</div>



	</div>


	<br>


	
	{if $Documentos.Estatus eq 2 }
	<div style="background-color: #307ecc"><font color="white">Sello Digital CFDI</font></div> 
	<div align="">
			

			{foreach from=$arrayCFDI key=key item=item}
			
				<font style="font-size: 55%;">{$item}</font>
			
			{/foreach}	
	</div>
	<div style="background-color: #307ecc"><font color="white">Sello del SAT</font></div> 
	<div align="">
			
			
				<font style="font-size: 55%;">{$Documentos.SelloSat}</font>
			
			
	</div>
	<div style="background-color: #307ecc"><font color="white">Cadena original del complemento de certificación digital del SAT</font></div> 
	<div align="">
			
			{foreach from=$arrayCompletos key=key item=item}
			
				<font style="font-size: 55%;">{$item}</font>
			
			{/foreach}	
			
			
			
			
	</div>
	{/if}


	<div style="background-color: #307ecc" align="center"><font color="white">Cuenta bancaria OPALO</font></div> 
	<div align="CENTER">
			BANCO: BANREGIO <br>
			SISTEMA OPALO BR SA DE SV
			<br>
			CTA: 134012460011<br>
			CLABE: 0583200-00001232350<br>

			
				
			
			
	</div>

</div>

