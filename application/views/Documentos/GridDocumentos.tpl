{extends file="structure/main.tpl"}

{block name=content}



      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Cotización</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
				

				{if $smarty.session.PERMISOS.agregar_cotizacion eq 1}
 				<div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}documentos/cotizacionNueva">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
 				</div>
 				{/if}


 				<div id="modal_cancelar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  				<br>
						<div align="center">
						<form name="formCcancelarEval" id="formCcancelarEval" action="{base_url()}documentos/deleteDocumentos" method="post" enctype="multipart/form-data"/>
						 <B>¿Quieres eliminar la cotizacion? </B>
						 <input type="hidden" name="eliminarID" id="eliminarID" value="">

						<p>
						  <button type="button" class="btn btn-primary btn-lg" onclick="cancelEvaluados();">Si</button>
						  <button type="button" data-dismiss="modal" class="btn btn-default btn-lg">No</button>
						</p>

						</form>
						</div>

						<br>
		        </div>



 				<div id="modal_clientes_autorizar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  
		                  <div class="modal-body" id="contenido_modal_clientes_autorizar">
		                  </div>
		                 <div class="modal-footer">
		                 	<button type="button" onclick="AutorizarCotizacionMostrar();" class="btn btn-blue">Autorizar</button>
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Cotizaciones
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th># Folio</th>
											<th>Cliente</th>

											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$Documentos key=key item=item}
										 {if $item.id neq ''}
											<tr>
												<td><font face="Arial">{$item.id}</font></td>
												<td><font face="Arial">{$item.NombreCliente}</font></td>

												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">

													{if $item.idCliente neq 0}
														<a class="green" style="cursor:pointer;">
															<i class="icon-ok bigger-130" onclick="AutorizarCotizacion({$item.id});"></i>
														</a>
													{/if}
													{if $smarty.session.PERMISOS.editar_cotizacion eq 1}

														<a class="green" href="{base_url()}documentos/EditarCotizacion/{$item.id}">
															<i class="icon-pencil bigger-130"></i>
														</a>
														<!--
														<a class="red" href="{base_url()}documentos/deleteDocumentos/{$item.id}">
															<i class="icon-trash bigger-130"></i>
														</a>-->
														<a class="red" href="#" onclick="eliminarCliente({$item.id});">
															<i class="icon-trash bigger-130"></i>
														</a>
													{/if}

															<a href="{base_url()}documentos/pdfCotizacion/{$item.id}">
															<IMG SRC="{base_url()}/inc/iconoPdf.png" width="30px" ALT="PDF">
														</a>
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
															
															 {if $item.idCliente neq 0}
															<a class="green" style="cursor:pointer;">
															<i class="icon-ok bigger-130" onclick="AutorizarCotizacion({$item.id});"></i>
															</a>
															{/if}

															{if $smarty.session.PERMISOS.editar_cotizacion eq 1}

																<a class="green" href="{base_url()}documentos/EditarCotizacion/{$item.id}">
																	<i class="icon-pencil bigger-130"></i>
																</a>
																<a class="red" href="#" onclick="eliminarCliente({$item.id});">
																	<i class="icon-trash bigger-130"></i>
																</a>
															{/if}

																	<a href="{base_url()}documentos/pdfCotizacion/{$item.id}">
																	<IMG SRC="{base_url()}/inc/iconoPdf.png" width="30px" ALT="PDF">
																</a>
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#cotizacion').attr('class','active');


		function AutorizarCotizacionMostrar(){

			var moduloSel = $('#moduloSel').val();
			if(moduloSel == ''){


					alertify.alert('Debe seleccionar a que modulo se autoriza (Recibo/Factura)');
					return false;
			}

			var idCotizacion = $('#idCotizacion').val();
			var total = $('#total').val();
			

			 $.ajax ({
                url     	: base_url + 'documentos/ActualizarAutorizacionCoti',
                type   		: 'POST',
                data    	: 'idCotizacion='+idCotizacion+'&total='+total+'&moduloSel='+moduloSel,
                dataType	: 'json',
                async  		: false,
                cache       : false,
                success     : function(data){
                    	
                		window.location.href = base_url + "documentos/cotizacion";

              		
                    	
                }
            });


		}


		function eliminarCliente(eliminarID){

			 $('#modal_cancelar').modal('show');
			 $('#eliminarID').val(eliminarID);
		}

		function cancelEvaluados(){

					$( "#formCcancelarEval" ).submit();

		}

</script>
    









{/block}
