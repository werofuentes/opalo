{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li>
			<i class="icon-briefcase"></i>
			<a href="#">Administracion</a>

			<span class="divider">
					<i class="icon-angle-right arrow-icon"></i>
			</span>
			<a href="#">Notas de credito</a>
			</li>
		</ul><!--.breadcrumb-->

	</div>
	<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable">
										<form name="formAlta" id="formAlta" action="{base_url()}documentos/guardarNota" method="post" enctype="multipart/form-data"/>
										<ul class="nav nav-tabs" id="myTab">
											<input type="hidden" name="id" id="id" value="{$Documentos.id}"  size="15"/>

											<input class="span12" id="fechaTimestamp"  name="fechaTimestamp" type="hidden" value="">

											<input class="span12" id="UUID"  name="UUID" type="hidden" value="{$Documentos.FolioFactura}">


											<input class="span12" id="rfcReceptor"  name="rfcReceptor" type="hidden" value="{$Documentos.Rfc}">

											<input class="span12" id="nombreReceptor"  name="nombreReceptor" type="hidden" value="{$Documentos.NombreCliente}">


											<li class="active">
												<a data-toggle="tab" href="#datosregistrar">
													Registrar Nota de credito
												</a>
											</li>

                                         
										</ul>

										<div class="tab-content">
										
										
										
								<div id="datosregistrar" class="tab-pane in active">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tr bgcolor="#cedee8">
								     <td colspan="6">  
									 <table width="100%" border="0" cellspacing="0" cellpadding="0">
								         <tr>
										  <td><B>Folio:</B> &nbsp;{$Documentos.id}</td>
										  <td Colspan="2"><B>Cliente:</B>&nbsp; {$Documentos.NombreCliente}</td>
										   <td><B>UUID:</B> {$Documentos.FolioFactura}</td>
										  <td colspan=""><B>Total:</B>&nbsp;${$Documentos.Total}</td>
										  <td></td>
								
										 </tr>
								     </table>	 
									 </td>
								  </tr>
								  <tr>
								     <td colspan="6"><br><br></td>
								  </tr>
								  <tr>
								     <td colspan="2" width="33%" align="center">
								     	Forma de pago: 

								     	<select class="form-field-select-1" id="formaPago" name="formaPago">
															               <option value="" > Elige </option>
																	       <option value="01">Efectivo</option>
																	       <option value="02">Cheque normativo</option>
																	       <option value="03">Transferencia electronica de fondos</option>
																	       <option value="04">Tarjeta de Crédito</option>
																	       <option value="28">Tarjeta de Débito</option>
																	       <option value="99">Otros</option>
																	       <option value="98">No aplica</option>                             
										</select>
								     </td>
								     <td colspan="2" width="33%" align="center">
								     	<br>
										Uso CFDI
									
										<select class="form-field-select-1" id="usocfdi_33" name="usocfdi_33">
											<option value="" > Elige </option>
											{foreach from=$usocfdi_33Factura key=key item=item}
											<option value="{$item.codigo}">{$item.descripcion}</option>
											{/foreach}                         
										</select>
								     </td>
								     <td colspan="2" width="33%" align="center">
								     Monto credito
								     <input type="hidden" name="montoCreditoOcult" id="montoCreditoOcult" value="{$Documentos.Total}">
								     <input type="text" name="montoCredito" id="montoCredito" value="{$Documentos.Total}">
								     </td>

								  </tr>
								  
								</table>
											    
								</div>

										
									
									
									
											
											
											
											
										</div>
										</form>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
						
							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

					  <div style="margin-left:87px;margin-top:-65px;">
								<a  href="{base_url()}documentos/cobranza">
								   <button class="btn btn-app btn-success btn-mini">
								    <i class="icon-remove bigger-160"></i>
								    Cancelar
								   </button>
								</a>									 
							</div>	
							
							
							<div style="margin-left:157px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="guardar();">
							    <i class="icon-save bigger-160"></i>
							    Guardar
							   </button>									 
							</div>	
						

</div>

<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#cobranza').attr('class','active');


			$(function() {
		$( "#fecha_recibo" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});




	});





  function guardar(){
  		 
  			var formaPago 	 = $('#formaPago').val();
  			var usocfdi_33 	 = $('#usocfdi_33').val();
  			var montoCredito 	 = $('#montoCredito').val();
  			var montoCreditoOcult 	 = $('#montoCreditoOcult').val();
  
  			

			  if(formaPago == ''){
			  			
				  alertify.alert('Debe seleccionar una forma de pago');
				  return false;
			  }


			  if(usocfdi_33 == ''){
	
			  
				  alertify.alert('Debe seleccionar una forma de pago');
				  return false;
			  }


			  if(montoCredito == ''){
	
			  
				  alertify.alert('Debe ingresar un monto');
				  return false;
			  }

				
			  if(parseFloat(montoCredito) > parseFloat(montoCreditoOcult)){

			  	  alertify.alert('El monto es mayor al total de la factura');
				  return false;
			  }


			  if(formaPago != '' && usocfdi_33 != '' && montoCredito != ''){

			  		$('#fechaTimestamp').val(Date.now());

			  		setTimeout(function(){ $( "#formAlta" ).submit(); }, 1000);
			  		//$( "#formAlta" ).submit();
			  }

  }



</script>
{/block}
