{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-briefcase"></i>
							<a href="#">Administracion</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Evaluaciones</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>

	
	


				<div class="page-content">




					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<form name="formCotizar" id="formCotizar" action="{base_url()}documentos/guardarResultados" method="post" enctype="multipart/form-data"/>

							<input id="id" name="id" class="span7" value="{$Evaluados.id}"  type="hidden" >
							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >


										<div class="tab-content">
											<div id="informacion" class="tab-pane in active">
												<table width="100%" border="0">
								                   <tr>
								                     <td align="center">
								                        FOTO <br>
								                      
								                      	 {if $Evaluados.Foto eq ''}
								                         <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="{base_url()}inc/images_apro/facebook350.jpg" />
								                        
								                          {else}	
								                          <img id="fotoCandidato" name="fotoCandidato" width="100px" height="80px" src="{base_url()}{$Evaluados.Foto}" />
								                          {/if}
								                     
								                         


	
											         </td>
											         
											         <td>
											         <table width="100%" border="0">
											              
											         	<tr>
											          		<td width="50%"><label class="control-label" for="form-field-1"><B>Tipo evaluacion:</B></label>
											          		</td>
											                <td width="50%">
											                <div id="error_certificacion">
											                	<select class="form-field-select-1" id="certificacionId" name="certificacionId" disabled>
																		 <option value="" > Elige </option>
																		 {foreach from=$Certificaciones key=key item=item}
																		 <option value="{$item.id}" {if $Evaluados.idCertificado eq $item.id} selected="selected" {/if}>{$item.Certificaciones}</option>
																		 {/foreach}
																 </select>
														 		 <span id="error_1A" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>

											              <tr>
											          		<td width="20%"><label class="control-label" for="form-field-1"><B>Empresa:</B></label>
											          		</td>
											                <td width="80%">
											                <div id="error_idEmpresa">
											                	<select class="form-field-select-1" id="idEmpresa" name="idEmpresa" disabled>
															 		<option value="" > Elige </option>
															  		{foreach from=$Empresas key=key item=item}
													                <option value="{$item.id}" {if $Evaluados.idEmpresa eq $item.id} selected="selected" {/if}>{$item.RazonSocial}</option>
														      		{/foreach}
														 		 </select>
														 		 <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>
											               <tr>
											          		<td width="20%"><label class="control-label" for="form-field-1"><B>Nombre:</B></label>
											          		</td>
											                <td width="80%">
											                <div id="error_Nombres">
															 <label class="control-label" for="form-field-1">
															 <br>
															 <input id="Nombres" name="Nombres"  class="span12" value="{$Evaluados.Nombre}" type="text" placeholder="Nombres" >
															 </label>											           
															 <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
													 		 </div> 
											                </td>
											              </tr>
											              <tr>
											          		<td width="20%"><label class="control-label" for="form-field-1"><B>Estatus general:</B></label>
											          		</td>
											                <td width="80%">
											                <div id="error_idEmpresa">
											                	<select class="form-field-select-1" id="EstatusGeneral2" name="EstatusGeneral2" disabled>
															 		<option value="" > Elige </option>
															  		{foreach from=$EstatusGeneral key=key item=item}
													                <option value="{$item.id}" {if $Evaluados.Estatus eq $item.id} selected="selected" {/if}>{$item.Descripcion}</option>
														      		{/foreach}
														 		 </select>
														 		 <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
														 	  <input id="EstatusGeneral" name="EstatusGeneral"  class="span12" value="{$Evaluados.Estatus}" type="hidden" placeholder="Nombres" >
											                </td>
											              </tr>
											              
											               <tr>
											          		<td width="20%"><label class="control-label" for="form-field-1"><B>Fecha limite:</B></label>
											          		</td>
											                <td width="80%">
											                <div id="error_fechalimite">
											                	 <input id="FechaLimite" name="FechaLimite"  class="span5" value="{$Evaluados.FechaLimite}" type="text" placeholder="Fecha limite" readonly>

														 		 <span id="error_1limite" style="display: none" class="help-inline">Campo Obligatorio </span> 
														 	 </div>
											                </td>
											              </tr>
											          </table>
											         </td>
											       </tr>
											    </table>
										
												<br>

												<div class="table-header">
													Resultados
												</div>

												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="25%">Examen</th>
																<th width="25%">Estatus</th>
																<th width="40%">Observaciones</th>
																<th width="10%"> </th>							
															</tr>
															{$htmlTr}
														</thead>
													 </table>
													
												</div>
											</div>



											</div>

										</div>	

									</div>
								</div>
							  </div>	



							  </form>	




							  <div style="margin-left:87px;margin-top:-65px;">
								<a  href="{base_url()}documentos/evaluacion">
								   <button class="btn btn-app btn-success btn-mini">
								    <i class="icon-remove bigger-160"></i>
								    Salir
								   </button>
								</a>									 
							</div>	
							
							<div style="margin-left:157px;margin-top:-65px;">
							   <button class="btn btn-app btn-success btn-mini" onclick="guardarCotizacion();">
							    <i class="icon-save bigger-160"></i>
							    Guardar
							   </button>									 
							</div>	

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->

	

					
	
	
	
     
</div> 


<script type="text/javascript">
		$('#administracion').attr('class','active open');
		$('#evaluaciones').attr('class','active');

		

		function guardarCotizacion(){
				$( "#formCotizar" ).submit();
		}


		$(function() {
		$( "#FechaLimite" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});

	



	});
	
</script>	

{/block}
