{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
					</ul><!--.breadcrumb-->
		</div> 
		<br>
		<br>
		<div class="span7">
			  <div class="row-fluid">
			  <div class="widget-box">
			  <div class="widget-header">
				<h4>Reporte Evaluaciones realizadas</h4>
			</div>
			<div class="widget-body">
			<form id="formSolicitud" name="formSolicitud" action="{base_url()}reporte/psicologareporte" method="post"/>	
			  <br><br>
			    <table width="100%">
			      <tr>
			        <td align="center">
			          <div id="error_FechaInicio">
				          <div class="controls">
				          <label class="control-label" for="form-field-1">Inicio:
						    <input id="FechaInicio" name="FechaInicio" class="span6" type="text" placeholder="Fecha Inicio">
						  </label>	
						  <span id="error_667" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			        <td align="center">
			         <div id="error_FechaFin">
				          <div class="controls">
			          <label class="control-label" for="form-field-1">Fin:
					    <input id="FechaFin" name="FechaFin" class="span6" type="text" placeholder="Fecha Fin">
					  </label>	
					   <span id="error_668" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			      </tr>
			      <tr>
			        <td align="center" colspan="1">
			          <div id="error_FechaInicio">
				          <div class="controls">
				          <label class="control-label" for="form-field-1">Psicologa(o):
						    

						    <select class="form-field-select-1" id="idPsicologa" name="idPsicologa">
									<option value="Todos" > Todos </option>
									{foreach from=$Psicologa key=key item=item}
									 <option value="{$item.id}">{$item.NombrePsicologa}</option>
									{/foreach}
						    </select>


						  </label>	

						  <span id="error_667" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>

			        <td align="center" colspan="1">
			          <div id="error_FechaInicio">
				          <div class="controls">
				          <label class="control-label" for="form-field-1">Sucursal:
						    

						    <select class="form-field-select-1" id="idSucursal" name="idSucursal">
									<option value="Todos" > Todos </option>
									{foreach from=$Sucursal key=key item=item}
									 <option value="{$item.idSucursal}">{$item.Sucursal}</option>
									{/foreach}
						    </select>


						  </label>	

						  <span id="error_667" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			       
			      </tr>
				 </table> 
				 </form>
				 </div>
				 
				</div>	
				 <div style="margin-left:549px;margin-top:3px;">
				<button class="btn btn-app btn-success btn-mini" onclick="reporte();">
				   <i class="icon-bar-chart bigger-160"></i>
						    Generar
				</button>							 
			</div>
			  </div>	  
		</div>
</div>

<script type="text/javascript">



 $(function() {
	$( "#FechaInicio" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});
	$( "#FechaFin" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});
});


		$('#reportes').attr('class','active open');
		$('#reportes_psicologa').attr('class','active');
		
		
	   function reporte(){
		
			var FechaInicio = $('#FechaInicio').val();
			var FechaFin = $('#FechaFin').val();
			
			if(FechaInicio == ''){
			  	$('#error_FechaInicio').addClass("control-group error");
			  	document.getElementById('error_667').style.display='block';
			  }else{
			   	$('#error_FechaInicio').removeClass("control-group error");
			    $('#error_FechaInicio').addClass("control-group");
			    document.getElementById('error_667').style.display='none';
			  
			  }
			  
			  if(FechaFin == ''){
			  	$('#error_FechaFin').addClass("control-group error");
			  	document.getElementById('error_668').style.display='block';
			  }else{
			   	$('#error_FechaFin').removeClass("control-group error");
			    $('#error_FechaFin').addClass("control-group");
			    document.getElementById('error_668').style.display='none';
			  
			  }
			  
			  
		
			if(FechaInicio != '' && FechaFin != ''){
			   $( "#formSolicitud" ).submit();
			}
		 		
		  
		}
		
</script>
{/block}
