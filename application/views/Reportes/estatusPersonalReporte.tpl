{extends file="structure/main.tpl"}

{block name=content}

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Estatus de personal</a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<input id="TotalCancelado" name="TotalCancelado" type="hidden" value="{$TotalCancelado}">
							<input id="TotalR1" name="TotalR1" type="hidden" value="{$TotalR1}">
							<input id="TotalR2" name="TotalR2" type="hidden" value="{$TotalR2}"> 
							<input id="TotalR3" name="TotalR3" type="hidden" value="{$TotalR3}">
							<input id="TotalEnproceso" name="TotalEnproceso" type="hidden" value="{$TotalEnproceso}">
							<input id="TotalsinResultado" name="TotalsinResultado" type="hidden" value="{$TotalsinResultado}">

							<input id="empresas" name="empresas" type="hidden" value="{$empresas}">
							<input id="Totalempresas" name="Totalempresas" type="hidden" value="{$Totalempresas}">


							<div class="row-fluid">
								<div class="span6" align="center">
								    <div id="canvas-holder" style="width:100%">
										<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>"
									</div>

								</div>
								<div class="span6">
										<div style="width: 100%">
											<div id="container2" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
										</div>
								</div>
						    </div>


							<div class="row-fluid">
								<div class="table-header">
									Estatus personal
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>ID sistema</th>
											<th>Nombre personal</th>
											<th>Empresa</th>
											<th>Fecha registro</th>
											<th>Estatus</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$resultados key=key item=item}
										 {if $item.id neq ''}
											<tr>
												<td><font face="Arial">{$item.id}</font></td>
												<td><font face="Arial">{$item.Nombre}</font></td>
												<td><font face="Arial">{$item.nombreEmpresa}</font></td>
												<td><font face="Arial">{$item.FechaRegistro}</font></td>
												<td><font face="Arial">{$item.descEstatus}</font></td>

											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                    	
						<div >
							<a  href="{base_url()}reporte/EstatusPersonal">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_estatus_personal').attr('class','active');
		
		
		$(function () {

			TotalCancelado = $('#TotalCancelado').attr('value');
			TotalR1 = $('#TotalR1').attr('value');
			TotalR2 = $('#TotalR2').attr('value');
			TotalR3 = $('#TotalR3').attr('value');
			TotalEnproceso = $('#TotalEnproceso').attr('value');
			TotalsinResultado = $('#TotalsinResultado').attr('value');
				
			    $('#container').highcharts({
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: 'Estatus personal evaluado'
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: ["Sin Resultado", 'R1 Aceptable', 'R3 No Aceptable', 'R2 Aceptable reservas', 'En proceso', 'Cancelado'],
			            title: {
			                text: null
			            }
			        },
			  legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'top',
			            floating: true,
			            borderWidth: 1,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: true
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'No. Evaluados',
			            data: [parseInt(TotalsinResultado), parseInt(TotalR1), parseInt(TotalR3), parseInt(TotalR2), parseInt(TotalEnproceso), parseInt(TotalCancelado)]
			        }]
			    });
			});





		$(function () {

			empresas = $('#empresas').attr('value');

				res = empresas.split(",");
			

			Totalempresas = $('#Totalempresas').attr('value');
			
			res2 = Totalempresas.split(",");
			for(var i=0; i<res2.length; i++) { res2[i] = parseInt(res2[i]); } 

		
			    $('#container2').highcharts({
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: 'Empresa/Evaluados'
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: res,
			            title: {
			                text: null
			            }
			        },
			  legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'top',
			            floating: true,
			            borderWidth: 1,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: true
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'No. Evaluados',
			            data: res2
			        }]
			    });
			});
		
</script>
{/block}

