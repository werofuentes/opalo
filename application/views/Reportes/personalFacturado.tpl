{extends file="structure/main.tpl"}

{block name=content}

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Evaluados facturados y no facturados </a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->



							<div class="row-fluid">
								<div class="table-header">
									Evaluados facturados y no facturados
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Cliente</th>
											<th>Evaluados</th>
											<th>Evaluados facturados</th>
											<th>Evaluados no facturados</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$resultados key=key item=item}
										 {if $item.id neq ''}
											<tr>
												<td><font face="Arial">{$item.NombreCliente}</font></td>
												<td><font face="Arial">{$item.totalEvaluados}</font></td>
												<td><font face="Arial">{$item.NumeroFacturados}</font></td>
												<td><font face="Arial">{$item.NumeroNoFacturados}</font></td>

											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                    	
						<div >
							<a  href="{base_url()}reporte/pendientesEvaluados">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_pendientes_evaluados').attr('class','active');
		
		
		$(function () {

			TotalCancelado = $('#TotalCancelado').attr('value');
			TotalR1 = $('#TotalR1').attr('value');
			TotalR2 = $('#TotalR2').attr('value');
			TotalR3 = $('#TotalR3').attr('value');
			TotalEnproceso = $('#TotalEnproceso').attr('value');
			TotalsinResultado = $('#TotalsinResultado').attr('value');
				
			    $('#container').highcharts({
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: 'Estatus personal evaluado'
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: ["Sin Resultado", 'R1 Aceptable', 'R3 No Aceptable', 'R2 Aceptable reservas', 'En proceso', 'Cancelado'],
			            title: {
			                text: null
			            }
			        },
			  legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'top',
			            floating: true,
			            borderWidth: 1,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: true
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'No. Evaluados',
			            data: [parseInt(TotalsinResultado), parseInt(TotalR1), parseInt(TotalR3), parseInt(TotalR2), parseInt(TotalEnproceso), parseInt(TotalCancelado)]
			        }]
			    });
			});





		$(function () {

			empresas = $('#empresas').attr('value');

				res = empresas.split(",");
			

			Totalempresas = $('#Totalempresas').attr('value');
			
			res2 = Totalempresas.split(",");
			for(var i=0; i<res2.length; i++) { res2[i] = parseInt(res2[i]); } 

		
			    $('#container2').highcharts({
			        chart: {
			            type: 'bar'
			        },
			        title: {
			            text: 'Empresa/Evaluados'
			        },
			        subtitle: {
			            text: ''
			        },
			        xAxis: {
			            categories: res,
			            title: {
			                text: null
			            }
			        },
			  legend: {
			            layout: 'vertical',
			            align: 'right',
			            verticalAlign: 'top',
			            floating: true,
			            borderWidth: 1,
			            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
			            shadow: true
			        },
			        credits: {
			            enabled: false
			        },
			        series: [{
			            name: 'No. Evaluados',
			            data: res2
			        }]
			    });
			});
		
</script>
{/block}

