{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Freelance</a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Freelances
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											
											<th>Nombre freelance</th>
											<th>Total Evaluaciones</th>
											<th>Total Pago</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$resultadoTotal key=key item=item}
										 {if $item.id neq ''}
											<tr>
			
												<td><font face="Arial">{$item.nombreFreelance}</font></td>
												<td><font face="Arial">{$item.totalEvaluados}</font></td>

												<td><font face="Arial">{$item.totalEvaluados * $item.Costo}</font></td>

											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                    	
						<div >
							<a  href="{base_url()}reporte/Freelance">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_freelance').attr('class','active');
		
		

		
</script>
{/block}

