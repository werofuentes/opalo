{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Freelance</a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Freelances
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											
											<th>Nombre Psicologo(a)</th>
											<th>Total Evaluaciones</th>
											<th>Total Pago</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$resultadoTotal key=key item=item}
										 {if $item.id neq ''} 
											<tr style="cursor: pointer;" onclick="mostrarRenglon({$key});">

												<td><font face="Arial">{$item.NombrePsicologa}</font></td>
												<td><font face="Arial">{$item.totalEvaluados}</font></td>

												<td><font face="Arial">{$item.totalEvaluados * $item.Costo}</font></td>

											</tr>

											<tr id="Renglon_{$key}" style="display:none;">

												<td colspan="3" align="center">
														<table width="100%">
																<tr>
																  <th> Tipo de evaluacion</th>
																  <th> Total </th>
																</tr>

																 {foreach from=$item.DetalleCertidicacion key=key2 item=item2}
																<tr>
																<td>{$item2.CertifiNombre}</td>
																<td>{$item2.totalCerti}</td>
																</tr>
																{/foreach}

														</table>

												</td>

											</tr>

										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                    	
						<div >
							<a  href="{base_url()}reporte/Psicologa">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_psicologa').attr('class','active');
		


function mostrarRenglon(renglon){

		  if( $('#Renglon_'+renglon).is(":visible") ) {
		    $('#Renglon_'+renglon).hide(); 


		  } else {
		    $('#Renglon_'+renglon).show();
		  }
}

		
</script>
{/block}

