{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Movimiento personal</a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Movimientos
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>ID sistema</th>
											<th>Codigo empleado</th>
											<th>Nombre</th>
											<th>Empresa movimiento</th>
											<th>Fecha movimiento</th>
											<th>Fecha comentario</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$resultados key=key item=item}
										 {if $item.id neq ''}
											<tr>
												<td><font face="Arial">{$item.id}</font></td>
												<td><font face="Arial">{$item.CodigoEmpleado}</font></td>
												<td><font face="Arial">{$item.Nombre}</font></td>
												<td><font face="Arial">{$item.Empresa}</font></td>
												<td><font face="Arial">{$item.FechaRegistro}</font></td>
												<td><font face="Arial">{$item.Comentarios}</font></td>

											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                    	
						<div >
							<a  href="{base_url()}reporte/Movimientos">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_movimientos_personal').attr('class','active');
		
		

		
</script>
{/block}

