{extends file="structure/main.tpl"}

{block name=content}

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Estatus de personal</a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<input id="TotalCancelado" name="TotalCancelado" type="hidden" value="{$TotalCancelado}">
							<input id="TotalR1" name="TotalR1" type="hidden" value="{$TotalR1}">
							<input id="TotalR2" name="TotalR2" type="hidden" value="{$TotalR2}"> 
							<input id="TotalR3" name="TotalR3" type="hidden" value="{$TotalR3}">
							<input id="TotalEnproceso" name="TotalEnproceso" type="hidden" value="{$TotalEnproceso}">
							<input id="TotalsinResultado" name="TotalsinResultado" type="hidden" value="{$TotalsinResultado}">

							<input id="empresas" name="empresas" type="hidden" value="{$empresas}">
							<input id="Totalempresas" name="Totalempresas" type="hidden" value="{$Totalempresas}">


							<div class="row-fluid">
								<div class="span6" align="center">
								   <table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Total de evaluaciones</th>
										</tr>
									</thead>

									<tbody>

										<tr>
											<td><font face="Arial">{$NumeroTotalEvaluaciones}</font></td>
										</tr>
									
									</tbody>
								</table>

								</div>
								<div class="span6">
									<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">Total por sucursal</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$totalSucursales key=key item=item}

											<tr>
												<td><font face="Arial">{$item.Sucursal}</font></td>
												<td align="center"><font face="Arial">{$item.total}</font></td>
											</tr>
									
										{/foreach}
									</tbody>
								</table>


								</div>
						    </div>

						    <div class="row-fluid">
								<div class="span6" align="center">
								   <table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">Total de Psicologas</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$totalPsicologas key=key item=item}

											<tr>
												<td><font face="Arial">{$item.NombrePsicologa}</font></td>
												<td align="center"> <font face="Arial">{$item.total}</font></td>
											</tr>
									
										{/foreach}
									</tbody>
								</table>

								</div>
								<div class="span6">
									<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">Total trabajadoras sociales</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$totalFreelance key=key item=item}

											<tr>
												<td><font face="Arial">{$item.Nombre}</font></td>
												<td align="center"><font face="Arial">{$item.total}</font></td>
											</tr>
									
										{/foreach}
									</tbody>
								</table>


								</div>
						    </div>


						    <div class="row-fluid">
								<div class="span6" align="center">
								   <table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">Total por tipo de evaluaciones</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$tipoEvaluaciones key=key item=item}

											<tr>
												<td><font face="Arial">{$item.Certificaciones}</font></td>
												<td align="center"> <font face="Arial">{$item.total}</font></td>
											</tr>
									
										{/foreach}
									</tbody>
								</table>

								</div>
								<div class="span6">
									<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th colspan="2">Total estatus</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$estatusEvaluado key=key item=item}

											<tr>
												<td><font face="Arial">{$item.Descripcion}</font></td>
												<td align="center"><font face="Arial">{$item.total}</font></td>
											</tr>
									
										{/foreach}
									</tbody>
								</table>


								</div>
						    </div>


                    	
						<div >
							<a  href="{base_url()}reporte/evaluacionesRealizadas">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_evaluaciones_realizadas').attr('class','active');
		

		
</script>
{/block}

