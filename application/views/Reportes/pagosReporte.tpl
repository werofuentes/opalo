{extends file="structure/main.tpl"}

{block name=content}

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Pagos</a>
						</li>
					</ul><!--.breadcrumb-->
		</div> 

		<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<input id="TotalRecibos" name="TotalRecibos" type="hidden" value="{$TotalRecibos}">
							<input id="TotalFacturas" name="TotalFacturas" type="hidden" value="{$TotalFacturas}">



							<input id="Totalporpagar" name="Totalporpagar" type="hidden" value="{$Totalporpagar}">
							<input id="Totalpagar" name="Totalpagar" type="hidden" value="{$Totalpagar}">
							<input id="Totalsinpagar" name="Totalsinpagar" type="hidden" value="{$Totalsinpagar}">



							


							<div class="row-fluid">
								<div class="span6" align="center">
								    <div id="canvas-holder" style="width:100%">
										<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
									</div>

								</div>

								<div class="span6" align="center">
								    <div id="canvas-holder" style="width:100%">
										<div id="container2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
									</div>

								</div>

						    </div>


							<div class="row-fluid">
								<div class="table-header">
									Pagos
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Folio</th>
											<th>Cliente/Empresa</th>
											<th>Fecha registro</th>
											<th>Documento</th>
											<th>Total importe</th>
											<th>Por pagar</th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$resultado key=key item=item}
										 {if $item.id neq ''}
											<tr style="cursor: pointer;">
												<td onclick="mostrarRenglon({$item.id})"><font face="Arial">{$item.id}</font></td>
												<td onclick="mostrarRenglon({$item.id})"><font face="Arial">{$item.NombreCliente}</font></td>
												<td onclick="mostrarRenglon({$item.id})"><font face="Arial">{$item.Fecha}</font></td>
												<td onclick="mostrarRenglon({$item.id})"><font face="Arial">
													{if $item.Estatus eq 2}
														Factura
													 {/if}

													 {if $item.Estatus eq 3}
														Recibo
													 {/if}
												</font>

												</td>

												{if $item.Estatus eq 2}

												<td onclick="mostrarRenglon({$item.id})"><font face="Arial">{$item.Total|number_format:2:".":","}</font></td>
												
												<td onclick="mostrarRenglon({$item.id})"><font face="Arial">{$item.PorPagar|number_format:2:".":","}</font></td>
												{/if}

												{if $item.Estatus eq 3}
													<td onclick="mostrarRenglon({$item.id})"><font face="Arial">
														
														{math assign="Total" equation="x/y" x=$item.Total y=1.16 format="%.2f"}

														{$Total}
													</font></td>
												
													<td onclick="mostrarRenglon({$item.id})"><font face="Arial">{$item.PorPagar|number_format:2:".":","}</font></td>

												 {/if}
											</tr>

											
											<tr id="detalle_{$item.id}" style="display: none">


												<td colspan="5">
													  <table id="sample-table-2" class="table table-striped table-bordered table-hover">
													  	<tr>
															<th>No recibo</th>
															<th>Fecha abono</th>
															<th>Monto</th>
														</tr>
														{foreach from=$item.PagosDocumento key=key2 item=item2}
														<tr>
															<td><font face="Arial">{$item2.NoRecibo}</font></td>
															<td><font face="Arial">{$item2.Fecha}</font></td>
															<td><font face="Arial">{$item2.Monto}</font></td>
															

														</tr>
														{/foreach}

													  </table>
												</td>
												
											</tr>



										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                    	
						<div >
							<a  href="{base_url()}reporte/Pagos">
							   <button class="btn btn-app btn-success btn-mini">
							    <i class="icon-remove bigger-160"></i>
							    Salir
							   </button>
							</a>									 
						</div>	
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->


		
</div>

<script type="text/javascript">

		$('#reportes').attr('class','active open');
		$('#reportes_pagos').attr('class','active');


		function mostrarRenglon(renglon){

		  if( $('#detalle_'+renglon).is(":visible") ) {
		    $('#detalle_'+renglon).hide(); 


		  } else {
		    $('#detalle_'+renglon).show();
		  }

}
		






		$(function () {
			    $('#container').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false,
			            type: 'pie'
			        },
			        title: {
			            text: 'Pagos'
			        },
			      
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                   
			                    style: {
			                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{
			            name: 'Total',
			            colorByPoint: true,
			            data: [{
			                name: 'Pagado parcialmente',
			                y: parseInt($('#Totalporpagar').attr('value'))
			            }, {
			                name: 'Pagado',
			                y: parseInt($('#Totalpagar').attr('value')),
			                sliced: true,
			                selected: true
			            }, {
			                name: 'Sin pago',
			                y: parseInt($('#Totalsinpagar').attr('value')),
			                sliced: true,
			                selected: true
			            }]
			        }]
			    });
			});



		$(function () {
			    $('#container2').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false,
			            type: 'pie'
			        },
			        title: {
			            text: 'Documentos'
			        },
			      
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
			                   
			                    style: {
			                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
			                    }
			                }
			            }
			        },
			        series: [{
			            name: 'Total',
			            colorByPoint: true,
			            data: [{
			                name: 'Factuas',
			                y: parseInt($('#TotalFacturas').attr('value'))
			            }, {
			                name: 'Recibo',
			                y: parseInt($('#TotalRecibos').attr('value')),
			                sliced: true,
			                selected: true
			            }]
			        }]
			    });
			});
		
</script>
{/block}

