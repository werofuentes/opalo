{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
		<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-bar-chart"></i>
							<a href="#">Reportes</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
					</ul><!--.breadcrumb-->
		</div> 
		<br>
		<br>
		<div class="span7">
			  <div class="row-fluid">
			  <div class="widget-box">
			  <div class="widget-header">
				<h4>Reporte pagos</h4>
			</div>
			<div class="widget-body">
			<form id="formSolicitud" name="formSolicitud" action="{base_url()}reporte/pagosreporte" method="post"/>	
			  <br><br>
			    <table width="100%">
			      <tr>
			        <td align="center">
			          <div id="error_FechaInicio">
				          <div class="controls">
				          <label class="control-label" for="form-field-1">Inicio:
						    <input id="FechaInicio" name="FechaInicio" class="span6" type="text" placeholder="Fecha Inicio">
						  </label>	
						  <span id="error_667" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			        <td align="center">
			         <div id="error_FechaFin">
				          <div class="controls">
			          <label class="control-label" for="form-field-1">Fin:
					    <input id="FechaFin" name="FechaFin" class="span6" type="text" placeholder="Fecha Fin">
					  </label>	
					   <span id="error_668" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			      </tr>
			      <tr>
			        <td align="center">
			          <div id="error_FechaInicio">
				          <div class="controls">
				          <label class="control-label" for="form-field-1">Empresa:
						    

						    <select class="form-field-select-1" id="idEmpresa" name="idEmpresa">
									<option value="Todas" > Todas </option>
									{foreach from=$Empresas key=key item=item}
									 <option value="{$item.id}">{$item.Nombre}</option>
									{/foreach}
						    </select>


						  </label>	

						  <span id="error_667" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			        <td align="center">
			         <div id="error_FechaFin">
				          <div class="controls">
			          <label class="control-label" for="form-field-1">Documentos:
					    

					    	<select class="form-field-select-1" id="tipoDocumento" name="tipoDocumento">
									<option value="Todos" > Todos </option>
									
									<option value="2" > Facturas </option>
									<option value="3" > Recibos </option>

						    </select>
					  </label>	
					   <span id="error_668" style="display: none" class="help-inline">Campo Obligatorio </span> 
						  
						  </div>
					  </div>
			        </td>
			      </tr>
			      <tr>
			        <td align="center" colspan="2">
			          <div id="error_FechaInicio">
				          <div class="controls">
				          <label class="control-label" for="form-field-1">Estatus pagos:
						    

						    <select class="form-field-select-1" id="estatusPago" name="estatusPago">
									<option value="Todas" > Todos </option>
								
									 <option value="pendientes">Pendiente</option>
									 <option value="pagadas">Pagadas</option>
							
						    </select>


						  </label>	


						  
						  </div>
					  </div>
			        </td>
			        
			      </tr>
				 </table> 
				 </form>
				 </div>
				 
				</div>	
				 <div style="margin-left:549px;margin-top:3px;">
				<button class="btn btn-app btn-success btn-mini" onclick="reporte();">
				   <i class="icon-bar-chart bigger-160"></i>
						    Generar
				</button>							 
			</div>
			  </div>	  
		</div>
</div>

<script type="text/javascript">



 $(function() {
	$( "#FechaInicio" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});
	$( "#FechaFin" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	   
      		}
		});
});


		$('#reportes').attr('class','active open');
		$('#reportes_pagos').attr('class','active');
		
		
	   function reporte(){
		
			var FechaInicio = $('#FechaInicio').val();
			var FechaFin = $('#FechaFin').val();
			
			if(FechaInicio == ''){
			  	$('#error_FechaInicio').addClass("control-group error");
			  	document.getElementById('error_667').style.display='block';
			  }else{
			   	$('#error_FechaInicio').removeClass("control-group error");
			    $('#error_FechaInicio').addClass("control-group");
			    document.getElementById('error_667').style.display='none';
			  
			  }
			  
			  if(FechaFin == ''){
			  	$('#error_FechaFin').addClass("control-group error");
			  	document.getElementById('error_668').style.display='block';
			  }else{
			   	$('#error_FechaFin').removeClass("control-group error");
			    $('#error_FechaFin').addClass("control-group");
			    document.getElementById('error_668').style.display='none';
			  
			  }
			  
			  
		
			if(FechaInicio != '' && FechaFin != ''){
			   $( "#formSolicitud" ).submit();
			}
		 		
		  
		}
		
</script>
{/block}
