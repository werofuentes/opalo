{extends file="structure/main.tpl"}

{block name=content}

      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-group"></i>
							<a href="#">Empleados</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>


					</ul><!--.breadcrumb-->

				</div>
			   
			   {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2}	
				<div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}employees/formEmployeesAdd">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
 				</div>
 				 {/if}	
 				
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Empleados
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>CODIGO</th>
											<th>NOMBRE</th>
						
											<th>EMAIL</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$empleados key=key item=item}
										 {if $item.idEmpleado neq ''}
											<tr>
												<td>{$item.idEmpleado}</td>
												<td>{$item.Nombres} {$item.ApellidoPaterno} {$item.ApellidoMaterno} </td>
										
												<td>{$item.Mail}</td>
												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
	
														<a class="green" href="{base_url()}employees/formEditEmployees/{$item.idEmpleado}">
															<i class="icon-pencil bigger-130"></i>
														</a>
	                                                    {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2}
														<a class="red" href="{base_url()}employees/deleteEmpleado/{$item.idEmpleado}">
															<i class="icon-trash bigger-130"></i>
														</a>
														{/if}
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																<a href="{base_url()}employees/formEditEmployees/{$item.idEmpleado}" class="tooltip-success" data-rel="tooltip" title="Edit">
																	<span class="green">
																		<i class="icon-edit bigger-120"></i>
																	</span>
																</a>
															</li>
                                                            {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2}
															<li>
																<a href="{base_url()}employees/deleteEmpleado/{$item.idEmpleado}" class="tooltip-error" data-rel="tooltip" title="Delete">
																	<span class="red">
																		<i class="icon-trash bigger-120"></i>
																	</span>
																</a>
															</li>
															{/if}	
														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>

							{$NumPaginas}

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#empleados').attr('class','active');

</script>
    


{/block}
