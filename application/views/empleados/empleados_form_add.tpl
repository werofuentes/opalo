{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
	 <ul class="breadcrumb">
		<li>
			<i class="icon-group"></i>
			<a href="#">Empleados</a>
			<span class="divider">
				<i class="icon-angle-right arrow-icon"></i>
			</span>
		</li>
		<li>
			<a href="#">Nuevo Empleado</a>
		</li>
		</ul><!--.breadcrumb-->
	</div>
	
					     
	 <span id="error_general" style="display: none;color:red;margin-left:20px;" class="help-inline"></span> 	   

 <form id="formEmpleados" name="formEmpleados" action="{base_url()}employees/addEmployees" method="post"/>			

				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >
										<ul class="nav nav-tabs" id="myTab">
											<li class="active" id="empleado-tab">
												<a data-toggle="tab" href="#investigador">
													EMPLEADO
												</a>
											</li>

											<li id="empleado-tab2">
												<a data-toggle="tab" href="#medios_contacto">
													MEDIOS DE CONTACTO
												
												</a>
											</li>
											
											<li id="empleado-tab3">
												<a data-toggle="tab" href="#direccion">
													DIRECCION
												
												</a>
											</li>
											
											<li id="empleado-tab4">
												<a data-toggle="tab" href="#referencias">
													REFERENCIAS
												
												</a>
											</li>
											
											<li id="empleado-tab5">
												<a data-toggle="tab" href="#cuenta">
													CUENTA BANCARIA
												
												</a>
											</li>
											
											<li id="empleado-tab6">
												<a data-toggle="tab" href="#credencial">
													DATOS CREDENCIAL
												
												</a>
											</li>
											
											<li id="empleado-tab7">
												<a data-toggle="tab" href="#munApoyo">
													MUNICIPIO DE APOYO
												
												</a>
											</li>

											
										</ul>

										<div class="tab-content">
											<div id="investigador" class="tab-pane in active">
												
												<table width="100%" border="0">
												  <tr>
												      <td>
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">NOMBRE(S)</label>
																           <input id="Nombres"  name="Nombres" type="text" placeholder="Nombres" style='text-transform:uppercase;'>
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												      <td>
												       <div id="error_ApellidoPaterno" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">APELLIDO PATERNO</label>
																           <input id="ApellidoPaterno" name="ApellidoPaterno" type="text" placeholder="ApellidoPaterno" style='text-transform:uppercase;'>
																           <span id="error_1_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												      <td>
												        <div id="error_ApellidoMaterno" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">APELLIDO MATERNO</label>
																           <input id="ApellidoMaterno" name="ApellidoMaterno" type="text" placeholder="ApellidoMaterno" style='text-transform:uppercase;'>
																           <span id="error_1_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td> 
												  </tr>  
												  <tr>
												      <td>
												         <div id="error_edad" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">EDAD</label>
																           <input id="edad" name="edad" type="text" placeholder="Edad" style='text-transform:uppercase;'>
																           <span id="error_1_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div>
												      </td>
												      <td>
												        <div id="error_tipo_sexo" class="control-group">
															      <label class="control-label"  for="form-field-1">SEXO</label>
															      	<div class="controls">
															      	<select class="form-field-select-1" id="tipo_sexo" name="tipo_sexo">
															               <option value="" > Elige </option>
									                                       <option value="1">MASCULINO</option>
									                                       <option value="2">FEMENINO</option>
									                                 
									                                 
																    </select>
																    <span id="error_667" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
														 </div>
												      </td>
												      <td>
												       <div id="error_estadocivil" class="control-group">
																     <div >   
															              <label class="control-label" for="form-field-1">ESTADO CIVIL</label>
																           <select class="form-field-select-1" id="estadocivil" name="estadocivil" >
																	          <option value="" > Elige </option>
																			   {foreach from=$Estadocivil key=key item=item}
											                                       	<option value="{$item.idEdoCivil}">{$item.Descripcion}</option>
											                                   {/foreach}
																		    </select>
																           <span id="error_2_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																       </div>
														    </div>
												      </td>
												  </tr>
												  
												  
												   <tr>
												      <td>
												          <div id="error_escolaridad" class="control-group">
																     <div >   
															              <label class="control-label" for="form-field-1">ESCOLARIDAD</label>
																           <select class="form-field-select-1" id="escolaridad" name="escolaridad" >
																	          <option value="" > Elige </option>
																			   {foreach from=$Escolaridad key=key item=item}
											                                       	<option value="{$item.idEscolaridad}">{$item.Descripcion}</option>
											                                   {/foreach}
																		    </select>
																           <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																       </div>
														    </div> 	
												      </td>
												      <td>
												       <div id="error_tipo_usuario" class="control-group">
															      <label class="control-label"  for="form-field-1">TIPO DE USUARIO</label>
															      	<div class="controls">
															      	<select class="form-field-select-1" id="tipo_usuario" name="tipo_usuario" onchange="mostrarFreelance();">
															               <option value="" > Elige </option>
																	
																	       <option value="2">RH</option>
																	       <option value="3">COORDINADOR</option>
									                                       <option value="4">EJECUTIVO DE CUENTA</option>
									                                       <option value="5">INVESTIGADOR</option>
									                                 
									                                 
																    </select>
																    <span id="error_66" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
															   </div>
															   <!--
													    	<div id="freelance" class="control-group" style="display: none">
												               <div class="controls">
																<label>
																	<input type="checkbox" name="freelance" id="freelance" /> 
																	<span class="lbl">FREELANCE</span>
																</label>
											                   </div>
	                                                      </div> 	-->
												      </td>
												      <td>
												       <div class="control-group">
															      <label class="control-label"  for="form-field-1">ACTIVO</label>
															      	<div class="controls">
															      	<select class="form-field-select-1" id="activo" name="activo">
															          <option value="0" > NO </option>
															          <option value="1"> SI </option>
															          
																    </select>
																    <span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
															</div>
												      </td>
												  </tr>
												</table>
	                                                     Foto (Maximo 82px x 82px y formato png):<br><br> 
	                                                         <input name="fotos" id="fotos" type="file" value="" >
															 <input name="descripcion" id="descripcion" type="hidden" value="" >
	                                                         <br>
	                                                         <input type="text" id="conteo_fotos" name="conteo_fotos" value="{$TOT_IMG}" readonly="readonly" style="width: 20px; text-align: center;"/> de 1</p>
														    <span id="err_foto"></span>
														    <div id="conte_fotos" style="overflow:hidden; float:right; width: 375px; height: 50px; text-align: center; margin: 21px 10px 0 0;">
															</div>
														    <input type="hidden" id="contador_fotos" name="contador_fotos" value="{$TOT_IMG}"/>
														    <br>
														  
															<div id="fotos_box" style="margin-left:75px;margin-bottom: 15px; overflow: hidden; width: 200px;">
																<ul id="fotos_slide" class="ul_drag">
																     {$IMAGENES}
																</ul>
	                                                   
	                                                       </div> 
											</div>

											<div id="medios_contacto" class="tab-pane">
											 <table width="100%" border="0"> 
											   <tr>
											     <td>
											       <div id="error_tel_local" class="control-group">
													<div class="row-fluid">
														<label for="form-field-mask-2">
															TELEFONO LOCAL
															<small class="text-warning">xxxx-xxxx</small>
														</label>

														<div class="input-prepend">
															<span class="add-on">
																<i class="icon-phone"></i>
															</span>
															<input class="span12" type="text" id="TelCasa" name="TelCasa" style='text-transform:uppercase;'/>
														    <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
														    
														</div>
													</div>
												   </div>
											     </td>
											     <td>
											       <div id="error_tel_celular" class="control-group">
												   <div class="row-fluid">
														<label for="form-field-mask-2">
															TELEFONO CELULAR
														</label>

														<div class="input-prepend">
															<span class="add-on">
																<i class="icon-phone"></i>
															</span>
															<input class="span12"  type="text" id="Cel" name="Cel" style='text-transform:uppercase;'/>
														    <span id="error_4" style="display: none" class="help-inline">Campo Obligatorio </span> 
														    
														</div>
													</div>
													</div>
											     </td>
											     <td>
													<div id="error_tel_recados" class="control-group">
													<div class="row-fluid">
														<label for="form-field-mask-2">
															TELEFONO RECADOS
														</label>

														<div class="input-prepend">
															<span class="add-on">
																<i class="icon-phone"></i>
															</span>
															<input class="span12"  type="text" id="OtroTel" name="OtroTel" style='text-transform:uppercase;'/>
														    <span id="error_5" style="display: none" class="help-inline">Campo Obligatorio </span> 
														    
														</div>
													</div>
												    </div>
											     </td>
											   </tr>
											   
											   <tr>
											     <td>
											     <div id="error_email" class="control-group">
														<label class="control-label" for="form-field-1">EMAIL</label>
														<div class="controls">
															<input  type="text" class="span8"   id="Mail" name="Mail" onKeyUp="validarCorreo(this.value);">
														    <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
														   
														</div>
												   </div>
											     </td>
											     <!--
											     <td>
											      <div class="control-group">
														<label class="control-label" for="form-field-1">SKYPE</label>
														<div class="controls">
															<input  type="text" class="span5"  id="facebook" name="facebook">														  
														</div>
												   </div>
											     </td>-->
											     <!--
											     <td>
											       <div class="row-fluid">
													<label for="form-field-8">OBSERVACIONES</label>
													<textarea  class="span12" id="observaciones" name="observaciones" style='text-transform:uppercase;'></textarea>
												</div>
											     </td>-->
											   </tr>
											 </table>
											</div>



											<div id="direccion" class="tab-pane">
											<table width="100%" border="0">
											 <tr>
											  <td>
											  <div id="error_dir_estado" class="control-group">
														<!--<label class="control-label" for="form-field-1">ESTADO</label>
														<div class="controls">
															<input id="Estado" class="span8"  name="Estado" type="text" placeholder="Estado" style='text-transform:uppercase;'>
															 <span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 
														</div>-->
													    <label class="control-label" for="form-field-1">ESTADO</label>
														  <select class="form-field-select-1" id="Estado" name="Estado" onchange="TipoMunicipio(this.value);">
															 <option value="" > Elige </option>
															  {foreach from=$Estados key=key item=item}
											                         	<option value="{$item.idEstado}">{$item.Nombre}</option>
											                  {/foreach}
														  </select>
													<span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
														
												</div>
											  <td>
											  <td>
											  <div id="error_dir_municipio" class="control-group">
														<!--<label class="control-label" for="form-field-1">MUNICIPIO</label>
														<div class="controls">
															<input id="Municipio" class="span8"  name="Municipio" type="text" placeholder="Municipio" style='text-transform:uppercase;'>
														     <span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 
														</div>-->
														<label class="control-label" for="form-field-1">MUNICIPIO</label>
														<select class="form-field-select-1" id="Municipio" name="Municipio">
															 <option value="" > Selecciona Estado </option>
														</select>
														<span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 
												</div>
											  <td>
											  <td>
											  <div id="error_dir_calle_num" class="control-group">
														<label class="control-label" for="form-field-1">CALLE Y NUMERO</label>
														<div class="controls">
															<input id="Domicilio1" class="span12"  name="Domicilio1" type="text" placeholder="Calle y Numero" style='text-transform:uppercase;'>
															<span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span> 
														</div>
												</div>
											  <td>
											 </tr>
											 <tr>
											  <td>
											  <div id="error_dir_colonia" class="control-group">
														<label class="control-label" for="form-field-1">COLONIA</label>
														<div class="controls">
															<input id="Colonia" class="span12"  name="Colonia" type="text" placeholder="Colonia" style='text-transform:uppercase;'>
															<span id="error_10" style="display: none" class="help-inline">Campo Obligatorio </span> 
														</div>
													</div>
											  <td>
											  <td>
											   <div id="error_dir_cp" class="control-group">
														<label class="control-label" for="form-field-1">CODIGO POSTAL</label>
														<div class="controls">
															<input id="CodigoPost" class="span4"  name="CodigoPost" type="text" placeholder="Codigo Postal" style='text-transform:uppercase;'>
															<span id="error_11" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
												</div>
											  <td>
											  <td><td>
											 </tr>
											</table>		
											</div>


											<div id="referencias" class="tab-pane">
                                                <table width="100%" border="0"> 
                                                 <tr>
                                                   <td>
                                                    <div id="error_ref_nombre" class="control-group">
														<label class="control-label" for="form-field-1">NOMPRE</label>
														<div class="controls">
															<input id="ref_nombre" name="ref_nombre" type="text" placeholder="Nombre Completo" style='text-transform:uppercase;'>
															<span id="error_12" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
                                                   </td>
                                                   <td>
                                                   <div id="error_ref_apellidoP" class="control-group">
														<label class="control-label" for="form-field-1">APELLIDO PATERNO</label>
														<div class="controls">
															<input id="ApellidoPaternoRPE" name="ApellidoPaternoRPE" type="text" placeholder="Apellido Paterno" style='text-transform:uppercase;'>
															<span id="error_123" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
                                                   </td>
                                                   <td>
                                                   <div id="error_ref_apellidoM" class="control-group">
														<label class="control-label" for="form-field-1">APELLIDO MATERNO</label>
														<div class="controls">
															<input id="ApellidoMaternoRPE" name="ApellidoMaternoRPE" type="text" placeholder="Apellido Materno" style='text-transform:uppercase;'>
															<span id="error_124" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
                                                   </td>
                                                 </tr>
                                                 <tr>
                                                   <td>
                                                   <div id="error_ref_parentesco" class="control-group">
														<label class="control-label" for="form-field-1">PARENTESCO</label>
														<div class="controls">
								
															 <select class="form-field-select-1" id="ref_parentesco" name="ref_parentesco" >
																	<option value="" > Elige </option>
																	{foreach from=$Parentesco key=key item=item}
											                            	<option value="{$item.idParentesco}">{$item.Descripcion}</option>
											                         {/foreach}
															  </select>
															
															
															<span id="error_13" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
                                                   </td>
                                                   <td>
                                                    <div id="error_ref_tiempo" class="control-group">
														<label class="control-label" for="form-field-1">TIEMPO DE CONOCERSE</label>
														<div class="controls">
															<input id="ref_tiempo" name="ref_tiempo" type="text" placeholder="Tiempo de Conocerse" style='text-transform:uppercase;'>
															<span id="error_14" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
                                                   </td>
                                                   <td>
                                                   <div id="error_ref_tel_casa" class="control-group">
												    <div class="row-fluid">
														<label for="form-field-mask-2">
															TELEFONO DE CASA
														</label>

														<div class="input-prepend">
															<span class="add-on">
																<i class="icon-phone"></i>
															</span>
															<input class="span8"  type="text" id="ref_tel_casa" name="ref_tel_casa" style='text-transform:uppercase;'/>
															<span id="error_15" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
													</div>
                                                   </td>
                                                 </tr> 
                                                 <tr>
                                                  <td>
                                                  <div id="error_ref_cel" class="control-group">
													 <div class="row-fluid">
														<label for="form-field-mask-2">
															TELEFONO CELULAR
														</label>

														<div class="input-prepend">
															<span class="add-on">
																<i class="icon-phone"></i>
															</span>
															<input class="span8"  type="text" id="ref_cel" name="ref_cel" style='text-transform:uppercase;'/>
															<span id="error_16" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													</div>
													</div>
                                                  </td>
                                                  <td></td>
                                                  <td></td>
                                                 </tr>  
                                                </table>
											</div>
											
											
											<div id="cuenta" class="tab-pane">
                                               <table width="100%" border="0"> 
                                                <tr>
                                                 <td>
                                                  <div id="error_banc_cuentahabiente" class="control-group">
														<label class="control-label" for="form-field-1">CUENTAHABIENTE</label>
														<div class="controls">
															<input id="banc_cuentahabiente" name="banc_cuentahabiente" type="text" placeholder="Cuentahabiente" style='text-transform:uppercase;'>
														</div>
														<span id="error_cuenta1" style="display: none" class="help-inline">Campo Obligatorio </span>
														
													</div>
                                                 </td>
                                                 <td>
                                                  <div id="error_banc_banco" class="control-group">
														<label class="control-label" for="form-field-1">BANCO</label>
														<div class="controls">
														 <select class="form-field-select-1" id="banc_banco" name="banc_banco" >
																	<option value="" > Elige </option>
																	{foreach from=$Bancos key=key item=item}
											                            	<option value="{$item.idBancos}">{$item.Descripcion}</option>
											                         {/foreach}
															  </select>
														</div>
														<span id="error_cuenta2" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
                                                 </td>
                                                 <td>
                                                   <div id="error_banc_num_sucursal" class="control-group">
														<label class="control-label" for="form-field-1">SUCURSAL</label>
														<div class="controls">
															<input id="banc_num_sucursal" name="banc_num_sucursal" type="text" placeholder="Sucursal" style='text-transform:uppercase;'>
														</div>
														<span id="error_cuenta3" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
                                                 </td>
                                                </tr>
                                                <tr>
                                                 <td>
                                                   <div  id="error_banc_num_cuenta" class="control-group">
														<label class="control-label" for="form-field-1">NUMERO DE CUENTA</label>
														<div class="controls">
															<input id="banc_num_cuenta" name="banc_num_cuenta" type="text" placeholder="Numero de cuenta" style='text-transform:uppercase;'>
														</div>
														<span id="error_cuenta4" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
                                                 </td>
                                                 <td>
                                                  <div id="error_banc_clabeinterbancaria" class="control-group">
														<label class="control-label" for="form-field-1">CLABE INTERBANCARIA</label>
														<div class="controls">
															<input id="banc_clabeinterbancaria" name="banc_clabeinterbancaria" type="text" placeholder="Clabe Interbancaria" style='text-transform:uppercase;'>
														</div>
														<span id="error_cuenta5" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
                                                 </td>
                                                 <td>
                                                 	<div id="error_banc_numero_tarjeta" class="control-group">
														<label class="control-label" for="form-field-1">NUMERO DE TARJETA</label>
														<div class="controls">
															<input id="banc_numero_tarjeta" name="banc_numero_tarjeta" type="text" placeholder="Numero de Tarjeta" style='text-transform:uppercase;'>
														</div>
														<span id="error_cuenta6" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
                                                 </td>
                                               </table>
											</div>
											
											<div id="credencial" class="tab-pane">
	                                             <table width="100%" border="0"> 
	                                               <tr>
	                                                 <td>
	                                                   <div id="error_cred_num_seguro" class="control-group">
														<label class="control-label" for="form-field-1">NUMERO DE SEGURO SOCIAL</label>
														<div class="controls">
															<input id="cred_num_seguro" name="cred_num_seguro" type="text" placeholder="Numero de Seguro Social" style='text-transform:uppercase;'>
														</div>
														<span id="error_credencial1" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
	                                                 </td>
	                                                 <td>
	                                                 <div id="error_cred_curp" class="control-group">
														<label class="control-label" for="form-field-1">CURP</label>
														<div class="controls">
															<input id="cred_curp" name="cred_curp" type="text" placeholder="Curp">
														</div>
														<span id="error_credencial2" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
	                                                 </td>
	                                                 <td>
	                                                  <div id="error_cred_curp" id="error_cred_sangre" class="control-group">
														<label class="control-label" for="form-field-1">TIPO DE SANGRE</label>
														<div class="controls">
														    <select class="form-field-select-1" id="cred_sangre" name="cred_sangre" >
																	<option value="" > Elige </option>
																	{foreach from=$gruposangre key=key item=item}
											                            	<option value="{$item.idGrupoSanguineo}">{$item.Descripcion}</option>
											                         {/foreach}
															  </select>
														</div>
														<span id="error_credencial3" style="display: none" class="help-inline">Campo Obligatorio </span>
													</div>
	                                                 </td>
	                                               </tr>
	                                               <tr>
	                                                 <td>
	                                                  <div  class="row-fluid">
														<label for="form-field-8">ALERGIAS</label>
														<textarea  class="span12" id="cred_alergias" name="cred_alergias"></textarea>
														</div>
	                                                 </td>
	                                                 <td>
														<div id="error_cred_nom_emergencia" class="control-group">
															<label class="control-label" for="form-field-1">NOMBRE DE EMERGENCIA</label>
															<div class="controls">
																<input id="cred_nom_emergencia" name="cred_nom_emergencia" type="text" placeholder="Nombre de Emergencias" style='text-transform:uppercase;'>
															</div>
															<span id="error_credencial4" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
													
	                                                 </td>
	                                                 <td>
														<div id="error_cred_num_emergencia" class="control-group">
															<label class="control-label" for="form-field-1">NUMERO DE EMERGENCIA</label>
															<div class="controls">
																<input id="cred_num_emergencia" name="cred_num_emergencia" type="text" placeholder="Numero de Emergencias" style='text-transform:uppercase;'>
															</div>
															<span id="error_credencial5" style="display: none" class="help-inline">Campo Obligatorio </span>
														</div>
	                                                 </td>
	                                               </tr>
	                                             </table>
											</div>
											
											
											
											<div id="munApoyo" class="tab-pane">
	                                            <table width="100%" border="0"> 
	                                             <tr>
	                                              <td width="40%" align="center">
	                                               MUNICIPIOS DONDE PUEDES APOYAR SIN VIÁTICOS
	                                              </td>
	                                              <td width="40%" align="center">
	                                               MUNICIPIOS DONDE PUEDES APOYAR CON VIÁTICOS
	                                              </td>
	                                              <td width="20%" align="center">
	                                               COSTO
	                                              </td>
	                                             </tr>
	                                             <tr>
	                                             <td width="40%" align="center">
													<input id="mun_sin_viaticos1" name="mun_sin_viaticos1" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                              </td>
	                                              <td width="40%" align="center">
	                                                <input id="mun_con_viaticos1" name="mun_con_viaticos1" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                                
	                                              </td>
	                                              <td width="20%" align="center">
	                                                 <input id="costo_viatico1" name="costo_viatico1" class="span6" type="text" placeholder="$$$" style='text-transform:uppercase;'>
	                                              </td>
	                                             </tr>
	                                             <tr>
	                                               <td colspan="3"><br></td>
	                                             </tr>
	                                             <tr>
	                                             <td width="40%" align="center">
													<input id="mun_sin_viaticos2" name="mun_sin_viaticos2" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                              </td>
	                                              <td width="40%" align="center">
	                                                <input id="mun_con_viaticos2" name="mun_con_viaticos2" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                                
	                                              </td>
	                                              <td width="20%" align="center">
	                                                 <input id="costo_viatico2" name="costo_viatico2" class="span6" type="text" placeholder="$$$" style='text-transform:uppercase;'>
	                                              </td>
	                                             </tr>
	                                             <tr>
	                                               <td colspan="3"><br></td>
	                                             </tr>
	                                             <tr>
	                                             <td width="40%" align="center">
													<input id="mun_sin_viaticos3" name="mun_sin_viaticos3" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                              </td>
	                                              <td width="40%" align="center">
	                                                <input id="mun_con_viaticos3" name="mun_con_viaticos3" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                                
	                                              </td>
	                                              <td width="20%" align="center">
	                                                 <input id="costo_viatico3" name="costo_viatico3" class="span6" type="text" placeholder="$$$" style='text-transform:uppercase;'>
	                                              </td>
	                                             </tr>
	                                              <tr>
	                                               <td colspan="3"><br></td>
	                                             </tr>
	                                              <tr>
	                                             <td width="40%" align="center">
													<input id="mun_sin_viaticos4" name="mun_sin_viaticos4" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                              </td>
	                                              <td width="40%" align="center">
	                                                <input id="mun_con_viaticos4" name="mun_con_viaticos4" class="span12" type="text" placeholder="" style='text-transform:uppercase;'>	
	                                                
	                                              </td>
	                                              <td width="20%" align="center">
	                                                 <input id="costo_viatico4" name="costo_viatico4" class="span6" type="text" placeholder="$$$" style='text-transform:uppercase;'>
	                                              </td>
	                                             </tr>
	                                             <tr>
	                                               <td colspan="3"><br><br></td>
	                                             </tr>
	                                             <tr>
	                                              <td colspan="3" width="100%" >
	                                               DISPOSICIÓN DE DÍAS Y HORARIOS PARA APLICAR ESTUDIOS
	                                              </td>
	                                             </tr>
	                                             <tr>
	                                               <td colspan="8"><br></td>
	                                             </tr>

	                                              {foreach from=$dias key=key item=item}
	                                              <tr>
	                                               <td width="10%">
	                                                <div  class="control-group" >
												               <div class="controls">
																<label>
																	<input type="checkbox" name="dias[]" id="dias[]" value="{$item.idDiasSemana}" /> 
																	<span class="lbl">{$item.Nombre}</span>
																	
																</label>
											                   </div>
											                   <input  id="hora-{$item.idDiasSemana}" name="hora-{$item.idDiasSemana}" class="span8" type="text" value="" placeholder="{$horaActual}">
	                                                      </div> 	
	                                               </td>
	                                                 </tr>
	                                               {/foreach}
	                                             
	                                            </table>
											</div>
	
										</div>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
							

							
					

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
</form>

	<div style="margin-left:857px;margin-top:-65px;">
		<a  href="{base_url()}employees">
		   <button class="btn btn-app btn-success btn-mini">
		    <i class="icon-remove bigger-160"></i>
		    Cancelar
		   </button>
		</a>									 
	</div>	
	
	<div style="margin-left:935px;margin-top:-65px;">
	   <button class="btn btn-app btn-success btn-mini" onclick="guardarUser();">
	    <i class="icon-save bigger-160"></i>
	    Guardar
	   </button>									 
	</div>	
     
</div> 


<script type="text/javascript">

$(function() {
		$( "#edad" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1950:2014",
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	      EncontrarEdad1(textoFecha);	
      		}
		});
	});

function mostrarFreelance(){
  
  var tipo_usuario = $('#tipo_usuario').val();
  
  
  if(tipo_usuario == 5){
     document.getElementById('freelance').style.display='block';
  }else{
     document.getElementById('freelance').style.display='none';
  }
  
}

function guardarUser(){
 

  var nombre = $('#Nombres').val();
  var ApellidoPaterno = $('#ApellidoPaterno').val();
  var ApellidoMaterno = $('#ApellidoMaterno').val();
  var escolaridad = $('#escolaridad').val();
  var estadocivil = $('#estadocivil').val();
  var edad = $('#edad').val();
  var tipo_sexo = $('#tipo_sexo').val();
  var tel_local = $('#TelCasa').val();
  var tel_celular = $('#Cel').val();
  var tel_recados = $('#OtroTel').val();
  var email = $('#Mail').val();
  var tipo_usuario = $('#tipo_usuario').val();
  
 
  
 
  /////
   var dir_estado = $('#Estado').val();
   var dir_municipio = $('#Municipio').val();
   var dir_calle_num = $('#Domicilio1').val();
   var dir_colonia = $('#Colonia').val();
   var dir_cp = $('#CodigoPost').val();
   
   /////
   var ref_nombre = $('#ref_nombre').val();
   var ApellidoPaternoRPE = $('#ApellidoPaternoRPE').val();
   var ApellidoMaternoRPE = $('#ApellidoMaternoRPE').val();
   var ref_parentesco = $('#ref_parentesco').val();
   var ref_tiempo = $('#ref_tiempo').val();
   var ref_tel_casa = $('#ref_tel_casa').val();
   var ref_cel = $('#ref_cel').val();
   

  
  if(nombre == ''){
  			
  			$('#empleado-tab').attr('class','active');
			$('#empleado-tab2').attr('class','');
			$('#empleado-tab3').attr('class','');
			$('#empleado-tab4').attr('class','');
			$('#empleado-tab5').attr('class','');
			$('#empleado-tab6').attr('class','');
			$('#empleado-tab7').attr('class','');
			
			
			$('#investigador').attr('class','tab-pane active');
			$('#medios_contacto').attr('class','tab-pane');
			$('#direccion').attr('class','tab-pane');
			$('#referencias').attr('class','tab-pane');
			$('#cuenta').attr('class','tab-pane');
			$('#credencial').attr('class','tab-pane');
			$('#munApoyo').attr('class','tab-pane');
			
			$('#Nombres').focus();
  
  
  	$('#error_nombre').addClass("control-group error");
  	document.getElementById('error_1').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_nombre').removeClass("control-group error");
    $('#error_nombre').addClass("control-group");
    document.getElementById('error_1').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
   if(ApellidoPaterno == ''){
   
   			$('#empleado-tab').attr('class','active');
			$('#empleado-tab2').attr('class','');
			$('#empleado-tab3').attr('class','');
			$('#empleado-tab4').attr('class','');
			$('#empleado-tab5').attr('class','');
			$('#empleado-tab6').attr('class','');
			$('#empleado-tab7').attr('class','');
			
			
			$('#investigador').attr('class','tab-pane active');
			$('#medios_contacto').attr('class','tab-pane');
			$('#direccion').attr('class','tab-pane');
			$('#referencias').attr('class','tab-pane');
			$('#cuenta').attr('class','tab-pane');
			$('#credencial').attr('class','tab-pane');
			$('#munApoyo').attr('class','tab-pane');
			
			$('#ApellidoPaterno').focus();
   
   
  	$('#error_ApellidoPaterno').addClass("control-group error");
  	document.getElementById('error_1_1').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  			return 1;
  }else{
   	$('#error_ApellidoPaterno').removeClass("control-group error");
    $('#error_ApellidoPaterno').addClass("control-group");
    document.getElementById('error_1_1').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(ApellidoMaterno == ''){
  
  		
  		$('#empleado-tab').attr('class','active');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane active');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ApellidoMaterno').focus();
  
  
  
  	$('#error_ApellidoMaterno').addClass("control-group error");
  	document.getElementById('error_1_2').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ApellidoMaterno').removeClass("control-group error");
    $('#error_ApellidoMaterno').addClass("control-group");
    document.getElementById('error_1_2').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  if(edad == ''){
  
  		$('#empleado-tab').attr('class','active');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane active');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#edad').focus();
  
  	$('#error_edad').addClass("control-group error");
  	document.getElementById('error_1_3').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_edad').removeClass("control-group error");
    $('#error_edad').addClass("control-group");
    document.getElementById('error_1_3').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  if(tipo_sexo == ''){
  
  		$('#empleado-tab').attr('class','active');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane active');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#tipo_sexo').focus();
  
  	$('#error_tipo_sexo').addClass("control-group error");
  	document.getElementById('error_667').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_tipo_sexo').removeClass("control-group error");
    $('#error_tipo_sexo').addClass("control-group");
    document.getElementById('error_667').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  
  if(tipo_usuario == ''){
  
  		$('#empleado-tab').attr('class','active');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane active');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#tipo_usuario').focus();
  
  	$('#error_tipo_usuario').addClass("control-group error");
  	document.getElementById('error_66').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_tipo_usuario').removeClass("control-group error");
    $('#error_tipo_usuario').addClass("control-group");
    document.getElementById('error_66').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  
  if(escolaridad == ''){
  
  		$('#empleado-tab').attr('class','active');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane active');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#escolaridad').focus();
  
  	$('#error_escolaridad').addClass("control-group error");
  	document.getElementById('error_2').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_escolaridad').removeClass("control-group error");
    $('#error_escolaridad').addClass("control-group");
    document.getElementById('error_2').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  
  if(estadocivil == ''){
  
  		$('#empleado-tab').attr('class','active');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane active');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#estadocivil').focus();
  
  	$('#error_estadocivil').addClass("control-group error");
  	document.getElementById('error_2_2').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_estadocivil').removeClass("control-group error");
    $('#error_estadocivil').addClass("control-group");
    document.getElementById('error_2_2').style.display='none';
        document.getElementById('error_general').style.display='none';
  }

  
  if(tel_local == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','active');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane active');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#tel_local').focus();
  
  
  	$('#error_tel_local').addClass("control-group error");
  	document.getElementById('error_3').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_tel_local').removeClass("control-group error");
    $('#error_tel_local').addClass("control-group");
    document.getElementById('error_3').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  
  if(tel_celular == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','active');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane active');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#tel_celular').focus();
  
  	$('#error_tel_celular').addClass("control-group error");
  	document.getElementById('error_4').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_tel_celular').removeClass("control-group error");
    $('#error_tel_celular').addClass("control-group");
    document.getElementById('error_4').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  /*
  if(tel_recados == ''){
  	$('#error_tel_recados').addClass("control-group error");
  	document.getElementById('error_5').style.display='block';
  	document.getElementById('error_general').style.display='block';
  }else{
   	$('#error_tel_recados').removeClass("control-group error");
    $('#error_tel_recados').addClass("control-group");
    document.getElementById('error_5').style.display='none';
        document.getElementById('error_general').style.display='none';
  }  
  */
  
  if(email == ''){
  		
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','active');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane active');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#email').focus();
  
  
  	$('#error_email').addClass("control-group error");
  	document.getElementById('error_6').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_email').removeClass("control-group error");
    $('#error_email').addClass("control-group");
    document.getElementById('error_6').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  
  if(dir_estado == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','active');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane active');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#dir_estado').focus();
  
  	$('#error_dir_estado').addClass("control-group error");
  	document.getElementById('error_7').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_dir_estado').removeClass("control-group error");
    $('#error_dir_estado').addClass("control-group");
    document.getElementById('error_7').style.display='none';
        document.getElementById('error_general').style.display='none';
  }  
  
  
  if(dir_municipio == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','active');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane active');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#Municipio').focus();
  
  	$('#error_dir_municipio').addClass("control-group error");
  	document.getElementById('error_8').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_dir_municipio').removeClass("control-group error");
    $('#error_dir_municipio').addClass("control-group");
    document.getElementById('error_8').style.display='none';
        document.getElementById('error_general').style.display='none';
  } 
  
  
  if(dir_calle_num == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','active');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane active');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#Domicilio1').focus();
  
  
  	$('#error_dir_calle_num').addClass("control-group error");
  	document.getElementById('error_9').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_dir_calle_num').removeClass("control-group error");
    $('#error_dir_calle_num').addClass("control-group");
    document.getElementById('error_9').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  if(dir_colonia == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','active');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane active');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#Colonia').focus();
  
  	$('#error_dir_colonia').addClass("control-group error");
  	document.getElementById('error_10').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_dir_colonia').removeClass("control-group error");
    $('#error_dir_colonia').addClass("control-group");
    document.getElementById('error_10').style.display='none';
        document.getElementById('error_general').style.display='none';
  } 
  
  
  if(dir_cp == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','active');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane active');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#CodigoPost').focus();
  
  	$('#error_dir_cp').addClass("control-group error");
  	document.getElementById('error_11').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_dir_cp').removeClass("control-group error");
    $('#error_dir_cp').addClass("control-group");
    document.getElementById('error_11').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  
  if(ref_nombre == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ref_nombre').focus();
  
  
  	$('#error_ref_nombre').addClass("control-group error");
  	document.getElementById('error_12').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_nombre').removeClass("control-group error");
    $('#error_ref_nombre').addClass("control-group");
    document.getElementById('error_12').style.display='none';
        document.getElementById('error_general').style.display='none';
  } 
  
  if(ApellidoMaternoRPE == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ApellidoMaternoRPE').focus();
  
  
  	$('#error_ref_apellidoM').addClass("control-group error");
  	document.getElementById('error_123').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_apellidoM').removeClass("control-group error");
    $('#error_ref_apellidoM').addClass("control-group");
    document.getElementById('error_123').style.display='none';
        document.getElementById('error_general').style.display='none';
  }   
  
  if(ApellidoPaternoRPE == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ApellidoPaternoRPE').focus();
  
  	$('#error_ref_apellidoP').addClass("control-group error");
  	document.getElementById('error_124').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_apellidoP').removeClass("control-group error");
    $('#error_ref_apellidoP').addClass("control-group");
    document.getElementById('error_124').style.display='none';
        document.getElementById('error_general').style.display='none';
  } 
  
  
  if(ref_parentesco == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ref_parentesco').focus();
  
  
  	$('#error_ref_parentesco').addClass("control-group error");
  	document.getElementById('error_13').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_parentesco').removeClass("control-group error");
    $('#error_ref_parentesco').addClass("control-group");
    document.getElementById('error_13').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  
  if(ref_tiempo == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ref_tiempo').focus();
  
  
  	$('#error_ref_tiempo').addClass("control-group error");
  	document.getElementById('error_14').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_tiempo').removeClass("control-group error");
    $('#error_ref_tiempo').addClass("control-group");
    document.getElementById('error_14').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  if(ref_tel_casa == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ref_tel_casa').focus();
  
  
  	$('#error_ref_tel_casa').addClass("control-group error");
  	document.getElementById('error_15').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_tel_casa').removeClass("control-group error");
    $('#error_ref_tel_casa').addClass("control-group");
    document.getElementById('error_15').style.display='none';
        document.getElementById('error_general').style.display='none';
  }
  
  
  if(ref_cel == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','active');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane active');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#ref_cel').focus();
  
  
  	$('#error_ref_cel').addClass("control-group error");
  	document.getElementById('error_16').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_ref_cel').removeClass("control-group error");
    $('#error_ref_cel').addClass("control-group");
    document.getElementById('error_16').style.display='none';
        document.getElementById('error_general').style.display='none';
  }  
  
  
  
  
     ////
   var banc_cuentahabiente = $('#banc_cuentahabiente').val();
   var banc_banco = $('#banc_banco').val();
   var banc_num_sucursal = $('#banc_num_sucursal').val();
   var banc_num_cuenta = $('#banc_num_cuenta').val();
   var banc_clabeinterbancaria = $('#banc_clabeinterbancaria').val();
   var banc_numero_tarjeta = $('#banc_numero_tarjeta').val();
  
  if(banc_cuentahabiente == ''){
   
   		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','active');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane active');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#banc_cuentahabiente').focus();
   
   
  	$('#error_banc_cuentahabiente').addClass("control-group error");
  	document.getElementById('error_cuenta1').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_banc_cuentahabiente').removeClass("control-group error");
    $('#error_banc_cuentahabiente').addClass("control-group");
    document.getElementById('error_cuenta1').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(banc_banco == ''){
  		
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','active');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane active');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#banc_banco').focus();
  
  
  	$('#error_banc_banco').addClass("control-group error");
  	document.getElementById('error_cuenta2').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_banc_banco').removeClass("control-group error");
    $('#error_banc_banco').addClass("control-group");
    document.getElementById('error_cuenta2').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(banc_num_sucursal == ''){
   
   		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','active');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane active');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#banc_num_sucursal').focus();
   
   
  	$('#error_banc_num_sucursal').addClass("control-group error");
  	document.getElementById('error_cuenta3').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_banc_num_sucursal').removeClass("control-group error");
    $('#error_banc_num_sucursal').addClass("control-group");
    document.getElementById('error_cuenta3').style.display='none';
    document.getElementById('error_general').style.display='none';
  } 
  
  if(banc_num_cuenta == ''){
   
   		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','active');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane active');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#banc_num_cuenta').focus();
   
   
  	$('#error_banc_num_cuenta').addClass("control-group error");
  	document.getElementById('error_cuenta4').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_banc_num_cuenta').removeClass("control-group error");
    $('#error_banc_num_cuenta').addClass("control-group");
    document.getElementById('error_cuenta4').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
 if(banc_clabeinterbancaria == ''){
   
   		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','active');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane active');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#banc_clabeinterbancaria').focus();
   
   
  	$('#error_banc_clabeinterbancaria').addClass("control-group error");
  	document.getElementById('error_cuenta5').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_banc_clabeinterbancaria').removeClass("control-group error");
    $('#error_banc_clabeinterbancaria').addClass("control-group");
    document.getElementById('error_cuenta5').style.display='none';
    document.getElementById('error_general').style.display='none';
  }  
  
  
  if(banc_numero_tarjeta == ''){
   
   		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','active');
		$('#empleado-tab6').attr('class','');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane active');
		$('#credencial').attr('class','tab-pane');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#banc_numero_tarjeta').focus();
   
   
  	$('#error_banc_numero_tarjeta').addClass("control-group error");
  	document.getElementById('error_cuenta6').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_banc_numero_tarjeta').removeClass("control-group error");
    $('#error_banc_numero_tarjeta').addClass("control-group");
    document.getElementById('error_cuenta6').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
   
   
   
  
  var cred_num_seguro = $('#cred_num_seguro').val();
  var cred_curp = $('#cred_curp').val();
  var cred_sangre = $('#cred_sangre').val();
  var cred_nom_emergencia = $('#cred_nom_emergencia').val();
  var cred_num_emergencia = $('#cred_num_emergencia').val();
  
  if(cred_num_seguro == ''){
   
   		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','active');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane active');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#cred_num_seguro').focus();
   
  	$('#error_cred_num_seguro').addClass("control-group error");
  	document.getElementById('error_credencial1').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_cred_num_seguro').removeClass("control-group error");
    $('#error_cred_num_seguro').addClass("control-group");
    document.getElementById('error_credencial1').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(cred_curp == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','active');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane active');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#cred_curp').focus();
  
  
  	$('#error_cred_curp').addClass("control-group error");
  	document.getElementById('error_credencial2').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_cred_curp').removeClass("control-group error");
    $('#error_cred_curp').addClass("control-group");
    document.getElementById('error_credencial2').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(cred_sangre == ''){
  
  		
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','active');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane active');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#cred_sangre').focus();
  
  
  	$('#error_cred_sangre').addClass("control-group error");
  	document.getElementById('error_credencial3').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  }else{
   	$('#error_cred_sangre').removeClass("control-group error");
    $('#error_cred_sangre').addClass("control-group");
    document.getElementById('error_credencial3').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(cred_nom_emergencia == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','active');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane active');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#cred_nom_emergencia').focus();
  
  
  	$('#error_cred_nom_emergencia').addClass("control-group error");
  	document.getElementById('error_credencial4').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_cred_nom_emergencia').removeClass("control-group error");
    $('#error_cred_nom_emergencia').addClass("control-group");
    document.getElementById('error_credencial4').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
  
  if(cred_num_emergencia == ''){
  
  		$('#empleado-tab').attr('class','');
		$('#empleado-tab2').attr('class','');
		$('#empleado-tab3').attr('class','');
		$('#empleado-tab4').attr('class','');
		$('#empleado-tab5').attr('class','');
		$('#empleado-tab6').attr('class','active');
		$('#empleado-tab7').attr('class','');
		
		
		$('#investigador').attr('class','tab-pane');
		$('#medios_contacto').attr('class','tab-pane');
		$('#direccion').attr('class','tab-pane');
		$('#referencias').attr('class','tab-pane');
		$('#cuenta').attr('class','tab-pane');
		$('#credencial').attr('class','tab-pane active');
		$('#munApoyo').attr('class','tab-pane');
		
		$('#cred_num_emergencia').focus();
  
  	$('#error_cred_num_emergencia').addClass("control-group error");
  	document.getElementById('error_credencial5').style.display='block';
  	document.getElementById('error_general').style.display='block';
  	
  		return 1;
  	
  }else{
   	$('#error_cred_num_emergencia').removeClass("control-group error");
    $('#error_cred_nom_emergencia').addClass("control-group");
    document.getElementById('error_credencial5').style.display='none';
    document.getElementById('error_general').style.display='none';
  }
 
  
  
   if (nombre != '' && cred_num_emergencia != '' && cred_nom_emergencia != '' && cred_sangre != '' && cred_curp != '' && cred_num_seguro != '' && banc_numero_tarjeta != '' && banc_clabeinterbancaria != '' && banc_num_cuenta != '' && banc_num_sucursal != '' && banc_banco != '' && banc_cuentahabiente != '' && ApellidoPaterno != '' && ApellidoMaterno != '' && edad != '' &&  tipo_sexo != ''  && escolaridad != '' && estadocivil != '' && tel_local != '' && tel_celular != '' && email != '' && dir_estado != '' && dir_municipio != '' && dir_calle_num != '' && dir_colonia != '' && dir_cp != '' && ref_nombre != '' && ref_parentesco != ''  && ref_tiempo != '' && ref_tel_casa != '' && ref_cel != ''  ){
 
        $( "#formEmpleados" ).submit();
   }



}


		$('#empleados').attr('class','active');

</script> 
{/block}
