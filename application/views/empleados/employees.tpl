{extends file="structure/main.tpl"}

{block name=content}
<style>
#searchform {
	    width: 280px;
	  height:30px;
	    padding: 8px;
	  margin:-44px 150px 11px 91px;
	    background: #ccc;
	    border-radius: 4px;
	    box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 2px 0 rgba(255,255,255,.5);
	}
	 
	#searchform input {
	    width: 190px;
	  height:20px;
	    padding: 5px;
	    float: left;
	    border: 0;
	    background: #eee;
	    border-radius: 3px 0 0 3px;
	  box-shadow: 0 1px 1px rgba(0,0,0,.4) inset;
	}
	 
	#searchform input:focus {
	    outline: 0;
	  height:21px;
	    background: #fff;
	    box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
	}
	 
	#searchform input:-webkit-input-placeholder {
	   color: #999;
	   font-weight: normal;
	   font-style: italic;
	}
	 
	#searchform input:-moz-placeholder {
	    color: #999;
	    font-weight: normal;
	    font-style: italic;
	}
	 
	#searchform input:-ms-input-placeholder {
	    color: #999;
	    font-weight: normal;
	    font-style: italic;
	}   
	 
	#searchform button {
	    position: relative;
	    float: right;
	    border: 0;
	    cursor: pointer;
	    height: 30px;
	    width: 80px;
	    font-size:15px;
	    color: #fff;
	    background: #438cdb;
	    border-radius: 0 3px 3px 0;
	    text-shadow: 0 -1px 0 rgba(0, 0 ,0, .3);
	}  
	 
	#searchform button:hover{
	    background: #2672e0;
	}
	 
	#searchform button:active,
	#searchform button:focus{
	    background: #2672e0;
	}
	 
	#searchform button:before { /* flecha */
	    content: '';
	    position: absolute;
	    border-width: 8px 8px 8px 0;
	    border-style: solid solid solid none;
	    border-color: transparent #438cdb transparent;
	    top: 8px;
	    left: -5px;
	}
	 
	#searchform button:hover:before{
	    border-right-color: #2672e0;
	}
	 
	#searchform button:focus:before{
	    border-right-color: #2672e0;
} 
</style>

  
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-group"></i>
							<a href="#">Empleados</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>


					</ul><!--.breadcrumb-->

				</div>
			   <div style="margin-left:30px;margin-top:15px;">
			   {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2}	
				<div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}employees/formEmployeesAdd">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
				<form id="searchform" name="searchform" action="{base_url()}employees" method="post">
				     <input type="text" id="palabra_clave" name="palabra_clave" placeholder="Buscar aqu&iacute;..." >
				     <button type="submit">Buscar</button>
				 </form> 
				</div>
				{else}
 				<div style="margin-left:30px;margin-top:66px;">
 				<form id="searchform" name="searchform" action="{base_url()}employees" method="post">
				     <input type="text" id="palabra_clave" name="palabra_clave" placeholder="Buscar aqu&iacute;..." >
				     <button type="submit">Buscar</button>
				 </form> 
 				</div>
 				 {/if}	
 				
 				
 				
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Empleados
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>CODIGO</th>
											<th>NOMBRE</th>
						
											<th>EMAIL</th>
											<th>TELEFONO</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$empleados key=key item=item}
										 {if $item.idEmpleado neq ''}
										       
											<tr>
												<td>{$item.idEmpleado}</td>
												<td>{$item.Nombres} {$item.ApellidoPaterno} {$item.ApellidoMaterno} </td>
										
												<td>{$item.Mail}</td>
												<td>{$item.TelCasa}</td>
												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
													    {if $smarty.session.PERMISOS.per_typ_id neq 4}
														<a class="green" href="{base_url()}employees/formEditEmployees/{$item.idEmpleado}">
															<i class="icon-pencil bigger-130"></i>
														</a>
														{/if}
	                                                    {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2}
														<a class="red" href="{base_url()}employees/deleteEmpleado/{$item.idEmpleado}">
															<i class="icon-trash bigger-130"></i>
														</a>
														{/if}
														<!--
														 {if $smarty.session.PERMISOS.per_typ_id eq 4 || $smarty.session.PERMISOS.per_typ_id eq 1}
														<a class="green" id="Observaciones{$item.idEmpleado}" href="{base_url()}employees/mostrarObservaciones/{$item.idEmpleado}">
															<i class="icon-comment bigger-130"></i>
														</a>
														{/if}-->
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																<a href="{base_url()}employees/formEditEmployees/{$item.idEmpleado}" class="tooltip-success" data-rel="tooltip" title="Edit">
																	<span class="green">
																		<i class="icon-edit bigger-120"></i>
																	</span>
																</a>
															</li>
                                                            {if $smarty.session.PERMISOS.per_typ_id eq 1 || $smarty.session.PERMISOS.per_typ_id eq 2}
															<!--<li>
																<a href="{base_url()}employees/deleteEmpleado/{$item.idEmpleado}" class="tooltip-error" data-rel="tooltip" title="Delete">
																	<span class="red">
																		<i class="icon-trash bigger-120"></i>
																	</span>
																</a>
															</li>-->
															{/if}	
														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										     <script type="text/javascript">
															//$("#Observaciones{$item.idEmpleado}").fancybox();
													</script>
										{/foreach}
									</tbody>
								</table>
							</div>

							{$NumPaginas}

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#empleados').attr('class','active');

</script>
    


{/block}
