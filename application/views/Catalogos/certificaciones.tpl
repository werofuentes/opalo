{extends file="structure/main.tpl"}
{block name=content}




<div class="main-content" id="kek">

			<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-inbox"></i>
							<a href="#">Catálogos</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Certificaciones</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>

	<div class="page-content">
		<div class="page-header position-relative">
			<h4>
			<B>Agregar certificacion</B>
			</h4>
		</div><!--/.page-header-->

	   <form id="formArchivo" name="formArchivo" action="{base_url()}catalogos/certificacionesGuardar" method="post" enctype="multipart/form-data">	


		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_etiqueta" class="control-group">	
						<div class="span2"><div align="right">Certificacion:</div></div>
						<div class="span6"><input type="text" value="" id="servicioss" name="servicioss" class="span11"/>
							<span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	


		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_etiqueta" class="control-group">	
						<div class="span2"><div align="right">Abreviatura:</div></div>
						<div class="span6"><input type="text" value="" id="servicioss_abrev" name="servicioss_abrev" class="span11"/>
							<span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	


		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_claveUnidad" class="control-group">	
						<div class="span2"><div align="right">Clave unidad:</div></div>
						<div class="span6"><input type="text" value="" id="claveUnidad" name="claveUnidad" class="span3" />
							<span id="error_4" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	


		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_claveProdServ" class="control-group">	
						<div class="span2"><div align="right">Clave producto servicio:</div></div>
						<div class="span6"><input type="text" value="" id="claveProdServ" name="claveProdServ" class="span6" />
							<span id="error_4" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	



		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div class="span2"><div align="right">Examenes:</div></div>
					<div class="span6">
					{$mostraRenglones}
					</div>
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	




	  </form>

					
			 
				<div style="margin-left:18px;margin-top:120px;">
				  <a  href="{base_url()}catalogos/certificaciones">
				   <button class="btn btn-app btn-success btn-mini" >
				    <i class="icon-remove bigger-160"></i>
				    Cancelar
				   </button>
				   </a>									 
				</div>
				<div style="margin-left:88px;margin-top:-66px;">
				   <button class="btn btn-app btn-success btn-mini" onclick="guardarArchivo();">
				    <i class="icon-save bigger-160"></i>
				    Guardar
				   </button>							 
				</div>





	
	 </div><!--/.page-content-->	
</div><!--/.main-content-->	


<script type="text/javascript">
	$('#catalogos').attr('class','active open');
	$('#certificacion').attr('class','active');


	function display_item(accion,num_item)
	{											
	  if(accion == 2){
	    document.getElementById('item_detalle_'+num_item).style.display = "block";
	  }
	  if(accion == 1){
	    document.getElementById('item_detalle_'+num_item).style.display = "none";
	          $('#nombre_'+num_item).val('');
	  }

	}

	function guardarArchivo(){

		var etiqueta = $('#servicioss').val();


		if(etiqueta == ''){
			 	$('#error_etiqueta').addClass("control-group error");
			 	document.getElementById('error_3').style.display='block';

		 }else{
			 	$('#error_etiqueta').removeClass("control-group error");
    			$('#error_etiqueta').addClass("control-group");
			 	document.getElementById('error_3').style.display='none';
		}



		
		if(etiqueta != ''){

		 $( "#formArchivo" ).submit();
		}
	
	}
	
</script>	

{/block}
