{extends file="structure/main.tpl"}

{block name=content}



      
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-book"></i>
							<a href="#">Catalogos</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Sucursales</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>
				

				{if $smarty.session.PERMISOS.agregar_sucursales eq 1}
 				<div style="margin-left:30px;margin-top:15px;">
				<a class="shortcut-button" href="{base_url()}catalogos/sucursalesNueva">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
 				</div>
 				{/if}


 				<div id="modal_cancelar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  				<br>
						<div align="center">
						<form name="formCcancelarEval" id="formCcancelarEval" action="{base_url()}catalogos/deletesucursales" method="post" enctype="multipart/form-data"/>
						 <B>¿Quiere eliminar la sucursal? </B>
						 <input type="hidden" name="eliminarID" id="eliminarID" value="">

						<p>
						  <button type="button" class="btn btn-primary btn-lg" onclick="cancelEvaluados();">Si</button>
						  <button type="button" data-dismiss="modal" class="btn btn-default btn-lg">No</button>
						</p>

						</form>
						</div>

						<br>
		        </div>

 	
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Sucursales
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Sucursal</th>


											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$Sucursales key=key item=item}
										 {if $item.idSucursal neq ''}
											<tr>
												<td><font face="Arial">{$item.Sucursal}</font></td>


												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
													{if $smarty.session.PERMISOS.editar_sucursales eq 1}
														<a class="green" href="{base_url()}catalogos/sucursalesEditar/{$item.idSucursal}">
															<i class="icon-pencil bigger-130"></i>
														</a>
														<!--
														<a class="red" href="{base_url()}catalogos/deletesucursales/{$item.idSucursal}">
															<i class="icon-trash bigger-130"></i>
														</a>-->
														<a class="red" href="#" onclick="eliminarCliente({$item.idSucursal});"> 
																<i class="icon-trash bigger-130"></i>
															</a>
													{/if}


													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">


															<li>
																{if $smarty.session.PERMISOS.editar_sucursales eq 1}
																	<a class="green" href="{base_url()}catalogos/sucursalesEditar/{$item.idSucursal}">
																		<i class="icon-pencil bigger-130"></i>
																	</a>
																	<a class="red" href="#" onclick="eliminarCliente({$item.idSucursal});"> 
																<i class="icon-trash bigger-130"></i>
																</a>
																{/if}
															</li>

														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										{/foreach}
									</tbody>
								</table>
							</div>
                             {$NumPaginas}
							

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#catalogos').attr('class','active open');
		$('#sucursales').attr('class','active');


		function eliminarCliente(eliminarID){

			 $('#modal_cancelar').modal('show');
			 $('#eliminarID').val(eliminarID);
		}

		function cancelEvaluados(){

					$( "#formCcancelarEval" ).submit();

		}

</script>
    









{/block}
