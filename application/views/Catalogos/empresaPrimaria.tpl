{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
	 				<ul class="breadcrumb">
						<li>
							<i class="icon-book"></i>
							<a href="#">Catalogos</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Empreasas secundarias</a>
						</li>


					</ul><!--.breadcrumb-->
	</div>
	
					     
	 <span id="error_general" style="display: none;color:red;margin-left:20px;" class="help-inline"></span> 	   

 <form id="formEmpleados" name="formEmpleados" action="{base_url()}catalogos/primariasGuardar" method="post"/>			

				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >
										<ul class="nav nav-tabs" id="myTab">
											<li class="active" id="empleado-tab">
												<a data-toggle="tab" href="#investigador">
													Datos de empresa secundaria
												</a>
											</li>

									
											
											
										</ul>

										<div class="tab-content">
											<div id="investigador" class="tab-pane in active">
												
												<table width="100%" border="0">
												  <tr>
												      <td colspan="3">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Nombre empresa</label>
																           <input class="span12" id="Nombres"  name="Nombres" type="text" placeholder="Nombres" >
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio</label>
																           <input class="span12" id="Domicilio"  name="Domicilio" type="text" placeholder="Domicilio">
																           <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>
												  <tr>
												      <td colspan="3">
												          <div id="error_nombrecontacto" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Nombre de contacto</label>
																           <input class="span12" id="NombreContacto"  name="NombreContacto" type="text" placeholder="Contacto">
																           <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>  
												  <tr>
												      <td>
												         <div id="error_correo" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Correo (Contacto)</label>
																           <input id="correo" name="correo" type="text" placeholder="Correo">
																           <span id="error_4" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div>
												      </td>
												      <td>
												        <div id="error_telefono" class="control-group">
															      <label class="control-label"  for="form-field-1">Telefono (Empresa)</label>
															      	<div class="controls">
															      	<input id="Telefono" name="Telefono" type="text" placeholder="Telefono">
																           <span id="error_5" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
														 </div>
												      </td>

												      <td>
												        <div id="error_celular" class="control-group">
															      <label class="control-label"  for="form-field-1">Celular (Contacto)</label>
															      	<div class="controls">
															      	<input id="Celular" name="Celular" type="text" placeholder="Celular">
																           <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
														 </div>
												      </td>
												      
												  </tr>
												  
												   <tr>
												      <td colspan="">
												        <div id="error_medio" class="control-group">
															      <label class="control-label"  for="form-field-1">Medio que se entero de nosotros </label>
															      	<div class="controls">
															      		<select class="form-field-select-1" id="medio" name="medio">
															               <option value="" > Elige </option>
									                                       <option value="Television">Television</option>
									                                       <option value="Radio">Radio</option>
									                                       <option value="Internet">Internet</option>
									                                       <option value="Recomendacion">Recomendacion</option>
									                                 
									                                 
																    </select>
																    <span id="error_7" style="display: none" class="help-inline">Campo Obligatorio </span> 

															      	
															       </div>
														 </div>
												      </td>

												      <td colspan="">
												        <div id="error_vendedor" class="control-group">
															      <label class="control-label"  for="form-field-1">Vendedor</label>
															      	<div class="controls">
															      		<select class="form-field-select-1" id="vendedor" name="vendedor">
															               <option value="" > Elige </option>
									                                      	
									                                 {foreach from=$Vendedores key=key item=item}
												                         	<option value="{$item.id}">{$item.Nombre}</option>
												                  {/foreach}
									                                 
									                                 
																    </select>
																    <span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 

															      	
															       </div>
														 </div>
												      </td>

												      
												      
												  </tr>
												  
										
												</table>
	                                                   
											</div>

							
											
							
	
										</div>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
							

							
					

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
</form>

	<div style="margin-left:87px;margin-top:-65px;">
		<a  href="{base_url()}employees">
		   <button class="btn btn-app btn-success btn-mini">
		    <i class="icon-remove bigger-160"></i>
		    Cancelar
		   </button>
		</a>									 
	</div>	
	
	<div style="margin-left:157px;margin-top:-65px;">
	   <button class="btn btn-app btn-success btn-mini" onclick="guardarEmpresa();">
	    <i class="icon-save bigger-160"></i>
	    Guardar
	   </button>									 
	</div>	
     
</div> 


<script type="text/javascript">

$(function() {
		$( "#edad" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1950:2014",
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	      EncontrarEdad1(textoFecha);	
      		}
		});
	});

function mostrarFreelance(){
  
  var tipo_usuario = $('#tipo_usuario').val();
  
  
  if(tipo_usuario == 5){
     document.getElementById('freelance').style.display='block';
  }else{
     document.getElementById('freelance').style.display='none';
  }
  
}

function guardarEmpresa(){
 

  var Nombres  = $('#Nombres').val();
  var Domicilio = $('#Domicilio').val();
  var NombreContacto = $('#NombreContacto').val();
  var correo = $('#correo').val();
  var Telefono = $('#Telefono').val();
  var Celular = $('#Celular').val();
  var medio = $('#medio').val();

  
 

   

  
  if(Nombres == ''){
  			
  
	  	$('#error_nombre').addClass("control-group error");
	  	document.getElementById('error_1').style.display='block';
  	
 
  	
  }else{
	   	$('#error_nombre').removeClass("control-group error");
	    $('#error_nombre').addClass("control-group");
	    document.getElementById('error_1').style.display='none';
  }



  if(Domicilio == ''){
  		
  
	  	$('#error_domicilio').addClass("control-group error");
	  	document.getElementById('error_2').style.display='block';
  	
 
  	
  }else{
	   	$('#error_domicilio').removeClass("control-group error");
	    $('#error_domicilio').addClass("control-group");
	    document.getElementById('error_2').style.display='none';
  }


  if(NombreContacto == ''){
  		
  
	  	$('#error_nombrecontacto').addClass("control-group error");
	  	document.getElementById('error_3').style.display='block';
  	
 
  	
  }else{
	   	$('#error_nombrecontacto').removeClass("control-group error");
	    $('#error_nombrecontacto').addClass("control-group");
	    document.getElementById('error_3').style.display='none';
  }


  if(correo == ''){
  		
  
	  	$('#error_correo').addClass("control-group error");
	  	document.getElementById('error_4').style.display='block';
  	
 
  	
  }else{
	   	$('#error_correo').removeClass("control-group error");
	    $('#error_correo').addClass("control-group");
	    document.getElementById('error_4').style.display='none';
  }



  if(Telefono == ''){
  		
  
	  	$('#error_telefono').addClass("control-group error");
	  	document.getElementById('error_5').style.display='block';
  	
 
  	
  }else{
	   	$('#error_telefono').removeClass("control-group error");
	    $('#error_telefono').addClass("control-group");
	    document.getElementById('error_5').style.display='none';
  }



  if(Celular == ''){
  		
  
	  	$('#error_celular').addClass("control-group error");
	  	document.getElementById('error_6').style.display='block';
  	
 
  	
  }else{
	   	$('#error_celular').removeClass("control-group error");
	    $('#error_celular').addClass("control-group");
	    document.getElementById('error_6').style.display='none';
  }



  if(medio == ''){
  		
  
	  	$('#error_medio').addClass("control-group error");
	  	document.getElementById('error_7').style.display='block';
  	
 
  	
  }else{
	   	$('#error_medio').removeClass("control-group error");
	    $('#error_medio').addClass("control-group");
	    document.getElementById('error_7').style.display='none';
  }
  
  

  
  
  
  
  

   

  
 	if(Nombres != '' && Domicilio != '' && NombreContacto != '' && correo != '' && Telefono != '' && Celular != '' && medio != ''){
 		$( "#formEmpleados" ).submit();
 	}
 
  

 
  
  
 
        //$( "#formEmpleados" ).submit();




}


		$('#catalogos').attr('class','active open');
		$('#empresasPrimarias').attr('class','active');

</script> 
{/block}
