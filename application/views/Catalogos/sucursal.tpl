{extends file="structure/main.tpl"}
{block name=content}




<div class="main-content" id="kek">

			<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-book"></i>
							<a href="#">Catalogos</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Sucursales</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>

	<div class="page-content">
		<div class="page-header position-relative">
			<h4>
			<B>Agregar sucursal</B>
			</h4>
		</div><!--/.page-header-->

	   <form id="formArchivo" name="formArchivo" action="{base_url()}catalogos/sucursalesGuardar" method="post" enctype="multipart/form-data">	


		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_sucursal" class="control-group">	
						<div class="span2"><div align="right">Sucursal:</div></div>
						<div class="span6"><input type="text" value="" id="sucursal" name="sucursal" class="span11"/>
							<span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	


			<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_abreviatura" class="control-group">	
						<div class="span2"><div align="right">Abreviatura:</div></div>
						<div class="span6"><input type="text" value="" id="abreviatura" name="abreviatura" class="span11"/>
							<span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	



		 </form>

					
			 
				<div style="margin-left:18px;margin-top:120px;">
				  <a  href="{base_url()}catalogos/sucursales">
				   <button class="btn btn-app btn-success btn-mini" >
				    <i class="icon-remove bigger-160"></i>
				    Cancelar
				   </button>
				   </a>									 
				</div>
				<div style="margin-left:88px;margin-top:-66px;">
				   <button class="btn btn-app btn-success btn-mini" onclick="guardarArchivo();">
				    <i class="icon-save bigger-160"></i>
				    Guardar
				   </button>							 
				</div>





	
	 </div><!--/.page-content-->	
</div><!--/.main-content-->	


<script type="text/javascript">
	$('#catalogos').attr('class','active open');
	$('#sucursales').attr('class','active');


	function guardarArchivo(){


		var sucursal = $('#sucursal').val();
		var abreviatura = $('#abreviatura').val();


		if(sucursal == ''){
			 	$('#error_sucursal').addClass("control-group error");
			 	document.getElementById('error_1').style.display='block';

		 }else{
			 	$('#error_sucursal').removeClass("control-group error");
    			$('#error_sucursal').addClass("control-group");
			 	document.getElementById('error_1').style.display='none';
		}

		if(abreviatura == ''){
			 	$('#error_abreviatura').addClass("control-group error");
			 	document.getElementById('error_2').style.display='block';

		 }else{
			 	$('#error_abreviatura').removeClass("control-group error");
    			$('#error_abreviatura').addClass("control-group");
			 	document.getElementById('error_2').style.display='none';
		}



		
		if(sucursal != '' && abreviatura != ''){

		 $( "#formArchivo" ).submit();
		}
	
	}
	
</script>	

{/block}
