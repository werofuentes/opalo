{extends file="structure/main.tpl"}

{block name=content}
<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
	 				<ul class="breadcrumb">
						<li>
							<i class="icon-book"></i>
							<a href="#">Catalogos</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Empreasas primarias</a>
						</li>


					</ul><!--.breadcrumb-->
	</div>
	
	<div id="modal_examenes" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  		
					
									<br>
					<div align="center">
					Buscar : <input id="search" type="text" value="" name="search">
					</div>
		                  <div class="modal-body" id="contenido_modal_examenes">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>
					     

 <form id="formEmpleados" name="formEmpleados" action="{base_url()}catalogos/secundariasUpdate" method="post"/>			
 				<input class="span12" id="id"  name="id" value="{$datosSec.id}" type="hidden">
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->

							<div class="row-fluid">
								<div class="span66">
									<div class="tabbable" >
										<ul class="nav nav-tabs" id="myTab">
											<li class="active" id="empleado-tab">
												<a data-toggle="tab" href="#investigador">
													Datos de empresa primaria
												</a>
											</li>

									
											
											
										</ul>

										<div class="tab-content">
											<div id="investigador" class="tab-pane in active">
												
												<table width="100%" border="0">

											
												  <tr>
												      <td colspan="3">
												          <div id="error_idEmpresaPrimaria" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Empresa secundaria</label>
																           <select class="form-field-select-1" id="idEmpresaPrimaria" name="idEmpresaPrimaria">
																			 <option value="" > Elige </option>
																			  {foreach from=$EmpresasPrimarias key=key item=item}
															                <option value="{$item.id}" {if $datosSec.IdEmpresaPrimaria eq $item.id} selected="selected" {/if}>{$item.Nombre}</option>
															                  {/foreach}
																		  </select>





																           <span id="error_13" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>
											
												  <tr>
												      <td colspan="3">
												          <div id="error_nombre" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Nombre empresa</label>
																           <input class="span12" id="Nombres"  name="Nombres" value="{$datosSec.Nombre}" type="text" placeholder="Nombres">
																           <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_razon_social" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Razon Social</label>
																           <input class="span12" id="RazonSocial" value="{$datosSec.RazonSocial}" name="RazonSocial" type="text" placeholder="Razon social">
																           <span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 

												  <tr>
												      <td colspan="2">
												          <div id="error_rfc" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">RFC</label>
																           <input class="span12" value="{$datosSec.Rfc}" id="Rfc"  name="Rfc" type="text" placeholder="RFC">
																           <span id="error_9" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												      <td>
												         <div id="error_correo_facturas" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Correo facturas</label>
																           <input  value="{$datosSec.CorreoFactuas}" class="span12" id="CorreoFactuas" name="CorreoFactuas" type="text" placeholder="Correo facturas">
																           <span id="error_10" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div>
												      </td>
												  </tr> 
												  <!--
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio fiscal</label>
																           <input value="{$datosSec.DomicilioFiscal}" class="span12" id="DomicilioFiscal"  name="DomicilioFiscal" type="text" placeholder="Domicilio fiscal">
																           <span id="error_11" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  -->
												  <tr>
												      <td colspan="2">
												          <div id="error_calleFiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Calle</label>
																           <input  value="{$datosSec.calleFiscal}" class="span12" id="calleFiscal"  name="calleFiscal" type="text" placeholder="Calle fiscal">
																           <span id="error_11_calleFiscal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>

												      <td >
												          <div id="error_numroExtFiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Numero ext.</label>
																           <input   value="{$datosSec.numroExtFiscal}" class="span12" id="numroExtFiscal"  name="numroExtFiscal" type="text" placeholder="Numero ext.">
																           <span id="error_11_numroExtFiscal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 


												  <tr>
												      <td colspan="1">
												          <div id="error_numeroIntFiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Numero int.</label>
																           <input  value="{$datosSec.numeroIntFiscal}" class="span12" id="numeroIntFiscal"  name="numeroIntFiscal" type="text" placeholder="Numero int.">
																           <span id="error_11_numeroIntFiscal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>

												      <td colspan="2">
												          <div id="error_ColoniaFiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Colonia</label>
																           <input  value="{$datosSec.ColoniaFiscal}" class="span12" id="ColoniaFiscal"  name="ColoniaFiscal" type="text" placeholder="Colonia fiscal">
																           <span id="error_11_ColoniaFiscal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 


												  <tr>
												      <td colspan="1">
												          <div id="error_municipioFiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Municipio</label>
																           <input  value="{$datosSec.municipioFiscal}" class="span12" id="municipioFiscal"  name="municipioFiscal" type="text" placeholder="Municipio fiscal">
																           <span id="error_11_municipioFiscal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>

												      <td colspan="1">
												          <div id="error_estadoFiscal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Estado</label>
																           <input  value="{$datosSec.estadoFiscal}" class="span12" id="estadoFiscal"  name="estadoFiscal" type="text" placeholder="Estado fiscal">
																           <span id="error_11_estadoFiscal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>

												       <td colspan="1">
												          <div id="error_codigoPostal" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Codigo postal</label>
																           <input   value="{$datosSec.codigoPostal}"class="span12" id="codigoPostal"  name="codigoPostal" type="text" placeholder="Codigo postal fiscal">
																           <span id="error_11_codigoPostal" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_entrega_credencial" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Domicilio entrega credencial</label>
																           <input value="{$datosSec.DomicilioEntregaCredencial}" class="span12" id="DomicilioEntregaCredencial"  name="DomicilioEntregaCredencial" type="text" placeholder="Domicilio entrega credencial">
																           <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>
												  <tr>
												      <td colspan="3">
												          <div id="error_nombrecontacto" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Nombre de contacto</label>
																           <input class="span12" id="NombreContacto" value="{$datosSec.NombreContacto}"  name="NombreContacto" type="text" placeholder="Contacto" style='text-transform:uppercase;'>
																           <span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>  
												  <tr>
												      <td>
												         <div id="error_correo" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Correo (Contacto)</label>
																           <input value="{$datosSec.CorreoElectronico}"  id="correo" name="correo" type="text" placeholder="Correo">
																           <span id="error_4" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div>
												      </td>
												      <td>
												        <div id="error_telefono" class="control-group">
															      <label class="control-label"  for="form-field-1">Telefono (Empresa)</label>
															      	<div class="controls">
															      	<input value="{$datosSec.Telefono}"  id="Telefono" name="Telefono" type="text" placeholder="Telefono">
																           <span id="error_5" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
														 </div>
												      </td>

												      <td>
												        <div id="error_celular" class="control-group">
															      <label class="control-label"  for="form-field-1">Celular (Contacto)</label>
															      	<div class="controls">
															      	<input value="{$datosSec.Celular}" id="Celular" name="Celular" type="text" placeholder="Celular">
																           <span id="error_6" style="display: none" class="help-inline">Campo Obligatorio </span> 
															       </div>
														 </div>
												      </td>
												      
												  </tr>
												  
												   <tr>
												      
												      <td colspan="1">
												         <div id="error_correo_resultados" class="control-group">
																  <div >
																           <label class="control-label" for="form-field-1">Correo resultados</label>
																           <input value="{$datosSec.CorreoResultados}" id="CorreoResultados" name="CorreoResultados" type="text" placeholder="Correo resultados">
																           <span id="error_12" style="display: none" class="help-inline">Campo Obligatorio </span> 
																           
																    </div>  
														   </div>
												      </td>

												      <td colspan="1">
												         <div id="error_vendedor" class="control-group">
															      <label class="control-label"  for="form-field-1">Vendedor</label>
															      	<div class="controls">
															      		<select class="form-field-select-1" id="vendedor" name="vendedor">
															               <option value="" > Elige </option>
									                                      	
									                                 {foreach from=$Vendedores key=key item=item}
												                         	<option value="{$item.id}" {if $datosSec.Vendedor eq $item.id} selected="selected" {/if}>{$item.Nombre}</option>
												                  {/foreach}
									                                 
									                                 
									                                 
																    </select>
																    <span id="error_8" style="display: none" class="help-inline">Campo Obligatorio </span> 

															      	
															       </div>
														 </div>
												      </td>

												      
												      
												  </tr>
												  
										
												</table>
												<div class="table-header">
													Precio certificaciones
												</div>
												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Certificacion</th>
																<th width="35%">Precio</th>
																<th width="30%"> </th>							
															</tr>

														</thead>
														{$DetalleCertificaciones}
													 </table>
													
													</div>
												</div>
	                                                   
											</div>

							
											
							
	
										</div>
									</div>
								</div><!--/span-->
							</div><!--/row-->

							<div class="space"></div> <!-- aqui no-->
							

							
					

							<!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
</form>
	
	<div style="margin-left:87px;margin-top:-65px;">
		<a  href="{base_url()}catalogos/secundaria">
		   <button class="btn btn-app btn-success btn-mini">
		    <i class="icon-remove bigger-160"></i>
		    Cancelar
		   </button>
		</a>									 
	</div>	
	
	{if $smarty.session.PERMISOS.sololectura_empresa_primaria eq 0}
	<div style="margin-left:157px;margin-top:-65px;">
	   <button class="btn btn-app btn-success btn-mini" onclick="guardarEmpresa();">
	    <i class="icon-save bigger-160"></i>
	    Guardar
	   </button>									 
	</div>	
	{/if}
     
</div> 

{if $totalFilas eq 0}
<script type="text/javascript">
	AddFilaCertificacion(1);
</script>

{/if}
{if $smarty.session.PERMISOS.sololectura_empresa_primaria eq 1}
<script type="text/javascript">
	DisableEnableForm(document.formEmpleados,true);
</script>
{/if}




<script type="text/javascript">


$(function() {
		$( "#edad" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "1950:2014",
			monthNamesShort: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		   onSelect: function(textoFecha, objDatepicker){
         	    
         	      EncontrarEdad1(textoFecha);	
      		}
		});
	});

function mostrarFreelance(){
  
  var tipo_usuario = $('#tipo_usuario').val();
  
  
  if(tipo_usuario == 5){
     document.getElementById('freelance').style.display='block';
  }else{
     document.getElementById('freelance').style.display='none';
  }
  
}

function guardarEmpresa(){
 

  //var idEmpresaPrimaria  = $('#idEmpresaPrimaria').val();
  var Nombres  = $('#Nombres').val();
  var RazonSocial  = $('#RazonSocial').val();
  var Rfc  = $('#Rfc').val();
  var CorreoFactuas  = $('#CorreoFactuas').val();
  //var DomicilioFiscal = $('#DomicilioFiscal').val();

   var calleFiscal     = $('#calleFiscal').val();
  var numroExtFiscal  = $('#numroExtFiscal').val();
  var numeroIntFiscal = $('#numeroIntFiscal').val();
  var ColoniaFiscal   = $('#ColoniaFiscal').val();
  var municipioFiscal = $('#municipioFiscal').val();
  var estadoFiscal 	  = $('#estadoFiscal').val();
  var codigoPostal    = $('#codigoPostal').val();


  var DomicilioEntregaCredencial = $('#DomicilioEntregaCredencial').val();
  var NombreContacto = $('#NombreContacto').val();
  var correo = $('#correo').val();
  var Telefono = $('#Telefono').val();
  var Celular = $('#Celular').val();
  var CorreoResultados = $('#CorreoResultados').val();
  
 
 	
  /*
  if(idEmpresaPrimaria == ''){
  			
  
	  	$('#error_idEmpresaPrimaria').addClass("control-group error");
	  	document.getElementById('error_13').style.display='block';
  	
 
  	
  }else{
	   	$('#error_idEmpresaPrimaria').removeClass("control-group error");
	    $('#error_idEmpresaPrimaria').addClass("error_13-group");
	    document.getElementById('error_1').style.display='none';
  } */

  
  if(Nombres == ''){
  			
  
	  	$('#error_nombre').addClass("control-group error");
	  	document.getElementById('error_1').style.display='block';
  	
 
  	
  }else{
	   	$('#error_nombre').removeClass("control-group error");
	    $('#error_nombre').addClass("control-group");
	    document.getElementById('error_1').style.display='none';
  }


  if(RazonSocial == ''){
  			
  
	  	$('#error_razon_social').addClass("control-group error");
	  	document.getElementById('error_8').style.display='block';
  	
 
  	
  }else{
	   	$('#error_razon_social').removeClass("control-group error");
	    $('#error_razon_social').addClass("control-group");
	    document.getElementById('error_8').style.display='none';
  }



  if(Rfc == ''){
  			
  
	  	$('#error_rfc').addClass("control-group error");
	  	document.getElementById('error_9').style.display='block';
  	
 
  	
  }else{
	   	$('#error_rfc').removeClass("control-group error");
	    $('#error_rfc').addClass("control-group");
	    document.getElementById('error_9').style.display='none';
  }


  if(CorreoFactuas == ''){
  			
  
	  	$('#error_correo_facturas').addClass("control-group error");
	  	document.getElementById('error_10').style.display='block';
  	
 
  	
  }else{
	   	$('#error_correo_facturas').removeClass("control-group error");
	    $('#error_correo_facturas').addClass("control-group");
	    document.getElementById('error_10').style.display='none';
  }



/*
  if(DomicilioFiscal == ''){
  		
  
	  	$('#error_domicilio_fiscal').addClass("control-group error");
	  	document.getElementById('error_11').style.display='block';
  	
 
  	
  }else{
	   	$('#error_domicilio_fiscal').removeClass("control-group error");
	    $('#error_domicilio_fiscal').addClass("control-group");
	    document.getElementById('error_11').style.display='none';
  }*/

   if(calleFiscal == ''){
  		
  
	  	$('#error_calleFiscal').addClass("control-group error");
	  	document.getElementById('error_11_calleFiscal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_calleFiscal').removeClass("control-group error");
	    $('#error_calleFiscal').addClass("control-group");
	    document.getElementById('error_11_calleFiscal').style.display='none';
  }


  if(numroExtFiscal == ''){
  		
  
	  	$('#error_numroExtFiscal').addClass("control-group error");
	  	document.getElementById('error_11_numroExtFiscal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_numroExtFiscal').removeClass("control-group error");
	    $('#error_numroExtFiscal').addClass("control-group");
	    document.getElementById('error_11_numroExtFiscal').style.display='none';
  }


  if(numeroIntFiscal == ''){
  		
  
	  	$('#error_numeroIntFiscal').addClass("control-group error");
	  	document.getElementById('error_11_numeroIntFiscal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_numeroIntFiscal').removeClass("control-group error");
	    $('#error_numeroIntFiscal').addClass("control-group");
	    document.getElementById('error_11_numeroIntFiscal').style.display='none';
  }



  if(ColoniaFiscal == ''){
  		
  
	  	$('#error_ColoniaFiscal').addClass("control-group error");
	  	document.getElementById('error_11_ColoniaFiscal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_ColoniaFiscal').removeClass("control-group error");
	    $('#error_ColoniaFiscal').addClass("control-group");
	    document.getElementById('error_11_ColoniaFiscal').style.display='none';
  }


  if(municipioFiscal == ''){
  		
  
	  	$('#error_municipioFiscal').addClass("control-group error");
	  	document.getElementById('error_11_municipioFiscal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_municipioFiscal').removeClass("control-group error");
	    $('#error_municipioFiscal').addClass("control-group");
	    document.getElementById('error_11_municipioFiscal').style.display='none';
  }



  if(estadoFiscal == ''){
  		
  
	  	$('#error_estadoFiscal').addClass("control-group error");
	  	document.getElementById('error_11_estadoFiscal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_estadoFiscal').removeClass("control-group error");
	    $('#error_estadoFiscal').addClass("control-group");
	    document.getElementById('error_11_estadoFiscal').style.display='none';
  }



  if(codigoPostal == ''){
  		
  
	  	$('#error_codigoPostal').addClass("control-group error");
	  	document.getElementById('error_11_codigoPostal').style.display='block';
  	
 
  	
  }else{
	   	$('#error_codigoPostal').removeClass("control-group error");
	    $('#error_codigoPostal').addClass("control-group");
	    document.getElementById('error_11_codigoPostal').style.display='none';
  }


  if(DomicilioEntregaCredencial == ''){
  		
  
	  	$('#error_domicilio_entrega_credencial').addClass("control-group error");
	  	document.getElementById('error_2').style.display='block';
  	
 
  	
  }else{
	   	$('#error_domicilio_entrega_credencial').removeClass("control-group error");
	    $('#error_domicilio_entrega_credencial').addClass("control-group");
	    document.getElementById('error_2').style.display='none';
  }





  if(NombreContacto == ''){
  		
  
	  	$('#error_nombrecontacto').addClass("control-group error");
	  	document.getElementById('error_3').style.display='block';
  	
 
  	
  }else{
	   	$('#error_nombrecontacto').removeClass("control-group error");
	    $('#error_nombrecontacto').addClass("control-group");
	    document.getElementById('error_3').style.display='none';
  }


  if(correo == ''){
  		
  
	  	$('#error_correo').addClass("control-group error");
	  	document.getElementById('error_4').style.display='block';
  	
 
  	
  }else{
	   	$('#error_correo').removeClass("control-group error");
	    $('#error_correo').addClass("control-group");
	    document.getElementById('error_4').style.display='none';
  }



  if(Telefono == ''){
  		
  
	  	$('#error_telefono').addClass("control-group error");
	  	document.getElementById('error_5').style.display='block';
  	
 
  	
  }else{
	   	$('#error_telefono').removeClass("control-group error");
	    $('#error_telefono').addClass("control-group");
	    document.getElementById('error_5').style.display='none';
  }



  if(Celular == ''){
  		
  
	  	$('#error_celular').addClass("control-group error");
	  	document.getElementById('error_6').style.display='block';
  	
 
  	
  }else{
	   	$('#error_celular').removeClass("control-group error");
	    $('#error_celular').addClass("control-group");
	    document.getElementById('error_6').style.display='none';
  }





  if(CorreoResultados == ''){
  
	  	$('#error_correo_resultados').addClass("control-group error");
	  	document.getElementById('error_12').style.display='block';
 
  	
  }else{
	   	$('#error_correo_resultados').removeClass("control-group error");
	    $('#error_correo_resultados').addClass("control-group");
	    document.getElementById('error_12').style.display='none';
  }
  
  
	/*if(Nombres != '' && RazonSocial != '' && CorreoFactuas != '' && DomicilioFiscal != '' && DomicilioEntregaCredencial != '' && NombreContacto != ''  && correo != '' && Telefono != ''  && Celular != '' && CorreoResultados != '' ){
 		$( "#formEmpleados" ).submit();
 	}*/


	if(Nombres != '' && RazonSocial != '' &&  codigoPostal != '' &&  estadoFiscal != '' &&  municipioFiscal != ''   &&  ColoniaFiscal != ''  &&  numeroIntFiscal != ''  &&  numroExtFiscal != ''  &&  calleFiscal != '' && CorreoFactuas != '' && DomicilioEntregaCredencial != '' && NombreContacto != ''  && correo != '' && Telefono != ''  && Celular != '' && CorreoResultados != '' ){
 		$( "#formEmpleados" ).submit();
 	}
 
  

}


		$('#catalogos').attr('class','active open');
		$('#empresasSecundarias').attr('class','active');

</script> 
{/block}
