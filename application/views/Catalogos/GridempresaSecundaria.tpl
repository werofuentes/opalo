{extends file="structure/main.tpl"}

{block name=content}
<style>
#searchform {
	    width: 280px;
	  height:30px;
	    padding: 8px;
	  margin:-44px 150px 11px 91px;
	    background: #ccc;
	    border-radius: 4px;
	    box-shadow: 0 1px 1px rgba(0,0,0,.4) inset, 0 2px 0 rgba(255,255,255,.5);
	}
	 
	#searchform input {
	    width: 190px;
	  height:20px;
	    padding: 5px;
	    float: left;
	    border: 0;
	    background: #eee;
	    border-radius: 3px 0 0 3px;
	  box-shadow: 0 1px 1px rgba(0,0,0,.4) inset;
	}
	 
	#searchform input:focus {
	    outline: 0;
	  height:21px;
	    background: #fff;
	    box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
	}
	 
	#searchform input:-webkit-input-placeholder {
	   color: #999;
	   font-weight: normal;
	   font-style: italic;
	}
	 
	#searchform input:-moz-placeholder {
	    color: #999;
	    font-weight: normal;
	    font-style: italic;
	}
	 
	#searchform input:-ms-input-placeholder {
	    color: #999;
	    font-weight: normal;
	    font-style: italic;
	}   
	 
	#searchform button {
	    position: relative;
	    float: right;
	    border: 0;
	    cursor: pointer;
	    height: 30px;
	    width: 80px;
	    font-size:15px;
	    color: #fff;
	    background: #438cdb;
	    border-radius: 0 3px 3px 0;
	    text-shadow: 0 -1px 0 rgba(0, 0 ,0, .3);
	}  
	 
	#searchform button:hover{
	    background: #2672e0;
	}
	 
	#searchform button:active,
	#searchform button:focus{
	    background: #2672e0;
	}
	 
	#searchform button:before { /* flecha */
	    content: '';
	    position: absolute;
	    border-width: 8px 8px 8px 0;
	    border-style: solid solid solid none;
	    border-color: transparent #438cdb transparent;
	    top: 8px;
	    left: -5px;
	}
	 
	#searchform button:hover:before{
	    border-right-color: #2672e0;
	}
	 
	#searchform button:focus:before{
	    border-right-color: #2672e0;
} 
</style>

  
       <div class="main-content">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-book"></i>
							<a href="#">Catalogos</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Empreasas primarias</a>
						</li>


					</ul><!--.breadcrumb-->

				</div>


				<div id="modal_cancelar" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  				<br>
						<div align="center">
						<form name="formCcancelarEval" id="formCcancelarEval" action="{base_url()}catalogos/deleteSecundarias" method="post" enctype="multipart/form-data"/>
						 <B>¿Quiere eliminar el registro de empresa primaria? </B>
						 <input type="hidden" name="eliminarID" id="eliminarID" value="">

						<p>
						  <button type="button" class="btn btn-primary btn-lg" onclick="cancelEvaluados();">Si</button>
						  <button type="button" data-dismiss="modal" class="btn btn-default btn-lg">No</button>
						</p>

						</form>
						</div>

						<br>
		        </div>


		        <div id="modal_clientes_ver" class="modal container fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" style="display: none;margin-left:margin-left: -446px;width: 710px;">
		  
		                  <div class="modal-body" id="contenido_modal_clientes_ver">
		                  </div>
		                 <div class="modal-footer">
		                   <button type="button" data-dismiss="modal" class="btn btn-blue">Cerrar</button>
		                 </div>
		        </div>
			


				<div style="margin-left:30px;margin-top:15px;">
				

				{if $smarty.session.PERMISOS.agregar_empresa_primaria eq 1}
				<a class="shortcut-button" href="{base_url()}catalogos/secundariasNueva">
					<span>
						<button class="btn btn-app btn-success btn-mini radius-4">
						<i class="icon-check  bigger-200"></i>
						Nuevo
						</button>
					</span>
				</a>
				{/if}


				<form id="searchform" name="searchform" action="{base_url()}catalogos/secundaria" method="post">
				     <input type="text" id="palabra_clave" name="palabra_clave" placeholder="Buscar aqu&iacute;..." >
				     <button type="submit">Buscar</button>
				 </form> 
				</div>

 				
 				
 				
 				
				<div class="page-content">
					<div class="row-fluid">
						<div class="span12">
							<!--PAGE CONTENT BEGINS-->
							<div class="row-fluid">
								<div class="table-header">
									Empresas secundarias
								</div>

								<table id="sample-table-2" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Razon social</th>
											<th>RFC</th>
											<th></th>
										</tr>
									</thead>

									<tbody>
									   {foreach from=$EmpresasSecundaria key=key item=item}
										 {if $item.id neq ''}
										       
											<tr>
												<td style="cursor:pointer;" onclick="gridVerCliente({$item.id});">{$item.RazonSocial}</td>
												<td>{$item.Rfc}</td>
												<td class="td-actions">
													<div class="hidden-phone visible-desktop action-buttons">
														
														{if $smarty.session.PERMISOS.editar_empresa_primaria eq 1}
														<a class="green" href="{base_url()}catalogos/secundariasEditar/{$item.id}">
															<i class="icon-pencil bigger-130"></i>
														</a>
														
															{if $smarty.session.SEUS.use_typ_id eq 1}
															<!--
															<a class="red" href="{base_url()}catalogos/deleteSecundarias/{$item.id}">
																<i class="icon-trash bigger-130"></i>
															</a>-->
															<a class="red" href="#" onclick="eliminarCliente({$item.id});"> 
																<i class="icon-trash bigger-130"></i>
															</a>
															{/if}
														{/if}
												
													</div>
													<div class="hidden-desktop visible-phone">
													<div class="inline position-relative">
														<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown">
															<i class="icon-caret-down icon-only bigger-120"></i>
														</button>

														<ul class="dropdown-menu dropdown-icon-only dropdown-yellow pull-right dropdown-caret dropdown-close">

															{if $smarty.session.PERMISOS.editar_empresa_primaria eq 1}
															<li>
																<a class="green" href="{base_url()}catalogos/secundariasEditar/{$item.id}">
																	<i class="icon-pencil bigger-120"></i>
																</a>
															</li>
															{/if}
                                                            
														</ul>
													</div>
												</div>
												</td>
											</tr>
										  {/if}
										     <script type="text/javascript">
															//$("#Observaciones{$item.idEmpleado}").fancybox();
													</script>
										{/foreach}
									</tbody>
								</table>
							</div>

							{$NumPaginas}

							</div><!--PAGE CONTENT ENDS-->
						</div><!--/.span-->
					</div><!--/.row-fluid-->
				</div><!--/.page-content-->
				
				
				
       </div>


<script type="text/javascript">
		$('#catalogos').attr('class','active open');
		$('#empresasSecundarias').attr('class','active');


		function eliminarCliente(eliminarID){

			 $('#modal_cancelar').modal('show');
			 $('#eliminarID').val(eliminarID);
		}

		function cancelEvaluados(){

					$( "#formCcancelarEval" ).submit();

		}



</script>
    


{/block}
