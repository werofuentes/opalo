{extends file="structure/main.tpl"}
{block name=content}




<div class="main-content" id="kek">

			<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-inbox"></i>
							<a href="#">Catálogos</a>
							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
							<a href="#">Examenes</a>
						</li>
					</ul><!--.breadcrumb-->

				</div>

	<div class="page-content">
		<div class="page-header position-relative">
			<h4>
			<B>Agregar examen</B>
			</h4>
		</div><!--/.page-header-->

	   <form id="formArchivo" name="formArchivo" action="{base_url()}catalogos/serviciosGuardar" method="post" enctype="multipart/form-data">	

	   <div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_certificacion" class="control-group">	
						<div class="span2"><div align="right">Certificacion:</div></div>
						<div class="span6">
							 <select class="form-field-select-1" id="certificacionId" name="certificacionId">
									 <option value="" > Elige </option>
									 {foreach from=$Certificaciones key=key item=item}
									 <option value="{$item.id}">{$item.Certificaciones}</option>
									 {/foreach}
							 </select>


							<span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	

		<div class="row-fluid">
			<div class="span12">
				<div class="center">
					<div id="error_etiqueta" class="control-group">	
						<div class="span2"><div align="right">Examen:</div></div>
						<div class="span6"><input type="text" value="" id="servicioss" name="servicioss" class="span11"/>
							<span id="error_3" style="display: none" class="help-inline">Campo Obligatorio </span> 
						</div>
					</div>	
				</div><!--/center-->	
			</div><!--/span12-->	
		</div><!--/row-fluid-->	



		 </form>

					
			 
				<div style="margin-left:18px;margin-top:120px;">
				  <a  href="{base_url()}catalogos/servicios">
				   <button class="btn btn-app btn-success btn-mini" >
				    <i class="icon-remove bigger-160"></i>
				    Cancelar
				   </button>
				   </a>									 
				</div>
				<div style="margin-left:88px;margin-top:-66px;">
				   <button class="btn btn-app btn-success btn-mini" onclick="guardarArchivo();">
				    <i class="icon-save bigger-160"></i>
				    Guardar
				   </button>							 
				</div>





	
	 </div><!--/.page-content-->	
</div><!--/.main-content-->	


<script type="text/javascript">
	$('#catalogos').attr('class','active open');
	$('#servicio').attr('class','active');


	function guardarArchivo(){


		var certificacionId = $('#certificacionId').val();
		var etiqueta = $('#servicioss').val();


		if(certificacionId == ''){
			 	$('#error_certificacion').addClass("control-group error");
			 	document.getElementById('error_1').style.display='block';

		 }else{
			 	$('#error_certificacion').removeClass("control-group error");
    			$('#error_certificacion').addClass("control-group");
			 	document.getElementById('error_1').style.display='none';
		}

		if(etiqueta == ''){
			 	$('#error_etiqueta').addClass("control-group error");
			 	document.getElementById('error_3').style.display='block';

		 }else{
			 	$('#error_etiqueta').removeClass("control-group error");
    			$('#error_etiqueta').addClass("control-group");
			 	document.getElementById('error_3').style.display='none';
		}



		
		if(etiqueta != '' && certificacionId != ''){

		 $( "#formArchivo" ).submit();
		}
	
	}
	
</script>	

{/block}
