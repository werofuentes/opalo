{extends file="structure/main.tpl"}

{block name=content}
    

<div class="main-content">
   <div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li>
							<i class="icon-book"></i>
							<a href="#">Catalogos</a>

							<span class="divider">
								<i class="icon-angle-right arrow-icon"></i>
							</span>
						</li>
						
						<li>
							<a href="#">Psicologa(o)</a>
						</li>


					</ul><!--.breadcrumb-->

	</div>

	<div class="span7">
	  <div class="row-fluid">
		  <div class="widget-box">
			<div class="widget-header">
				<h4>Psicologa(o)</h4>
			</div>
			<div class="widget-body">
			   <div class="widget-main">
				  <form class="form-horizontal" id="formVendedor" name="formVendedor" action="{base_url()}catalogos/psicologasUpdate" method="post"/>

				  	<input type="hidden" id="id" name="id"  value="{$datosPsicologa.id}"/>
					
						  <div id="error_nombre" class="control-group">
							<label class="control-label" for="form-field-1">Psicologa(o)</label>
							<div  class="controls">
							<input class="form-field-1" type="text" id="name" name="name"  value="{$datosPsicologa.NombrePsicologa}" placeholder="Nombre" />
							    <span id="error_1" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>						
						   </div>

						   <div id="error_porcentaje" class="control-group">
							<label class="control-label" for="form-field-1">Costo x Examen</label>
							<div  class="controls">
							<input class="form-field-1" type="text" id="costo" name="costo"  value="{$datosPsicologa.CostoPsicologa}" placeholder="Costo" />
							    <span id="error_2" style="display: none" class="help-inline">Campo Obligatorio </span> 
							</div>						
						   </div>

			

						  
					</form>	

					{if $smarty.session.PERMISOS.sololectura_freelance eq 0}

					<div style="margin-left:251px;margin-top:15px;">
							
							    <button class="btn btn-app btn-success btn-mini" onclick="guardarVendedor();">
								   <i class="icon-save bigger-160"></i>
								    Guardar
								 </button>
					</div>	
					{/if}

					
					<div style="margin-left:320px;margin-top:-65px;">
					  <a  href="{base_url()}catalogos/freelance">
					   	<button class="btn btn-app btn-success btn-mini" >
					     <i class="icon-undo bigger-160"></i>
					     Salir
					    </button>
					   </a>									 
					</div>	


				   </div>
			</div>
		</div> 					
	</div>
</div>  

{if $smarty.session.PERMISOS.sololectura_psicologa eq 1}
<script type="text/javascript">
	DisableEnableForm(document.formVendedor,true);
</script>
{/if}


<script type="text/javascript">
		$('#catalogos').attr('class','active open');
		$('#psicologa').attr('class','active');
		
		function guardarVendedor(){

			 var nombre = $('#name').val();
		

			 if(nombre == ''){
			 	$('#error_nombre').addClass("control-group error");
			 	document.getElementById('error_1').style.display='block';

			 }else{
			 	$('#error_nombre').removeClass("control-group error");
    			$('#error_nombre').addClass("control-group");
			 	document.getElementById('error_1').style.display='none';
			 }
			 


			

			 if(nombre != ''){
			 	$( "#formVendedor" ).submit();
			 }
		    
		}

</script>
{/block}
