$(document).ready(function(){
	

	/* form contact us */
	$("#form_calculator").validate({
		errorElement : "span",
		errorClass   : "error",
		focusInvalid : false,
		rules : {
			property_value : "required",
			deposit        : "required",
			term           : "required"
		},
		errorPlacement : function(error, element) {
			error.appendTo(element);
		},
		messages : {
			property_value : "Valor del inmueble",
			deposit 	   : "Indica el enganche",
			term           : "Plazo a pagar"
		}
	});

	
	
	$("input[name=calcular]").click(function(){
	
		if($("#form_calculator").valid())
		{
			$("#resultados_calculadora").show();
			$("#resultados_calculadora").find("tbody").remove();
			$("#msg").show();
		
			$.ajax({
				type     : "POST",
				dataType : "html",
				url      : base_url + "calculator/getBanks",
				data     : $("#form_calculator").serialize(),
				success  : function(data) {
					$("#msg").hide();
					$("#resultados_calculadora").append(data);
		
				},
				error : function(request, status, error) {
					alert('Hay un error: ' + status.statusText);
					$("#msg").hide();
				}
			});
		}
	}); 

});
