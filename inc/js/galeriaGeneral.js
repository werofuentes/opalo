$(document).ready(function(){
	
	$('#fotos').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_fotos_galeriaGeneral/foto/img_mient/640/434',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos',
		buttonText		: "Fotos",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								 //alert(base_url + 'elevator/sube_fotos/foto/temporal/1024/728');
								 var cuenta=$('input[name="fotos[]"]').length;//numero de fotos
								 var nuevo_queueSizeLimit = 30 - cuenta;
								 $('#fotos').uploadifySettings('scriptData', {'cuenta': cuenta, 'limite' : 30});
								 $('#fotos').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
								
									 var cuenta = $('#contador_fotos').val();
									
									 $('#fotos_slide').append(response);
									
 									 cuenta = parseInt(cuenta) + 1;
									 $('#contador_fotos').val(cuenta);
									 $('#conteo_fotos').val(cuenta);
									 $('#conteo_fotos').removeClass('error');
									 $('#err_foto').html('');
								
							}
	});

 $("#fotos_slide").dragsort({ dragSelector: "div", placeHolderTemplate: "<li></li>",dragBetween: true });
});



function delete_foto(id)
{
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos').val(cuenta);
	$('#conteo_fotos').val(cuenta);
	
	$('#li_'+id).remove();
	$('#hide_'+id).remove();
	
	delete_archivo('uploads/img/img_mient/'+archivo);
}


function delete_foto_galeriageneral(nombre, archivo ,id_elm)
{
	
	$('#li_'+nombre).remove();
	$('#hide_'+nombre).remove();
	
	
	
	
	delete_foto_geleneral_dos('uploads/img/img_galeriaGeneral/'+archivo,id_elm);
}


function delete_archivo(file)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo',
			data		:	{'archivo' : file },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}


function delete_foto_geleneral_dos(file,id)
{
	
	
	
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_generalgaleriapho',
			data		:	{'archivo' : file, 'id': id },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}



function validar_formulario(){
	
	var name = $('#name').attr('value');
	var usuario = $('#usuario').attr('value');

	
	if(name == '' && usuario == ''){

		 var elDiv = document.getElementById('errorNombre');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorUsuariosSelect');
		 elDiv2.style.display='block';
		 
		 
		return false; 
	}
	
	
	if(name == '' && usuario != ''){

		 var elDiv = document.getElementById('errorNombre');
		 elDiv.style.display='block';
		 
		return false; 
	}
	
	if(name != '' && usuario == ''){

		 var elDiv2 = document.getElementById('errorUsuariosSelect');
		 elDiv2.style.display='block';
		 
		return false; 
	}
	
	
	if(name != '' && usuario != ''){
		 
		return true; 
	}
	

	
}
	
	
	

