$(document).ready(function(){
	
	$('#fotos').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_fotos_categoria/foto/img_mient/1024/728',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos',
		buttonText		: "Fotos",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								 //alert(base_url + 'elevator/sube_fotos/foto/temporal/1024/728');
								 var cuenta=$('input[name="fotos[]"]').length;//numero de fotos
								 var nuevo_queueSizeLimit = 1 - cuenta;
								 $('#fotos').uploadifySettings('scriptData', {'cuenta': cuenta, 'limite' : 1});
								 $('#fotos').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
							
								
								  if(response!='error'){
								  	
								  	var error = response.split(' ');
								  }
					
								
								 if(response!='error' && error[0]!='NoPng' && error[1]!='EsMayor')
								 {
									 var cuenta = $('#contador_fotos').val();
									
									 $('#fotos_slide').append(response);
									
 									 cuenta = parseInt(cuenta) + 1;
									 $('#contador_fotos').val(cuenta);
									 $('#conteo_fotos').val(cuenta);
									 $('#conteo_fotos').removeClass('error');
									 $('#err_foto').html('');
								}else{
									
									if(error[0]=='NoPng'){
										alert('La imagen No se Formato PNG');
										
									}
									
									if(error[1]=='EsMayor'){
										
										alert('Las Dimenciones de la imagen son mayores a las 82px x 82px');
									}
									
									
								}
								
								
							}
	});

    //$("#fotos_slide").dragsort({ dragSelector: "div", placeHolderTemplate: "<li style='background-color:gray !important;'></li>" });
    $("#fotos_slide").dragsort({ dragSelector: "li", dragEnd: function() { }, dragBetween: true, placeHolderTemplate: "<li></li>" });
});



function delete_foto(id)
{
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos').val(cuenta);
	$('#conteo_fotos').val(cuenta);
	
	$('#li_'+id).remove();
	$('#hide_'+id).remove();
	
	delete_archivo('uploads/img/img_mient/'+archivo);
}


function delete_foto_subcat(nombre, archivo ,id_elm)
{
	
	$('#li_'+nombre).remove();
	$('#hide_'+nombre).remove();
	delete_archivo_subcat_2('uploads/img/img_subcat/'+archivo,id_elm);
}


function delete_archivo(file)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo',
			data		:	{'archivo' : file },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}


function delete_archivo_subcat_2(file,id)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_subcat',
			data		:	{'archivo' : file, 'id': id },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}



function validar_formulario(){
	
	var name = $('#name').attr('value');
	var categorias = $('#categorias').attr('value');

	
	if(name == '' && categorias == ''){

		 var elDiv = document.getElementById('NombreSubcategoria');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategoriaSelect');
		 elDiv2.style.display='block';
		 
		 
		return false; 
	}
	
	
	if(name == '' && categorias != ''){

		 var elDiv = document.getElementById('NombreSubcategoria');
		 elDiv.style.display='block';
		 
		return false; 
	}
	
	if(name != '' && categorias == ''){

		 var elDiv2 = document.getElementById('errorCategoriaSelect');
		 elDiv2.style.display='block';
		 
		return false; 
	}
	
	
	if(name != '' && categorias != ''){
		 
		return true; 
	}
	

	
}
/*
function tipoCategoria(){
	
	var categorias = $('#categorias').attr('value');

	if(categorias == 7){

		 var elDiv = document.getElementById('FechaVerEvento');
		 elDiv.style.display='block';
		 return false; 
	}else{
		
		 var elDiv = document.getElementById('FechaVerEvento');
		 elDiv.style.display='none';
		 return false; 
	}
	
}
	*/
	

