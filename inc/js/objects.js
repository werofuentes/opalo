$(document).ready(function(){
	
	$('#fotos').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_fotos_objetos/foto/img_mient/1024/728',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos',
		buttonText		: "Fotos",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								 //alert(base_url + 'elevator/sube_fotos/foto/temporal/1024/728');
								 var cuenta=$('input[name="fotos[]"]').length;//numero de fotos
								 var nuevo_queueSizeLimit = 30 - cuenta;
								 $('#fotos').uploadifySettings('scriptData', {'cuenta': cuenta, 'limite' : 30});
								 $('#fotos').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
								
								if(response!='error'){
								  	
								  	var error = response.split(' ');
								  }
								
								
								 if(response!='error' && error[0]!='EsMayor' )
								 {
									 var cuenta = $('#contador_fotos').val();
									
									 $('#fotos_slide').append(response);
									
 									 cuenta = parseInt(cuenta) + 1;
									 $('#contador_fotos').val(cuenta);
									 $('#conteo_fotos').val(cuenta);
									 $('#conteo_fotos').removeClass('error');
									 $('#err_foto').html('');
								}else{
									
									if(error[0]=='EsMayor'){
										
										alert('Las Dimenciones de la imagen son mayores a las 640px x 434px');
									}
									
								}
							}
	});



    $("#fotos_slide").dragsort({ dragSelector: "div", placeHolderTemplate: "<li></li>",dragBetween: true });

  
	
		$('.dlocalidad').change(function(){
			
			var quien = $(this).attr('id');
			
			switch(quien)
			{
				case 'states': 		$('#datamapa').val('Mexico, '+$('#states option:selected').html());
										map.setZoom(7);
										break;
										
				case 'municipios':		$('#datamapa').val('Mexico, '+$('#states option:selected').html()+', '+$('#municipios option:selected').html());
										map.setZoom(10);
										break;

			}
			
			codeAddress();
	});
	
	
    
});


	var geocoder;
	var map;
	var marker;

	function las_cordenadas(cordenadas)
	{
		var first = String(cordenadas);
			
		first=first.replace(/\(/,'');
		first=first.replace(/\)/,'');
		first=first.replace(' ','');
		
		var ll = first.split(',');
		
		$('#latitud').val(ll[0]);
		$('#longitud').val(ll[1]);
	}



	function codeAddress()
	{
		var address = $('#datamapa').val();
		alert(address);
		geocoder.geocode( { 'address': address}, function(results, status)
		{
			if (status == google.maps.GeocoderStatus.OK)
			{
				var cordenadas = results[0].geometry.location;
				
				map.setCenter(cordenadas);
				marker.setPosition(cordenadas);
				
				las_cordenadas(cordenadas);
			}
			else
			{
				alert("Error al geolocalizar, debido a: " + status);
			}
		});
	}

	function por_cordenadas()
	{
		var lat = $('#latitud').val();
		var lon = $('#longitud').val();
		var cor = new google.maps.LatLng(lat, lon);
		
		map.setCenter(cor);
		marker.setPosition(cor);
		map.setZoom(13);
	}

function inicia_mapa(opcion)
	{
		
		//alert(opcion);
		geocoder		= new google.maps.Geocoder();
		var opciones	= {zoom: 7, mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl:false}
		map				= new google.maps.Map(document.getElementById("mapa"), opciones);
		marker			= new google.maps.Marker({map: map, draggable: true});
		
		if(opcion == 1)
		{
			codeAddress();
		}
		else
		{
			por_cordenadas();
		}
		
		google.maps.event.addListener(marker,'dragend',function(event){
			las_cordenadas(event.latLng);
		});
	}


$(window).load(function(){
	
	if('' != $("#id").val() && 0 != $("#id").val())
	{
		//cambiando(0);
		// Tienes que hacer un inicia mapa 2
		inicia_mapa(2);
	}
	else
	{
		inicia_mapa(1);
	}
});



function subcategoriasMostrar(id){
	
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/subcategorias',
			data		:	{'categoria' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								$('#Subcategorias').html(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}


function statesMostrar(id){
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/statesMunicipios',
			data		:	{'stados' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								$('#municipios').html(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}



function delete_foto(id)
{
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos').val(cuenta);
	$('#conteo_fotos').val(cuenta);
	
	$('#li_'+id).remove();
	// $('#hide_'+id).remove();
	
	delete_archivo('uploads/img/img_mient/'+archivo);
}


function delete_foto_objeto(nombre, archivo ,id_elm)
{
	
	$('#li_'+nombre).remove();
	$('#hide_'+nombre).remove();
	
	delete_archivo_objeto('uploads/img/img_objetos/'+archivo,id_elm);
}


function delete_archivo(file)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo',
			data		:	{'archivo' : file },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}


function delete_archivo_objeto(file,id)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_objeto',
			data		:	{'archivo' : file, 'id': id },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}




function validar_formulario_objectos(){
	
	var title         = $('#title').attr('value');
	var categorias    = $('#categorias').attr('value');
	var Subcategorias = $('#Subcategorias').attr('value');
	var Estimaciones  = $('#Estimaciones').attr('value');
	var guias         = $('#guias').attr('value');
	var activo        = $('#activo').attr('value');

   
if(categorias != 1 && categorias != 11){
   
    if(title != '' && categorias != '' && Subcategorias != '' && Estimaciones != '' && guias != ''){
    	return true;
    }
    
    if(title == '' && categorias != '' && Subcategorias != '' && Estimaciones != '' && guias != ''){
    	var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias == '' && Subcategorias != '' && Estimaciones != '' && guias != ''){
    	var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias != '' && Subcategorias == '' && Estimaciones != '' && guias != ''){
    	var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias != '' && Subcategorias != '' && Estimaciones == '' && guias != ''){
    	var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias != '' && Subcategorias != '' && Estimaciones != '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    
    
     if(title == '' && categorias == '' && Subcategorias != '' && Estimaciones != '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title == '' && categorias == '' && Subcategorias != '' && Estimaciones != '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias == '' && Subcategorias == '' && Estimaciones != '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    
    if(title != '' && categorias != '' && Subcategorias == '' && Estimaciones == '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias != '' && Subcategorias != '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    
    
    if(title == '' && categorias == '' && Subcategorias == '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    if(title != '' && categorias == '' && Subcategorias == '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    if(title == '' && categorias != '' && Subcategorias == '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    if(title == '' && categorias == '' && Subcategorias != '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    if(title == '' && categorias == '' && Subcategorias == '' && Estimaciones != '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
     if(title == '' && categorias == '' && Subcategorias == '' && Estimaciones == '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    
    if(title != '' && categorias != '' && Subcategorias == '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    if(title == '' && categorias != '' && Subcategorias != '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
     if(title == '' && categorias == '' && Subcategorias != '' && Estimaciones != '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    
    if(title == '' && categorias == '' && Subcategorias == '' && Estimaciones != '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='block';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
    if(title != '' && categorias != '' && Subcategorias != '' && Estimaciones == '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='none';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='block';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    
    if(title == '' && categorias != '' && Subcategorias != '' && Estimaciones != '' && guias == ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='none';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='block';
		 
		 
		 return false; 
    }
    
    
    if(title == '' && categorias == '' && Subcategorias != '' && Estimaciones != '' && guias != ''){
    	 var elDiv = document.getElementById('errorTitle');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorCategorias');
		 elDiv2.style.display='block';
		 
		 
		 var elDiv3 = document.getElementById('errorSubcategorias');
		 elDiv3.style.display='none';
		 
		 var elDiv4 = document.getElementById('errorEstimaciones');
		 elDiv4.style.display='none';
		 
		 
		 var elDiv5 = document.getElementById('errorguias');
		 elDiv5.style.display='none';
		 
		 
		 return false; 
    }
    
    
   }
	
}


function tipoCategoria(){

	$(document).ready(function(){
			
		var categorias = parseInt($('#categorias').attr('value'));
		
	
	  if(categorias ==  1 || categorias ==  '' || categorias ==  NaN || categorias ==  11){
		
			$('#difDestino').hide();
			$('#objecto1').hide();
			$('#objecto2').hide();
			$('#objecto3').hide();
			$('#FechaVerEvento').hide();
			 
		}else{
				
			$('#difDestino').show();
			$('#objecto1').show();
			$('#objecto2').show();
			$('#objecto3').show();
			
			if(categorias == 5  ||  15){
				$('#FechaVerEvento').show();
			}else{
				
				$('#FechaVerEvento').hide();
			}
			 
		}
	
		
	});
}
	
	

