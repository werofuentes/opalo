 $(function() {
 
 $('.input-mask-phone').mask('(999) 9999-9999');
	
});


$(document).ready(function(){
	
	$('#fotos').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_foto_empleado/foto/img_mient/1024/728',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos',
		buttonText		: "Fotos",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								 var cuenta=$('input[name="fotos[]"]').length;//numero de fotos
								 var nuevo_queueSizeLimit = 1 - cuenta;
								 $('#fotos').uploadifySettings('scriptData', {'cuenta': cuenta, 'limite' : 1});
								 $('#fotos').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
							
									 var cuenta = $('#contador_fotos').val();
									
									 $('#fotos_slide').append(response);
									
 									 cuenta = parseInt(cuenta) + 1;
									 $('#contador_fotos').val(cuenta);
									 $('#conteo_fotos').val(cuenta);
									 $('#conteo_fotos').removeClass('error');
									 $('#err_foto').html('');

								
								
							}
	});

});



function delete_foto(id)
{
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos').val(cuenta);
	$('#conteo_fotos').val(cuenta);
	
	$('#li_'+id).remove();
	$('#hide_'+id).remove();
	
	delete_archivo('uploads/img/mientras/'+archivo);
}

function delete_archivo(file)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_empleado',
			data		:	{'archivo' : file },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}


function delete_foto_empleados(nombre, archivo,id)
{
	

	$('#li_'+nombre).remove();
	$('#hide_'+nombre).remove();
	delete_archivo_empleados_2('uploads/img/img_mient/'+archivo,id);
}


function delete_archivo_empleados_2(file,id)
{

	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_empleado2',
			data		:	{'archivo' : file, 'id': id },
			dataType	:	'html',
			success		:	function( response )
							{
							
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}
