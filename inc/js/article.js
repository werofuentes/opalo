$(document).ready(function(){
	
	$('#fotos').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_fotos_article/foto/img_mient/1024/728',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos',
		buttonText		: "Fotos",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								 //alert(base_url + 'elevator/sube_fotos/foto/temporal/1024/728');
								 var cuenta=$('input[name="fotos[]"]').length;//numero de fotos
								 var nuevo_queueSizeLimit = 4 - cuenta;
								 $('#fotos').uploadifySettings('scriptData', {'cuenta': cuenta, 'limite' : 4});
								 $('#fotos').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
								
								 if(response!='error'){
								  	
								  	var error = response.split(' ');
								  }
								
							
								
								if(response!='error' && error[0]!='EsMayor' )
								 {
									 var cuenta = $('#contador_fotos').val();
									
									 $('#fotos_slide').append(response);
									
 									 cuenta = parseInt(cuenta) + 1;
									 $('#contador_fotos').val(cuenta);
									 $('#conteo_fotos').val(cuenta);
									 $('#conteo_fotos').removeClass('error');
									 $('#err_foto').html('');
								}else{
									
									if(error[0]=='EsMayor'){
										
										alert('Las Dimenciones de la imagen son mayores a las 640px x 434px');
									}
									
									
								}
								
								
								
							}
	});
	
	
	
	
		$('#fotos_mapa').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_fotos_mapa/foto/img_mient/1024/728',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos_mapa',
		buttonText		: "Mapa",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								 //alert(base_url + 'elevator/sube_fotos/foto/temporal/1024/728');
								 var cuenta_mapa=$('input[name="fotos_mapa[]"]').length;//numero de fotos
								
								 var nuevo_queueSizeLimit = 1 - cuenta_mapa;
								 $('#fotos_mapa').uploadifySettings('scriptData', {'cuenta': cuenta_mapa, 'limite' : 1});
								 $('#fotos_mapa').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
								
								 if(response!='error'){
								  	
								  	var error = response.split(' ');
								  }
								
							
								
								// if(response!='error' && error[0]!='EsMayor' )
								 // {
									 var cuenta_mapa = $('#contador_fotos_mapa').val();
									
									 $('#fotos_slide_mapa').append(response);
									
 									 cuenta_mapa = parseInt(cuenta_mapa) + 1;
									 $('#contador_fotos_mapa').val(cuenta_mapa);
									 $('#conteo_fotos_mapa').val(cuenta_mapa);
									 $('#conteo_fotos_mapa').removeClass('error');
									 $('#err_foto_mapa').html('');
								// }else{
// 									
									// if(error[0]=='EsMayor'){
// 										
										// alert('Las Dimenciones de la imagen son mayores a las 640px x 434px');
									// }
// 									
// 									
								// }
								
								
								
							}
	});
	
	
	
$("#fotos_slide_mapa").dragsort({ dragSelector: "div", placeHolderTemplate: "<li></li>",dragBetween: true });
$("#fotos_slide").dragsort({ dragSelector: "div", placeHolderTemplate: "<li></li>",dragBetween: true });
    	


});






function delete_foto(id)
{
	
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos').val(cuenta);
	$('#conteo_fotos').val(cuenta);
	
	$('#li_'+id).remove();
	$('#hide_'+id).remove();
	
	delete_archivo('uploads/img/img_mient/'+archivo);
}



function delete_foto_mapa(id)
{
	
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos_mapa').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos_mapa').val(cuenta);
	$('#conteo_fotos_mapa').val(cuenta);
	
	$('#li_'+id).remove();
	$('#hide_'+id).remove();
	
	delete_archivo('uploads/img/img_mient/'+archivo);
}



function delete_foto_articulo_imagen(nombre, archivo ,id_elm)
{
	
	$('#li_'+nombre).remove();
	$('#hide_'+nombre).remove();
	
	delete_foto_articulo('uploads/img/img_article/'+archivo,id_elm);
}


function delete_foto_articulo_imagen_mapa(nombre, archivo ,id_elm)
{
	
	$('#li_'+nombre).remove();
	$('#hide_'+nombre).remove();
	
	delete_foto_articulo_mapa('uploads/img/img_article/'+archivo,id_elm);
}


function delete_archivo(file)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo',
			data		:	{'archivo' : file },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}


function delete_foto_articulo(file,id)
{

	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_articulo',
			data		:	{'archivo' : file, 'id': id },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}



function delete_foto_articulo_mapa(file,id)
{

	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo_articulo_mapa',
			data		:	{'archivo' : file, 'id': id },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}



function validar_formulario(){
	
	var name = $('#name').attr('value');
	var usuario = $('#usuario').attr('value');

	
	if(name == '' && usuario == ''){

		 var elDiv = document.getElementById('errorNombre');
		 elDiv.style.display='block';
		 
		 var elDiv2 = document.getElementById('errorUsuariosSelect');
		 elDiv2.style.display='block';
		 
		 
		return false; 
	}
	
	
	if(name == '' && usuario != ''){

		 var elDiv = document.getElementById('errorNombre');
		 elDiv.style.display='block';
		 
		return false; 
	}
	
	if(name != '' && usuario == ''){

		 var elDiv2 = document.getElementById('errorUsuariosSelect');
		 elDiv2.style.display='block';
		 
		return false; 
	}
	
	
	if(name != '' && usuario != ''){
		 
		return true; 
	}
	

	
}



	

