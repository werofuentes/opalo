$(document).ready(function(){
	
		$('.dlocalidad').change(function(){
			
			var quien = $(this).attr('id');
			
			switch(quien)
			{
				case 'states': 		$('#datamapa').val('Mexico, '+$('#states option:selected').html());
										map.setZoom(7);
										break;
										
				case 'municipios':		$('#datamapa').val('Mexico, '+$('#states option:selected').html()+', '+$('#municipios option:selected').html());
										map.setZoom(10);
										break;

			}
			
			codeAddress();
	});
	
	
    
});


	var geocoder;
	var map;
	var marker;

	function las_cordenadas(cordenadas)
	{
		var first = String(cordenadas);
			
		first=first.replace(/\(/,'');
		first=first.replace(/\)/,'');
		first=first.replace(' ','');
		
		var ll = first.split(',');
		
		//console.log(ll);

		$('#latitud').val(ll[0]);
		$('#longitud').val(ll[1]);
		
		$('#latitud1').val(ll[0]);
		$('#longitud1').val(ll[1]);

		por_cordenadas();
	}



	function codeAddress()
	{
		var address = $('#datamapa').val();
//alert(address);

		geocoder.geocode( { 'address': address}, function(results, status)
		{
			if (status == google.maps.GeocoderStatus.OK)
			{
				var cordenadas = results[0].geometry.location;
				//console.log(cordenadas);
				map.setCenter(cordenadas);
				marker.setPosition(cordenadas);
				
				las_cordenadas(cordenadas);
			}
			else
			{
				alert("Error al geolocalizar, debido a: " + status);
			}
		});
	}

	function por_cordenadas()
	{
		var lat = $('#latitud').val();
		var lon = $('#longitud').val();
		var cor = new google.maps.LatLng(lat, lon);
		
		map.setCenter(cor);
		marker.setPosition(cor);
		map.setZoom(15);
	}

function inicia_mapa(opcion)
	{
		
		geocoder		= new google.maps.Geocoder();
		var opciones	= {zoom: 15, streetViewControl:true}
		map				= new google.maps.Map(document.getElementById("mapa"), opciones);
		marker			= new google.maps.Marker({map: map, draggable: true});
		
		
		if(opcion == 1)
		{
			codeAddress();
		}
		else
		{
			por_cordenadas();
		}
		
		
		google.maps.event.addListener(marker,'dragend',function(event){
			las_cordenadas(event.latLng);
		});
		
		
	}
	
	
	function inicia_mapa1(opcion)
	{
		
		geocoder		= new google.maps.Geocoder();
		var opciones	= {zoom: 15, mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl:false}
		map				= new google.maps.Map(document.getElementById("mapa1"), opciones);
		marker			= new google.maps.Marker({map: map, draggable: true});
		
		
		if(opcion == 1)
		{
			codeAddress();
		}
		else
		{
			por_cordenadas();
		}
		
		
		google.maps.event.addListener(marker,'dragend',function(event){
			las_cordenadas(event.latLng);
		});
		
		
	}


function TipoMostrar(id){
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/tipoMostrar',
			data		:	{'tipo_usuario' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								$('#idTipoESE').html(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}

function TipoMunicipio(id){
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/tipoMunicipio',
			data		:	{'idEstado' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								$('#Municipio').html(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}

function TipoMunicipio2(id){
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/tipoMunicipio',
			data		:	{'idEstado' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								$('#dir_municipio').html(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}

function EncontrarEdad(id){
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/encontrarEdad',
			data		:	{'fecha' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								//alert(response);
								$('#Edad').val(response);
								$('#Edad1').val(response);
								//$('#idTipoESE').html(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}


function EncontrarEdad1(id){
	
		$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/encontrarEdad',
			data		:	{'fecha' : id },
			dataType	:	'html',
			success		:	function( response )
							{
								$('#edad').val(response);
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
	
}


function validarCorreo(correo) {
 
   $.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/validarExisteCorreo',
			data		:	{'correo' : correo },
			dataType	:	'html',
			success		:	function( response )
							{
							
								if(response == 'Existe'){
									
									alert('El correo ya existe')
									$('#Mail').val('');
								}

							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
   
}

function validarCorreo1(correo) {
 
   $.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/validarExisteCorreo',
			data		:	{'correo' : correo },
			dataType	:	'html',
			success		:	function( response )
							{
							
								if(response == 'Existe'){
									
									alert('El correo ya existe')
									$('#email').val('');
								}

							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
   
}

// $(window).load(function(){
// 	
	// if('' != $("#id").val() && 0 != $("#id").val())
	// {
		// //cambiando(0);
		// // Tienes que hacer un inicia mapa 2
		// inicia_mapa(2);
	// }
	// else
	// {
		// inicia_mapa(1);
	// }
// });

	
	

