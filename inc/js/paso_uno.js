$(document).ready(function(){
	
	var geocoder;
	var map;
	var marker;
	
	$('.dlocalidad').change(function(){
			
			var quien = $(this).attr('id');
			
			switch(quien)
			{
				case 'u_estado': 		$('#datamapa').val('Mexico, '+$('#u_estado option:selected').html());
										map.setZoom(7);
										break;
										
				case 'u_municipio':		$('#datamapa').val('Mexico, '+$('#u_estado option:selected').html()+', '+$('#u_municipio option:selected').html());
										map.setZoom(10);
										break;
										
				case 'u_colonia':		$('#datamapa').val('Mexico, '+$('#u_estado option:selected').html()+', '+$('#u_municipio option:selected').html()+', '+$('#u_colonia option:selected').html());
										map.setZoom(13);
										break;
			}
			
			codeAddress();
	});
	
	function las_cordenadas(cordenadas)
	{
		// alert(cordenadas);
		var first = String(cordenadas);
			
		first=first.replace(/\(/,'');
		first=first.replace(/\)/,'');
		first=first.replace(' ','');
		
		var ll = first.split(',');
		
		$('#latitud').val(ll[0]);
		$('#longitud').val(ll[1]);
	}
	
	function codeAddress()
	{
		var address = $('#datamapa').val();
		geocoder.geocode( { 'address': address}, function(results, status)
		{
			if (status == google.maps.GeocoderStatus.OK)
			{
				var cordenadas = results[0].geometry.location;
				
				map.setCenter(cordenadas);
				marker.setPosition(cordenadas);
				
				las_cordenadas(cordenadas);
			}
			else
			{
				alert("Error al geolocalizar, debido a: " + status);
			}
		});
	}
	
	function por_cordenadas()
	{
		var lat = $('#latitud').val();
		var lon = $('#longitud').val();
		var cor = new google.maps.LatLng(lat, lon);
		
		map.setCenter(cor);
		marker.setPosition(cor);
		map.setZoom(13);
	}
	
	function inicia_mapa(opcion)
	{
		geocoder		= new google.maps.Geocoder();
		var opciones	= {zoom: 7, mapTypeId: google.maps.MapTypeId.ROADMAP, streetViewControl:false}
		map				= new google.maps.Map(document.getElementById("mapa"), opciones);
		marker			= new google.maps.Marker({map: map, draggable: true});
		
		if(opcion == 1)
		{
			codeAddress();
		}
		else
		{
			por_cordenadas();
		}
		
		google.maps.event.addListener(marker,'dragend',function(event){
			las_cordenadas(event.latLng);
		});
	}
	
	$('#tipo_inmueble').change(function(){
		$('#img_1').css('display','');
		cambiando(1);
	});
	
	function cambiando(bandera)
	{
		$.ajax({
					type 		: 'POST',
					url			: base_url+'zp/alta_propiedad/obtener_atributos/',
					data		: {'tipo_prop' : $('#tipo_inmueble').val(), 'bandera': bandera},
					dataType	: 'html',
					success		: 	function( data )	
									{
										// alert(data);
										$.globalEval(data);
										
										if(bandera == 0)
										{
											cambios_venta();
											cambios_renta();
										}
										
										$('#img_1').css('display','none');
									},
					error		: 	function()
									{
										alert('Intentarlo mas tarde.');
									}
		});
	}
	
	// Actauliza el combo municipio segun el estado seleccionado y deja en blanco el combo colonia
	$("#u_estado").change(function(){
		$('#img_2').css('display','');
		$('#img_3').css('display','');
		$.ajax({
					type 		: 'POST',
					url			: base_url+'zp/alta_propiedad/obtener_municipios/',
					data		: {'estado' : $(this).val()},
					dataType	: 'html',
					success		: 	function( data )	
									{
										$("#u_municipio").html(data);
										$("#u_colonia").html('<option value="">-Selecciona-</option>');
										$('#img_2').css('display','none');
										$('#img_3').css('display','none');
									},
					error		: 	function()
									{
										alert('Intentarlo mas tarde.');
									}
		});
	});
	
	$("#u_municipio").change(function(){
		$('#img_3').css('display','');
		$.ajax({
					type 		: 'POST',
					url			: base_url+'zp/alta_propiedad/obtener_colonias/',
					data		: {'estado' :$("#u_estado").val(), 'municipio' :$(this).val()},
					dataType	: 'html',
					success		: 	function( data )	
									{
										$("#u_colonia").html(data);
										$('#img_3').css('display','none');
									},
					error		: 	function()
									{
										alert('Intentarlo mas tarde.');
									}
		});
	});
	
	// Validaciones
	$("#commentForm").validate({
		errorElement: "span",
		errorClass: "error",
		// focusInvalid: false,
		rules:
		{
			'tipo[]'			: "required",
			'tipo_inmueble'		: { min: 1 }, //"required",
			'uso_inmueble[]'	: "required",
			'm2terreno'			: "required",
			'u_estado'			: { min: 1 }, //"required",
			'u_municipio'		: { min: 1 }, //"required",
			'u_colonia'			: { min: 1 }, //"required",
			'descripcion'		: "required"
		},
	
		messages:
		{
			'tipo[]'			: 'Elige al menos un Tipo de Operaci&oacute;n.',
			'tipo_inmueble'		: 'Elige un tipo de Inmueble.',
			'uso_inmueble[]'	: 'Elige al menos un Uso de suelo.',
			'precio_venta'		: 'Especifique Precio de Venta.',
			'precio_renta'		: 'Especifique Precio de Renta.',
			'NoHabitaciones'			: 'Obligatorio.',
			'NoBanos'				: 'Obligatorio.',
			'm2terreno'			: 'Obligatorio.',
			'm2construidos'		: 'Obligatorio.',
			'descripcion'		: 'Debe escribir una descripción.',
			'u_estado'			: 'Elige un Estado.',
			'u_municipio'		: 'Elige un Municipio.',
			'u_colonia'			: 'Elige una Colonia.',
		},
	
		errorPlacement: function(error, element)
		{
			// error.appendTo(element.next());
			switch(element.attr('name'))
			{
				case 'tipo[]'			: error.appendTo('#err_tipo'); break;
				case 'tipo_inmueble'	: error.appendTo('#err_inmueble'); break;
				case 'uso_inmueble[]'	: error.appendTo('#err_uso_inmueble'); break;
				case 'precio_venta'		: error.appendTo('#err_precio_venta'); break;
				case 'precio_renta'		: error.appendTo('#err_precio_renta'); break;
				case 'precio_traspaso'	: error.appendTo('#err_precio_traspaso'); break;
				case 'NoHabitaciones'		: error.appendTo('#err_recamaras'); break;
				case 'NoBanos'			: error.appendTo('#err_banos'); break;
				case 'm2terreno'		: error.appendTo('#err_terreno'); break;
				case 'm2construidos'		: error.appendTo('#err_construccion'); break;
				case 'descripcion'		: error.appendTo('#err_descripcion'); break;
				case 'u_estado'			: error.appendTo('#err_u_estado'); break;
				case 'u_municipio'		: error.appendTo('#err_u_municipio'); break;
				case 'u_colonia'		: error.appendTo('#err_u_colonia'); break;
			}
		}
	});

	$('#fin_venta').click(function(){
		cambios_venta();
	});
	
	$('#fin_renta').click(function(){
		cambios_renta();
	});
	
	function cambios_venta()
	{
		var inm=$("#tipo_inmueble option:selected").html();
		if(inm!='Seleccione un tipo de propiedad')
		{
			if($('#fin_venta:checked').length>0)
			{
				if(inm=='Terrenos')
				{
					$('#PrecioM2').fadeIn(1000).css('display','block');
				}
				$('#PrecioVenta').fadeIn(1000).css('display','block');
				$('#precio_venta').attr('class','precio_venta required');
			}
			else
			{
				$('#PrecioVenta').fadeOut(1000).css('display','none');
				$('#PrecioM2').fadeOut(1000).css('display','none');
				$('#precio_venta').removeAttr('class','precio_venta required');
			}
		}
	}
	
	function cambios_renta()
	{
		if($('#fin_renta:checked').length>0)
		{
			$('#PrecioRenta').fadeIn(1000).css('display','block');
			$('#precio_renta').attr('class','precio_renta required');
			
		}
		else
		{
			$('#PrecioRenta').fadeOut(1000).css('display','none');
			$('#precio_renta').removeAttr('class','precio_renta required');
		}
	}

	if($("#id_propiedad").length>0)
	{
		cambiando(0);
		// Tienes que hacer un inicia mapa 2
		inicia_mapa(2);
	}
	else
	{
		// parche para poder desactivar los checkbox
		$("input[name='uso_inmueble[]']").attr('disabled',true);
		$("input[name='tipo[]']").attr('disabled',true);
		inicia_mapa(1);
	}
});