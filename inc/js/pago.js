$(window).load(function(){
	/*
	$('input[name="pago"]').click(function() {   
		// alert($('input[name="pago"]:checked').val());
		if($('input[name="pago"]:checked').val()== 'deposito')
		{
			$('#deposito_banco').slideDown("slow");
			//$('#instrucciones').slideDown("slow");
			//$("#frase").addClass("frase required");
		}
		if($('input[name="pago"]:checked').val()== 'paypal')
		{
			$('#deposito_banco').slideUp("slow");
			//$('#instrucciones').slideUp("slow");
		}
	});
	*/
	$("#deposito_banco, p[data-deposit='deposito']").hide();
	
	$("input[name='pago']").change(function(){
		
		if($("input[name='pago']:checked").val() == 'deposito')
		{
			$("#deposito_banco").slideDown("slow");
			$("p[data-deposit='deposito']").slideDown("slow");
		}
		else
		{
			$("#deposito_banco").slideUp("slow");
			$("p[data-deposit='deposito']").slideUp("slow");
		}
	});
	
	$(".continuar_btn").live("click",function(){
		if($("input[name='pago']:checked").val() == 'deposito')
		{
			//alert('deposito');
			$('#deposito').submit();
		}
		else if($("input[name='pago']:checked").val() == 'paypal')
		{
			//alert('paypal');
			$('#paypalform').submit();
		}
		else
		{
			alert('Debe seleccionar una forma de pago para poder continuar');
		}
	});
	
});