$(document).ready(function(){
	
	$('.carrusel_vert_detalle').jcarousel({vertical: true, scroll: 2, wrap: "circular"});
	
	$('.detalle_video_min').fancybox({'type':'iframe'});
	
	$('.info_contacto_modulo_detalle_min').click(function(){
		$(this).hide('fast');
		$('.contenido_modulo_detalles_min').show('slow');
		
		$.ajax({
			type: "POST",
			dataType: "html",
			url:  base_url + "recomendar_amigo/click_telefono_fracc",
			data: {'click_to' : true},
			success: function(data){
				
					//alert('click agregado');
			}
		});

	});
	
	$("#mapa").goMap({
		zoom: 15,
		scaleControl: true,
		maptype: 'ROADMAP',
		mapTypeControl: true,
		mapTypeControlOptions:	{
									position: 'TOP_RIGHT',
									style: 'DROPDOWN_MENU'
							},
		markers:  [{latitude: $("input[name=ubi_latitud]").val(), longitude:  $("input[name=ubi_longitud]").val() }],
        overlays: [{
                      type: 'circle',
                      id: 'c',
                      color: '#FBB041',
                      weight: 1,
                      fillColor: '#E6B957',
                      latitude: $("input[name=ubi_latitud]").val(),
                      longitude:  $("input[name=ubi_longitud]").val(),
                      radius: 300
                  }]
	});
	
	
	/* Enviar a un amigo*/
	
	$("#send_amigos").live('click', function(){
		
		/* form contact us */
		$("#form_recomendar").validate({
			errorElement: "span",									 										
			errorClass: "error",
			focusInvalid: false,			
			rules: {
				your_name		: "required",
				your_email		: {
										required : true,
										email 	 : true
								  },
				friend_email	:{
										required : true,
										email 	 : true
								  }
			},
			errorPlacement: function(error, element) {
				error.appendTo(element.parent());
			},
			messages: {
				your_name		: "Indica tu nombre",
				your_email 		: "Indica un email válido",
				friend_email	: "Indica un email válido"
			}
		});
		
		if($("#form_recomendar").valid())
		{
			$("#enviar_amigo").hide();
			$("#loader_msg").show();
			$.ajax({
				type: "POST",
				dataType: "html",
				url:  base_url + "recomendar_amigo/send_amigo_fracc",
				data: $("#form_recomendar").serialize(),
				success: function(data){
					
					if("error" != data){
						$("#enviar_amigo").show();
						$("#loader_msg").hide();
						$.fancybox({padding: 0, content: data});
					}
					else
						alert(data);
				}
			});
		}
		
	});
	
	/* end enviar amigo */
	
	/* enviar al vendedor */
	
	$("#send_vendedor").live('click', function(){
		
		/* form contact us */
		$("#form_vendedor").validate({
			errorElement: "span",									 										
			errorClass: "error",
			focusInvalid: false,			
			rules: {
				your_name		: "required",
				your_email		: {
										required : true,
										email 	 : true
								  },
				your_telephone	: "required",
				your_comment	: "required",
			},
			errorPlacement: function(error, element) {
				error.appendTo(element.parent());
			},
			messages: {
				your_name		: "Indica tu nombre",
				your_email 		: "Indica un email válido",
				your_telephone	: "Indica tu teléfono",
				your_comment	: "Indica tus comentarios"
			}
		});
		
		if($("#form_vendedor").valid())
		{
			$("#enviar_vendedor").hide();
			$("#loader_msg").show();
			$.ajax({
				type: "POST",
				dataType: "html",
				url:  base_url + "recomendar_amigo/send_vendedor_fracc",
				data: $("#form_vendedor").serialize(),
				success: function(data){
					
					if("error" != data){
						$("#loader_msg").hide();
						$.fancybox({padding: 0, content: data});
					}
					else
						alert(data);
				}
			});
		}
		
	});
	
	/* end enviar al vendedor */
});