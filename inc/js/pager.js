$(document).ready(function(){
	
	$('.page_class').click(function(){
		
		var rel = $(this).attr('rel');
		
		if(rel=='page_numpage')
		{
			var num = $(this).html();
		}
		else
		{
			var num = '';
		}
		
		// alert(rel+'-'+num);
		
		$.ajax({
			
			url: base_url + '/pager/pagina',
			type: 'POST',
			dataType: 'html',
			data: { 'accion' : rel, 'num' : num },
			success: function(response){
				// alert(response);
				document.location=base_url+'searchengine/searchPaginator/';
			},
			error: function(){
				alert('no ajax');
			}
		});
	});
	
	
	
	$('.pagesel_class').change(function(){
		var rel = $(this).attr('rel');
		
		if(rel=='page_numpage' || rel=='page_respp')
		{
			var num = $(this).val();
		}
		else
		{
			var num = '';
		}
		
		$.ajax({
			
			url: base_url + '/pager/pagina',
			type: 'POST',
			dataType: 'html',
			data: { 'accion' : rel, 'num' : num },
			success: function(response){
				// alert(response);
				document.location=response;
			},
			error: function(){
				alert('no ajax');
			}
		});
	});
	
});