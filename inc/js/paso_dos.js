$(document).ready(function(){
	
	$('#fotos').uploadify({
		
		uploader		: base_url + 'inc/js/uploadify/uploadify.swf',
		script			: base_url + 'elevator/sube_fotos_propiedad/foto/temporal_propiedades/1024/728',
		cancelImg		: base_url + 'inc/js/uploadify/cancel.png',
		auto			: true,
		queueID			: 'conte_fotos',
		buttonText		: "Agregar fotos",
		fileExt			: '*.jpg;*.jpeg;*.gif;*.png;*.JPG;*.JPEG;*.GIF;*.PNG',
		fileDataName 	: 'file_foto',
		multi			: true,
		queueSizeLimit	: 30,
		onSelect		: function(event,ID,fileObj)
							{
								var cuenta=$('input[name="fotos[]"]').length;//numero de fotos
								var nuevo_queueSizeLimit = 30 - cuenta;
								$('#fotos').uploadifySettings('scriptData', {'cuenta': cuenta, 'limite' : 30});
								$('#fotos').uploadifySettings('queueSizeLimit', nuevo_queueSizeLimit);
							},
		onComplete		: function(event,queueID,fileObj,response,data)
							{
								// alert(response);
								if(response!='error')
								{
									var cuenta = $('#contador_fotos').val();
									
									$('#fotos_slide').append(response);
									
 									cuenta = parseInt(cuenta) + 1;
									$('#contador_fotos').val(cuenta);
									$('#conteo_fotos').val(cuenta);
									$('#conteo_fotos').removeClass('error');
									$('#err_foto').html('');
								}
							}
	});
	
	$("#fotos_slide").dragsort({ dragSelector: "div", placeHolderTemplate: "<li style='background-color:gray !important;'></li>" });
	
	// Validaciones
	$("#elForm").validate({
		errorElement: "span",
		errorClass: "error",
		// focusInvalid: false,
		rules:
		{
			'conteo_fotos'		: { min: 1 }
		},
	
		messages:
		{
			'conteo_fotos'		: 'Faltan fotos.'
		},
	
		errorPlacement: function(error, element)
		{
			// error.appendTo(element.next());
			switch(element.attr('name'))
			{
				case 'conteo_fotos'			: error.appendTo('#err_foto'); break;
			}
		}
	});
});

function delete_foto(id)
{
	var archivo = $('#hide_'+id).val();
	var cuenta = $('#contador_fotos').val();
	
	cuenta = parseInt(cuenta) - 1;
	$('#contador_fotos').val(cuenta);
	$('#conteo_fotos').val(cuenta);
	
	$('#li_'+id).remove();
	// $('#hide_'+id).remove();
	
	delete_archivo('uploads/img/temporal_propiedades/'+archivo);
}

function delete_archivo(file)
{
	$.ajax({
			type 		:	'POST',
			url			:	base_url + 'elevator/borra_archivo',
			data		:	{'archivo' : file },
			dataType	:	'html',
			success		:	function( response )
							{
								
							},
			error		:	function()
							{
								alert('Actualize su navegador.');
							}
		});
}