var NumeroDeFilasCotizacion = 1 ;
var NumeroDeFilasResultado = 1 ;


function DisableEnableForm(xForm,xHow){
  objElems = xForm.elements;
  for(i=0;i<objElems.length;i++){
    objElems[i].disabled = xHow;
  }
}



function recuperar(){
				
				var recuperar_email = $('#recuperar_email').val();
				$.ajax({
						type 		:	'POST',
						url			:'login/recuperar',
						data		:	{'email' : recuperar_email },
						dataType	:	'html',
						success		:	function( response )
										{
                                                                              
											if(response == 'si'){
												
												alert('Tu contraseña ha sido enviado a tu correo');
												
											}else{
												alert('Correo Invalido');
												
											}
										},
						error		:	function()
										{
											alert('Actualize su navegador.');
										}
					});
				
}



function AddFilaCotizacion(NumFila){
							$.ajax( {
												async:false,
												dataType: "html",
												type: "POST",
												url: base_url + 'documentos/filasCotizacion' ,
												data: 'NumFila=' + NumFila ,
												global: true,
												ifModified: false,
												processData:true,
												contentType: "application/x-www-form-urlencoded",
												success: function(datos){
													$('#tabla_de_filas_cotizacion').append(datos);
													NumeroDeFilasCotizacion++ ;
												}
											});
											return false ;
}


function AddFilaCertificacion(NumFila){
              $.ajax( {
                        async:false,
                        dataType: "html",
                        type: "POST",
                        url: base_url + 'catalogos/filasCertificacion' ,
                        data: 'NumFila=' + NumFila ,
                        global: true,
                        ifModified: false,
                        processData:true,
                        contentType: "application/x-www-form-urlencoded",
                        success: function(datos){
                          $('#tabla_de_filas_cotizacion').append(datos);
                          NumeroDeFilasCotizacion++ ;
                        }
                      });
                      return false ;
}


function MasFilaPedidos(id){
		

		if($('#Nombres').val() != ''){

			if($('#servicio_' + id).val() != ''){
					
					AddFilaCotizacion(NumeroDeFilasCotizacion) ;
			
			}else{

					alertify.alert('Debe agregar un servicio primero');

			}
		}else{

			alertify.alert('Debe seleccionar un cliente');

		}
												
	
												
											
}



function MasFilaCertificacion(id){
    

    if($('#Nombres').val() != ''){

      if($('#servicio_' + id).val() != ''){
          
          AddFilaCertificacion(NumeroDeFilasCotizacion) ;
      
      }else{

          alertify.alert('Debe agregar un examen primero');

      }
    }else{

      alertify.alert('Debe agregar Nombre y Razon social');

    }
                        
  
                        
                      
}



function AddFilaResultado(NumFila,resultadosRenglon){

     var certificacionId = $('#certificacionId').val();
              $.ajax( {
                        async:false,
                        dataType: "html",
                        type: "POST",
                        url: base_url + 'documentos/filasResultado' ,
                        data: 'NumFila=' + (NumFila+1) + '&certificacionId='+certificacionId+'&resultadosRenglon='+resultadosRenglon ,
                        global: true,
                        ifModified: false,
                        processData:true,
                        contentType: "application/x-www-form-urlencoded",
                        success: function(datos){
                          $('#tabla_de_filas_cotizacion').append(datos);
                          NumeroDeFilasResultado++ ;
                        }
                      });
                      return false ;
}

function MasFilaResultado(id){
    

  
      if($('#servicio_' + id).val() != ''){

         

          if(id > 1){
              IdServicio = '';
              for (var i = 1; i <=id ; i++) {
                  IdServicio += $('#servicio_' + i).val()+'-';
              }

          }else{
            IdServicio =  $('#servicio_' + id).val();
          }

          
          AddFilaResultado(NumeroDeFilasResultado,IdServicio) ;
      
      }else{

          alertify.alert('Debe elegir un servicio');

      }
                        
                      
}

function MenosFilaResultados(id){
        $('#fila_cotizacion_' + id).remove();
        NumeroDeFilasCotizacion = NumeroDeFilasCotizacion - 1;
      
}


function MasFilaPedidosEditar(id){
        
        NumeroDeFilasCotizacion = $('#totalFilas').val();


        if($('#Nombres').val() != ''){

            if($('#servicio_' + id).val() != ''){
                    
                    AddFilaCotizacion(NumeroDeFilasCotizacion) ;
            
            }else{

                    alertify.alert('Debe agregar un servicio primero');

            }
        }else{

            alertify.alert('Debe seleccionar un cliente');

        }
                                                
    
                                                
                                            
}


function MenosFilaPedidos(id){
				$('#fila_cotizacion_' + id).remove();
				NumeroDeFilasCotizacion = NumeroDeFilasCotizacion - 1;
			
}


function MenosFilaCertificacion(id){
        $('#fila_cotizacion_' + id).remove();
        NumeroDeFilasCotizacion = NumeroDeFilasCotizacion - 1;
      
}


function MenosFilaPedidosEditar(id){

                NumeroDeFilasCotizacion = $('#totalFilas').val();
                NumeroDeFilasCotizacion  = parseInt(NumeroDeFilasCotizacion) - 1;
                $('#totalFilas').val(NumeroDeFilasCotizacion);

                $('#fila_cotizacion_' + id).remove();
                NumeroDeFilasCotizacion = NumeroDeFilasCotizacion - 1;
            
}


 function MostrarCliente(){
         $.ajax ({
                url     : base_url + 'documentos/mostrarClientes',
                type    : 'POST',
                data    : '',
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_clientes').html(data);
                    $('#modal_clientes').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay listado</strong></div>';
                        $('#contenido_modal_clientes').html(content);
                       $('#modal_clientes').modal('show');
                   }
                }
            });
      }


function RelacionarEvaluados(){

     IdEmpresaSecundaria = $('#IdEmpresaSecundaria').val();
     idCotizacion = $('#idCotizacion').val();

   $.ajax ({
                url     : base_url + 'documentos/mostrarEvaluados',
                type    : 'POST',
                data    : 'IdEmpresaSecundaria='+IdEmpresaSecundaria+'&idCotizacion='+idCotizacion,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_evaluados').html(data);
                    $('#modal_evualuados').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay listado</strong></div>';
                        $('#contenido_modal_evaluados').html(content);
                       $('#modal_evualuados').modal('show');
                   }
                }
            });

}



 function cargarClientes(id){
 	 $.ajax ({
                url     	: base_url + 'documentos/buscarClientes',
                type   		: 'POST',
                data    	: 'id='+id,
                dataType	: 'json',
                async  		: false,
                cache       : false,
                success     : function(data){
                    	console.log(data);

                    	$('#Nombres').val(data['RazonSocial']);
                    	$('#Rfc').val(data['Rfc']);

                      var DomicilioFiscal = '';
                      if(data['calleFiscal'] != '' && data['calleFiscal'] != null){
                        DomicilioFiscal = DomicilioFiscal+data['calleFiscal'];
                      }

                      if(data['numroExtFiscal'] != '' && data['numroExtFiscal'] != null){
                        DomicilioFiscal = DomicilioFiscal+', '+data['numroExtFiscal'];
                      }

                      if(data['numeroIntFiscal'] != '' && data['numeroIntFiscal'] != null){
                        DomicilioFiscal = DomicilioFiscal+', '+data['numeroIntFiscal'];
                      }


                      if(data['ColoniaFiscal'] != '' && data['ColoniaFiscal'] != null){
                        DomicilioFiscal = DomicilioFiscal+', '+data['ColoniaFiscal'];
                      }

                       if(data['municipioFiscal'] != '' && data['municipioFiscal'] != null){
                        DomicilioFiscal = DomicilioFiscal+', '+data['municipioFiscal'];
                      }

                      if(data['estadoFiscal'] != '' && data['estadoFiscal'] != null){
                        DomicilioFiscal = DomicilioFiscal+', '+data['estadoFiscal'];
                      }

                      if(data['codigoPostal'] != '' && data['codigoPostal'] != null){
                        DomicilioFiscal = DomicilioFiscal+', '+data['codigoPostal'];
                      }

                    	$('#DomicilioFiscal').val(DomicilioFiscal);
                    	$('#IdEmpresaSecundaria').val(data['id']);

                    	$('#modal_clientes').modal('hide');
                }
            });
 }


  function cargarServicios(id,numFila){
 	 $.ajax ({
                url     	: base_url + 'documentos/buscarServicios',
                type   		: 'POST',
                data    	: 'id='+id+'&idEmpresa='+$('#IdEmpresaSecundaria').val(),
                dataType	: 'json',
                async  		: false,
                cache       : false,
                success     : function(data){
                    	


                    	$('#servicio_'+numFila).val(data['Certificaciones']);
                    	$('#id_servicio_'+numFila).val(data['id']);
                      $('#claveUnidad_'+numFila).val(data['claveUnidad']);
                      $('#claveProdServ_'+numFila).val(data['claveProdServ']);
                      $('#precio_'+numFila).val(data['Precio']);
                      $('#cantidad_'+numFila).val(data['Cantidad']);

                      $('#importe_'+numFila).val(data['Cantidad']*data['Precio']);

                    	$('#modal_servicio').modal('hide');
                    	
                }
            });
 }


   function cargarServiciosClientes(id,numFila){
   $.ajax ({
                url       : base_url + 'documentos/buscarServicios',
                type      : 'POST',
                data      : 'id='+id+'&idEmpresa='+$('#IdEmpresaSecundaria').val(),
                dataType  : 'json',
                async     : false,
                cache       : false,
                success     : function(data){
                      


                      $('#servicio_'+numFila).val(data['Certificaciones']);
                      $('#id_servicio_'+numFila).val(data['id']);
                      $('#precio_'+numFila).val(data['Precio']);
                      $('#cantidad_'+numFila).val(data['Cantidad']);
                      $('#modal_examenes').modal('hide');
                      
                }
            });
 }


 function renglon(numFila){

 		 
 	if($('#Nombres').val() != ''){
 		 $.ajax ({
                url     : base_url + 'documentos/mostrarServicios',
                type    : 'POST',
                data    : 'numeroFila='+numFila,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_servicio').html(data);
                    $('#modal_servicio').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay listado</strong></div>';
                        $('#contenido_modal_servicio').html(content);
                       $('#modal_servicio').modal('show');
                   }
                }
            });
 	}else{
 			alertify.alert('Debe seleccionar un cliente');
 	}

 }


  function renglonCertificacion(numFila){

     
  if($('#Nombres').val() != ''){
     $.ajax ({
                url     : base_url + 'documentos/mostrarServiciosClientes',
                type    : 'POST',
                data    : 'numeroFila='+numFila,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_examenes').html(data);
                    $('#modal_examenes').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay listado</strong></div>';
                        $('#contenido_modal_examenes').html(content);
                       $('#modal_examenes').modal('show');
                   }
                }
            });
  }else{
      alertify.alert('Debe agregar Nombre y Razon social');
  }

 }


function CalcularFilaCotizacion(numFila){

		var cantidad = $('#cantidad_' + numFila).val();
		var precio = $('#precio_' + numFila).val();


		if(cantidad != '' && precio != ''){
			var importe = (cantidad) * ((precio));

			$('#importe_' + numFila).val(importe);
		}

}



function AutorizarCotizacion(id){

 


         $.ajax ({
                url     : base_url + 'documentos/mostrarDocumentoAutorizar',
                type    : 'POST',
                data    : 'id='+id,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_clientes_autorizar').html(data);
                          $('#modal_clientes_autorizar').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay información</strong></div>';
                        $('#contenido_modal_clientes_autorizar').html(content);
                       $('#modal_clientes_autorizar').modal('show');
                   }
                }
            });
      
}




function verFactura(id){

 


         $.ajax ({
                url     : base_url + 'documentos/mostrarDocumentoFacturar',
                type    : 'POST',
                data    : 'id='+id,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_clientes_autorizar').html(data);
                          $('#modal_clientes_autorizar').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay información</strong></div>';
                        $('#contenido_modal_clientes_autorizar').html(content);
                       $('#modal_clientes_autorizar').modal('show');
                   }
                }
            });
      
}


function verFacturaRecibo(id){

 


         $.ajax ({
                url     : base_url + 'documentos/mostrarDocumentoFacturarRecibo',
                type    : 'POST',
                data    : 'id='+id,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_clientes_autorizar').html(data);
                          $('#modal_clientes_autorizar').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay información</strong></div>';
                        $('#contenido_modal_clientes_autorizar').html(content);
                       $('#modal_clientes_autorizar').modal('show');
                   }
                }
            });
      
}


function gridVerCliente(id){


   $.ajax ({
                url     : base_url + 'catalogos/mostrarClientesGrid',
                type    : 'POST',
                data    : 'id='+id,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_clientes_ver').html(data);
                          $('#modal_clientes_ver').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay información</strong></div>';
                        $('#contenido_modal_clientes_ver').html(content);
                       $('#modal_clientes_ver').modal('show');
                   }
                }
            });

}

function verEvaluaciones(id){

    $.ajax ({
                url     : base_url + 'documentos/mostrarEvaluaciones',
                type    : 'POST',
                data    : 'id='+id,
                async   : false,
                cache   : false,
                success : function(data){
                   if( 1 < data.length ){
                        // Mostramos la modal
                        $('#contenido_modal_clientes_autorizar').html(data);
                          $('#modal_clientes_autorizar').modal('show');

                   } else {
              
                        content = '<div class="alert alert-danger" role="alert"><strong>No hay información</strong></div>';
                        $('#contenido_modal_clientes_autorizar').html(content);
                       $('#modal_clientes_autorizar').modal('show');
                   }
                }
            });
}


function VerDetallerEvaluacion(){

    $('#modal_clientes_autorizar').modal('hide');

    content = 'Formulario para agregar';
        $('#contenido_modal_clientes_autorizar_detalle').html(content);
         $('#modal_clientes_autorizar_detalle').modal('show');
}


function TipoMunicipio(id){
  
    $.ajax({
      type    : 'POST',
      url     : base_url + 'elevator/tipoMunicipio',
      data    : {'idEstado' : id },
      dataType  : 'html',
      success   : function( response )
              {
                $('#Municipio').html(response);
              },
      error   : function()
              {
                alert('Actualize su navegador.');
              }
    });
  
}


function Personal(id){
  
    $.ajax({
      type    : 'POST',
      url     : base_url + 'elevator/tipoPersonal',
      data    : {'idEmpresa' : id },
      dataType  : 'html',
      success   : function( response )
              {
                $('#Empleado').html(response);
              },
      error   : function()
              {
                alert('Actualize su navegador.');
              }
    });
  
}


function TipoEvaluado(id){
  
    $.ajax({
      type    : 'POST',
      url     : base_url + 'elevator/evaluadoMostrar',
      data    : {'idEvaluado' : id },
      dataType  : 'html',
      success   : function( response )
              {
                $('#idEvaluado').html(response);
              },
      error   : function()
              {
                alert('Actualize su navegador.');
              }
    });
  
}








