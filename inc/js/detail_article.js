$(document).ready(function(){
	
	/* Enviar a un amigo*/
	
	$("#send_amigos").live('click', function(){
		
		/* form contact us */
		$("#form_recomendar").validate({
			errorElement: "span",									 										
			errorClass: "error",
			focusInvalid: false,			
			rules: {
				your_name		: "required",
				your_email		: {
										required : true,
										email 	 : true
								  },
				friend_email	:{
										required : true,
										email 	 : true
								  }
			},
			errorPlacement: function(error, element) {
				error.appendTo(element.parent());
			},
			messages: {
				your_name		: "Indica tu nombre",
				your_email 		: "Indica un email válido",
				friend_email	: "Indica un email válido"
			}
		});
		
		if($("#form_recomendar").valid())
		{
			$("#enviar_amigo").hide();
			$("#loader_msg").show();
			$.ajax({
				type: "POST",
				dataType: "html",
				url:  base_url + "recomendar_amigo/send_art_amigo",
				data: $("#form_recomendar").serialize(),
				success: function(data){
					
					if("error" != data){
						$("#enviar_amigo").show();
						$("#loader_msg").hide();
						$.fancybox({padding: 0, content: data});
					}
					else
						alert(data);
				}
			});
		}
		
	});
	
	/* end enviar amigo */
	
});
