$(function() { 
	var emailreg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;	
	$(".boton_enviar").click(function(){  
		$(".error").fadeOut().remove();		
		
        if ($(".nombre").val() == "" || $(".nombre").val().length<5) {  
			$(".nombre").focus().after('<span class="error">Ingrese su nombre (mínimo 5 carácteres)</span>');  
			$(".nombre").addClass('faltante');
			return false;  
		}  
        if ($(".email").val() == "" || !emailreg.test($(".email").val())) {
			$(".email").focus().after('<span class="error">Ingrese un email válido usted@dominio.com</span>');
			$(".email").addClass('faltante');  
			return false;  
		}  
        if ($(".telefono").val() == "" || isNaN($(".telefono").val()) || $(".telefono").val().length<10){  
			$(".telefono").focus().after('<span class="error">Ingrese un Teléfono válido de 10 números </span>');
			$(".telefono").addClass('faltante');  
			return false;  
		}  
        if ($(".mensaje").val() == "") {  
			$(".mensaje").focus().after('<span class="error">Ingrese un mensaje</span>');   
			$(".mensaje").addClass('faltante');
			return false;			  
		}  
    });  
	$(".nombre, .asunto, .mensaje, .telefono").bind('blur keyup', function(){  
        if ($(this).val() != "") {  			
			$('.error').fadeOut();
			$(this).removeClass('faltante');
			return false;  
		}  
	});	
	$(".email").bind('blur keyup', function(){  
        if ($(".email").val() != "" && emailreg.test($(".email").val())) {	
			$('.error').fadeOut();  
			$(this).removeClass('faltante');
			return false;  
		}  
	});
});