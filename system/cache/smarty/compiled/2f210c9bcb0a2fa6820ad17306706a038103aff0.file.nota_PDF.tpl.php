<?php /* Smarty version Smarty-3.1.10, created on 2018-03-21 15:01:37
         compiled from "application/views/Documentos/nota_PDF.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7728027035ab2c8313b49f7-09185344%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f210c9bcb0a2fa6820ad17306706a038103aff0' => 
    array (
      0 => 'application/views/Documentos/nota_PDF.tpl',
      1 => 1520447746,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7728027035ab2c8313b49f7-09185344',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'Documentos' => 0,
    'iva' => 0,
    'total' => 0,
    'arrayCFDI' => 0,
    'item' => 0,
    'arrayCompletos' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_5ab2c83151ea33_67294967',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ab2c83151ea33_67294967')) {function content_5ab2c83151ea33_67294967($_smarty_tpl) {?><html>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<link href="<?php echo base_url();?>
inc/css_apro/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>
inc/css_apro/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/font-awesome.min.css" />
		

        <link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/chosen.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/daterangepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/colorpicker.css" /> 
		

		<!--[if IE 7]>
		  <link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/font-awesome-ie7.min.css" />
		<![endif]-->

		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/ace-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/ace-skins.min.css" />
<body>
<div class="tab-content">
<table width="100%" >
	<tr>
		<td width="60%">
		 	<img src="<?php echo base_url();?>
/inc/logo.jpg"/>
		 	<br>
		 	<div align="center">
		 	
			Sistema Opalo BR s.a. de c.v.
			<br>
			RFC: SOB151104AS6
		 	<br>
		 	Av. Miguel Ángel #14
			Col. Real vallarta, C.P. 45020 Zapopan, Jalisco.<br>
			Tel.(33) 1594 5644<br>
			www.sistemaopalo.com.mx
			

	
			</div>
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white">Folio:</font></div> <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['nota'];?>


		</td>
	</tr>

	<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
	<tr>
		<td width="60%">
		 	
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white"> Folio Nota de credito:</font></div> <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['nofol'];?>

		</td>
	</tr>
		<tr>
		<td width="60%">
		 	
 		</td>
		<td width="40%" align="center">
				<div style="background-color: #307ecc"><font color="white"> No. de serie del certificado del CSD:</div></font></div> <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['noert'];?>


		</td>
	</tr>
	<?php }?>
</table>



	<div id="informacion" class="tab-pane in active">
		<div class="table-header">

				Información

				<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
				 	Facturación
				<?php }else{ ?>
					Recibo

				<?php }?>
		</div>
		<table width="100%" border="0">
			<tr>											 			            
			<td width="100%" align="left">
			  <B>Fecha Documento:</B><?php echo $_smarty_tpl->tpl_vars['Documentos']->value['Fecha'];?>

			</td>
			</tr>

		</table>

													<table width="100%" border="0">
														<tr>
												      <td colspan="2">
												          <div id="error_nombre" class="control-group">
																  <div >
																         <B>Empresa:</B>
																           <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['NombreCliente'];?>

																  
			
																           
																    </div>  
														   </div> 
												      </td>
												      <td colspan="1">
												          <div id="error_rfc" class="control-group">
																  <div >
																          <B>RFC:</B>
																           <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['Rfc'];?>

																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Domicilio fiscal:</B>
																           <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['DireccionCliente'];?>

																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr>
												  <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Telefono:</B>
																           <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['Telefono'];?>

																          
																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 

												   <tr>
												      <td colspan="3">
												          <div id="error_domicilio_fiscal" class="control-group">
																  <div >
																           <B>Comentarios:</B>
																         	<?php echo $_smarty_tpl->tpl_vars['Documentos']->value['Comentarios'];?>

																           
																    </div>  
														   </div> 
												      </td>
												  </tr> 
													
												</table>
												<div class="table-header">
													Detalle 
													<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
													 	Facturación
													<?php }else{ ?>
														Recibo

													<?php }?>
												</div>

												<div class="widget-body">
												<div class="widget-main">
													<table id="tabla_de_filas_cotizacion" class="table table-striped table-bordered table-hover">		
														<thead>
															<tr>
															
																<th width="35%">Descripción</th>
																<th width="20%">Cantidad</th>
																<th width="20%">Precio</th>
																<th width="15%">Importe</th>					
															</tr>
													

															<tr>
															
																<th width="35%">NOTA DE CREDITO</th>
																<th width="20%">1</th>
																<th width="20%">$<?php echo number_format($_smarty_tpl->tpl_vars['Documentos']->value['novalimp'],2,".",",");?>
</th>
																<th width="15%">$<?php echo number_format($_smarty_tpl->tpl_vars['Documentos']->value['novalimp'],2,".",",");?>
</th>					
															</tr>
													
														</thead>

														
													
													 </table>
													  <table width="100%">
													       <tr>
													       		<td colspan="2" width="52%" align="center">
													       			
													       			Este documento es una representación impresa de un CFDI
													       		</td>

													       		<td align="right" colspan="1" width="32%">
													       		<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
													       		<B>Subtotal:</B>
													       		<?php }else{ ?>
													       		<B>Total:</B>
													       		<?php }?>
													       		</td>
													       		<td align="center" colspan="1" width="16%">$<?php echo number_format($_smarty_tpl->tpl_vars['Documentos']->value['novalimp'],2,".",",");?>
</td>
													       </tr>
													       <?php $_smarty_tpl->tpl_vars['iva'] = new Smarty_variable($_smarty_tpl->tpl_vars['Documentos']->value['novalimp']*0.16, null, 0);?>
													       <tr>
													       		<td colspan="2" width="52%" align="center">
													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']!=''){?>
													       			Metodo de pago: 
																<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']!='98'){?>
													       			<?php echo $_smarty_tpl->tpl_vars['Documentos']->value['formaPago'];?>

																<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='01'){?>
													       				Efectivo
													       			<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='02'){?>
													       				Cheque normativo
													       			<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='03'){?>
													       				Transferencia electronica de fondos
													       			<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='04'){?>
													       				Tarjeta de Crédito
													       			<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='28'){?>
													       				Tarjeta de Débito
													       			<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='99'){?>
													       				Otros
													       			<?php }?>

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['formaPago']=='98'){?>
													       				No aplica
													       			<?php }?>
													       			, 
													       			
													       			<?php }?>
													       			

													       			<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['CuentaBancariaCliente']!=''){?>
													       			cuenta 
													       			<?php echo $_smarty_tpl->tpl_vars['Documentos']->value['CuentaBancariaCliente'];?>
 <?php echo $_smarty_tpl->tpl_vars['Documentos']->value['bancoCliente'];?>

													       			<?php }?>
													       		</td>
													       		<td align="right" colspan="1" width="32%">
													       		<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
													       		<B>Iva:</B>
													       		<?php }?>
													       		</td>
													       		<td align="center" colspan="1" width="16%">
													       		<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
													       		$<?php echo number_format($_smarty_tpl->tpl_vars['iva']->value,2,".",",");?>

													       		<?php }?>
													       		</td>
													       </tr>
													       <?php $_smarty_tpl->tpl_vars['total'] = new Smarty_variable($_smarty_tpl->tpl_vars['Documentos']->value['novalimp']+$_smarty_tpl->tpl_vars['iva']->value, null, 0);?>
													       <tr>
													       		<td colspan="2" width="52%">
													       			
													       			
													       		</td>
													       		<td align="right" colspan="1" width="32%">
													       		<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
													       		<B>Total:</B>
													       		<?php }?>
													       		</td>
													       		<td align="center" colspan="1" width="16%">
													       		<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
													       		$<?php echo number_format($_smarty_tpl->tpl_vars['total']->value,2,".",",");?>

													       		<?php }?>
													       		</td>
													       </tr>
													 </table>
													
												</div>
											
												


											</div>



	</div>


	<br>


	
	<?php if ($_smarty_tpl->tpl_vars['Documentos']->value['Estatus']==2){?>
	<div style="background-color: #307ecc"><font color="white">Sello Digital CFDI</font></div> 
	<div align="">
			

			<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['arrayCFDI']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
			
				<font style="font-size: 55%;"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</font>
			
			<?php } ?>	
	</div>
	<div style="background-color: #307ecc"><font color="white">Sello del SAT</font></div> 
	<div align="">
			
			
				<font style="font-size: 55%;"><?php echo $_smarty_tpl->tpl_vars['Documentos']->value['SelloSat'];?>
</font>
			
			
	</div>
	<div style="background-color: #307ecc"><font color="white">Cadena original del complemento de certificación digital del SAT</font></div> 
	<div align="">
			
			<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['arrayCompletos']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
			
				<font style="font-size: 55%;"><?php echo $_smarty_tpl->tpl_vars['item']->value;?>
</font>
			
			<?php } ?>	
			
			
			
			
	</div>
	<?php }?>


	<div style="background-color: #307ecc" align="center"><font color="white">Cuenta bancaria OPALO</font></div> 
	<div align="CENTER">
			BANCO: BANREGIO <br>
			SISTEMA OPALO BR SA DE SV
			<br>
			CTA: 134012460011<br>
			CLABE: 0583200-00001232350<br>

			
				
			
			
	</div>

</div>
</body>
</html>
<?php }} ?>