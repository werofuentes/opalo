<?php /* Smarty version Smarty-3.1.10, created on 2018-02-16 00:53:59
         compiled from "application/views/structure/menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4784103565a8680072e4791-62958500%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'caa8d179269fcb4da018d5c8056845e81d4e4cb6' => 
    array (
      0 => 'application/views/structure/menu.tpl',
      1 => 1496598685,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4784103565a8680072e4791-62958500',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_5a86800740bce9_92190248',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a86800740bce9_92190248')) {function content_5a86800740bce9_92190248($_smarty_tpl) {?><div class="sidebar" id="sidebar">
				<ul class="nav nav-list">


					<?php if ($_SESSION['PERMISOS']['per_empresa_primaria']==1||$_SESSION['PERMISOS']['per_empresa_secundaria']==1||$_SESSION['PERMISOS']['per_certificaciones']==1||$_SESSION['PERMISOS']['per_examenes']==1||$_SESSION['PERMISOS']['per_vendedores']==1||$_SESSION['PERMISOS']['per_sucursales']==1){?>
					<li id="catalogos">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-book"></i>
							<span class="menu-text"> Catalogos </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">

						  <!--
							<li id="empresasPrimarias">
								<a  href="<?php echo base_url();?>
catalogos/primarias">
									<i class="icon-double-angle-right"></i>
									Empresas Primarias
								</a>
							</li>
							
							<li id="empresasSecundarias">
								<a  href="<?php echo base_url();?>
catalogos/secundaria">
									<i class="icon-double-angle-right"></i>
									Empresas Secundarias
								</a>
							</li>
							-->

							<?php if ($_SESSION['PERMISOS']['per_empresa_primaria']==1){?>
							<li id="empresasSecundarias">
								<a  href="<?php echo base_url();?>
catalogos/secundaria">
									<i class="icon-double-angle-right"></i>
									Empresas Primarias
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_empresa_secundaria']==1){?>
							<li id="empresasPrimarias">
								<a  href="<?php echo base_url();?>
catalogos/primarias">
									<i class="icon-double-angle-right"></i>
									Empresas Secundarias
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_certificaciones']==1){?>
							<li id="certificacion">
								<a  href="<?php echo base_url();?>
catalogos/certificaciones">
									<i class="icon-double-angle-right"></i>
									Certificaciones
								</a>
							</li>
							<?php }?>



							<?php if ($_SESSION['PERMISOS']['per_examenes']==1){?>
							<!--<li id="servicio">
								<a  href="<?php echo base_url();?>
catalogos/servicios">
									<i class="icon-double-angle-right"></i>
									Examenes
								</a>
							</li>-->
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_vendedores']==1){?>
							<li id="vendedores">
								<a  href="<?php echo base_url();?>
catalogos/vendedores">
									<i class="icon-double-angle-right"></i>
									Vendedores
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_freelance']==1){?>
							<li id="freelance">
								<a  href="<?php echo base_url();?>
catalogos/freelance">
									<i class="icon-double-angle-right"></i>
									Trabajadora social
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_psicologa']==1){?>
							<li id="psicologa">
								<a  href="<?php echo base_url();?>
catalogos/psicologas">
									<i class="icon-double-angle-right"></i>
									Psicologas(os)
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_sucursales']==1){?>
							<li id="sucursales">
								<a  href="<?php echo base_url();?>
catalogos/sucursales">
									<i class="icon-double-angle-right"></i>
									Sucursales
								</a>
							</li>
							<?php }?>
						</ul>
					</li>
					<?php }?>

					<?php if ($_SESSION['PERMISOS']['per_cotizacion']==1||$_SESSION['PERMISOS']['per_evaluacion']==1||$_SESSION['PERMISOS']['per_factrec']==1||$_SESSION['PERMISOS']['per_cobranza']==1){?>
					<li id="administracion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-briefcase"></i>
							<span class="menu-text"> Administracion </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">


							<?php if ($_SESSION['PERMISOS']['per_cotizacion']==1){?>
							<li id="cotizacion">
								<a  href="<?php echo base_url();?>
documentos/cotizacion">
									<i class="icon-double-angle-right"></i>
									Cotización
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_evaluacion']==1){?>
							<li id="evaluaciones">
								<a  href="<?php echo base_url();?>
documentos/evaluacion">
									<i class="icon-double-angle-right"></i>
									Evaluación
								</a>
							</li>
							<?php }?>
							

							<?php if ($_SESSION['PERMISOS']['per_factrec']==1){?>
							<li id="recibos">
								<a  href="<?php echo base_url();?>
documentos/Recibos">
									<i class="icon-double-angle-right"></i>
									Recibos
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_factrec']==1){?>
							<li id="facturacion">
								<a  href="<?php echo base_url();?>
documentos/facturacionRecibos">
									<i class="icon-double-angle-right"></i>
									Facturación
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_cobranza']==1){?>
							<li id="cobranza">
								<a  href="<?php echo base_url();?>
documentos/cobranza">
									<i class="icon-double-angle-right"></i>
									Cobranza
								</a>
							</li>
							<?php }?>
						</ul>
					</li>
					<?php }?>


					<?php if ($_SESSION['PERMISOS']['per_generarCredencial']==1||$_SESSION['PERMISOS']['per_registroMovimientos']==1){?>

					<li id="credencializacion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-credit-card"></i>
							<span class="menu-text"> Credencialización </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">

							<?php if ($_SESSION['PERMISOS']['per_generarCredencial']==1){?>
							<li id="generar">
								<a  href="<?php echo base_url();?>
credencial/generar">
									<i class="icon-double-angle-right"></i>
									Generar Credencial
								</a>
							</li>
							<?php }?>
								
							<?php if ($_SESSION['PERMISOS']['per_registroMovimientos']==1){?>
							<li id="movimientos">
								<a  href="<?php echo base_url();?>
credencial/movimientos">
									<i class="icon-double-angle-right"></i>
									Registros de movimientos
								</a>
							</li>
							<?php }?>
						</ul>
					</li>

					<?php }?>
	
					
                <?php if ($_SESSION['PERMISOS']['per_reporte_mov_personal']==1||$_SESSION['PERMISOS']['per_reporte_estatus_personal']==1||$_SESSION['PERMISOS']['per_reporte_pagos']==1||$_SESSION['PERMISOS']['per_reporte_pendientes_facturar']==1||$_SESSION['PERMISOS']['per_reporte_pendientes_cobrar']==1){?>

					<li id="reportes">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-bar-chart"></i>
							<span class="menu-text"> Reportes </span>

							<b class="arrow icon-angle-down"></b>
						</a>
						
						<ul class="submenu">
							
							<?php if ($_SESSION['PERMISOS']['per_reporte_estatus_personal']==1){?>
							<li id="reportes_estatus_personal">
								<a  href="<?php echo base_url();?>
reporte/EstatusPersonal">
									<i class="icon-double-angle-right"></i>
									Estatus de personal
								</a>
							</li>
							<?php }?>
							<?php if ($_SESSION['PERMISOS']['per_reporte_pagos']==1){?>
							<li id="reportes_pagos">
								<a  href="<?php echo base_url();?>
reporte/Pagos">
									<i class="icon-double-angle-right"></i>
									Cobranza
								</a>
							</li>
							<?php }?>

							<li id="reportes_freelance">
								<a  href="<?php echo base_url();?>
reporte/Freelance">
									<i class="icon-double-angle-right"></i>
									<!--Trabajadora social-->
									ESE realizados
								</a>
							</li>

							<li id="reportes_psicologa">
								<a  href="<?php echo base_url();?>
reporte/Psicologa">
									<i class="icon-double-angle-right"></i>
									<!--Psicologa-->
									Evaluaciones realizadas
								</a>
							</li>


							<li id="reportes_pendientes_evaluados">
								<a  href="<?php echo base_url();?>
reporte/pendientesEvaluados">
									<i class="icon-double-angle-right"></i>
									Evaluados pendientes de facturar
								</a>
							</li>

							<li id="reportes_evaluaciones_realizadas">
								<a  href="<?php echo base_url();?>
reporte/evaluacionesRealizadas">
									<i class="icon-double-angle-right"></i>
									Resumen de Evaluaciones 
								</a>
							</li>

							<?php if ($_SESSION['PERMISOS']['per_reporte_mov_personal']==1){?>
							<li id="reportes_movimientos_personal">
								<a  href="<?php echo base_url();?>
reporte/Movimientos">
									<i class="icon-double-angle-right"></i>
									Movimientos personal
								</a>
							</li>
							<?php }?>

							<!--
							<?php if ($_SESSION['PERMISOS']['per_reporte_pendientes_facturar']==1){?>
							<li id="reportes_pendiente_facturar">
								<a  href="<?php echo base_url();?>
reporte/FacturasRecibos">
									<i class="icon-double-angle-right"></i>
									Facturas/Recibos
								</a>
							</li>
							<?php }?>
							-->
							
						</ul>
					</li>
    					<?php }?>
                   
                  	 <?php if ($_SESSION['PERMISOS']['per_usuarios_sistema']==1||$_SESSION['PERMISOS']['per_perfil_usuario']==1){?>

					<li id="configuaracion">
						<a  href="#" class="dropdown-toggle">
							<i class="icon-inbox"></i>
							<span class="menu-text"> Configuracion </span>

							<b class="arrow icon-angle-down"></b>
						</a>
                        
						<ul class="submenu">

							<?php if ($_SESSION['PERMISOS']['per_usuarios_sistema']==1){?>
							<li id="configuaracion_usuarios">
								<a  href="<?php echo base_url();?>
users">
									<i class="icon-double-angle-right"></i>
									Usuarios del Sistema
								</a>
							</li>
							<?php }?>

							<?php if ($_SESSION['PERMISOS']['per_perfil_usuario']==1){?>

								<li id="configuaracion_tipos">
									<a  href="<?php echo base_url();?>
type">
										<i class="icon-double-angle-right"></i>
										Perfil de Usuario
									</a>
								</li>
							<?php }?>

						</ul>
					</li>

					<?php }?>

				</ul><!--/.nav-list-->

				<div class="sidebar-collapse" id="sidebar-collapse">
					<i class="icon-double-angle-left"></i>
				</div>
			</div>
<?php }} ?>