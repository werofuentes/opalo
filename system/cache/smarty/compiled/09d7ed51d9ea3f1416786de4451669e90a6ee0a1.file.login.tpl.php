<?php /* Smarty version Smarty-3.1.10, created on 2018-02-16 01:02:05
         compiled from "application/views/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10408395665a8681ed77d582-57051379%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '09d7ed51d9ea3f1416786de4451669e90a6ee0a1' => 
    array (
      0 => 'application/views/login.tpl',
      1 => 1486013062,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10408395665a8681ed77d582-57051379',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.10',
  'unifunc' => 'content_5a8681ed7f7120_63362341',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5a8681ed7f7120_63362341')) {function content_5a8681ed7f7120_63362341($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		
		<title>DIMAC: Sistema opalo| Inicia sesión</title>
		
		<meta name="description" content="ESTUDIO SOCIOECONOMICO" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		


		
		<!--basic styles-->

		<link href="<?php echo base_url();?>
inc/css_apro/bootstrap.min.css" rel="stylesheet" />
		<link href="<?php echo base_url();?>
inc/css_apro/bootstrap-responsive.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href=<?php echo base_url();?>
/inc/css_apro/font-awesome-ie7.min.css" />
		<![endif]-->
		


		<!--page specific plugin styles-->

		<!--fonts-->

		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />

		<!--ace styles-->

		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/ace.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/ace-responsive.min.css" />
		<link rel="stylesheet" href="<?php echo base_url();?>
inc/css_apro/ace-skins.min.css" />
		<script src="<?php echo base_url();?>
inc/js/general.js"></script>

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="<?php echo base_url();?>
/inc/css_apro/ace-ie.min.css" />
		<![endif]-->
		
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


	</head>
  
  
  	<body class="login-layout">
		<div class="main-container container-fluid">
			<div class="main-content">
				<div class="row-fluid">
					<div class="span12">
						<div class="login-container">
							<div class="row-fluid">
								<div class="center">
									<h1>
									<IMG SRC="<?php echo base_url();?>
/inc/logo.jpg" WIDTH=266 HEIGHT=180 ALT="DIMAC">
									</h1>
									<h4 class="blue">SISTEMA OPALO</h4>
								</div>
							</div>

							<div class="space-6"></div>

							<div class="row-fluid">
								<div class="position-relative">
									<div id="login-box" class="login-box visible widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header blue lighter bigger">
													<i class="icon-coffee green"></i>
													Ingresa informacion de acceso
												</h4>

												<div class="space-6"></div>
              
												
													<form action="<?php echo base_url();?>
login/valid" method="post">
											  <fieldset>
														<label>
															<span class="block input-icon input-icon-right">
																<input type="text" name="usuario" id="usuario" class="span12" placeholder="Usuario" />
																<i class="icon-user"></i>
															</span>
														</label>

														<label>
															<span class="block input-icon input-icon-right">
																<input type="password" type="password" name="password" id="password" class="span12" placeholder="Contraseña" />
																<i class="icon-lock"></i>
															</span>
														</label>

														<div class="space"></div>

														<div class="clearfix">
														<input class="width-35 pull-right btn btn-small btn-primary" type="submit" value="Entrar" />
															
														</div>

														<div class="space-4"></div>
												</fieldset>
											    </form>
									
											</div><!--/widget-main-->

											<div class="toolbar clearfix">
												<div>
													<a href="#" onclick="show_box('forgot-box'); return false;" class="forgot-password-link">
														<i class="icon-arrow-left"></i>
														Recuperar contraseña
													</a>
												</div>

												<div>
													<a href="#" onclick="show_box('signup-box'); return false;" class="user-signup-link">
														<!--I want to register-->
														
<!--<i class="icon-arrow-right"></i>-->
													</a>
												</div>
											</div>
										</div><!--/widget-body-->
									</div><!--/login-box-->

									<div id="forgot-box" class="forgot-box widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header red lighter bigger">
													<i class="icon-key"></i>
													Recuperar Contraseña
												</h4>

												<div class="space-6"></div>
												<p>
													Ingrese su correo electrónico 
												</p>

						
													<fieldset>
														<label>
															<span class="block input-icon input-icon-right">
																<input type="email" id="recuperar_email" name="recuperar_email" class="span12" placeholder="Email" />
																<i class="icon-envelope"></i>
															</span>
														</label>

														<div class="clearfix">
															<button onclick="recuperar();" class="width-35 pull-right btn btn-small btn-danger">
																<i class="icon-lightbulb"></i>
																Enviar
															</button>
														</div>
													</fieldset>
										
											</div><!--/widget-main-->

											<div class="toolbar center">
												<a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
													Volver al login
													<i class="icon-arrow-right"></i>
												</a>
											</div>
										</div><!--/widget-body-->
									</div><!--/forgot-box-->

									<div id="signup-box" class="signup-box widget-box no-border">
										<div class="widget-body">
											<div class="widget-main">
												<h4 class="header green lighter bigger">
													<i class="icon-group blue"></i>
													New User Registration
												</h4>

												<div class="space-6"></div>
												<p> Enter your details to begin: </p>

												<form />
													<fieldset>
														<label>
															<span class="block input-icon input-icon-right">
																<input type="email" class="span12" placeholder="Email" />
																<i class="icon-envelope"></i>
															</span>
														</label>

														<label>
															<span class="block input-icon input-icon-right">
																<input type="text" class="span12" placeholder="Username" />
																<i class="icon-user"></i>
															</span>
														</label>

														<label>
															<span class="block input-icon input-icon-right">
																<input type="password" class="span12" placeholder="Password" />
																<i class="icon-lock"></i>
															</span>
														</label>

														<label>
															<span class="block input-icon input-icon-right">
																<input type="password" class="span12" placeholder="Repeat password" />
																<i class="icon-retweet"></i>
															</span>
														</label>

														<label>
															<input type="checkbox" />
															<span class="lbl">
																I accept the
																<a href="#">User Agreement</a>
															</span>
														</label>

														<div class="space-24"></div>

														<div class="clearfix">
															<button type="reset" class="width-30 pull-left btn btn-small">
																<i class="icon-refresh"></i>
																Reset
															</button>

															<button onclick="return false;" class="width-65 pull-right btn btn-small btn-success">
																Register
																<i class="icon-arrow-right icon-on-right"></i>
															</button>
														</div>
													</fieldset>
												</form>
											</div>

											<div class="toolbar center">
												<a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
													<i class="icon-arrow-left"></i>
													Back to login
												</a>
											</div>
										</div><!--/widget-body-->
									</div><!--/signup-box-->
								</div><!--/position-relative-->
							</div>
						</div>
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div>
		</div><!--/.main-container-->

		<!--basic scripts-->

		<!--[if !IE]>-->

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

		<!--<![endif]-->

		<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->

		<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='<?php echo base_url();?>
/inc/js_apro/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!--<![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='<?php echo base_url();?>
/inc/js_apro/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
		
		function ingresar(){

		document.forms["iusa"].submit();
		}
		
		
	
		
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url();?>
inc/js_apro/bootstrap.min.js"></script>

		<!--page specific plugin scripts-->

		<!--ace scripts-->

		<script src="<?php echo base_url();?>
inc/js_apro/ace-elements.min.js"></script>
		<script src="<?php echo base_url();?>
inc/js_apro/ace.min.js"></script>

		<!--inline scripts related to this page-->

		<script type="text/javascript">
			function show_box(id) {
			 $('.widget-box.visible').removeClass('visible');
			 $('#'+id).addClass('visible');
			}
			
			
			
			
		
			
		</script>
	</body>
  
  
</html>
<?php }} ?>